package de.lorenzkock.anwesenheitserfassungclient.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.databinding.ActivityRaumUndTerminSelectBinding;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiClient;

public class RaumUndTerminSelectActivity extends AppCompatActivity {

    /**
     * UI Binding & Elemente
     */
    private AppBarConfiguration appBarConfiguration;

    /**
     * Initialisierung der Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!this.hasApiClient()) {
            Intent intent = new Intent(RaumUndTerminSelectActivity.this, ConfigActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return;
        }
        ActivityRaumUndTerminSelectBinding binding = ActivityRaumUndTerminSelectBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        this.appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            this.deaktiviereApiKonfiguration();
            Intent intent = new Intent(RaumUndTerminSelectActivity.this, ConfigActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public SharedPreferences getApiPreferences() {
        return getSharedPreferences("apiConfig", Context.MODE_PRIVATE);
    }

    public boolean hasApiClient() {
        SharedPreferences sharedPreferences = getApiPreferences();
        return sharedPreferences.getBoolean("aktiv", false);
    }

    public ApiClient getApiClient() {

        SharedPreferences sharedPreferences = getApiPreferences();
        String baseUrl = sharedPreferences.getString("baseUrl", "");
        String benutzername = sharedPreferences.getString("benutzername", "");
        String passwort = sharedPreferences.getString("passwort", "");

        ApiClient client = new ApiClient();
        client.setBasePath(baseUrl);
        client.setUsername(benutzername);
        client.setPassword(passwort);

        return client;
    }

    public void deaktiviereApiKonfiguration() {
        SharedPreferences sharedPreferences = getApiPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("aktiv", false);
        editor.apply();
    }

}