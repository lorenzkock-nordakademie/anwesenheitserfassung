package de.lorenzkock.anwesenheitserfassungclient.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextWatcher;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;
import java.util.Map;

import de.lorenzkock.anwesenheitserfassungclient.api.KonfigurationApi;
import de.lorenzkock.anwesenheitserfassungclient.databinding.ActivityConfigBinding;
import de.lorenzkock.anwesenheitserfassungclient.databinding.FragmentConfigBinding;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleApiCallback;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleTextWatcher;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiClient;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;

public class ConfigActivity extends AppCompatActivity {

    private FragmentConfigBinding fragmentConfigBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        de.lorenzkock.anwesenheitserfassungclient.databinding.ActivityConfigBinding binding = ActivityConfigBinding.inflate(getLayoutInflater());
        fragmentConfigBinding = binding.activityConfigFragment;

        fragmentConfigBinding.configTest.setOnClickListener(c -> this.testeKonfiguration());
        fragmentConfigBinding.configSubmit.setOnClickListener(c -> this.speichereApiKonfiguration());

        // Disable Speichern-Button wenn Texte verändert wurden
        TextWatcher watcher = (SimpleTextWatcher) editable -> fragmentConfigBinding.configSubmit.setEnabled(false);

        fragmentConfigBinding.configUrl.addTextChangedListener(watcher);
        fragmentConfigBinding.configUser.addTextChangedListener(watcher);
        fragmentConfigBinding.configPassword.addTextChangedListener(watcher);

        SharedPreferences preferences = getApiPreferences();

        fragmentConfigBinding.configUrl.setText(preferences.getString("baseUrl", ""));
        fragmentConfigBinding.configUser.setText(preferences.getString("benutzername", ""));
        fragmentConfigBinding.configPassword.setText(preferences.getString("passwort", ""));

        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);
    }


    private void speichereApiKonfiguration() {
        String baseUrl = this.fragmentConfigBinding.configUrl.getText().toString();
        String benutzername = this.fragmentConfigBinding.configUser.getText().toString();
        String passwort = this.fragmentConfigBinding.configPassword.getText().toString();

        SharedPreferences sharedPreferences = getApiPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("baseUrl", baseUrl);
        editor.putString("benutzername", benutzername);
        editor.putString("passwort", passwort);
        editor.putBoolean("aktiv", true);
        editor.apply();

        this.navigate();
    }

    private void navigate() {
        Intent intent = new Intent(ConfigActivity.this, RaumUndTerminSelectActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public SharedPreferences getApiPreferences() {
        return getSharedPreferences("apiConfig", Context.MODE_PRIVATE);
    }

    private void testeKonfiguration() {

        ApiClient client = new ApiClient();
        client.setBasePath(this.fragmentConfigBinding.configUrl.getText().toString());
        client.setUsername(this.fragmentConfigBinding.configUser.getText().toString());
        client.setPassword(this.fragmentConfigBinding.configPassword.getText().toString());

        KonfigurationApi api = new KonfigurationApi(client);

        try {
            api.testKonfigurationAsync(new SimpleApiCallback<Void>() {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    runOnUiThread(() -> Toast.makeText(ConfigActivity.this, "Die eingegebenen Daten sind nicht valide.", Toast.LENGTH_SHORT).show());
                }

                @Override
                public void onSuccess(Void result, int statusCode, Map<String, List<String>> responseHeaders) {
                    runOnUiThread(() -> {
                        Toast.makeText(ConfigActivity.this, "Die Konfiguration ist in Ordnung.", Toast.LENGTH_SHORT).show();
                        fragmentConfigBinding.configSubmit.setEnabled(true);
                    });
                }
            });
        } catch(Exception e) {
            runOnUiThread(() -> Toast.makeText(ConfigActivity.this, "Die Konfiguration ist fehlerhaft.", Toast.LENGTH_SHORT).show());
        }

    }

}