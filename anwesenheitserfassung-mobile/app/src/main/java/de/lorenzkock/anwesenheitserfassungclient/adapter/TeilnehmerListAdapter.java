package de.lorenzkock.anwesenheitserfassungclient.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.helper.TeilnehmerAktionGewaehltCallback;
import de.lorenzkock.anwesenheitserfassungclient.model.PersonZuTermin;

public abstract class TeilnehmerListAdapter extends BaseAdapter {


    static LayoutInflater inflater = null;
    private final TeilnehmerAktionGewaehltCallback personGewaehltCallback;
    private List<PersonZuTermin> teilnehmer;

    public TeilnehmerListAdapter(Context context, List<PersonZuTermin> teilnehmer, TeilnehmerAktionGewaehltCallback personGewaehltCallback) {
        super();
        this.teilnehmer = teilnehmer;
        this.personGewaehltCallback = personGewaehltCallback;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setTeilnehmer(List<PersonZuTermin> teilnehmer) {
        this.teilnehmer = Optional.ofNullable(teilnehmer).orElse(Collections.emptyList());
    }

    @Override
    public int getCount() {
        return teilnehmer.size();
    }

    @Override
    public Object getItem(int i) {
        return teilnehmer.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public TeilnehmerAktionGewaehltCallback getCallback() {
        return this.personGewaehltCallback;
    }

    static class ViewHolder {
        TextView tvName, tvKennung;
        ImageView ivStatus;
        LinearLayout ivStatusContainer;

        ImageButton ibAktion;

    }
}
