package de.lorenzkock.anwesenheitserfassungclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.helper.DateHelper;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminList;

public class TerminListAdapter extends BaseAdapter {


    private static LayoutInflater inflater = null;
    private final Context context;
    private List<TerminList> termine;

    public TerminListAdapter(Context context, List<TerminList> termine) {
        super();
        this.context = context;
        this.termine = termine;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setTermine(List<TerminList> termine) {
        this.termine = Optional.ofNullable(termine).orElse(Collections.emptyList());
    }

    @Override
    public int getCount() {
        return termine.size();
    }

    @Override
    public Object getItem(int i) {
        return termine.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_termin, parent, false);
            viewHolder.tvVeranstaltungName = convertView.findViewById(R.id.termin_row_veranstaltung_name);
            viewHolder.tvRaum = convertView.findViewById(R.id.termin_row_raum);
            viewHolder.tvStart = convertView.findViewById(R.id.termin_row_start);
            viewHolder.tvEnde = convertView.findViewById(R.id.termin_row_ende);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TerminList termin = (TerminList) this.getItem(i);
        viewHolder.tvVeranstaltungName.setText(termin.getVeranstaltung().getName());
        viewHolder.tvRaum.setText(termin.getRaeume() == null || termin.getRaeume().isEmpty() ? "-" : String.join(", ", termin.getRaeume().stream().map(this::getRaumName).collect(Collectors.toList())));
        viewHolder.tvStart.setText(toDateString(termin.getStart()));
        viewHolder.tvEnde.setText(toDateString(termin.getEnde()));

        return convertView;
    }

    private String toDateString(OffsetDateTime date) {
        if (date == null) return "-";
        return String.format("%s %s", DateHelper.formatDateTime(date), context.getString(R.string.clock));
    }

    private String getRaumName(Raum raum) {
        if (raum == null) return "-";
        return raum.getBezeichnung();
    }


    private static class ViewHolder {
        TextView tvVeranstaltungName, tvRaum, tvStart, tvEnde;

    }
}
