package de.lorenzkock.anwesenheitserfassungclient.helper;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungclient.api.TermineApi;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminDetail;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminList;
import de.lorenzkock.anwesenheitserfassungclient.view.ErfassungActivity;

public class AktuellerTerminErmittler implements Runnable {

    final private ErfassungActivity activity;
    final private TermineApi termineApi;

    public AktuellerTerminErmittler(ErfassungActivity activity, TermineApi termineApi) {
        this.activity = activity;
        this.termineApi = termineApi;
    }

    @Override
    public void run() {
        // Prüfe, ob Termin bereits vorüber ist
        if (activity.getTermin() != null) {
            if (!this.istTerminInSpanne(activity.getTermin())) {
                activity.removeTermin();
            }
        }

        // Wenn momentan kein Termin ausgewählt ist, versuche einen zu finden
        if (activity.getTermin() == null) {
            String raumId = Optional.ofNullable(activity.getSelectedRaum()).map(Raum::getId).orElse(null);
            try {
                // Lese den ersten Termin (ggf. zum Raum), der noch aktiv ist.
                this.termineApi.getAktuelleTermineAsync(raumId, new SimpleApiCallback<List<TerminList>>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        if(statusCode == 401) {
                            activity.gibFehlerAus("Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.");
                            activity.handleUnauthorizedException();
                        }
                        if(statusCode == 404) {
                            activity.gibFehlerAus("Der Raum wurde nicht mehr gefunden. Bitte neuen Raum wählen.");
                            activity.geheZurRaumauswahl();
                        }
                        if(statusCode == 500) {
                            activity.gibFehlerAus("Es ist ein Server-Fehler aufgetreten.");
                        }
                        activity.gibFehlerAus("Es ist ein Fehler aufgetreten.");
                    }

                    @Override
                    public void onSuccess(List<TerminList> result, int statusCode, Map<String, List<String>> responseHeaders) {
                        // Wenn es einen Termin vom Backend gibt, muss dieser noch nicht abgelaufen sein.
                        result.stream().findFirst().ifPresent(t -> activity.setTerminId(t.getId()));
                    }
                });
            } catch (ApiException e) {
                activity.gibFehlerAus("Es ist ein Fehler aufgetreten.");
            }
        }

        // Führe nach Pufferzeit erneut die Prüfung durch
        this.activity.triggerAutomatischeTerminErmittlung(10000);
    }

    private boolean istTerminInSpanne(TerminDetail termin) {
        OffsetDateTime now = OffsetDateTime.now();
        return !termin.getEnde().isBefore(now) && !termin.getStart().isAfter(now);
    }

}
