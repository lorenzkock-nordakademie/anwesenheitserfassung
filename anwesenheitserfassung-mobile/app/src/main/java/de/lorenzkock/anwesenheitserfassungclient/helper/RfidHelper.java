package de.lorenzkock.anwesenheitserfassungclient.helper;

public class RfidHelper {

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHRfidID(byte[] bytes) {
        String rfidID = "";
        for (int j = 0; j < bytes.length; j++) {
            char[] hexChars = new char[2];
            int v = bytes[j] & 0xFF;
            hexChars[0] = HEX_ARRAY[v >>> 4];
            hexChars[1] = HEX_ARRAY[v & 0x0F];
            rfidID = String.format("%s%s%s", rfidID, j > 0 ? ":":"", new String(hexChars));
        }
        return rfidID;
    }

}
