package de.lorenzkock.anwesenheitserfassungclient.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.helper.TeilnehmerAktionGewaehltCallback;
import de.lorenzkock.anwesenheitserfassungclient.model.PersonZuTermin;

public class TeilnehmerListAdapterAusweisZuordnen extends TeilnehmerListAdapter {

    public TeilnehmerListAdapterAusweisZuordnen(Context context, List<PersonZuTermin> teilnehmer, TeilnehmerAktionGewaehltCallback zuordnenCallback) {
        super(context, teilnehmer, zuordnenCallback);
    }
    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_teilnehmer, parent, false);

            viewHolder.tvName = convertView.findViewById(R.id.teilnehmer_row_name);
            viewHolder.tvKennung = convertView.findViewById(R.id.teilnehmer_row_nummer);
            viewHolder.ivStatus = convertView.findViewById(R.id.teilnehmer_row_status);
            viewHolder.ivStatusContainer = convertView.findViewById(R.id.teilnehmer_row_status_container);
            viewHolder.ibAktion = convertView.findViewById(R.id.teilnehmer_row_aktion);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PersonZuTermin person = (PersonZuTermin) this.getItem(i);
        viewHolder.tvName.setText(String.format("%s, %s", person.getPerson().getNachname(), person.getPerson().getVorname()));
        viewHolder.tvKennung.setText(person.getPerson().getKennung());

        viewHolder.ivStatusContainer.getLayoutParams().width = 20;

        int aktionIcon = R.drawable.badge;
        viewHolder.ibAktion.setVisibility(View.VISIBLE);
        viewHolder.ibAktion.setImageResource(aktionIcon);
        viewHolder.ibAktion.setOnClickListener((c) -> this.getCallback().aktion(person.getPerson().getId()));

        return convertView;
    }

}
