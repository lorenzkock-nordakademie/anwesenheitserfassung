package de.lorenzkock.anwesenheitserfassungclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;

public class RaumListAdapter extends BaseAdapter {


    private static LayoutInflater inflater = null;
    private List<Raum> raeume;

    public RaumListAdapter(Context context, List<Raum> raeume) {
        super();
        this.raeume = raeume;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setRaeume(List<Raum> raeume) {
        this.raeume = Optional.ofNullable(raeume).orElse(Collections.emptyList());
    }

    @Override
    public int getCount() {
        return raeume.size();
    }

    @Override
    public Object getItem(int i) {
        return raeume.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_raum, parent, false);
            viewHolder.tvName = convertView.findViewById(R.id.raum_row_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Raum r = (Raum) this.getItem(i);
        viewHolder.tvName.setText(r.getBezeichnung());

        return convertView;
    }

    private static class ViewHolder {
        TextView tvName;

    }
}
