package de.lorenzkock.anwesenheitserfassungclient.helper;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateHelper {
    public static String formatDateTime(OffsetDateTime date) {
        ZonedDateTime zoned = date.toZonedDateTime().withZoneSameInstant(ZoneId.systemDefault());
        return zoned.format(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm"));
    }

}
