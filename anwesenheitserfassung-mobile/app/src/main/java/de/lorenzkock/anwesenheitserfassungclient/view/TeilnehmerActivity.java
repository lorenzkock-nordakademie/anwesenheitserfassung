package de.lorenzkock.anwesenheitserfassungclient.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.adapter.TeilnehmerListAdapter;
import de.lorenzkock.anwesenheitserfassungclient.adapter.TeilnehmerListAdapterErfassen;
import de.lorenzkock.anwesenheitserfassungclient.api.ErfassungApi;
import de.lorenzkock.anwesenheitserfassungclient.api.TermineApi;
import de.lorenzkock.anwesenheitserfassungclient.databinding.ActivityTeilnehmerBinding;
import de.lorenzkock.anwesenheitserfassungclient.databinding.FragmentTeilnehmerBinding;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleApiCallback;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiClient;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.ErfassungErstellenMitPerson;
import de.lorenzkock.anwesenheitserfassungclient.model.ErfassungErstellenMitPersonErgebnis;
import de.lorenzkock.anwesenheitserfassungclient.model.PersonZuTermin;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminDetail;

public class TeilnehmerActivity extends AppCompatActivity {

    Handler anzeigeZuruecksetzenHandler = null;
    private FragmentTeilnehmerBinding fragmentBinding;
    private TeilnehmerListAdapterErfassen teilnehmerListAdapter;
    private TerminDetail selectedTermin;
    private Raum selectedRaum;
    private TermineApi termineApi;
    private ErfassungApi erfassungApi;
    private String TAG = "TeilnehmerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityTeilnehmerBinding binding = ActivityTeilnehmerBinding.inflate(getLayoutInflater());
        fragmentBinding = binding.activityTeilnehmerContent.teilnehmerContentFragment;

        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle argumente = intent.getExtras();
            if (argumente != null) {
                this.selectedTermin = (TerminDetail) argumente.getSerializable("termin");
                this.selectedRaum = (Raum) argumente.getSerializable("raum");
            }
        }

        // Initialisiere Api
        ApiClient apiClient = this.getApiClient();
        this.termineApi = new TermineApi(apiClient);
        this.erfassungApi = new ErfassungApi(apiClient);

        // Initialisiere Liste
        this.teilnehmerListAdapter = new TeilnehmerListAdapterErfassen(this, Collections.emptyList(), this::erfasseTeilnehmerManuell);
        this.fragmentBinding.teilnehmerList.setAdapter(this.teilnehmerListAdapter);

        this.fragmentBinding.teilnehmerStatusClose.setOnClickListener((c) -> this.showList());

        // Konfiguriere das Verhalten beim Refresh der Liste
        this.fragmentBinding.teilnehmerSwipeLayout.setOnRefreshListener(this::getTeilnehmerDaten);

        // Lade die Teilnehmer Daten
        this.getTeilnehmerDaten();
    }

    public ApiClient getApiClient() {
        SharedPreferences sharedPreferences = getApiPreferences();
        String baseUrl = sharedPreferences.getString("baseUrl", "");
        String benutzername = sharedPreferences.getString("benutzername", "");
        String passwort = sharedPreferences.getString("passwort", "");

        ApiClient client = new ApiClient();
        client.setBasePath(baseUrl);
        client.setUsername(benutzername);
        client.setPassword(passwort);

        return client;
    }

    public SharedPreferences getApiPreferences() {
        return getSharedPreferences("apiConfig", Context.MODE_PRIVATE);
    }

    private void getTeilnehmerDaten() {
        if (this.selectedTermin != null) {
            try {
                this.termineApi.getTeilnehmerByTerminAsync(this.selectedTermin.getId(), new SimpleApiCallback<List<PersonZuTermin>>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        Activity activity = TeilnehmerActivity.this;
                        activity.runOnUiThread(() -> {
                            fragmentBinding.teilnehmerSwipeLayout.setRefreshing(false);
                            fragmentBinding.teilnehmerListLoading.setVisibility(View.INVISIBLE);

                            Log.e(TAG, String.format("Fehler beim Abruf der Teilnehmer: Status %s: %s", statusCode, e.getMessage()), e);
                            if (statusCode == 401) {
                                Toast.makeText(activity, "Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.", Toast.LENGTH_LONG).show();
                            } else if (statusCode == 404) {
                                Toast.makeText(activity, "Der Termin wurde nicht gefunden.", Toast.LENGTH_LONG).show();
                            } else if (statusCode == 500) {
                                Toast.makeText(activity, "Beim Abruf der Teilnehmer ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(activity, "Beim Abruf der Teilnehmer ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            }

                        });
                    }

                    @Override
                    public void onSuccess(List<PersonZuTermin> result, int statusCode, Map<String, List<String>> responseHeaders) {
                        runOnUiThread(() -> {

                            teilnehmerListAdapter.setTeilnehmer(result.stream()
                                    .sorted(
                                            Comparator.comparing(p -> ((PersonZuTermin)p).getPerson().getNachname()).
                                                    thenComparing(p -> ((PersonZuTermin)p).getPerson().getVorname()))
                                    .collect(Collectors.toList()));
                            teilnehmerListAdapter.notifyDataSetChanged();
                            fragmentBinding.teilnehmerSwipeLayout.setRefreshing(false);
                            fragmentBinding.teilnehmerListEmpty.setVisibility(!result.isEmpty() ? View.INVISIBLE : View.VISIBLE);
                            fragmentBinding.teilnehmerListLoading.setVisibility(View.INVISIBLE);
                        });
                    }
                });
            } catch (ApiException e) {
                Toast.makeText(this, "Beim Abruf der Teilnehmer ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                Log.e(TAG, String.format("Fehler beim Abruf der Teilnehmer: %s", e.getMessage()), e);
            }
        }

    }

    private void erfasseTeilnehmerManuell(String personId) {
        if (personId != null) {

            showStatus(getString(R.string.please_wait), getString(R.string.waiting_for_result), R.drawable.dots, false);
            runOnUiThread(() -> {
                this.fragmentBinding.teilnehmerStatus.setVisibility(View.VISIBLE);
                this.fragmentBinding.teilnehmerList.setVisibility(View.GONE);
                this.fragmentBinding.teilnehmerStatusClose.setVisibility(View.GONE);
            });

            ErfassungErstellenMitPerson data = new ErfassungErstellenMitPerson();
            data.setPersonId(personId);
            data.setZeitpunkt(OffsetDateTime.now());
            data.setTerminId(this.selectedTermin.getId());
            data.setRaumId(this.selectedRaum == null ? null : this.selectedRaum.getId());

            try {
                this.erfassungApi.erstelleErfassungMittelsPersonAsync(data, new SimpleApiCallback<ErfassungErstellenMitPersonErgebnis>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        showStatus(getString(R.string.fail_manual_scan), getString(R.string.fail_manual_scan_technical), R.drawable.outline_cancel_24, true);
                        waitAndShowList(4000);
                    }

                    @Override
                    public void onSuccess(ErfassungErstellenMitPersonErgebnis result, int statusCode, Map<String, List<String>> responseHeaders) {
                        getTeilnehmerDaten();
                        switch (result.getErgebnis()) {
                            case ERFOLGREICH:
                                showStatus(getString(R.string.scan_success), getPersonName(result), R.drawable.outline_check_circle_24, true);
                                break;
                            case ERFOLGREICH_AUSGEHEND:
                            case ERFOLGREICH_AUSGEHEND_WIEDERKEHREND:
                                showStatus(getString(R.string.scan_success_outgoing), getPersonName(result), R.drawable.arrow_left_green, true);
                                break;
                            case ERFOLGREICH_WIEDERKEHREND:
                                showStatus(getString(R.string.scan_success_ingoing), getPersonName(result), R.drawable.arrow_right_green, true);
                                break;
                            case ERFOLGREICH_MEHRFACH:
                                showStatus(getString(R.string.scan_success), getPersonName(result), R.drawable.refresh_green, true);
                                break;
                            case NICHT_ERFOLGREICH_MEHRFACH:
                                showStatus(getString(R.string.scan_fail_multiple), getPersonName(result), R.drawable.outline_cancel_24, true);
                                break;
                            case NICHT_ERFOLGREICH_KEIN_TEILNEHMER:
                                showStatus(getString(R.string.scan_fail_not_allowed_id), getPersonName(result), R.drawable.outline_cancel_24, true);
                                break;
                            case NICHT_ERFOLGREICH_TERMIN_UNBEKANNT:
                                showStatus(getString(R.string.scan_fail_unknown_event), "", R.drawable.outline_cancel_24, true);
                                break;
                            case NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN:
                                showStatus(getString(R.string.scan_fail_unknown_person), "", R.drawable.outline_cancel_24, true);
                                break;
                        }
                        waitAndShowList(4000);
                    }
                });
            } catch (ApiException e) {
                showStatus(getString(R.string.fail_manual_scan), getString(R.string.fail_manual_scan_technical), R.drawable.outline_cancel_24, true);
                waitAndShowList(4000);
            }
        }
    }

    /**
     * Handler zum Zeigen der Liste nach einem Status
     *
     * @param delay Puffer, bis Liste angezeigt werden soll
     */
    private void waitAndShowList(int delay) {
        runOnUiThread(() -> {

            // Erzeuge einen Handler, wenn keiner existiert
            if (this.anzeigeZuruecksetzenHandler == null) {
                this.anzeigeZuruecksetzenHandler = new Handler();
            }

            // Erzeugen des verzögerten Tasks
            this.anzeigeZuruecksetzenHandler.postDelayed(() -> runOnUiThread(this::showList), delay);
        });
    }

    private void cancelWaitAndShow() {
        if (this.anzeigeZuruecksetzenHandler != null) {
            this.anzeigeZuruecksetzenHandler.removeCallbacksAndMessages(null);
        }
    }

    private void showList() {
        this.cancelWaitAndShow();
        runOnUiThread(() -> {
            this.fragmentBinding.teilnehmerStatus.setVisibility(View.GONE);
            this.fragmentBinding.teilnehmerList.setVisibility(View.VISIBLE);
        });
    }

    private void showStatus(String headline, String subline, int icon, boolean abbrechenButton) {
        runOnUiThread(() -> {
            fragmentBinding.teilnehmerStatusHeadline.setText(headline);
            fragmentBinding.teilnehmerStatusSubline.setText(subline);
            fragmentBinding.teilnehmerStatusIcon.setImageResource(icon);
            if (abbrechenButton) {
                this.fragmentBinding.teilnehmerStatusClose.setVisibility(View.VISIBLE);
            }
        });
    }

    private String getPersonName(ErfassungErstellenMitPersonErgebnis ergebnis) {
        return Optional.ofNullable(ergebnis.getPerson())
                .map(person -> String.format("%s %s", person.getVorname(), person.getNachname()))
                .orElse("");
    }

}