package de.lorenzkock.anwesenheitserfassungclient.helper;

import android.util.Log;

import java.util.List;
import java.util.Map;

import de.lorenzkock.anwesenheitserfassungclient.api.TermineApi;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminDetail;
import de.lorenzkock.anwesenheitserfassungclient.view.ErfassungActivity;

public class TerminAktualisierenTask implements Runnable {

    final private ErfassungActivity erfassungActivity;
    final private TermineApi termineApi;
    final private boolean repeat;
    private String TAG = "TerminAktualisierenTask";

    public TerminAktualisierenTask(ErfassungActivity erfassungActivity, TermineApi termineApi) {
        this(erfassungActivity, termineApi, true);
    }

    public TerminAktualisierenTask(ErfassungActivity erfassungActivity, TermineApi termineApi, boolean repeat) {
        this.erfassungActivity = erfassungActivity;
        this.termineApi = termineApi;
        this.repeat = repeat;
    }

    @Override
    public void run() {
        // Prüfe, ob Termin bereits vorüber ist
        if(erfassungActivity != null && erfassungActivity.getTerminId() != null) {
            try {
                // Lese den ersten Termin (ggf. zum Raum), der noch aktiv ist.
                this.termineApi.getTerminByIdAsync(erfassungActivity.getTerminId(), new SimpleApiCallback<TerminDetail>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        if(statusCode == 401) {
                            erfassungActivity.gibFehlerAus("Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.");
                            erfassungActivity.handleUnauthorizedException();
                        } else if(statusCode == 404) {
                            erfassungActivity.gibFehlerAus("Der Raum wurde nicht mehr gefunden. Bitte neuen Raum wählen.");
                            erfassungActivity.geheZurRaumauswahl();
                        } else if(statusCode == 500) {
                            erfassungActivity.gibFehlerAus("Beim Abruf der Termine ist ein Server-Fehler aufgetreten.");
                            Log.e(TAG, String.format("Server-Fehler beim Abruf der Termine zu einem Raum: %s", e.getMessage()), e);
                        } else {
                            erfassungActivity.gibFehlerAus("Beim Abruf der Termine ist ein Fehler aufgetreten.");
                            Log.e(TAG, String.format("Fehler beim Abruf der Termine zu einem Raum: Status %s: %s", statusCode, e.getMessage()), e);
                        }
                    }
                    @Override
                    public void onSuccess(TerminDetail result, int statusCode, Map<String, List<String>> responseHeaders) {
                        erfassungActivity.runOnUiThread(() -> erfassungActivity.setTermin(result));
                    }
                });
            } catch (ApiException e) {
                erfassungActivity.gibFehlerAus("Beim Abruf der Termine ist ein Fehler aufgetreten.");
                Log.e(TAG, String.format("Fehler beim Abruf der Termine zu einem Raum: %s", e.getMessage()), e);
            } catch(Exception e) {
                erfassungActivity.gibFehlerAus("Beim Abruf der Termine ist ein Fehler aufgetreten.");
                Log.e(TAG, String.format("Fehler beim Abruf der Termine zu einem Raum: %s", e.getMessage()), e);
            }
        }
        if(repeat && erfassungActivity != null) {
            erfassungActivity.triggerTerminAktualisierung(10000);
        }
    }

}
