package de.lorenzkock.anwesenheitserfassungclient.view;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.adapter.RaumListAdapter;
import de.lorenzkock.anwesenheitserfassungclient.api.RaeumeApi;
import de.lorenzkock.anwesenheitserfassungclient.databinding.FragmentRaumSelectBinding;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleApiCallback;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;

public class RaumSelectFragment extends Fragment {

    private FragmentRaumSelectBinding binding;
    private RaeumeApi api;
    private RaumListAdapter listAdapter;
    private String TAG = "RaumSelectFragment";

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Erzeuge Binding zu den Komponenten des Fragments
        binding = FragmentRaumSelectBinding.inflate(inflater, container, false);

        // Initialisiere die API
        final RaumUndTerminSelectActivity mainActivity = (RaumUndTerminSelectActivity)getContext();
        if(mainActivity != null) {
            this.api = new RaeumeApi(mainActivity.getApiClient());

            // Erzeuge einen Adapter zur Datenverarbeitung in der Liste und weise den zu
            this.listAdapter = new RaumListAdapter(getContext(), Collections.emptyList());
            this.binding.raumSelectList.setAdapter(this.listAdapter);

            // Konfiguriere das Verhalten, wenn ein Raum gewählt wurde
            this.binding.raumSelectList.setOnItemClickListener((adapterView, view, i, l) -> {
                Activity activity = getActivity();
                if(activity != null) {
                    activity.runOnUiThread(() -> {
                        Raum raum = (Raum) listAdapter.getItem(i);
                        navigate(raum);
                    });
                }
            });

            // Konfiguriere das Verhalten beim Refresh der Liste
            this.binding.raumSelectSwipeLayout.setOnRefreshListener(this::getRaumData);

            // Konfiguriere die Aktion, wenn kein Raum gewählt wird
            binding.raumSelectOhne.setOnClickListener(view -> navigate(null));

            // Lade die Raum-Daten initial
            this.getRaumData();
        }

        return binding.getRoot();
    }

    /**
     * Navigiere zur nächsten Ansicht
     * @param raum Raum der als Filter für die nächste Ansicht dient
     */
    private void navigate(Raum raum) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("raum", raum);
        NavHostFragment.findNavController(RaumSelectFragment.this)
                .navigate(R.id.action_RaumSelect_to_TerminSelect, bundle);
    }

    /**
     * Lade die Daten aller Veranstaltungen
     */
    private void getRaumData() {
        try {
            api.getAktiveRaeumeAsync(new SimpleApiCallback<List<Raum>>() {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    Activity activity = getActivity();
                    if(activity != null) {
                        activity.runOnUiThread(() -> {
                            binding.raumSelectSwipeLayout.setRefreshing(false);
                            binding.raumListLoading.setVisibility(View.INVISIBLE);

                            Log.e(TAG, String.format("Fehler beim Abruf der Räume: Status %s: %s", statusCode, e.getMessage()), e);
                            if(statusCode == 401) {
                                Toast.makeText(getContext(), "Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.", Toast.LENGTH_LONG).show();
                            } else if(statusCode == 500) {
                                Toast.makeText(getContext(), "Beim Abruf der Räume ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getContext(), "Beim Abruf der Räume ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            }

                        });
                    }
                }

                @Override
                public void onSuccess(List<Raum> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    Activity activity = getActivity();
                    if(activity != null) {
                        activity.runOnUiThread(() -> {
                            listAdapter.setRaeume(result);
                            listAdapter.notifyDataSetChanged();
                            binding.raumSelectSwipeLayout.setRefreshing(false);
                            binding.raumListEmpty.setVisibility(!result.isEmpty() ? View.INVISIBLE : View.VISIBLE);
                            binding.raumListLoading.setVisibility(View.INVISIBLE);
                        });
                    }
                }

            });
        } catch (ApiException e) {
            Toast.makeText(getContext(), "Beim Abruf der Räume ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
            Log.e(TAG, String.format("Fehler beim Abruf der Räume: %s", e.getMessage()), e);
        }
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}