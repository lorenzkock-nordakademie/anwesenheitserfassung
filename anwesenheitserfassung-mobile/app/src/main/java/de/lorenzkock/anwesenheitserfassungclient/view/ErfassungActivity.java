package de.lorenzkock.anwesenheitserfassungclient.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.api.ErfassungApi;
import de.lorenzkock.anwesenheitserfassungclient.api.RfidApi;
import de.lorenzkock.anwesenheitserfassungclient.api.TermineApi;
import de.lorenzkock.anwesenheitserfassungclient.databinding.ActivityErfassungBinding;
import de.lorenzkock.anwesenheitserfassungclient.databinding.FragmentErfassungBinding;
import de.lorenzkock.anwesenheitserfassungclient.helper.AktuellerTerminErmittler;
import de.lorenzkock.anwesenheitserfassungclient.helper.DateHelper;
import de.lorenzkock.anwesenheitserfassungclient.helper.LegitimationPruefenHelper;
import de.lorenzkock.anwesenheitserfassungclient.helper.LegitimationPruefenSuccessCallback;
import de.lorenzkock.anwesenheitserfassungclient.helper.RfidHelper;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleApiCallback;
import de.lorenzkock.anwesenheitserfassungclient.helper.TerminAktualisierenTask;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiClient;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.Ausweis;
import de.lorenzkock.anwesenheitserfassungclient.model.AusweisVerknuepfen;
import de.lorenzkock.anwesenheitserfassungclient.model.AusweisVerknuepfenErgebnis;
import de.lorenzkock.anwesenheitserfassungclient.model.Berechtigungen;
import de.lorenzkock.anwesenheitserfassungclient.model.ErfassungErstellenMitAusweis;
import de.lorenzkock.anwesenheitserfassungclient.model.ErfassungErstellenMitAusweisErgebnis;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminDetail;
import de.lorenzkock.anwesenheitserfassungclient.model.Veranstaltung;

public class ErfassungActivity extends AppCompatActivity {

    /**
     * RFID Konfiguration, welche Tags erkannt werden sollen
     */
    public static int READER_FLAGS = NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    /**
     * Attribute zur Durchführung von RFID Scans
     */
    public NfcAdapter nfcAdapter;
    public NfcAdapter.ReaderCallback defaultRfidReaderCallback;
    Handler anzeigeZuruecksetzenHandler = null;
    /**
     * Übergebene Daten aus voriger View
     */
    private Raum selectedRaum;
    /**
     * Attribute zur Bestimmung und Anzeige der Veranstaltung und des Termins
     */
    private String terminId;
    private TerminDetail selectedTermin;
    /**
     * Attribute zur Durchführung regelmäßiger Aufgaben
     */
    private Handler handler;
    private Runnable aktuellerTerminErmittlungTask;
    private Runnable terminAktualisierenTask;
    /**
     * APIs zur Kommunikation mit dem Backend
     */
    private TermineApi termineApi;
    private ErfassungApi erfassungApi;
    private RfidApi rfidApi;
    /**
     * Hilfsvariablen zur RFID-Ermittlung
     */

    private boolean modusFremdausweisScan = false;
    private boolean modusFremdausweisBestaetigen = false;
    /**
     * UI Binding & Elemente
     */
    private AppBarConfiguration appBarConfiguration;
    private ActivityErfassungBinding binding;
    private FragmentErfassungBinding fragmentBinding;
    /**
     * Attribute zur Ermittlung, ob RFID Scans ignoriert werden sollen
     */
    private String letzteRfidSnr;
    private OffsetDateTime letzterRfidScanZeitpunkt;
    /**
     * Men+
     */

    private Menu menu;
    private String TAG = "ErfassungActivity";

    /**
     * Initialisierung der Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            this.selectedRaum = (Raum) arguments.getSerializable("raum");
            this.terminId = arguments.getString("terminId");
        }

        this.binding = ActivityErfassungBinding.inflate(getLayoutInflater());
        this.fragmentBinding = this.binding.activityErfassungFragment;

        setSupportActionBar(this.binding.toolbar);

        this.appBarConfiguration = new AppBarConfiguration.Builder().build();

        // Init NFC
        this.nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        // Anzeige des Raums und der Veranstaltung aus der vorigen Auswahl
        if (this.selectedRaum != null) {
            this.fragmentBinding.scanFragmentSelectedRaumBezeichnung.setText(this.selectedRaum.getBezeichnung());
        } else {
            this.fragmentBinding.scanFragmentSelectedRaumContainer.setVisibility(View.GONE);
        }
        this.printTerminInfo(this.selectedTermin);

        // Setze die Listener zu den Buttons der View
        this.fragmentBinding.scanFragmentAdditionalButtonFremdausweis.setOnClickListener((c) -> this.runOnUiThread(this::aktiviereFremdausweisModus));
        this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisAbbrechen.setOnClickListener((c) -> this.runOnUiThread(() -> this.deaktiviereFremdausweisModus(true)));
        this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisBestaetigen.setOnClickListener((c) -> this.runOnUiThread(this::startFremdausweisBestaetigenModus));
        this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisBestaetigenAbbrechen.setOnClickListener((c) -> this.runOnUiThread(() -> this.cancelFremdausweisBestaetigenModus(true)));

        // Erstelle die Apis für Kommunikation mit dem Backend
        ApiClient client = this.getApiClient();
        this.termineApi = new TermineApi(client);
        this.erfassungApi = new ErfassungApi(client);
        this.rfidApi = new RfidApi(client);

        // Erstelle ein default Callback, wenn ein RFID Ausweis gescannt wird
        this.defaultRfidReaderCallback = this::handleTagFuerErfassung;

        // Regelmäßige Aufgaben
        this.handler = new Handler();

        // Erstelle eine Aufgabe, die regelmäßig prüft, ob der Termin noch aktuell ist
        // Tu dies nur, wenn keine Veranstaltung manuell gewählt wurde.
        if (this.terminId == null) {
            this.starteAutomatischeTerminErmittlung();
        }
        this.starteTerminAktualsisierung();

        setContentView(this.binding.getRoot());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_erfassung, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        LegitimationPruefenSuccessCallback successCallback = (b) -> runOnUiThread(() -> Toast.makeText(this, "Erfolgreich legitimiert", Toast.LENGTH_SHORT).show());
        Runnable errorCallback = () -> runOnUiThread(() -> Toast.makeText(this, "Fehler bei der Legitimierung", Toast.LENGTH_SHORT).show());
        Runnable cancelCallback = () -> runOnUiThread(() -> this.setRfidScannerCallback(this.defaultRfidReaderCallback));
        LegitimationPruefenHelper legitimationPruefen = (berechtigungen) -> berechtigungen.getRollen().contains("VERWALTUNG_GERAET");

        if (id == R.id.action_settings || id == R.id.action_selection || id == R.id.action_event_member || id == R.id.action_event_connect_ids) {
            if (id == R.id.action_settings) {
                successCallback = (b) -> {
                    this.geheZurKonfiguration();
                };
            }
            if (id == R.id.action_selection) {
                successCallback = (b) -> {
                    this.geheZurRaumauswahl();
                };
            }
            if (id == R.id.action_event_member) {
                if (this.selectedTermin != null) {
                    successCallback = (b) -> {
                        Intent intent = new Intent(this, TeilnehmerActivity.class);
                        intent.putExtra("termin", this.selectedTermin);
                        startActivity(intent);
                    };
                    legitimationPruefen = (b) -> b.getRollen().contains("ANWESENHEIT_NACHTRAGEN") ||
                            (this.selectedTermin != null && b.getVeranstaltungen().contains(this.selectedTermin.getVeranstaltung().getId()));
                } else {
                    return false;
                }

            }
            if (id == R.id.action_event_connect_ids) {
                if (this.selectedTermin != null) {
                    successCallback = (b) -> {
                        Intent intent = new Intent(this, ConnectIdActivity.class);
                        intent.putExtra("termin", this.selectedTermin);
                        startActivity(intent);
                    };
                    legitimationPruefen = (b) -> b.getRollen().contains("ANWESENHEIT_NACHTRAGEN") ||
                            (this.selectedTermin != null && b.getVeranstaltungen().contains(this.selectedTermin.getVeranstaltung().getId()));
                } else {
                    return false;
                }

            }
            this.legitimationPruefen(successCallback, errorCallback, cancelCallback, legitimationPruefen);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        if (this.nfcAdapter != null) {
            this.setRfidScannerCallback(this.defaultRfidReaderCallback);
        }
        this.initModusFremdausweis();
        this.initFremdausweisBestaetigenModus();
        this.printAndShowDefaultStatus();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (this.handler != null) {
            this.handler.removeCallbacksAndMessages(null);
        }
    }

    public SharedPreferences getApiPreferences() {
        return getSharedPreferences("apiConfig", Context.MODE_PRIVATE);
    }

    public ApiClient getApiClient() {

        SharedPreferences sharedPreferences = getApiPreferences();
        String baseUrl = sharedPreferences.getString("baseUrl", "");
        String benutzername = sharedPreferences.getString("benutzername", "");
        String passwort = sharedPreferences.getString("passwort", "");

        ApiClient client = new ApiClient();
        client.setBasePath(baseUrl);
        client.setUsername(benutzername);
        client.setPassword(passwort);

        return client;
    }

    public void deaktiviereApiKonfiguration() {
        SharedPreferences sharedPreferences = getApiPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("aktiv", false);
        editor.apply();
    }

    public void setRfidScannerCallback(NfcAdapter.ReaderCallback callback) {
        if (callback != null) {
            this.nfcAdapter.enableReaderMode(this, callback, READER_FLAGS, null);
        } else {
            this.nfcAdapter.disableReaderMode(this);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // Methode zum automatischen Ermitteln von Terminen
    //////////////////////////////////////////////////////////////////////////////////////////

    public void legitimationPruefen(LegitimationPruefenSuccessCallback successCallback, Runnable errorCallback, Runnable cancelCallback, LegitimationPruefenHelper legitimationPruefung) {
        this.binding.legitimationAlert.setVisibility(View.VISIBLE);
        this.binding.activityErfassungContent.setVisibility(View.GONE);
        this.binding.legitimationAlertAbort.setOnClickListener((v) -> {
            this.binding.legitimationAlert.setVisibility(View.GONE);
            this.binding.activityErfassungContent.setVisibility(View.VISIBLE);
            cancelCallback.run();
        });

        if (this.nfcAdapter != null) {
            this.nfcAdapter.enableReaderMode(this, (tag) -> {
                String rfidId = RfidHelper.bytesToHRfidID(tag.getId());
                if (this.sollRfidScanVerarbeitenZeitDifferenz(rfidId)) {
                    runOnUiThread(() -> {
                        try {
                            this.rfidApi.getBerechtigungenZuAusweisAsync(rfidId, new SimpleApiCallback<Berechtigungen>() {
                                @Override
                                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                                    errorCallback.run();
                                }

                                @Override
                                public void onSuccess(Berechtigungen result, int statusCode, Map<String, List<String>> responseHeaders) {
                                    if (legitimationPruefung.legitimiert(result)) {
                                        runOnUiThread(() -> {
                                            binding.legitimationAlert.setVisibility(View.GONE);
                                            binding.activityErfassungContent.setVisibility(View.VISIBLE);
                                        });
                                        successCallback.legitimiert(result);
                                    } else {
                                        errorCallback.run();
                                    }
                                }
                            });
                        } catch (ApiException e) {
                            errorCallback.run();
                        }
                    });
                }
            }, READER_FLAGS, null);
        }
    }

    /**
     * Starten der automatischen Ermittlung von Terminen
     */
    private void starteAutomatischeTerminErmittlung() {
        this.aktuellerTerminErmittlungTask = new AktuellerTerminErmittler(this, this.termineApi);
        this.handler.postDelayed(this.aktuellerTerminErmittlungTask, 0);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Methoden zum Verwalten des Raums
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Löst eine automatische Ermittlung eines Termins aus, kann von außen ausgelöst werden
     *
     * @param delay Verzögerung bis zur nächsten Durchführung
     */
    public void triggerAutomatischeTerminErmittlung(int delay) {
        if (this.handler != null) {
            this.handler.postDelayed(this.aktuellerTerminErmittlungTask, delay);
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    // Methoden zum Setzen und Lesen von Terminen sowie der Behandlung von Termin-Änderungen
    //////////////////////////////////////////////////////////////////////////////////////////

    public Raum getSelectedRaum() {
        return this.selectedRaum;
    }

    /**
     * Gibt die ID des ausgewählten Termins zurück.
     *
     * @return ID des ausgewählten Termins
     */
    public String getTerminId() {
        return this.terminId;
    }

    /**
     * Setzt die ID eines Termins, bspw. bei der automatischen Ermittlung eines Termins
     *
     * @param id ID des Termins
     */
    public void setTerminId(String id) {
        this.terminId = id;
        if (this.terminId != null) {
            // Löst eine einmalige Aktualisierung des Termins aus, damit alle Daten zum Termin geladen werden
            this.triggerSingleTerminAktualisierung();
        }
    }

    /**
     * Gibt das Termin-Objekt zurück, wird auf Grundlage der gesetzten ID geladen
     *
     * @return ermittelter oder gewählter Termin
     */
    public TerminDetail getTermin() {
        return this.selectedTermin;
    }

    /**
     * Setzt das Detail-Termin Objekt mit allen Informationen zu einem Termin
     */
    public void setTermin(TerminDetail termin) {

        // Ermittlung, ob der Termin aktualisiert wurde oder ein anderer Termin ausgewählt wurde
        boolean terminChanged = (this.selectedTermin == null && termin != null) ||
                (this.selectedTermin != null && termin == null) ||
                (this.selectedTermin != null && !this.selectedTermin.getId().equals(termin.getId()));

        if (terminChanged) {
            // Bei einer Änderung des Termins
            this.handleTerminChange(termin);
        } else {
            // Bei gleichem Termin mit anderen Daten
            if (termin != null && this.selectedTermin != null) {
                this.handleTerminUpdate(termin);
            }
        }

        // Zeige in jedem Fall die Informationen zu dem Termin an
        this.printTerminInfo(this.selectedTermin);
    }

    /**
     * Entferne den Termin, bspw. wenn der Termin vorüber ist
     */
    public void removeTermin() {
        this.selectedTermin = null;
        this.terminId = null;
        this.printTerminInfo(null);
        this.hideButtonFremdausweis();
        this.hideStatus();
        this.runOnUiThread(() -> this.menu.findItem(R.id.action_event_member).setVisible(false));
    }

    /**
     * Behandelt das Vorgehen bei einer Änderung des Termins
     *
     * @param termin Der neue Termin
     */
    private void handleTerminChange(TerminDetail termin) {
        this.selectedTermin = termin;
        this.initModusFremdausweis();

        this.runOnUiThread(() -> {
            // Aktiviere den Menü-Eintrag zur Einsicht der Teilnehmer
            this.menu.findItem(R.id.action_event_member).setVisible(termin != null);
        });

        // Wenn grade ein Fremdausweis bestätigt wird, soll der Vorgang nicht unterbrochen werden
        if (!this.modusFremdausweisBestaetigen) {
            this.printAndShowDefaultStatus();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // Methoden für die Verarbeitung von RFID Tags
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Behandelt das Vorgehen bei einer Anpassung des Termins
     *
     * @param termin Der zu aktualisierende Termin
     */
    private void handleTerminUpdate(TerminDetail termin) {

        Boolean fremdausweisAktuellErlaubt = this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis();
        this.selectedTermin = termin;

        // Wenn sich der Status zum Erlauben der Fremdausweise geändert hat, initialisiere die Buttons
        if (!this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis().equals(fremdausweisAktuellErlaubt)) {
            this.initModusFremdausweis();
        }
    }

    /**
     * Behandelt das Vorgehen sobald ein RFID-Tag erkannt wurde.
     *
     * @param tag Der erfasste RFID Tag
     */
    private void handleTagFuerErfassung(Tag tag) {
        if (this.terminId != null) {
            // Ermittlung der RFID Seriennummer
            String rfidId = RfidHelper.bytesToHRfidID(tag.getId());
            if (!this.sollRfidScanVerarbeitenZeitDifferenz(rfidId)) return;

            OffsetDateTime zeitpunktErfassung = OffsetDateTime.now();

            this.runOnUiThread(() -> {
                // Setze den Puffer zum Zurücksetzen der Statusanzeige zurück
                this.resetWaitigToShowDefaultStatus();

                // Deaktiviere UI-Elemente, solange eine Verarbeitung läuft
                this.hideButtonFremdausweis();
                this.hideButtonFremdausweisAbbrechen();
                this.hideButtonFremdausweisBestaetigen();

                // Zeige den Status, dass Erfassung verarbeitet wird
                this.printAndShowStatus(getString(R.string.waiting_for_result), getString(R.string.please_wait), R.drawable.dots);
            });

            // Prüfe, ob es sich um eine Random ID handelt
            if(rfidId.length() > 1 && rfidId.startsWith("08")) {
                this.runOnUiThread(() -> {
                    this.printAndShowStatus(getString(R.string.id_not_supported), getString(R.string.id_not_supported_subline), R.drawable.card_off);
                    this.waitAndShowDefaultStatus(5000);
                    this.initModusFremdausweis();
                });
                return;
            }

            // Sende die Daten an das Backend
            ErfassungErstellenMitAusweis data = new ErfassungErstellenMitAusweis();
            data.setTerminId(this.terminId);
            data.setRfidSnr(rfidId);
            data.setZeitpunkt(OffsetDateTime.now());
            data.setFremdAusweis(this.modusFremdausweisScan);
            data.setRaumId(this.selectedRaum == null ? null : this.selectedRaum.getId());

            try {
                this.erfassungApi.erstelleErfassungMittelsAusweisAsync(data, new SimpleApiCallback<ErfassungErstellenMitAusweisErgebnis>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        runOnUiThread(() -> {
                            if(statusCode == 401) {
                                gibFehlerAus("Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.");
                                handleUnauthorizedException();
                            } else if(statusCode == 500) {
                                gibFehlerAus("Beim Speichern der Erfassung ist ein Server-Fehler aufgetreten.");
                                waitAndShowDefaultStatus(0);
                                initModusFremdausweis();
                                Log.e(TAG, String.format("Server-Fehler beim Erstellen einer Erfassung mit einem Ausweis: %s", e.getMessage()), e);
                            } else {
                                gibFehlerAus("Beim Speichern der Erfassung ist ein Fehler aufgetreten.");
                                waitAndShowDefaultStatus(0);
                                initModusFremdausweis();
                                Log.e(TAG, String.format("Fehler beim Erstellen einer Erfassung mit einem Ausweis: Status %s: %s", statusCode, e.getMessage()), e);
                            }
                        });
                    }

                    @Override
                    public void onSuccess(ErfassungErstellenMitAusweisErgebnis result, int statusCode, Map<String, List<String>> responseHeaders) {

                        OffsetDateTime now = OffsetDateTime.now();
                        long diff = zeitpunktErfassung.until(now, ChronoUnit.MILLIS);
                        Log.i("AnalyseZeit", String.format("Ausweis erfasst;%s;%s;%s;%s",rfidId,zeitpunktErfassung, now, diff));
                        verarbeiteErfassungErgebnis(result);
                    }
                });
            } catch (ApiException e) {
                this.gibFehlerAus("Es ist ein Fehler aufgetreten.");
                waitAndShowDefaultStatus(0);
                initModusFremdausweis();
                Log.e(TAG, String.format("Fehler beim Erstellen einer Erfassung mit einem Ausweis: %s", e.getMessage()), e);
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // Methoden zur Verarbeitung von Backend-Antworten
    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bestimmt, ob ein Tag verarbeitet werden soll, oder ob eine doppelte Erfassung vorlag
     *
     * @param rfidSnr SNr des Tags
     * @return Information, ob Tag verarbeitet werden soll
     */
    public boolean sollRfidScanVerarbeitenZeitDifferenz(String rfidSnr) {

        if (this.letzteRfidSnr != null && this.letzteRfidSnr.equals(rfidSnr)) {
            long sekundenSeitLetztemScan = this.letzterRfidScanZeitpunkt.until(OffsetDateTime.now(), ChronoUnit.SECONDS);
            if (sekundenSeitLetztemScan < 10) return false;
        }

        this.letzterRfidScanZeitpunkt = OffsetDateTime.now();
        this.letzteRfidSnr = rfidSnr;

        return true;
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    // Methoden für die Verwendung und Verwaltung von Fremdausweisen
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Verarbeitet das Ergebnis einer Erfassungsanfrage an das Backend
     *
     * @param ergebnis Das Ergebnis des Backends
     */
    private void verarbeiteErfassungErgebnis(ErfassungErstellenMitAusweisErgebnis ergebnis) {
        this.runOnUiThread(() -> {
            ErfassungErstellenMitAusweisErgebnis.ErgebnisEnum ergebnisEnum = ergebnis.getErgebnis();

            // Setze einen Standardwert, wie lange der Status angezeigt werden soll.
            int delayForStatusReset = 4000;
            switch (ergebnisEnum) {
                case ERFOLGREICH:
                    switch (Optional.ofNullable(ergebnis.getAusweisTyp()).orElse(ErfassungErstellenMitAusweisErgebnis.AusweisTypEnum.EIGEN)) {
                        case EIGEN:
                            this.printAndShowStatus(getString(R.string.scan_success), this.getPersonName(ergebnis), R.drawable.outline_check_circle_24);
                            break;
                        case UNBESTAETIGTE_FREMD:
                            this.printAndShowStatus(getString(R.string.scan_success_foreign), getString(R.string.foreign_confirm_question), R.drawable.outline_help);
                            delayForStatusReset = 10000;
                            break;
                        case BESTAETIGTE_FREMD:
                            this.printAndShowStatus(getString(R.string.scan_success_foreign), this.getPersonName(ergebnis), R.drawable.outline_check_circle_24);
                            break;
                    }
                    break;
                case ERFOLGREICH_AUSGEHEND:
                case ERFOLGREICH_AUSGEHEND_WIEDERKEHREND:
                    this.printAndShowStatus(getString(R.string.scan_success_outgoing), this.getPersonName(ergebnis), R.drawable.arrow_left_green);
                    break;
                case ERFOLGREICH_WIEDERKEHREND:
                    this.printAndShowStatus(getString(R.string.scan_success_ingoing), this.getPersonName(ergebnis), R.drawable.arrow_right_green);
                    break;
                case ERFOLGREICH_MEHRFACH:
                    this.printAndShowStatus(getString(R.string.scan_success), this.getPersonName(ergebnis), R.drawable.arrow_right_green);
                    break;
                case NICHT_ERFOLGREICH_MEHRFACH:
                    this.printAndShowStatus(getString(R.string.scan_fail_multiple), this.getPersonName(ergebnis), R.drawable.outline_cancel_24);
                    break;
                case NICHT_ERFOLGREICH_KEIN_TEILNEHMER:
                    this.printAndShowStatus(getString(R.string.scan_fail_not_allowed_id), this.getPersonName(ergebnis), R.drawable.outline_cancel_24);
                    break;
                case NICHT_ERFOLGREICH_TERMIN_UNBEKANNT:
                    this.printAndShowStatus(getString(R.string.scan_fail_unknown_event), this.getPersonName(ergebnis), R.drawable.outline_cancel_24);
                    break;
                case NICHT_ERFOLGREICH_KEINE_FREMDAUSWEISE:
                    this.printAndShowStatus(getString(R.string.scan_fail_no_foreign_id), this.getPersonName(ergebnis), R.drawable.outline_cancel_24);
                    break;
                case NICHT_ERFOLGREICH_UNBEKANNTER_AUSWEIS:
                    this.printAndShowStatus(getString(R.string.scan_fail_unknown_id), this.getPersonName(ergebnis), R.drawable.outline_cancel_24);
                    break;
                case NICHT_ERFOLGREICH_ZEIT:
                    this.printAndShowStatus(getString(R.string.scan_warning), getString(R.string.scan_warning_time_subtitle), R.drawable.warning);
                    break;
                case ERFASSUNG_GESTARTET:
                    this.printAndShowStatus(getString(R.string.scan_start_success), getString(R.string.scan_start_success_subtitle), R.drawable.alarm_green);
                    break;
                case ERFASSUNG_BEREITS_GESTARTET:
                    this.printAndShowStatus(getString(R.string.scan_already_started), getString(R.string.scan_already_started_subtitle), R.drawable.alarm_orange);
                    break;
            }
            this.waitAndShowDefaultStatus(delayForStatusReset);
            this.initModusFremdausweis();
        });
    }

    /**
     * Initialisierung des Modus zur Bestätigung von Fremdausweisen.
     * Blendet die entsprechenden Buttons aus und ein, zeigt den Status und initialisiert den Prozess
     */
    private void initFremdausweisBestaetigenModus() {

        // Setze den entsprechenden Modus
        this.modusFremdausweisScan = false;
        this.modusFremdausweisBestaetigen = false;

        this.hideButtonFremdausweisBestaetigen();
        this.hideButtonFremdausweisBestaetigenAbbrechen();
        if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
            this.showButtonFremdausweisBestaetigen();
        }
    }

    /**
     * Initialisierung des Modus zur Bestätigung von Fremdausweisen.
     * Blendet die entsprechenden Buttons aus und ein, zeigt den Status und initialisiert den Prozess
     */
    private void startFremdausweisBestaetigenModus() {
        if (this.selectedTermin != null) {

            // Setze den entsprechenden Modus
            this.modusFremdausweisScan = false;
            this.modusFremdausweisBestaetigen = true;

            // Blende entsprechend Buttons ein und aus
            this.showButtonFremdausweisBestaetigenAbbrechen();
            this.hideButtonFremdausweisBestaetigen();
            this.hideButtonFremdausweis();
            this.hideButtonFremdausweisAbbrechen();

            // Setze den Handler zum Anzeigen des Default-Status zurück
            this.resetWaitigToShowDefaultStatus();

            if (this.selectedTermin.getVeranstaltung().getFremdausweisBestaetigen()) {
                this.fremdausweisBestaetigenSchrittBerechtigung();
            } else {
                this.fremdausweisBestaetigenSchrittFremdausweis(null);
            }
        }
    }

    /**
     * Beendet den Modus zur Bestätigung von Fremdausweisen
     *
     * @param showDefaultStatus Bestimmt, ob der Standard-Status angezeigt werden soll.
     */
    private void cancelFremdausweisBestaetigenModus(boolean showDefaultStatus) {

        // Setze den Modus auf false
        this.modusFremdausweisBestaetigen = false;

        // Zeige und blende Buttons aus
        this.hideButtonFremdausweisBestaetigenAbbrechen();
        this.showButtonFremdausweisBestaetigen();

        // Initialisiere ggf. den Modus zur Erfassung von Fremdausweisen
        this.initModusFremdausweis();

        // Zeigt wenn gefordert den Standard-Status an
        if (showDefaultStatus) {
            this.printAndShowDefaultStatus();
        }

        this.setRfidScannerCallback(this.defaultRfidReaderCallback);
    }

    /**
     * Initialisiert die Ansicht und den RFID-Scanner für den Schritt der Berechtigungsprüfung, wenn ein Fremdausweis gescannt werden soll
     */
    private void fremdausweisBestaetigenSchrittBerechtigung() {
        // Zeige den entsprechenden Status
        this.printAndShowStatus(getString(R.string.please_authorize), getString(R.string.please_authorize_subtitlte), R.drawable.auth);

        // Führt die Legitimation durch
        this.legitimationPruefen(
                // Erfolg: Rufe den nächsten Schritt im Prozess auf
                (b) -> this.fremdausweisBestaetigenSchrittFremdausweis(b.getRfidSnr()),
                // Fehler: Zeige eine Fehlermeldung
                () -> this.runOnUiThread(() -> Toast.makeText(this, "Fehler bei der Legitimierung!", Toast.LENGTH_SHORT).show()),
                // Abbruch: Breche das Bestätigen ab
                () -> this.cancelFremdausweisBestaetigenModus(true),
                // Regeln, die zutreffen müssen, damit Legitimation erfolgreich war
                (b) -> b.getVeranstaltungen().contains(this.getTermin().getVeranstaltung().getId()) || b.getRollen().contains("IDENTITAET_BESTAETIGEN")
        );
    }

    /**
     * Initialisiert den Schritt der Erfassung der Fremdausweise
     *
     * @param bestaetigendeIdRfidSnr RFID-Snr der bestätigenden Person, zur Dokumentation
     */
    private void fremdausweisBestaetigenSchrittFremdausweis(String bestaetigendeIdRfidSnr) {
        // Zeige die entsprechende Informationen
        this.printAndShowStatus(getString(R.string.please_scan_foreign), getString(R.string.please_scan_foreign_subtitle), R.drawable.cards);

        // Führt den RFID-Scan durch
        this.setRfidScannerCallback((tag) -> {

            // Ermitteln der Seriennummer des Tags
            String rfidId = RfidHelper.bytesToHRfidID(tag.getId());

            // Prüfe, ob es sich um eine Random ID handelt
            if(rfidId.length() > 1 && rfidId.startsWith("08")) {
                this.runOnUiThread(() -> {
                    this.printAndShowStatus(getString(R.string.id_not_supported), getString(R.string.id_not_supported_subline), R.drawable.card_off);
                    this.cancelFremdausweisBestaetigenModus(false);
                    this.waitAndShowDefaultStatus(5000);
                    this.initModusFremdausweis();
                });
                return;
            }

            // Prüfen, ob auf Grund kurzer Zeitdifferenz die Erfassung verarbeitet werden soll
            if (this.sollRfidScanVerarbeitenZeitDifferenz(rfidId)) {
                // Aufrufen des nächsten Schritts des Prozesses
                this.fremdausweisBestaetigenSchrittEigenausweis(bestaetigendeIdRfidSnr, rfidId);
            }
        });
    }

    /**
     * Initialisiert den Schritt der Erfassung der Eigenausweise
     *
     * @param bestaetigendeIdRfidSnr RFID-Snr der bestätigenden Person, zur Dokumentation
     * @param fremdausweisRfidSnr    RFID-Snr des fremden Ausweises
     */
    private void fremdausweisBestaetigenSchrittEigenausweis(String bestaetigendeIdRfidSnr, String fremdausweisRfidSnr) {
        // Zeige die entsprechende Informationen
        this.printAndShowStatus(getString(R.string.please_scan), getString(R.string.please_scan_own_subtitle), R.drawable.badge);

        // Führt den RFID-Scan durch
        this.setRfidScannerCallback((tag) -> {

            // Ermittlung der RFID ID des Tags
            String rfidId = RfidHelper.bytesToHRfidID(tag.getId());

            // Prüfen, ob auf Grund kurzer Zeitdifferenz die Erfassung verarbeitet werden soll
            if (this.sollRfidScanVerarbeitenZeitDifferenz(rfidId)) {

                // Anzeigen des Status, dass die Verknüpfung verarbeitet wird
                this.printAndShowStatus(getString(R.string.please_wait), getString(R.string.waiting_for_binding_result), R.drawable.dots);

                // Erzeugen der Daten für die Anfrage an das Backend
                AusweisVerknuepfen iv = new AusweisVerknuepfen();
                iv.setRfidSnrFremd(fremdausweisRfidSnr);
                iv.setRfidSnrEigen(rfidId);
                iv.setRfidSnrBerechtigt(bestaetigendeIdRfidSnr);

                try {
                    // Durchführen der Anfrage an das Backend
                    this.rfidApi.verknuepfeFremdAusweisAsync(iv, new SimpleApiCallback<AusweisVerknuepfenErgebnis>() {
                        @Override
                        public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                            handleFremdausweisVerknuepfenHttpError();
                        }

                        @Override
                        public void onSuccess(AusweisVerknuepfenErgebnis result, int statusCode, Map<String, List<String>> responseHeaders) {
                            handleFremdausweisVerknuepfenHttpSuccess(result);
                        }
                    });
                } catch (ApiException e) {
                    handleFremdausweisVerknuepfenHttpError();
                }
            }
        });
    }

    /**
     * Verarbeitung des Ergebnis der Verknüpfung, wenn keine technischen Fehler aufgetreten sind
     *
     * @param ergebnis Ergebnis der Verknüpfung
     */
    private void handleFremdausweisVerknuepfenHttpSuccess(AusweisVerknuepfenErgebnis ergebnis) {
        this.runOnUiThread(() -> {
            AusweisVerknuepfenErgebnis.ErgebnisEnum ergebnisEnum = ergebnis.getErgebnis();

            // Setze einen Standardwert, wie lange der Status angezeigt werden soll
            switch (ergebnisEnum) {
                case ERFOLGREICH:
                    this.printAndShowStatus(getString(R.string.binding_success), getString(R.string.binding_success_subtitle), R.drawable.outline_check_circle_24);
                    break;
                case FREMDAUSWEIS_BEREITS_VERKNUEPFT:
                    this.printAndShowStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_allready_connected), R.drawable.outline_cancel_24);
                    break;
                case FREMDAUSWEIS_NICHT_GEFUNDEN:
                    this.printAndShowStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_foreign_not_found), R.drawable.outline_cancel_24);
                    break;
                case EIGENAUSWEIS_NICHT_GEFUNDEN:
                    this.printAndShowStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_own_not_found), R.drawable.outline_cancel_24);
                    break;
                case EIGENAUSWEIS_IST_FREMDAUSWEIS:
                    this.printAndShowStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_own_is_foreign), R.drawable.outline_cancel_24);
                    break;
                case FREMDAUSWEIS_IST_EIGENAUSWEIS:
                    this.printAndShowStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_foreign_is_own), R.drawable.outline_cancel_24);
                    break;
            }

            // Beende den Modus zur Bestätigung von Fremdausweisen. Zeige nicht sofort den Standard-Status
            this.cancelFremdausweisBestaetigenModus(false);

            // Zeige nach Puffer-Zeit den Standard-Status
            this.waitAndShowDefaultStatus(4000);
        });
    }

    /**
     * Verarbeitung eines technischen Fehlers bei der Verknüpfung von Fremdausweisen
     */
    void handleFremdausweisVerknuepfenHttpError() {
        this.runOnUiThread(() -> {
            // Zeige den Status, dass Verknüpfung nicht möglich war
            this.printAndShowStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_technical), R.drawable.outline_cancel_24);

            // Beende den Modus zur Bestätigung von Fremdausweisen. Zeige nicht sofort den Standard-Status
            this.cancelFremdausweisBestaetigenModus(false);

            // Zeige nach Puffer-Zeit den Standard-Status
            this.waitAndShowDefaultStatus(4000);
        });
    }

    /**
     * Initialisiert die View und den Status für die Erfassung von Fremdausweisen
     */
    private void initModusFremdausweis() {
        this.modusFremdausweisScan = false;
        this.deaktiviereFremdausweisModus(false);
        if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
            this.showButtonFremdausweisBestaetigen();
        } else {
            this.hideButtonFremdausweisBestaetigen();
        }
    }

    /**
     * Aktiviert die Erfassung mittels eines Fremdausweises
     */
    private void aktiviereFremdausweisModus() {
        // Unterbreche mögliche Puffer zum Zurücksetzen des Status
        this.resetWaitigToShowDefaultStatus();

        // Nur wenn Fremdausweise auch zugelassen sind, sollen die Buttons und Status angezeigt werden
        if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {

            // Setze den Timer zurück, dass Ausweis nicht zweimal erkannt wird
            this.letzteRfidSnr = null;
            this.letzterRfidScanZeitpunkt = null;

            // Setze den Modus
            this.modusFremdausweisScan = true;

            // Zeige den Button zum Abbrechen und blende den zum Starten aus
            this.showButtonFremdausweisAbbrechen();
            this.hideButtonFremdausweis();

            // Zeige den entsprechenden Status, dass Fremdausweis gescannt werden soll
            this.printAndShowStatus(getString(R.string.please_scan_foreign), "", R.drawable.cards);
        } else {
            // Wenn Fremdausweise nicht erlaubt sind, blende die Ansicht aus
            this.deaktiviereFremdausweisModus(false);
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    // Methoden zur automatischen Aktualisierung des Termins
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deaktiviere den Modus zur Erfassung mittels eines Fremdausweises
     *
     * @param showDefaultStatus Bestimmt, ob der Standard-Status angezeigt werden soll
     */
    private void deaktiviereFremdausweisModus(boolean showDefaultStatus) {

        // Setzen des Modus auf false
        this.modusFremdausweisScan = false;

        // Ausblenden des Buttons zum Abbrechen
        this.hideButtonFremdausweisAbbrechen();

        if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
            // Wenn Fremdausweise zugelassen sind, soll der Button zum Erfassen mit einem Fremdausweises wieder angezeigt werden
            this.showButtonFremdausweis();
        } else {
            // Ansonsten soll dieser ausgeblendet werden
            this.hideButtonFremdausweis();
        }

        // Zeige, wenn gefordert, den Standard-Status
        if (showDefaultStatus) {
            this.printAndShowDefaultStatus();
        }
    }

    /**
     * Starte einen Task, der die regelmäßige Aktualisierung von Terminen durchführt
     */
    private void starteTerminAktualsisierung() {
        this.terminAktualisierenTask = new TerminAktualisierenTask(this, this.termineApi);
        this.handler.postDelayed(this.terminAktualisierenTask, 0);
    }

    /**
     * Wiederhole den Task zur Aktualisierung des Termins, kann z.B. aus dem Task
     * aufgerufen werden, um eine dauerhafte Aktualisierung zu erreichen
     *
     * @param delay Verzögerung mit der die nächste Aktualisierung durchgeführt wird
     */
    public void triggerTerminAktualisierung(int delay) {
        if (this.handler != null) {
            this.handler.postDelayed(this.terminAktualisierenTask, delay);
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    // UI Methoden
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Führt eine sofortige, nicht wiederholte Aktualisierung des Termins aus.
     * Kann verwendet werden, um nach einer Änderung sofort die Termin-Informationen zu laden.
     */
    public void triggerSingleTerminAktualisierung() {
        if (this.handler != null) {
            this.handler.postDelayed(new TerminAktualisierenTask(this, this.termineApi, false), 0);
        }
    }

    /**
     * Gib die Informationen zu einem Termin aus
     *
     * @param termin der anzuzeigende Termin
     */
    private void printTerminInfo(TerminDetail termin) {
        this.runOnUiThread(() -> {
            String veranstaltungName = Optional.ofNullable(termin).map(TerminDetail::getVeranstaltung).map(Veranstaltung::getName).orElse(getString(R.string.currently_no_event));
            this.fragmentBinding.scanFragmentSelectedVeranstaltungName.setText(veranstaltungName);
            String veranstaltungDauer = Optional.ofNullable(termin).map(this::getTerminDauerString).orElse("");
            this.fragmentBinding.scanFragmentSelectedVeranstaltungZeit.setText(veranstaltungDauer);
        });
    }

    /**
     * Zeige den Button, mit dem eine Erfassung mit einem Fremdausweis gestartet werden kann
     */
    private void showButtonFremdausweis() {
        this.runOnUiThread(() -> {
            if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
                this.fragmentBinding.scanFragmentAdditionalButtonFremdausweis.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Blende den Button aus, mit dem das Erfassen mittels eines Fremdausweises gestartet werden kann
     */
    private void hideButtonFremdausweis() {
        this.runOnUiThread(() -> this.fragmentBinding.scanFragmentAdditionalButtonFremdausweis.setVisibility(View.GONE));
    }

    /**
     * Zeige den Button, mit dem das Verknüpfen eines Fremdausweises gestartet werden kann
     */
    private void showButtonFremdausweisBestaetigen() {
        this.runOnUiThread(() -> {
            if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
                this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisBestaetigen.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Blende den Button aus, mit dem das Verknüpfen eines Fremdausweises gestartet werden kann
     */
    private void hideButtonFremdausweisBestaetigen() {
        this.runOnUiThread(() -> this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisBestaetigen.setVisibility(View.GONE));
    }

    /**
     * Zeige den Button, um das Verknüpfen eines Fremdausweises abzubrechen
     */
    private void showButtonFremdausweisBestaetigenAbbrechen() {
        this.runOnUiThread(() -> {
            if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
                this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisBestaetigenAbbrechen.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Blende den Button aus, um das Verknüpfen eines Fremdausweises abzubrechen
     */
    private void hideButtonFremdausweisBestaetigenAbbrechen() {
        this.runOnUiThread(() -> this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisBestaetigenAbbrechen.setVisibility(View.GONE));
    }

    /**
     * Zeige den Button, um eine Erfassung mit eines Fremdausweises abzubrechen
     */
    private void showButtonFremdausweisAbbrechen() {
        this.runOnUiThread(() -> {
            if (this.selectedTermin != null && this.selectedTermin.getVeranstaltung().getErlaubeFremdausweis()) {
                this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisAbbrechen.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Blende den Button aus, um die Erfassung mittels eines Fremdausweises abzubrechen
     */
    private void hideButtonFremdausweisAbbrechen() {
        this.runOnUiThread(() -> this.fragmentBinding.scanFragmentAdditionalButtonFremdausweisAbbrechen.setVisibility(View.GONE));
    }

    /**
     * Zeige den Status
     */
    private void showStatus() {
        this.runOnUiThread(() -> this.fragmentBinding.scanFragmentStatus.setVisibility(View.VISIBLE));
    }

    /**
     * Blende den Status aus
     */
    private void hideStatus() {
        this.runOnUiThread(() -> this.fragmentBinding.scanFragmentStatus.setVisibility(View.INVISIBLE));
    }

    /**
     * Zeige den Status mit Überschrift, Untertitel und Icon
     *
     * @param headline  Anzuzeigende Überschrift
     * @param subline   Anzuzeigender Untertitel
     * @param ressource Anzuzeigendes Icon
     */
    private void printAndShowStatus(String headline, String subline, int ressource) {
        this.runOnUiThread(() -> {
            this.fragmentBinding.scanFragmentHeadline.setText(headline);
            this.fragmentBinding.scanFragmentSubline.setText(subline);
            this.fragmentBinding.scanFragmentIcon.setImageResource(ressource);
            this.showStatus();
        });
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    // Hilfsmethoden
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Zeige einen Standard-Status an, der zu einer Erfassung auffordert
     */
    private void printAndShowDefaultStatus() {
        this.printAndShowStatus(getString(R.string.please_scan), "", R.drawable.badge);
    }

    /**
     * Formatiert den Zeitraum eines Termins als String
     *
     * @param termin Termin der zu Formatieren ist
     * @return Formatierter Zeitraum des Termins
     */
    private String getTerminDauerString(TerminDetail termin) {
        String startString = this.formatDateTime(termin.getStart());
        String endeString = this.formatDateTime(termin.getEnde());
        return String.format("%s - %s", startString, endeString);
    }

    /**
     * Formatiere einen Zeitpunkt zu einem String
     *
     * @param datetime Zu formatierende Zeit
     * @return Formatierter String
     */
    private String formatDateTime(OffsetDateTime datetime) {
        return String.format("%s %s", DateHelper.formatDateTime(datetime), getString(R.string.clock));
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    // Methoden zum Zurücksetzen des Status nach einem Scan
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Extrahiere aus einem Erfassungsergebnis die erfasste Person und formatiere den Namen der Person
     *
     * @param ergebnis Ergebnis der Erfassung
     * @return Vor- und Nachname der Person
     */
    private String getPersonName(ErfassungErstellenMitAusweisErgebnis ergebnis) {
        return Optional.ofNullable(ergebnis.getAusweis())
                .map(Ausweis::getPerson)
                .map(person -> String.format("%s %s", person.getVorname(), person.getNachname()))
                .orElse("");
    }

    /**
     * Handler zum Setzen des Standard-Status nach einer definierten Zeit
     *
     * @param delay Puffer, bis Status angezeigt werden soll
     */
    private void waitAndShowDefaultStatus(int delay) {

        // Erzeuge einen Handler, wenn keiner existiert
        if (this.anzeigeZuruecksetzenHandler == null) {
            this.anzeigeZuruecksetzenHandler = new Handler();
        }

        // Erzeugen des verzögerten Tasks
        this.anzeigeZuruecksetzenHandler.postDelayed(this::printAndShowDefaultStatus, delay);
    }

    /**
     * Setze den Handler zum Setzen des Standard-Status zurück. Der Task wird nicht ausgeführt.
     */
    private void resetWaitigToShowDefaultStatus() {
        if (this.anzeigeZuruecksetzenHandler != null) {
            this.anzeigeZuruecksetzenHandler.removeCallbacksAndMessages(null);
        }
    }

    public void handleUnauthorizedException() {
        this.geheZurKonfiguration();
    }

    public void geheZurKonfiguration() {
        this.runOnUiThread(() -> {
            this.deaktiviereApiKonfiguration();
            Intent intent = new Intent(this, ConfigActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
    }

    public void geheZurRaumauswahl() {
        this.runOnUiThread(() -> {
            Intent intent = new Intent(this, RaumUndTerminSelectActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
    }

    public void gibFehlerAus(String meldung) {
        this.runOnUiThread(() -> {
            Toast.makeText(this, meldung, Toast.LENGTH_LONG).show();
        });
    }


}