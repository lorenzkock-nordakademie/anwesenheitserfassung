package de.lorenzkock.anwesenheitserfassungclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.helper.TeilnehmerAktionGewaehltCallback;
import de.lorenzkock.anwesenheitserfassungclient.model.PersonZuTermin;

public class TeilnehmerListAdapterErfassen extends TeilnehmerListAdapter {

    public TeilnehmerListAdapterErfassen(Context context, List<PersonZuTermin> teilnehmer, TeilnehmerAktionGewaehltCallback personGewaehltCallback) {
        super(context, teilnehmer, personGewaehltCallback);
    }
    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_teilnehmer, parent, false);

            viewHolder.tvName = convertView.findViewById(R.id.teilnehmer_row_name);
            viewHolder.tvKennung = convertView.findViewById(R.id.teilnehmer_row_nummer);
            viewHolder.ivStatus = convertView.findViewById(R.id.teilnehmer_row_status);
            viewHolder.ibAktion = convertView.findViewById(R.id.teilnehmer_row_aktion);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PersonZuTermin person = (PersonZuTermin) this.getItem(i);
        viewHolder.tvName.setText(String.format("%s, %s", person.getPerson().getNachname(), person.getPerson().getVorname()));
        viewHolder.tvKennung.setText(person.getPerson().getKennung());

        Integer statusIcon = null;
        switch (person.getStatus()) {
            case ERFASST:
                statusIcon = R.drawable.outline_check_circle_24;
                break;
            case ERFASST_ANWESEND:
                statusIcon = R.drawable.arrow_right_green;
                break;
            case ERFASST_NICHT_ANWESEND:
                statusIcon = R.drawable.arrow_left_green;
                break;
            case KEINE_ERFASSUNG:
                statusIcon = R.drawable.outline_cancel_24;
                break;
        }

        viewHolder.ivStatus.setImageResource(statusIcon);

        Integer aktionIcon = null;
        if (person.getAktion() != null) {
            switch (person.getAktion()) {
                case ERFASSEN:
                    aktionIcon = R.drawable.outline_check_circle_24;
                    break;
                case ERFASSEN_AUSGEHEND:
                    aktionIcon = R.drawable.arrow_left_green;
                    break;
                case ERFASSEN_WIEDERKEHREND:
                    aktionIcon = R.drawable.arrow_right_green;
                    break;
                case ERFASSEN_MEHRFACH:
                    aktionIcon = R.drawable.refresh;
                    break;
                case KEINE_ERFASSUNG:
                    break;
            }
        }

        if (aktionIcon == null) {
            viewHolder.ibAktion.setVisibility(View.INVISIBLE);
            viewHolder.ibAktion.setOnClickListener(null);
        } else {
            viewHolder.ibAktion.setVisibility(View.VISIBLE);
            viewHolder.ibAktion.setImageResource(aktionIcon);
            viewHolder.ibAktion.setOnClickListener((c) -> this.getCallback().aktion(person.getPerson().getId()));
        }

        return convertView;
    }

}
