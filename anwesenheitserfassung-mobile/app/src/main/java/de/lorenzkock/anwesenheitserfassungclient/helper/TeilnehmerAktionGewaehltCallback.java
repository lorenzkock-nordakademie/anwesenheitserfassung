package de.lorenzkock.anwesenheitserfassungclient.helper;

public interface TeilnehmerAktionGewaehltCallback {
    void aktion(String personId);

}
