package de.lorenzkock.anwesenheitserfassungclient.helper;

import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiCallback;

public interface SimpleApiCallback<T> extends ApiCallback<T> {

    @Override
    default void onUploadProgress(long bytesWritten, long contentLength, boolean done) {

    }

    @Override
    default void onDownloadProgress(long bytesRead, long contentLength, boolean done) {

    }

}
