package de.lorenzkock.anwesenheitserfassungclient.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungclient.adapter.TerminListAdapter;
import de.lorenzkock.anwesenheitserfassungclient.api.TermineApi;
import de.lorenzkock.anwesenheitserfassungclient.databinding.FragmentTermineSelectBinding;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleApiCallback;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.Raum;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminList;

public class TerminSelectFragment extends Fragment {

    private FragmentTermineSelectBinding binding;
    private TermineApi termineApi;
    private TerminListAdapter listAdapter;
    private Raum selectedRaum;
    private String TAG = "TerminSelectFragment";

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Erzeuge Binding zu den Komponenten des Fragments
        binding = FragmentTermineSelectBinding.inflate(inflater, container, false);

        RaumUndTerminSelectActivity mainActivity = (RaumUndTerminSelectActivity)getContext();
        if(mainActivity != null) {
            // Lese den Raum, der im vorigen Schritt gewählt wurde
            this.selectedRaum = null;
            Bundle arguments = getArguments();
            if(arguments != null && arguments.containsKey("raum")) {
                this.selectedRaum = (Raum)getArguments().getSerializable("raum");
            }

            // Blende die Option, keine Veranstaltung zu wählen, aus, wenn kein Raum ausgewählt wurde
            if(this.selectedRaum == null) {
                this.binding.terminSelectOhne.setVisibility(View.INVISIBLE);
            }

            // Initialisiere die API
            this.termineApi = new TermineApi(mainActivity.getApiClient());

            // Erzeuge einen Adapter zur Datenverarbeitung in der Liste und weise den zu
            this.listAdapter = new TerminListAdapter(getContext(), Collections.emptyList());
            this.binding.terminList.setAdapter(this.listAdapter);

            // Konfiguriere das Verhalten, wenn eine Veranstaltung gewählt wurde
            this.binding.terminList.setOnItemClickListener((adapterView, view, i, l) -> {
                Activity activity = getActivity();
                if(activity != null) {
                    activity.runOnUiThread(() -> {
                        TerminList selectedTermin = (TerminList) listAdapter.getItem(i);
                        navigate(this.selectedRaum, selectedTermin);
                    });
                }
            });

            // Konfiguriere das Verhalten beim Refresh der Liste
            this.binding.terminSwipeLayout.setOnRefreshListener(() -> getTerminData(this.selectedRaum));

            // Konfiguriere die Aktion, wenn kein Raum gewählt wird
            this.binding.terminSelectOhne.setOnClickListener(view -> navigate(this.selectedRaum, null));

            // Lade die Daten zur Veranstaltung initial
            this.getTerminData(this.selectedRaum);
        }

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    /**
     * Navigiere zur nächsten Ansicht
     * @param raum Raum der als Filter für die nächste Ansicht dient
     */
    private void navigate(Raum raum, TerminList termin) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("raum", raum);
        bundle.putSerializable("terminId", Optional.ofNullable(termin).map(TerminList::getId).orElse(null));

        Intent intent = new Intent(getActivity(), ErfassungActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("raum", raum);
        intent.putExtra("terminId", Optional.ofNullable(termin).map(TerminList::getId).orElse(null));
        startActivity(intent);
    }


    /**
     * Lade die Daten aller Räume ggf. mit Filter ines Raums
     */
    private void getTerminData(Raum selectedRaum) {

        try {
            // Definiere, was mit den erhaltenen Daten passieren soll
            SimpleApiCallback<List<TerminList>> callback = new SimpleApiCallback<List<TerminList>>() {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    Activity activity = getActivity();
                    if(activity != null) {
                        activity.runOnUiThread(() -> {
                            binding.terminSwipeLayout.setRefreshing(false);
                            binding.terminListLoading.setVisibility(View.INVISIBLE);

                            Log.e(TAG, String.format("Fehler beim Abruf der Termine: Status %s: %s", statusCode, e.getMessage()), e);
                            if(statusCode == 401) {
                                Toast.makeText(getContext(), "Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.", Toast.LENGTH_LONG).show();
                            } else if(statusCode == 500) {
                                Toast.makeText(getContext(), "Beim Abruf der Termine ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getContext(), "Beim Abruf der Termine ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            }

                        });
                    }
                }
                @Override
                public void onSuccess(List<TerminList> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    Activity activity = getActivity();
                    if(activity != null) {
                        activity.runOnUiThread(() -> {
                            // Setze die gelesenen Daten und aktualisiere die Liste
                            listAdapter.setTermine(result);
                            listAdapter.notifyDataSetChanged();

                            // Beende die Anzeige zur Aktualisierung der Liste
                            binding.terminSwipeLayout.setRefreshing(false);
                            binding.terminListEmpty.setVisibility(!result.isEmpty() ? View.INVISIBLE : View.VISIBLE);
                            binding.terminListLoading.setVisibility(View.INVISIBLE);
                        });
                    }
                }
            };

            // Unterscheide, ob Termine nach einem Raum gesucht werden sollen oder alle Veranstaltungen
            if(selectedRaum == null) {
                termineApi.getAktiveTermineAsync(null, 50, 0, callback);
            } else {
                termineApi.getAktiveTermineAsync(selectedRaum.getId(), 50,0, callback);
            }

        } catch (ApiException e) {
            Log.e(TAG, String.format("Fehler beim Abruf der Termine: %s", e.getMessage()), e);
        }

    }

}