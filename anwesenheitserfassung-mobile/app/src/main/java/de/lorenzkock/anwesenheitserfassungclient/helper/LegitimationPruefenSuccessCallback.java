package de.lorenzkock.anwesenheitserfassungclient.helper;

import de.lorenzkock.anwesenheitserfassungclient.model.Berechtigungen;

public interface LegitimationPruefenSuccessCallback {
        void legitimiert(Berechtigungen berechtigungen);

}
