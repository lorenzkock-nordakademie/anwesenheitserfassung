package de.lorenzkock.anwesenheitserfassungclient.helper;

import de.lorenzkock.anwesenheitserfassungclient.model.Berechtigungen;

public interface LegitimationPruefenHelper {
        boolean legitimiert(Berechtigungen berechtigungen);

}
