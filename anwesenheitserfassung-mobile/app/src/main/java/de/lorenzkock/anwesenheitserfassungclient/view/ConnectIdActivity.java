package de.lorenzkock.anwesenheitserfassungclient.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.lorenzkock.anwesenheitserfassungclient.R;
import de.lorenzkock.anwesenheitserfassungclient.adapter.TeilnehmerListAdapterAusweisZuordnen;
import de.lorenzkock.anwesenheitserfassungclient.api.RfidApi;
import de.lorenzkock.anwesenheitserfassungclient.api.TermineApi;
import de.lorenzkock.anwesenheitserfassungclient.databinding.ActivityConnectIdBinding;
import de.lorenzkock.anwesenheitserfassungclient.databinding.FragmentConnectIdBinding;
import de.lorenzkock.anwesenheitserfassungclient.helper.RfidHelper;
import de.lorenzkock.anwesenheitserfassungclient.helper.SimpleApiCallback;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiClient;
import de.lorenzkock.anwesenheitserfassungclient.invoker.ApiException;
import de.lorenzkock.anwesenheitserfassungclient.model.EigenausweisMitPersonVerknuepfen;
import de.lorenzkock.anwesenheitserfassungclient.model.EigenausweisMitPersonVerknuepfenErgebnis;
import de.lorenzkock.anwesenheitserfassungclient.model.PersonZuTermin;
import de.lorenzkock.anwesenheitserfassungclient.model.TerminDetail;

public class ConnectIdActivity extends AppCompatActivity {

    /**
     * RFID Konfiguration, welche Tags erkannt werden sollen
     */
    public static int READER_FLAGS = NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    public NfcAdapter nfcAdapter;
    Handler anzeigeZuruecksetzenHandler = null;

    private ActivityConnectIdBinding binding;
    private FragmentConnectIdBinding fragmentBinding;
    private TeilnehmerListAdapterAusweisZuordnen teilnehmerListAdapter;
    private TerminDetail selectedTermin;
    private TermineApi termineApi;
    private RfidApi rfidApi;
    private final String TAG = "ConnectIdActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.binding = ActivityConnectIdBinding.inflate(getLayoutInflater());
        fragmentBinding = this.binding.activityConnectIdFragment;

        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle argumente = intent.getExtras();
            if (argumente != null) {
                this.selectedTermin = (TerminDetail) argumente.getSerializable("termin");
            }
        }

        // Init NFC
        this.nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        // Initialisiere Api
        ApiClient apiClient = this.getApiClient();
        this.termineApi = new TermineApi(apiClient);
        this.rfidApi = new RfidApi(apiClient);

        // Initialisiere Liste
        this.teilnehmerListAdapter = new TeilnehmerListAdapterAusweisZuordnen(this, Collections.emptyList(), this::ausweisErfassen);
        this.fragmentBinding.teilnehmerList.setAdapter(this.teilnehmerListAdapter);

        // Schließen Button beim Status einer Verknüpfung
        this.fragmentBinding.teilnehmerStatusClose.setOnClickListener((c) -> this.showList());

        // Abbrechen Button beim Scannen von Ausweisen
        this.binding.connectScanAlertAbort.setOnClickListener((v) -> {
            this.binding.connectScanAlert.setVisibility(View.GONE);
            this.binding.activityConnectIdContent.setVisibility(View.VISIBLE);
            if(this.nfcAdapter != null) {
                this.nfcAdapter.disableReaderMode(this);
            }
        });

        // Konfiguriere das Verhalten beim Refresh der Liste
        this.fragmentBinding.teilnehmerSwipeLayout.setOnRefreshListener(this::getTeilnehmerDaten);

        // Lade die Teilnehmer Daten
        this.getTeilnehmerDaten();
    }

    public ApiClient getApiClient() {
        SharedPreferences sharedPreferences = getApiPreferences();
        String baseUrl = sharedPreferences.getString("baseUrl", "");
        String benutzername = sharedPreferences.getString("benutzername", "");
        String passwort = sharedPreferences.getString("passwort", "");

        ApiClient client = new ApiClient();
        client.setBasePath(baseUrl);
        client.setUsername(benutzername);
        client.setPassword(passwort);

        return client;
    }

    public SharedPreferences getApiPreferences() {
        return getSharedPreferences("apiConfig", Context.MODE_PRIVATE);
    }

    private void getTeilnehmerDaten() {
        if (this.selectedTermin != null) {
            try {
                this.termineApi.getTeilnehmerByTerminAsync(this.selectedTermin.getId(), new SimpleApiCallback<List<PersonZuTermin>>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        Activity activity = ConnectIdActivity.this;
                        activity.runOnUiThread(() -> {
                            fragmentBinding.teilnehmerSwipeLayout.setRefreshing(false);
                            fragmentBinding.teilnehmerListLoading.setVisibility(View.INVISIBLE);

                            Log.e(TAG, String.format("Fehler beim Abruf der Teilnehmer: Status %s: %s", statusCode, e.getMessage()), e);
                            if (statusCode == 401) {
                                Toast.makeText(activity, "Der Benutzer ist nicht berechtigt. Bitte neu konfigurieren.", Toast.LENGTH_LONG).show();
                            } else if (statusCode == 404) {
                                Toast.makeText(activity, "Der Termin wurde nicht gefunden.", Toast.LENGTH_LONG).show();
                            } else if (statusCode == 500) {
                                Toast.makeText(activity, "Beim Abruf der Teilnehmer ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(activity, "Beim Abruf der Teilnehmer ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                            }

                        });
                    }

                    @Override
                    public void onSuccess(List<PersonZuTermin> result, int statusCode, Map<String, List<String>> responseHeaders) {
                        runOnUiThread(() -> {

                            teilnehmerListAdapter.setTeilnehmer(result.stream()
                                    .sorted(
                                            Comparator.comparing(p -> ((PersonZuTermin)p).getPerson().getNachname()).
                                                    thenComparing(p -> ((PersonZuTermin)p).getPerson().getVorname()))
                                    .collect(Collectors.toList()));
                            teilnehmerListAdapter.notifyDataSetChanged();
                            fragmentBinding.teilnehmerSwipeLayout.setRefreshing(false);
                            fragmentBinding.teilnehmerListEmpty.setVisibility(!result.isEmpty() ? View.INVISIBLE : View.VISIBLE);
                            fragmentBinding.teilnehmerListLoading.setVisibility(View.INVISIBLE);
                        });
                    }
                });
            } catch (ApiException e) {
                Toast.makeText(this, "Beim Abruf der Teilnehmer ist ein Fehler aufgetreten.", Toast.LENGTH_LONG).show();
                Log.e(TAG, String.format("Fehler beim Abruf der Teilnehmer: %s", e.getMessage()), e);
            }
        }

    }

    private NfcAdapter.ReaderCallback connectCallback(String personId) {

        this.binding.connectScanAlert.setVisibility(View.VISIBLE);
        this.binding.activityConnectIdContent.setVisibility(View.GONE);
        return (tag) -> {

            // Ermitteln der Seriennummer des Tags
            String rfidId = RfidHelper.bytesToHRfidID(tag.getId());

            EigenausweisMitPersonVerknuepfen verknuepfenData = new EigenausweisMitPersonVerknuepfen();
            verknuepfenData.rfidSnrEigen(rfidId);
            verknuepfenData.personId(personId);

            try {
                this.rfidApi.verknuepfeEigenAusweisAsync(verknuepfenData, new SimpleApiCallback<EigenausweisMitPersonVerknuepfenErgebnis>() {
                    @Override
                    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                        handleEigenausweisVerknuepfenHttpError();
                    }

                    @Override
                    public void onSuccess(EigenausweisMitPersonVerknuepfenErgebnis result, int statusCode, Map<String, List<String>> responseHeaders) {
                        handleEigenausweisVerknuepfenHttpSuccess(result);
                    }
                });
            } catch (ApiException e) {
                handleEigenausweisVerknuepfenHttpError();
            }

        };

    }


    /**
     * Verarbeitung des Ergebnis der Verknüpfung, wenn keine technischen Fehler aufgetreten sind
     *
     * @param ergebnis Ergebnis der Verknüpfung
     */
    private void handleEigenausweisVerknuepfenHttpSuccess(EigenausweisMitPersonVerknuepfenErgebnis ergebnis) {
        this.runOnUiThread(() -> {
            EigenausweisMitPersonVerknuepfenErgebnis.ErgebnisEnum ergebnisEnum = ergebnis.getErgebnis();

            // Setze einen Standardwert, wie lange der Status angezeigt werden soll
            switch (ergebnisEnum) {
                case ERFOLGREICH:
                    this.showStatus(getString(R.string.binding_success), getString(R.string.binding_success_subtitle), R.drawable.outline_check_circle_24);
                    break;
                case EIGENAUSWEIS_BEREITS_VERKNUEPFT:
                    this.showStatus(getString(R.string.binding_warning), getString(R.string.binding_success_existing_binding_same_person), R.drawable.outline_cancel_24);
                    break;
                case EIGENAUSWEIS_NEU_VERKNUEPFT:
                    this.showStatus(getString(R.string.binding_success), getString(R.string.binding_success_existing_binding_other_person), R.drawable.warning);
                    break;
                case PERSON_NICHT_GEFUNDEN:
                    this.showStatus(getString(R.string.binding_warning), getString(R.string.scan_fail_unknown_person), R.drawable.outline_cancel_24);
                    break;
            }

            // Zeige nach Puffer-Zeit den Standard-Status
            this.waitAndShowList(4000);
        });
    }

    /**
     * Verarbeitung eines technischen Fehlers bei der Verknüpfung von Eigenasuweis mit Person
     */
    void handleEigenausweisVerknuepfenHttpError() {
        this.runOnUiThread(() -> {
            // Zeige den Status, dass Verknüpfung nicht möglich war
            this.showStatus(getString(R.string.binding_warning), getString(R.string.binding_warning_technical), R.drawable.outline_cancel_24);

            // Zeige nach Puffer-Zeit den Standard-Status
            this.waitAndShowList(4000);
        });
    }


    private void ausweisErfassen(String personId) {
        if(personId != null) {
            if(this.nfcAdapter != null) {
                this.nfcAdapter.enableReaderMode(this, this.connectCallback(personId), READER_FLAGS, null);
            }
        }
    }

    /**
     * Handler zum Zeigen der Liste nach einem Status
     *
     * @param delay Puffer, bis Liste angezeigt werden soll
     */
    private void waitAndShowList(int delay) {
        runOnUiThread(() -> {

            // Erzeuge einen Handler, wenn keiner existiert
            if (this.anzeigeZuruecksetzenHandler == null) {
                this.anzeigeZuruecksetzenHandler = new Handler();
            }

            // Erzeugen des verzögerten Tasks
            this.anzeigeZuruecksetzenHandler.postDelayed(() -> runOnUiThread(this::showList), delay);
        });
    }

    private void cancelWaitAndShow() {
        if (this.anzeigeZuruecksetzenHandler != null) {
            this.anzeigeZuruecksetzenHandler.removeCallbacksAndMessages(null);
        }
    }

    private void showList() {
        this.cancelWaitAndShow();
        runOnUiThread(() -> {
            this.fragmentBinding.teilnehmerStatus.setVisibility(View.GONE);
            this.fragmentBinding.teilnehmerList.setVisibility(View.VISIBLE);
        });
    }

    private void showStatus(String headline, String subline, int icon) {
        runOnUiThread(() -> {
            fragmentBinding.teilnehmerStatusHeadline.setText(headline);
            fragmentBinding.teilnehmerStatusSubline.setText(subline);
            fragmentBinding.teilnehmerStatusIcon.setImageResource(icon);
            this.fragmentBinding.teilnehmerStatusClose.setVisibility(View.VISIBLE);

            this.binding.connectScanAlert.setVisibility(View.GONE);
            this.binding.activityConnectIdContent.setVisibility(View.VISIBLE);

            this.fragmentBinding.teilnehmerList.setVisibility(View.GONE);
            this.fragmentBinding.teilnehmerStatus.setVisibility(View.VISIBLE);
        });
    }

}