# Import von Daten in die Anwesenheitserfassung

Die Anwendung ermöglicht über den Punkt **Konfiguration** den Import verschiedener Daten, die für die Verwendung der Anwendung notwendig sind. Folgende Daten können über den Import importiert werden:

 - Räume
 - Personen
 - Benutzerausweise / -karten
 - Veranstaltungen
 - Termine
 - Zuordnungen von Personen zu Terminen oder Veranstaltungen

Der Import akzeptiert CSV-Dateien. Das Format der CSV-Dateien ist bedingt vorgegeben. Die folgenden Abschnitte beschreiben für jeden Punkt einzeln das Format sowie Abhängigkeiten.

# Formate des Imports

In folgendem Abschnitt werden die Formate der einzelnen CSV-Dateien beschrieben. Es existieren einige Anforderungen an das Format, die über alle Typen hinweg gültig sind:

 - Grundsätzlich ist es möglich, **Kennungen** zu verwenden. Diese ermöglichen einen Verweis von Objekten unterschiedlicher Typen, beispielsweise zwischen Veranstaltungen und Terminen. Durch die Verwendung von Kennungen kann auch auf bereits existierende Daten referenziert werden. Kennungen sind für den jeweiligen Typ eindeutig und können nicht mehrfach vergeben werden.
 - Werden **keine Kennungen** verwendet, wird versucht, eine Zuordnung über den Namen aufzulösen. Dabei werden nur Daten berücksichtigt, die in einem Import gleichzeitig verarbeitet werden. Bereits manuell oder durch einen vorigen Import angelegte Daten werden nur über die Kennung identifiziert.
 - Wird keine Kennung verwendet, wird diese generiert.
 - Kennungen dürfen keine Leerzeichen enthalten.
- In jeder CSV-Datei wird erwartet, dass in der ersten Zeile die Spaltentitel stehen.
- Zellen werden jeweils durch ein Semikolon (;) getrennt.
- Zeilen werden durch einen Umbruch getrennt.
- Wird ein Semikolon in einer Zelle gebraucht, ist die ganze Zelle mit doppelten Anführungszeichen (") zu umklammern.
- Die Reihenfolge der Spalten ist irrelevant. Die Benennung hingegen muss exakt übereinstimmen.
- Es werden Warnungen und Fehler erzeugt:
	- **Fehler** verhindern den Import aller oder einzelner Objekte eines Typs.
	- Bei **Warnungen** wird das Objekt angelegt, eine mögliche Referenz auf andere Objekte ist jedoch nicht erfolgreich.


## Import von Räumen

Folgende Spalten werden beim Import von Räumen akzeptiert:

- kennung *(optional)*
- bezeichnung

Als Kennung kann hier bspw. die Raumnummer in einem maschinenlesbaren Format verwendet werden.

Beispiel für eine minimalistische CSV-Datei zum Import von Räumen, die Kennung wird jeweils generiert:

    bezeichnung
    Raum 100
    Raum 200

Auch möglich ist folgendes Beispiel, bei der die Kennung mitgegeben wird.

    kennung;bezeichnung
    r100;Raum 100
    r200;Raum 200  

Die Kennung ist jeweils eindeutig, Bezeichnungen von Räumen können häufiger vergeben werden.

**Fehler** treten auf, wenn:

- die Kennung mehrfach verwendet wird.
- die Bezeichnung leer ist.

Es treten keine **Warnungen** auf.


## Import von Benutzerausweisen

Folgende Spalten werden beim Import von Benutzerausweisen akzeptiert:

- kennung *(optional)*
- rfidsnr
- person_kennung *(optional)*
- person_vorname *(optional)*
- person_nachname *(optional)*

Die Angabe der Personen-Daten ist optional. Durch die Angabe einer Personenkennung kann eine Zuordnung zu einer bereits angelegten Person erfolgen. Werden der Vorname oder Nachname angegeben, wird die Person bereits in diesem Schritt angelegt. Eine Zuordnung zu einer Person ist jedoch auch noch im Import-Schritt der Personen möglich.

Als Kennung kann in diesem Beispiel die Ausweisnummer eines Benutzerausweises vergeben werden.

Beispiel für eine minimalistische CSV-Datei zum Import von Benutzerausweisen, die Kennung wird jeweils generiert:

    rfidsnr
    01:02:03:80
    02:03:04:08

Auch möglich ist folgendes Beispiel, bei der die Kennung mitgegeben wird:

    kennung;rfidsnr
    A1;01:02:03:80
    A2;02:03:04:08

Mit folgendem Beispiel lassen sich bereits existierende Personen zuordnen. Eine Kennung einer Person ist bspw. die Matrikelnummer:

    kennung;rfidsnr;person_kennung
    A1;01:02:03:80;11111
    A2;02:03:04:08;22222

Mit folgendem Beispiel lassen sich Personen zuordnen, die in diesem Schritt erzeugt werden. Die Angabe der Kennung ist auch hier optional, sie wird erzeugt, wenn sie nicht angegeben wird. Zur Anlage der Person sind jedoch Vor- und Nachname notwendig. Wird eines nicht angegeben, wird die Person nicht angelegt.

    kennung;rfidsnr;person_kennung;person_vorname;person_nachname
    A1;01:02:03:80;11111;Max;Schiffer
    A2;02:03:04:08;22222;Petra Leo;Meyer


Die Kennung ist jeweils eindeutig, RFID-Seriennummern können mehrfach vergeben werden, alle anderen werden dann jedoch inaktiv geschaltet, da nur eine aktive Instanz mit einer Seriennummer existieren darf.

**Fehler** treten auf, wenn:

- die Kennung mehrfach verwendet wird.
- die RFID-Seriennummer leer ist.

**Warnungen** treten auf, wenn:

- Referenzierte Personen nicht gefunden werden.

## Import von Personen

Folgende Spalten werden beim Import von Personen akzeptiert:

- kennung *(optional)*
- vorname
- nachname
- ausweis_kennung *(optional)*
- ausweis_rfidsnr *(optional)*

Die Angabe der Ausweis-Daten ist optional. Durch die Angabe einer Ausweis-Kennung oder der Seriennummer kann eine Zuordnung zu einem bereits angelegten Ausweis (bei Kennung auch aus dem vorigen Schritt) erfolgen. Durch die Angabe einer RFID-Seriennummer kann in diesem Schritt ein Ausweis für diese Person angelegt werden.

Als Kennung kann in diesem Beispiel die Matrikelnummer oder Mitarbeiternummer einer Person vergeben werden.

Beispiel für eine minimalistische CSV-Datei zum Import von Personen, die Kennung wird jeweils generiert:

    vorname;nachname
    Peter;Schulz
    Birte;Peters

Auch möglich ist folgendes Beispiel, bei der die Kennung mitgegeben wird:

    kennung;vorname;nachname
    M1;Peter;Schulz
    Stu123;Birte;Peters

Mit folgendem Beispiel lassen sich bereits existierende Ausweise zuordnen. Die Kennung zur Person ist weiterhin optional:

    kennung;vorname;nachname;ausweis_kennung
    M1;Peter;Schulz;A1
    Stu123;Birte;Peters;A2

Mit folgendem Beispiel lassen sich Ausweise zuordnen, die in diesem Schritt erzeugt werden. Die Angabe der Kennung ist auch hier optional, sie wird erzeugt, wenn sie nicht angegeben wird. Zur Anlage des Ausweises ist die Angabe der RFID-Seriennummer notwendig, die Angabe der Kennung für den Ausweis ist optional:

    kennung;vorname;nachname;ausweis_kennung;ausweis_rfidsnr
    M1;Peter;Schulz;A1;11:22:33:80
    Stu123;Birte;Peters;A2;99:77:66:80


Die Kennung ist jeweils eindeutig, Vor- und Nachnamen können jeweils mehrfach vergeben werden. Auch hier werden Ausweise deaktiviert, die doppelt vergeben werden, allerdings nur, wenn sie bereits existierten.

**Fehler** treten auf, wenn:

- die Kennung mehrfach verwendet wird.
- der Vorname leer ist.
- der Nachname leer ist.

**Warnungen** treten auf, wenn:

- der Ausweis bereits einer anderen Person zugewiesen ist.
- der Ausweis zu einer Kennung nicht gefunden wurde und keine RFID-Seriennummer angegeben wurde.

## Import von Veranstaltungen

Folgende Spalten werden beim Import von Veranstaltungen akzeptiert:

- kennung *(optional)*
- name
- erlaube_eingehendausgehend *(optional, defalt: false)*
- erlaube_mehrfachbuchungen *(optional, defalt: false)*
- erlaube_fremdausweise *(optional, defalt: false)*
- fremdausweise_bestaetigen *(optional, defalt: false)*
- teilnehmer *(optional)*
- berechtigte *(optional)*

Für die Anlage einer Veranstaltung wird nur der Name benötigt. Der Parameter *teilnehmer* ermöglicht das Hinzufügen von Personen als Teilnehmer, mit dem Parameter *berechtigte* können berechtigte Personen angegeben werden. Die Angabe der Teilnehmer und Berechtigten erfolgt über Kommma-separierte Listen von Kennungen der Person.

Als Kennung kann in diesem Beispiel die Veranstaltungsnummer einer Veranstaltung vergeben werden.

Beispiel für eine minimalistische CSV-Datei zum Import von Veranstaltungen, die Kennung wird jeweils generiert:

    name
    MWINF20oA Softwareengineering
    MBWL21aB Grundlagen der BWL

Auch möglich ist folgendes Beispiel, bei der die Kennung mitgegeben wird:

    kennung;name
    mwinf20oa-softwareengineering;MWINF20oA Softwareengineering
    mbwl21ab-grundlagen-der-bwl;MBWL21aB Grundlagen der BWL

Mit folgendem Beispiel lassen sich die jeweiligen Parameter setzen:

	kennung;name;erlaube_eingehendausgehend;erlaube_mehrfachbuchungen;erlaube_fremdausweise;fremdausweise_bestaetigen
    mwinf20oa-softwareengineering;MWINF20oA Softwareengineering;1;1;0;0
    mbwl21ab-grundlagen-der-bwl;MBWL21aB Grundlagen der BWL;true;true;false;false

Folgendes Beispiel zeigt die Verknüpfung zu Personen, es können auch nur Teilnehmer oder Berechtigte angegeben werden:

    kennung;name;teilnehmer;berechtigte
    mwinf20oa-softwareengineering;MWINF20oA Softwareengineering;Stu1234,Stu9999;M1,M2
    mbwl21ab-grundlagen-der-bwl;MBWL21aB Grundlagen der BWL;Stu4444;M3

Das Anlegen von Personen ist in diesem Schritt nicht möglich. Ein Verweis erfolgt zwingend über die Kennung, bspw. eine Matrikelnummer.
Die Kennung ist jeweils eindeutig, Namen können jeweils mehrfach vergeben werden.

**Fehler** treten auf, wenn:

- die Kennung mehrfach verwendet wird.
- der Name leer ist.

**Warnungen** treten auf, wenn:

- Personen als Teilnehmer oder Berechtigte nicht gefunden werden.

## Import von Terminen

Folgende Spalten werden beim Import von Terminen akzeptiert:

- kennung *(optional)*
- start
- ende
- teilnehmer *(optional)*
- berechtigte *(optional)*
- raum_kennung *(optional)*
- raum_name *(optional)*
- veranstaltung_kennung *(optional, wenn veranstaltung_name gegeben ist)*
- veranstaltung_name *(optional, wenn veranstaltung_kennung gegeben ist)*
- veranstaltung_erlaube_eingehendausgehend *(optional, defalt: false)*
- veranstaltung_erlaube_mehrfachbuchungen *(optional, defalt: false)*
- veranstaltung_erlaube_fremdausweise *(optional, defalt: false)*
- veranstaltung_fremdausweise_bestaetigen *(optional, defalt: false)*
- veranstaltung_teilnehmer *(optional)*
- veranstaltung_berechtigte *(optional)*

Für die Anlage eines Termins werden der Start- und der End-Zeitpunkt benötigt. Beide Zeitpunkte müssen im Format **dd.MM.yyyy HH:mm** angegeben werden.
Über den Import von Terminen können Räume und Veranstaltungen angelegt werden. Eine Zuweisung von Teilnehmern und Berechtigten kann sowohl zum Termin als auch zur Veranstaltung erfolgen. Der Parameter *teilnehmer* ermöglicht das hinzufügen von Personen als Teilnehmer zum Termin, mit dem Parameter *berechtigte* können berechtigte Personen zum Termin angegeben werden. Die Angabe der Teilnehmer und Berechtigten erfolgt über Kommma-separierte Listen von Kennungen der Person.
Gleiches gilt für die Parameter *veranstaltung_teilnehmer* sowie *veranstaltung_berechtigte*. Die Personen werden der Veranstaltung zugewiesen.

Beispiel für eine minimalistische CSV-Datei zum Import von Terminen, die Kennung wird jeweils generiert:

    start;ende;veranstaltung_name
    01.01.2024 08:00;01.01.2024 18:30;MJUR10oA Einführung in das Prozessrecht
    02.01.2024 08:00;02.01.2024 18:30;MJUR10oA Einführung in das Prozessrecht
    02.01.2024 08:00;02.01.2024 18:30;MWINF21oa Strategisch Unternehmensführung

Die Veranstaltungen werden angelegt, wenn sie nicht schon durch den Schritt des Imports der Veranstaltungen erzeugt wurden. Die Veranstaltung *'MJUR10oA Einführung in das Prozessrecht'* wird nur einmal angelegt. Die Kennung der Veranstaltung kann ebenfalls angegeben werden, ist aber optional.

Auch möglich ist folgendes Beispiel, bei der die Kennung des Termins mitgegeben wird:

    kennung;start;ende;veranstaltung_name
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;MJUR10oA Einführung in das Prozessrecht
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;MJUR10oA Einführung in das Prozessrecht
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;MWINF21oa Strategisch Unternehmensführung

Folgendes Beispiel zeigt die Anlage von Terminen mit der Referenz zu Veranstaltungen über deren Kennung:

    kennung;start;ende;veranstaltung_kennung
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;mjur10oa-eipr
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;mjur10oa-eipr
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;mwinf21oa-suf

Dies setzt voraus, dass die Veranstaltungen bereits existieren, oder über den vorigen Schritt importiert wurden.

Bei der Anlage von Terminen ist ein Verweis auf existierende Räume möglich oder es lassen sich Räume anlegen, wenn der Raumname gegeben wird und der Raum noch nicht existiert. Dies zeigen die folgenden Beispiele:

    kennung;start;ende;veranstaltung_kennung;raum_kennung
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;mjur10oa-eipr;R100
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;mjur10oa-eipr;R100
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;mwinf21oa-suf;R200

    kennung;start;ende;veranstaltung_kennung;raum_name
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;mjur10oa-eipr;Raum 100
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;mjur10oa-eipr;Raum 100
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;mwinf21oa-suf;Raum 200

    kennung;start;ende;veranstaltung_kennung;raum_kennung;raum_name
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;mjur10oa-eipr;R100;Raum 100
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;mjur10oa-eipr;R100;Raum 100
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;mwinf21oa-suf;R200;Raum 200


Mit folgendem Beispiel lassen sich die jeweiligen Parameter zur Konfiguration der anzulegenden Veranstaltung setzen:

	kennung;start;ende;veranstaltung_kennung;veranstaltung_name;erlaube_eingehendausgehend;erlaube_mehrfachbuchungen;erlaube_fremdausweise;fremdausweise_bestaetigen;teilnehmer_veranstaltung
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;mjur10oa-eipr;MJUR10oA Einführung in das Prozessrecht;1;1;0;0;Stu12345,Stu9999
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;mjur10oa-eipr;MJUR10oA Einführung in das Prozessrecht;1;1;0;0;Stu12345,Stu9999
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;mwinf21oa-suf;MWINF21oa Strategisch Unternehmensführung;true;true;false;true;Stu5555,Stu3333

Folgendes Beispiel zeigt die Verknüpfung zu Personen, es können auch nur Teilnehmer oder Berechtigte angegeben werden:

    kennung;start;ende;veranstaltung_kennung;teilnehmer
    mjur10oa-eipr-1;01.01.2024 08:00;01.01.2024 18:30;mjur10oa-eipr;
    mjur10oa-eipr-2;02.01.2024 08:00;02.01.2024 18:30;mjur10oa-eipr
    mwinf21oa-suf-1;02.01.2024 08:00;02.01.2024 18:30;mwinf21oa-suf
Die hier angegebenen Personen sind nur Teilnehmer der einzelnen Termine und sind nicht zu allen Terminen der Veranstaltung zugelassen.

Das Anlegen von Personen ist in diesem Schritt nicht möglich. Ein Verweis erfolgt zwingend über die Kennung, bspw. eine Matrikelnummer.
Die Kennung von Terminen ist jeweils eindeutig.

**Fehler** treten auf, wenn:

- die Kennung mehrfach verwendet wird.
- der Start-Zeitpunkt leer ist.
- der Ende-Zeitpunkt leer ist.
- weder Veranstaltungs-Name noch Kennung einer Veranstaltung angegeben wird.
- ein Zeitpunkt im falschen Format ist.

**Warnungen** treten auf, wenn:

- Personen als Teilnehmer oder Berechtigte nicht gefunden werden.
- ein Raum nicht gefunden wird.


## Import von Zuordnungen von Personen zu Veranstaltungen / Terminen

Folgende Spalten werden beim Import von Zuordnungen von Personen zu Veranstaltungen oder Terminen akzeptiert:

- kennung_typ (termin / veranstaltung) *(optional, default: veranstaltung)*
- kennung
- person_kennung
- rolle (teilnehmer / berechtigter) *(optional, default: teilnehmer)*

Dieser Import ermöglicht das Zuweisen von Personen zu Terminen oder Veranstaltungen als Teilnehmer oder Berechtigter. Wird kein Typ für die Kennung angegeben, wird die Person zu einer Veranstaltung zugewiesen. Wird keine Rolle angegeben, wird die Person als Teilnehmer hinzugefügt.

Beispiel für eine minimalistische CSV-Datei:

    kennung;person_kennung
    mwinf21oa-se;Stu1234
    mwinf21oa-se;Stu9999
    mjur10oa-eipr;Stu3323

Durch die Angabe der Rolle können Teilnehmer und Berechtigte hinzugefügt werden:

    kennung;person_kennung;rolle
    mwinf21oa-se;Stu1234;teilnehmer
    mwinf21oa-se;Stu9999;teilnehmer
    mjur10oa-eipr;Stu3323;teilnehmer
    mjur10oa-eipr;M1;berechtigter

Folgendes Beispiel zeigt die Zuordnung sowohl zu Terminen als auch zu Veranstaltungen:

    kennung_typ;kennung;person_kennung;rolle
    veranstaltung;mwinf21oa-se;Stu1234;teilnehmer
    veranstaltung;mwinf21oa-se;Stu9999;teilnehmer
    termin;mjur10oa-eipr-1;Stu3323;teilnehmer
    termin;mjur10oa-eipr-1;M1;berechtigter

Es wird jeweils erwartet, dass Personen und Veranstaltungen sowie Termine bereits existieren.

Es treten keine **Fehler** auf.

**Warnungen** treten auf, wenn:

- Personen als Teilnehmer oder Berechtigte nicht gefunden werden.
- Veranstaltungen oder Termine nicht gefunden werden.


# Reihenfolge des Imports
Der Import wird in folgender Reihenfolge durchgefüht:

 1. Räume
 2. Ausweise
 3. Personen
 4. Veranstaltungen
 5. Termine
 6. Zuordnungen von Personen zu Terminen & Veranstaltungen

Keiner der einzelnen Schritte muss ausgeführt werden. Ein Import mit nur einer Datei ist möglich.
