
# Informationen zur Anwendung zur Erfassung von Anwesenheiten

In diesem Dokument werden technische Informationen zur Konfiguration, Installation sowie dem Betrieb des Backends sowie der App gegeben. Dabei werden teilweise verschiedene Alternativen aufgezeigt, zwischen denen sich jeweils entschieden werden kann. Außerdem wird der Inhalt des Repositories erläutert.

Inhalt dieses Dokuments:
- [Struktur des Repositories](#struktur-des-repositories)
- [Betrieb der Anwendung](#betrieb-der-anwendung)
	- [Backend](#backend)
	- [App auf dem Erfassungsgerät](#app-auf-dem-erfassungsgerät)
- [Datenbank](#datenbank)
	- [Container-Lösung](#verwendung-eines-docker-containers-für-eine-schnelle-einrichtung)
- [Konfiguration der Backend-Anwendung](#konfiguration-der-backend-anwendung)
- [Weiterentwicklung](#weiterentwicklung)
- [Benutzer und Zugriff](#benutzer-und-zugriff)

# Struktur des Repositories

Im Folgenden werden die Struktur und die Inhalte des Repositories beschrieben.
- **anwesenheitserfassung-server**
Enthält den Quellcode der Backend-Anwendung.
- **anwesenheitserfassung-mobile**
Enthält den Quellcode der Android App.
- **API Spezifikation**
Enthält die OpenAPI-Spezifikation der internen und externen Schnittstelle. Die *api-extern.yaml*-Datei kann verwendet werden, um ein Fremdsystem anzubinden.
- **Postman-Collections**
Enthält Collections für die Anwendung Postman, mit der die Endpunkte der internen und externen Schnittstelle angesprochen werden können. Siehe https://www.postman.com/.
- **Import Struktur Beispiele**
In diesem Verzeichnis werden verschiedene Beispiele zur Verfügung gestellt, wie Import-Dateien zum Import von Daten in die Backend-Anwendung aufgebaut sein können.
- **Szenarien**
In diesem Ordner sind Import-Dateien, Postman-Ausführungen und SQL-Skripte für unterschiedliche beispielhafte Szenarien (Hochschule, Kino, Aktionärsversammlung).
- **Anwendungen**
Enthält die Dateien, die zum Betrieb der Anwendung notwendig sind.
	- Backend
		- anwesenheitserfassung-backend.war (Betrieb im Anwendungsserver)
		- anwesenheitserfassung-backend.jar (Betrieb über eingebetteten Anwendungsserver)
		- application.properties (Beispiel-Konfiguration)
	- App
		- anwesenheitserfassung-app.apk
- **docker-compose.yml**
Datei zur Anlage einer MySQL-Datenbank für ein schnelles Setup einer lauffähigen Umgebung (siehe Datenbank).
- **MasterarbeitAnwesenheitserfassung.pdf**
Masterarbeit, auf der diese Anwendung basiert.

# Betrieb der Anwendung

Im Folgenden werden alle wichtigen Informationen zum Betrieb der jeweiligen Anwendungen zusammengetragen.

## Backend

### Voraussetzungen

Dieser Abschnitt beschreibt die technischen Voraussetzungen und Gegebenheiten, die für einen Betrieb des Backends herrschen müssen.

Die Backend-Anwendung ist eine Java-basierte Webanwendung auf der Grundlage von Servlets. Für den Betrieb gibt verschiedene Möglichkeiten, die im folgenden Abschnitt dargestellt werden. In jedem Fall sind für den Betrieb der Anwendungen folgende Voraussetzungen zu erfüllen:

- Java 21. Verfgbar für verschiedene Betriebssysteme bspw. verfügbar unter:
	- https://jdk.java.net/21/
	- https://adoptium.net/de/temurin/releases/?version=21
- Datenbank (weitere Informationen im Abschnitt Datenbank)
- Netzwerkverbindung (Damit App das Backend erreicht)
- Optional: Proxy-Server wie nginx oder Apache

### Möglichkeiten der Ausführung

Für die Ausführung der Anwendung stehen verschiedene Möglichkeiten zur Verfügung:

- Ausführung in einem Anwendungsserver (z.B. Apache Tomcat, Jetty)
- Ausführung in einer Cluster-Umgebung (z.B. Minikube, Kubernetes)
- Ausführung durch den eingebetteten Anwendungsserver (Embedded Tomcat, Jetty)

#### Anwendungsserver

Für die Ausführung in einem Anwendungsserver muss dieser entsprechend installiert werden. Als Beispiel wird hier der Tomcat Anwendungsserver empfohlen (https://tomcat.apache.org/download-10.cgi). Für die Ausführung des Tomcat Anwendungsservers wird ebenfalls eine Java Installation benötigt.

Installation der Anwendung nach der Installation und Start des Anwendungsservers:
- WAR-Datei in den  */webapps*-Ordner ablegen
	- Als ROOT.war wird die Anwendung unter der Root-Adresse Anwendungsservers ausgeliefert (bspw. http://localhost:8080/)
	- Wir die Datei anders genannt, muss der Dateiname in der URL angegeben werden (bswp. app.war kann unter http://localhost:8080/app/  erreicht werden)
- Im Standard ist das so genannte AutoDeployment aktiviert, wodurch eine Anwendung, die in das *webapps*-Verzeichnis abgelegt wird, automatisch gestartet wird.
- Ist das AutoDeployment nicht aktiviert, kann die Anwendung über den Tomcat Manager gestartet werden

Für weitere Informationen zum Betrieb der Anwendung kann die Tomcat-Dokumentation (https://tomcat.apache.org/tomcat-10.0-doc/deployer-howto.html) oder die Baeldung-Dokumentation (https://www.baeldung.com/spring-boot-war-tomcat-deploy) herangezogen werden,

Die Ausführung in einem Anwendungsserver bietet einige Vorteile:
- Session Management durch den Anwendungsserver
- Ausführung als Dienst
- Möglichkeit zur Verwaltung von SSL- Zertifikaten
- Skalierbarkeit
- Bessere Performance
- Professioneller

#### Cluster Umgebung

Für einen Betrieb in der Cluster-Umgebung Kubernetes wird auf folgende Quelle verwiesen: https://spring.io/guides/gs/spring-boot-kubernetes

#### Eingebetteter Anwendungsserver

Spring Boot liefert standardmäßig einen eingebundenen Anwendungsserver mit. Um die Anwendung in diesem auszuführen, muss die bereitgestellte .jar-Datei ausgeführt werden.

    java -jar anwesenheitserfassung-backend.jar

Dies ist der einfachste Weg der Ausführung der Anwendung, eignet sich jedoch nicht für einen produktiven Betrieb.

### Konfiguration

Im Folgenden werden die verschiedenen Möglichkeiten der Konfiguration genannt. Der Inhalt der Konfiguration wird im weiteren Verlauf beschrieben. Für weitere Informationen kann folgende Quelle herangezogen werden: https://docs.spring.io/spring-boot/docs/1.2.0.M1/reference/html/howto-properties-and-configuration.html

Unter dem Abschnitt **Konfiguration** werden Empfehlungen für die einzelnen Betriebs-Möglichkeiten gegeben.

#### Anwendungsserver

Folgende Möglichkeiten existieren zur Konfiguration der Anwendung:
- Bearbeiten der *application.propeties*-Datei unter */src/main/ressources* und anschließende Neu-Erstellung der *.war*-Datei (siehe unten).
- Definition der Java-Umgebungsvariable *spring_config_location* mit dem Verweis auf eine im Dateisystem abgelegte Konfigurations-Datei.
- Setzen von Java-Umgebungsvariablen, die die einzelnen Konfigurationen überschreiben

#### Eingebetteter Anwendungsserver

Folgende Möglichkeiten existieren zur Konfiguration der Anwendung:
- Bearbeiten der *application.propeties*-Datei unter */src/main/ressources* und anschließende Neu-Erstellung der *.jar*-Datei (siehe unten).
- Java Entwicklungsvariablen beim Start der Anwendung setzen

    java -Dproperty=value -jar anwesenheitserfassung-backend.jar


- Definition der Java-Umgebungsvariable *spring_config_location* mit dem Verweis auf eine im Dateisystem abgelegte Konfigurations-Datei.

    java -jar anwesenheitserfassung-backend.jar --spring_config_location=\var\conf

- Ablegen einer Datei *application.properties* im gleichen Verzeichnis der *.jar*-Datei

### Logging

Das Logging kann über die Datei */src/main/ressources/logback.xml* konfiguriert werden. Ohne Anpassung der Datei werden Log-Dateien über folgende Wege erzeugt:

#### Ausführung in IDE und über Java-Befehl

- In derKonsole
- In Datei: /target/anwesenheitserfassung.log

#### Ausführung über Anwendungsserver

- In Datei: /TOMCAT_VERZEICHNIS/logs/anwesenheitserfassung.log

## App auf dem Erfassungsgerät

### Voraussetzungen

Folgende Voraussetzungen sind für die Ausführung  der Android App notwendig:

- Android Smartphone:
	- Min. Android 7 / Nougat, API 25
	- Integriertes RFID / NFC-Lesegerät
	- Netzwerkfähig
- Mögliche Verbindung zur Backend-Anwendung

### Installation

Die Installation der App erfolgt über folgende Schritte:
1. Kopieren der *Anwendungen/anwesenheitserfassung-app.apk*-Datei auf das entsprechende Gerät
2. Öffnen der *.apk*-Datei
3. Akzeptieren der Berechtigungen

### Konfiguration

Die Konfiguration der App erfolgt innerhalb der App im Rahmen des Start-Bildschirms. Hier werden folgende Daten konfiguriert:
1. URL der Backend-Anwendung (Root-URL)
2. Benutzer mit der Berechtigung für die Nutzung der internen API
3. Passwort des Benutzers

# Datenbank

Die Wahl eines Datenbanksystems ist grundsätzlich freigestellt. Von Grund auf unterstützt das System folgende Datenbanksysteme, deren Einsatz getestet wurde:
- MySQL
- MariaDB
- MsSQL

Weitere Datenbanken, die über JDBC angebunden werden können, eignen sich grundsätzlich auch für den Einsatz. Im Einzelfall ist die Konfiguration gesondert zu prüfen.

Folgende JDBC-Treiber werden mit ausgeliefert:
- MySQL
- MariaDB
- MsSQL
- PostgreSQL
- Oracle DB

Folgende Konfiguration für die Datenbankverbindung ist der Standard:

    spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
	spring.datasource.username=anwesenheitserfassung
	spring.datasource.password=anwesenheitserfassung_pw
	spring.datasource.url=jdbc:mysql://localhost:3306/anwesenheitserfassung

Es können einzelne oder alle Konfigurationen überschrieben werden (siehe Konfiguration).

## Verwendung eines Docker-Containers für eine schnelle Einrichtung

Um schnell ein kompatibles Datenbanksystem aufzusetzen, eignet sich der Einsatz eines Docker-Containers. Die bereitgestellte Docker-Konfiguration ist mit der oben genannten Datenbankkonfiguration kompatibel. Die Datenbank sowie der Datenbankbenutzer werden angelegt und die Ports werden eingestellt.

Zur Ausführung wird eine Installation von Docker (z. B. https://www.docker.com/products/docker-desktop/) und Docker Compose (in Docker Desktop enthalten) benötigt.

Mit folgendem Befehl, ausgeführt im Ordner der *docker-compose.yml*, wird der Docker-Container erstellt und gestartet:

    docker-compose up

Nach Start des Docker-Containers kann die Anwendung ohne weitere Konfiguration gestartet werden.

# Konfiguration der Backend-Anwendung

Folgende Konfigurationen sind über eine Konfigurations-Datei anpassbar. Angegeben wird der jeweilige Standard, der ohne weitere Konfiguration herangezogen wird:

**Datenbank-Konfiguration**

    # JDBC-Treiber für die Verbidnung
    spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

    # JDBC-Verbindungs URL zum Server
	spring.datasource.url=jdbc:mysql://localhost:3306/anwesenheitserfassung

    # Benutzername und Passwort des Benutzers
	spring.datasource.username=anwesenheitserfassung
	spring.datasource.password=anwesenheitserfassung_pw

**Liquibase-Konfiguration**

    # Pfad der Haupt-Changelog-Datei, die auf weitere Changelog-Dateien verweist
    spring.liquibase.change-log=classpath:db/changelog/changelog-master.xml

	# Standard-Schema, das für die Datenbankerstellung verwendet werden soll (z.B. für Postgres relevant)
	spring.liquibase.default-schema=anwesenheitserfassung

**Liquibase-Konfiguration**

    # Pfad der Haupt-Changelog-Datei, die auf weitere Changelog-Dateien verweist
    spring.liquibase.change-log=classpath:db/changelog/changelog-master.xml

	# Standard-Schema, das für die Datenbankerstellung verwendet werden soll (z.B. für Postgres relevant)
	spring.liquibase.default-schema=anwesenheitserfassung

**Anwendungs-Konfiguration**

	# Gibt an, ab wie viel Minuten eine Anwesenheit verspätet ist. Negativer Wert deaktiviert die Ermittlung der Verspätung
    anwesenheit.verspaetetabminuten=15

    # Regulärer Ausdruck zur Ermittlung des Datums aus einem CIS-Export (Gruppenname datum muss angegeben werden, start und ende sind optional)
	import.cis.datum.regexp=^([a-zA-Z\\.]*)[ ]*(?<datum>\\d{2}\.\\d{2}\.\\d{4}){1}[ ]*(?<start>\\d{2}:\\d{2})?[- ]*(?<ende>\\d{2}:\\d{2})?$

	# Regulärer Ausdruck zur Ermittlung des Namens und der Kennung einer Veranstaltung aus dem Dateinamen (Mindestens die Gruppe name muss angegeben werden)
	import.cis.veranstaltung.regexp=^Teilnehmerliste(?<name>.*)\\_(?<kennung>.*).csv$

	# Standard-Wert für Start und Ende eines Termins, wenn beim CIS-Import keiner angegeben wird.
	import.cis.datum.default.start=08:00
	import.cis.datum.default.ende=18:00

	# Spaltenname für die Kennung, den Vornamen und den Nachnamen im CIS-Export
	import.cis.spalte.name.kennung=Matrikelnummer
	import.cis.spalte.name.vorname=Vorname
	import.cis.spalte.name.nachname=Name

**Weitere Konfiguration**
Über die angegeben Konfigurationen hinaus stehen alle z. B. Spring-Konfiguration zur Verfügung. Spring dokumentiert eine Liste häufig verwendeter Konfigurationen: https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html

Folgende Beispiele zeigen mögliche sinnvolle Konfigurationen:

    # Port, auf dem die Anwendung aufgerufen werden kann
    server.port=80

	# SSL-Konfiguration
	server.ssl.certificate=PFAD ZUM ZERTIFIKAT
	server.ssl.certificate-private-key=PFAD ZUM PRIVATEN SCHLÜSSEL

	# Logging
	logging.file.name=NAME DER LOG-DATEI
	logging.file.path=ORDNER, IN DEM LOG-DATEIEN ERSTELLT WERDEN SOLLEN
	logging.level.*=LEVEL DES GLOBALEN LOGGINGS (debug, info, warning, error)

	# Context-Pfad der Anwendung (Standard /) unter der sie aufgerufen werden kann
	server.servlet.context-path=PFAD

## Umgebungsbezogene Konfiguration

Für unterschiedliche Umgebungen (Entwicklung, Test, Betrieb) können unterschiedliche Konfigurationen angelegt werden. Je Umgebung wird eine Datei nach dem Muster *application-UMGEBUNG.properties* angelegt. In der *application.properties* können umgebungsübergreifende Konfigurationen vorgenommen werden.

Beim Build der Anwendung muss die Umgebung als Parameter gesetzt werden (oder wie unten angegeben in der *application.properties*-Datei):

    mvn clean package -Dspring.profiles.active=test

In diesem Fall würden die Konfigurationen der Dateien *application.properties* und *application-test.properties* herangezogen werden, wobei die umgebungsabhängige Konfiguration die globale Konfiguration ggf. überschreibt, wenn sie in beiden Dateien gegeben ist.

Für die Entwicklung muss der Parameter der aktuellen Umgebung ebenfalls gesetzt werden. Dies kann ebenfalls über eine Umgebungsvariable vorgenommen werden, oder das aktive Profil wird ebenfalls in der *application.properties* angegeben.

	# Aktives Profil    
    spring.profiles.active=development



## Bereitstellung einer Konfiguration

Je nach Art des Betriebs der Anwendung stehen unterschiedliche Möglichkeiten für die Bereitstellung der Konfiguration zur Verfügung. Folgende Wege werden empfohlen:

### Entwicklung

Im Rahmen der Entwicklung sollte die Konfiguration über die application.properties bzw. eine umgebungsbezogene Konfiguration vorgenommen werden.

### Ausführung einer JAR-Datei

Um die JAR-Datei bei Anpassung der Konfiguration nicht ändern zu müssen, empfiehlt es sich, mindestens die sich ändernden Konfigurationen in eine externe Konfigurationsdatei auszulagern. Diese kann bspw. im gleichen Verzeichnis der JAR-Datei abgelegt werden und überschreibt die Konfigurationen innerhalb der JAR-Datei.

### Ausführung in Anwendungs-Server

Um auch die WAR-Datei nicht erneut erstellen zu müssen, kann auch hier eine externe Konfiguration herangezogen werden. Dafür muss der Pfad der Konfigurations-Datei gesetzt werden. Hierfür muss eine Datei unter */TOMCAT-VERZEICHNIS/conf/Catalina/localhost/* mit dem Namen der WAR-Datei. Heißt die WAR-Datei bspw. *anwesenheitserfassung-backend.war* muss folgende Datei angelegt werden:

**anwesenheitserfassung-backend.xml**   

    <?xml version="1.0" encoding="UTF-8"?>
	<Context>
	    <Environment name="spring.config.additional-location" value="file:PFAD_ZUR_DATEI" type="java.lang.String"/>
	</Context>

Statt *spring.config.additional-location* kann auch *spring.config.location* verwendet werden, dann wird allerdings die Konfiguration innerhalb der WAR-Datei verworfen. Es müssten somit alle Konfigurationen angegeben werden.

Auch hier werden die Konfigurationen innerhalb der WAR-Datei überschrieben.

### Ausführung in Cluster-Umgebung

Die Konfiguration in der Cluster-Umgebung erfolgt individuell nach den Vorgaben der Umgebung.

# Weiterentwicklung

Die Voraussetzungen, um die Anwendung weiterentwickeln zu können, werden im Folgenden beschrieben.

## Backend-Anwendung

Folgende Anwendungen und Abhängigkeiten werden benötigt:

- JDK 21 (https://jdk.java.net/21/)
- Java IDE (IntelliJ, Eclipse, Visual Studio Code)
- Maven (https://maven.apache.org/install.html)
- Datenbank (siehe Datenbank)

Durch Öffnen des Projektes in der IDE als Maven-Projekt werden alle benötigten Abhängigkeiten automatisch heruntergeladen und installiert. Ggf. ist eine Anpassung der Konfiguration (siehe Konfiguration) vorzunehmen. Für die Entwicklung ist dabei die Datei *src/main/ressources/application.properties* relevant.

Die Anwendung kann in der IDE über das Ausführen der Hauptklasse *AnwesenheitserfassungServerApplication.java* gestartet werden.

### Erstellen einer Anwendungsdatei

Für die Backend-Anwendung stehen zwei mögliche Zielformate zur Auswahl: JAR und WAR. Die JAR-Datei kann direkt mit einem Java-Befehl ausgeführt werden, die WAR-Datei wird über einen Anwendungsserver gestartet (siehe Betrieb der Anwendung).

Vor der Erstellung der Datei muss ggf. das Ziel-Format in der *pom.xml*-Datei (im Hauptverzeichnis der Backend-Anwendung) geändert werden:

    <packaging>jar</packaging>

	<packaging>war</packaging>

Anschließend kann durch Maven die entsprechende Datei erstellt werden:

    mvn clean package

Entsprechend der Art der Konfiguration (siehe Betrieb der Anwendung) muss zuvor die *application.properties* angepasst werden.

Die JAR-/WAR-Datei wird in dem Verzeichnis *target* als *anwesenheitserfassung-server-VERSION.jar/war* abgelegt.

## Android-App

Für die Weiterentwicklung der Android-App sind folgende Komponenten notwendig:
- IDE (empfohlen: Android Studio https://developer.android.com/studio)
- Android API-Level / SDK 24 (https://developer.android.com/tools/releases/platforms?hl=de)
- Gradle (https://gradle.org/install/)
- JDK 17 (https://adoptium.net/de/temurin/archive/?version=17)
- Android-Smartphone (Für Scannen von RFID-Ausweisen)

Für die Ausführung auf einem physischen Gerät ist dies entsprechend einzurichten (https://developer.android.com/studio/run/device?hl=de). Für die Ausführung auf einem virtuellen Gerät muss dieses angelegt werden (https://developer.android.com/studio/run/managing-avds?hl=de). Hier ist kein Scan eines RFID-Ausweises möglich.

### Erstellen einer App-Datei

Um die Anwendung auf die einzelnen Geräte ausliefern zu können, muss eine *.apk*-Datei erzeugt werden. Diese kann auf Android Smartphones installiert werden. Um diese Datei zu erzeugen, muss im Build-Menü der Eintrag **APK > Build-APK** ausgewählt werden (https://developer.android.com/build/build-for-release?hl=de). Die Datei wird im Ordner */app/build/outputs/apk* des App-Verzeichnisses abgelegt.

Weitere Möglichkeiten werden hier dokumentiert: https://developer.android.com/build/build-for-release?hl=de

# Benutzer und Zugriff

Mit dem ersten Start der Anwendung werden bereits vier Benutzer angelegt:

- **administrator**
Der Benutzer kann die Anwendung administrieren. Das Passwort ist *admin*.

- **api_intern**
Der Benutzer dient zum Zugriff des Erfassungsgeräts auf die Anwendung. Das Passwort ist *api*.

- **api_extern**
Der Benutzer dient zum Zugriff auf die externe API. Das Passwort ist *api*.

- **verwaltung**
Mit diesem Benutzer können die Stammdaten der Anwendung verwaltet werden. Das Passwort ist *verwaltung*.

Die Anlage weiterer Benutzer ist mit dem *administrator* möglich. Werden Benutzer nicht benötigt, können Sie gelöscht werden. Zur Verwaltung des Erfassungsgeräts muss ein Benutzer mit der Berechtigung angelegt werden. Dieser Benutzer muss einer Person zugewiesen werden, die wiederum einem Ausweis zugewiesen ist. Über den Ausweis erfolgt die Berechtigung bis hin zum Benutzer.

Damit berechtigte Personen ihre Veranstaltungen in der Anwesenheitsliste sehen, muss für diese ein Benutzer angelegt werden, der mit der Person verknüpft ist.
