const regexLocalDateTime = new RegExp('^\\d{2}\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}$');
const regexKennung = new RegExp('^[^ ]*$');
const regexPasswort = new RegExp('^[^ ]*$');
const regexBenutzername = new RegExp('^[^ ]*$');


function validateVeranstaltungForm() {

	$("#validationError").hide();
	$("#validationError").html("");
	
	var veranstaltungKennung = $("#veranstaltungKennung").val();
	
	var errorList = [];
	
	if(veranstaltungName == '') {
		errorList.push("Der Name muss ausgefüllt werden.");
	}
	
	if(!regexKennung.test(veranstaltungKennung)) {
		errorList.push("Die Kennung darf keine Leerzeichen enthalten.");
	}
	
	if(errorList.length > 0) {
		$("#validationError").html(errorList.join("<br>"));
		$("#validationError").show();
		return false;
	} else {
		return true;
	}
		
}

function validateTerminBearbeitenForm() {

	$("#validationErrorTerminBearbeiten").hide();
	$("#validationErrorTerminBearbeiten").html("");
	
	var terminStart = $("#terminBearbeitenStart").val();
	var terminEnde = $("#terminBearbeitenEnde").val();
	var terminKennung = $("#terminBearbeitenKennung").val();
	
	var errorList = [];
	
	
	if(!regexKennung.test(terminKennung)) {
		errorList.push("Die Kennung darf keine Leerzeichen enthalten.");
	}
	if(!regexLocalDateTime.test(terminStart)) {
		errorList.push("Der Start-Zeitpunkt ist nicht valide");
	}
	if(!regexLocalDateTime.test(terminEnde)) {
		errorList.push("Der Ende-Zeitpunkt ist nicht valide");
	}
	
	if(errorList.length > 0) {
		$("#validationErrorTerminBearbeiten").html(errorList.join("<br>"));
		$("#validationErrorTerminBearbeiten").show();
		return false;
	} else {
		return true;
	}
		
}

function validateTerminErstellenForm() {

	$("#validationError").hide();
	$("#validationError").html("");
	
	var terminStart = $("#terminErstellenStart").val();
	var terminEnde = $("#terminErstellenEnde").val();
	var terminKennung = $("#terminErstellenKennung").val();
	
	var errorList = [];
	
	
	if(!regexKennung.test(terminKennung)) {
		errorList.push("Die Kennung darf keine Leerzeichen enthalten.");
	}
	if(!regexLocalDateTime.test(terminStart)) {
		errorList.push("Der Start-Zeitpunkt ist nicht valide");
	}
	if(!regexLocalDateTime.test(terminEnde)) {
		errorList.push("Der Ende-Zeitpunkt ist nicht valide");
	}
	
	if(errorList.length > 0) {
		$("#validationError").html(errorList.join("<br>"));
		$("#validationError").show();
		return false;
	} else {
		return true;
	}
		
}

function validatePersonForm() {

	$("#validationError").hide();
	$("#validationError").html("");
	
	var personVorname = $("#vorname").val();
	var personNachname = $("#nachname").val();
	var personKennung = $("#kennung").val();
	
	var errorList = [];
	
	if(personVorname == '') {
		errorList.push("Der Vorname muss ausgefüllt werden.");
	}
	if(personNachname == '') {
		errorList.push("Der Nachname muss ausgefüllt werden.");
	}
	if(!regexKennung.test(personKennung)) {
		errorList.push("Die Kennung darf keine Leerzeichen enthalten.");
	}
	
	if(errorList.length > 0) {
		$("#validationError").html(errorList.join("<br>"));
		$("#validationError").show();
		return false;
	} else {
		return true;
	}
	
}

function validateRaumForm() {

	$("#validationError").hide();
	$("#validationError").html("");
	
	var raumBezeichnung = $("#bezeichnung").val();
	var raumKennung = $("#kennung").val();
	
	var errorList = [];
	
	if(raumBezeichnung == '') {
		errorList.push("Die Bezeichnung muss ausgefüllt werden.");
	}
	if(!regexKennung.test(raumKennung)) {
		errorList.push("Die Kennung darf keine Leerzeichen enthalten.");
	}
	
	if(errorList.length > 0) {
		$("#validationError").html(errorList.join("<br>"));
		$("#validationError").show();
		return false;
	} else {
		return true;
	}
	
}

function validateAusweisForm() {

	$("#validationError").hide();
	$("#validationError").html("");
	
	var rfidSnr = $("#rfidSnr").val();
	var kennung = $("#kennung").val();
	
	var errorList = [];
	
	if(rfidSnr == '') {
		errorList.push("Die RFID Seriennummer muss ausgefüllt werden.");
	}
	if(kennung == '-') {
		errorList.push("Die Kennung '-' ist aus technischen Gründen unzulässig.");
	}
	if(!regexKennung.test(kennung)) {
		errorList.push("Die Kennung darf keine Leerzeichen enthalten.");
	}
	
	if(errorList.length > 0) {
		$("#validationError").html(errorList.join("<br>"));
		$("#validationError").show();
		return false;
	} else {
		return true;
	}
	
}

function validateBenutzerForm(bearbeiten) {

	$("#validationError").hide();
	$("#validationError").html("");
	
	var benutzername = $("#benutzername").val();
	var passwort = $("#passwort").val();
	
	var errorList = [];
	
	if(benutzername == '') {
		errorList.push("Der Benutzername muss ausgefüllt werden.");
	}
	if(!regexBenutzername.test(benutzername)) {
		errorList.push("Der Benutzername darf keine Leerzeichen enthalten.");
	}
	if(passwort == '' && !bearbeiten) {
		errorList.push("Das Passwort muss ausgefüllt werden.");
	}
	if(!regexPasswort.test(passwort)) {
		errorList.push("Das Passwort darf keine Leerzeichen enthalten.");
	}
	
	if(errorList.length > 0) {
		$("#validationError").html(errorList.join("<br>"));
		$("#validationError").show();
		return false;
	} else {
		return true;
	}
	
}