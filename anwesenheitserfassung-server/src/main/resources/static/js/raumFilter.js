var raeumeFilterUrl = '?bezeichnung={0}&kennung={1}&archiv={2}&seite={3}&eintraegeJeSeite={4}';

function raeumeTabelleFilter(input) {
	var filterSeite = 1;	
	var filterEintraegeJeSeite = $("#raeumeTabelle").data("filter-eintraege-je-seite") ?? 10;
	
	var filterBezeichnung = $("#raeumeTabelle").data("filter-bezeichnung") ?? "";
	var filterKennung = $("#raeumeTabelle").data("filter-kennung") ?? "";
	var filterArchiv = $("#raeumeTabelle").data("filter-archiv") ?? "";
		
	var filterFeld = $(input).data("feld");
	if(filterFeld == "bezeichnung") {
		filterBezeichnung = $(input).val() ?? "";
	}
	if(filterFeld == "kennung") {
		filterKennung = $(input).val() ?? "";
	}
	if(filterFeld == "seite") {
		filterSeite = $(input).data("seite");
	}
	if(filterFeld == "eintraegeJeSeite") {
		filterEintraegeJeSeite = $(input).data("eintraege-je-seite");
	}
	if(filterFeld == "archiv") {
		filterArchiv = $(input).val();
	}
		
	ladeRaeumeTabelle( filterBezeichnung, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite);
}

function ladeRaeumeTabelle(filterBezeichnung, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite) {
	var filterUrl = stringFormat(raeumeFilterUrl, filterBezeichnung, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite);
	$.get(filterUrl, function( data ) {
		$('#raeumeTabelleContainer').html($(data).find('#raeumeTabelleContainer').html());
		$('#raeumeFilterArchivSelect').selectpicker('render');
	});
}
