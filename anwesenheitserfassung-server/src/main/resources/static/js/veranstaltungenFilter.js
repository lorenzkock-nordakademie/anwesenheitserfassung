var veranstaltungenFilterUrl = '?name={0}&kennung={1}&archiv={2}&seite={3}&eintraegeJeSeite={4}';

function veranstaltungenTabelleFilter(input) {

	var filterSeite = 1;	
	var filterEintraegeJeSeite = $("#veranstaltungenTabelle").data("filter-eintraege-je-seite") ?? 10;
	
	var filterName = $("#veranstaltungenTabelle").data("filter-name") ?? "";
	var filterKennung = $("#veranstaltungenTabelle").data("filter-kennung") ?? "";
	var filterArchiv = $("#veranstaltungenTabelle").data("filter-archiv") ?? "";
		
	var filterFeld = $(input).data("feld");
	if(filterFeld == "name") {
		filterName = $(input).val() ?? "";
	}
	if(filterFeld == "kennung") {
		filterKennung = $(input).val() ?? "";
	}
	if(filterFeld == "seite") {
		filterSeite = $(input).data("seite");
	}
	if(filterFeld == "eintraegeJeSeite") {
		filterEintraegeJeSeite = $(input).data("eintraege-je-seite");
	}
	if(filterFeld == "archiv") {
		filterArchiv = $(input).val();
	}
		
	ladeVeranstaltungenTabelle( filterName, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite);
}

function ladeVeranstaltungenTabelle(filterName, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite) {
	var filterUrl = stringFormat(veranstaltungenFilterUrl, filterName, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite);
	$.get(filterUrl, function( data ) {
		$('#veranstaltungenTabelleContainer').html($(data).find('#veranstaltungenTabelleContainer').html());
		$('#veranstaltungenFilterArchivSelect').selectpicker('render');
	});
}
