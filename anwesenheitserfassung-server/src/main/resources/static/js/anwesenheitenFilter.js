var anwesenheitenTermineFilterUrl = 'anwesenheiten/termineZuVeranstaltung/{0}';
var anwesenheitenFilterUrl = '?veranstaltungen={0}&raeume={1}&zeitraum={2}&vergangene={3}&seite={4}&eintraegeJeSeite={5}';

function anwesenheitenAuswahlFilterTermine(input) {
	var filterVeranstaltung = $(input).val();
		
	ladeTermineZuVeranstaltung( filterVeranstaltung);
}

function ladeTermineZuVeranstaltung(filterVeranstaltung) {
	var filterUrl = stringFormat(anwesenheitenTermineFilterUrl, filterVeranstaltung);
	$.get(filterUrl, function( data ) {
		$('#termineZuVeranstaltung').html($(data).find('#termineZuVeranstaltung').html());
		$('#terminZuVeranstaltungSelect').selectpicker('render');
	});
}

function anwesenheitenTabelleFilter(input) {
	var filterSeite = 1;
	var filterEintraegeJeSeite = $("#meineAnwesenheitenTabelle").data("filter-eintraege-je-seite") ?? 10;

	var filterVeranstaltungen = $("#meineAnwesenheitenTabelle").data("filter-veranstaltungen") ?? "";
	var filterRaeume = $("#meineAnwesenheitenTabelle").data("filter-raeume") ?? "";
	var filterZeitraum = $("#meineAnwesenheitenTabelle").data("filter-zeitraum") ?? "";
	var filterVergangene = $("#meineAnwesenheitenTabelle").data("filter-vergangene") ?? "";

	var filterFeld = $(input).data("feld");
	if(filterFeld == "veranstaltungen") {
		filterVeranstaltungen = $(input).val() ?? "";
	}
	if(filterFeld == "raeume") {
		filterRaeume = $(input).val() ?? "";
	}
	if(filterFeld == "zeitraum") {
		filterZeitraum = $(input).val() ?? "";
	}
	if(filterFeld == "seite") {
		filterSeite = $(input).data("seite");
	}
	if(filterFeld == "eintraegeJeSeite") {
		filterEintraegeJeSeite = $(input).data("eintraege-je-seite");
	}
	if(filterFeld == "vergangene") {
		filterVergangene = $(input).val();
	}

	ladeMeineAnwesenheitenTabelle( filterVeranstaltungen, filterRaeume, filterZeitraum, filterVergangene, filterSeite, filterEintraegeJeSeite);
}

function ladeMeineAnwesenheitenTabelle(filterVeranstaltungen, filterRaeume, filterZeitraum, filterVergangene, filterSeite, filterEintraegeJeSeite) {
	var filterUrl = stringFormat(anwesenheitenFilterUrl, filterVeranstaltungen, filterRaeume, filterZeitraum, filterVergangene, filterSeite, filterEintraegeJeSeite);
	$.get(filterUrl, function( data ) {
		$('#meineAnwesenheitenTabelleContainer').html($(data).find('#meineAnwesenheitenTabelleContainer').html());
		$('#veranstaltungFilterSelect').selectpicker('render');
		$('#raumFilterSelect').selectpicker('render');
		$('#veranstaltungenFilterVergangeneSelect').selectpicker('render');
		$( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });
	});
}
