var ausweiseFilterUrl = '?rfidSnr={0}&kennung={1}&personen={2}&aktiv={3}&eigenausweis={4}&seite={5}&eintraegeJeSeite={6}';

function ausweiseTabelleFilter(input) {
	
	var filterSeite = 1;	
	var filterEintraegeJeSeite = $("#ausweiseTabelle").data("filter-eintraege-je-seite") ?? 10;
	
	var filterRfidSnr = $("#ausweiseTabelle").data("filter-rfid-snr") ?? "";
	var filterKennung = $("#ausweiseTabelle").data("filter-kennung") ?? "";
	var filterPersonen = $("#ausweiseTabelle").data("filter-personen") ?? "";
	var filterAktiv = $("#ausweiseTabelle").data("filter-aktiv") ?? "";
	var filterEigenausweis = $("#ausweiseTabelle").data("filter-eigenausweis") ?? "";
		
	var filterFeld = $(input).data("feld");
	if(filterFeld == "rfidSnr") {
		filterRfidSnr = $(input).val() ?? "";
	}
	if(filterFeld == "kennung") {
		filterKennung = $(input).val() ?? "";
	}
	if(filterFeld == "person") {
		filterPersonen = $(input).val() ?? "";
	}
	if(filterFeld == "aktiv") {
		filterAktiv = $(input).val();
	}
	if(filterFeld == "eigenausweis") {
		filterEigenausweis = $(input).val();
	}
	if(filterFeld == "seite") {
		filterSeite = $(input).data("seite");
	}
	if(filterFeld == "eintraegeJeSeite") {
		filterEintraegeJeSeite = $(input).data("eintraege-je-seite");
	}
		
	ladeAusweiseTabelle(filterRfidSnr, filterKennung, filterPersonen, filterAktiv, filterEigenausweis, filterSeite, filterEintraegeJeSeite);
}

function ladeAusweiseTabelle(filterRfidSnr, filterKennung, filterPersonen, filterAktiv, filterEigenausweis, filterSeite, filterEintraegeJeSeite) {
	var filterUrl = stringFormat(ausweiseFilterUrl, filterRfidSnr, filterKennung, filterPersonen, filterAktiv, filterEigenausweis, filterSeite, filterEintraegeJeSeite);
	$.get(filterUrl, function( data ) {
		$('#ausweiseTabelleContainer').html($(data).find('#ausweiseTabelleContainer').html());
		$("#personenFilterSelect").selectpicker("render");
		$("#ausweisFilterAktivSelect").selectpicker("render");
		$("#ausweisFilterEigenausweisSelect").selectpicker("render");
	});
}
