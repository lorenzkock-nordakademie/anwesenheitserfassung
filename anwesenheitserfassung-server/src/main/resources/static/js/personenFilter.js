var personenFilterUrl = '?vorname={0}&nachname={1}&kennung={2}&archiv={3}&seite={4}&eintraegeJeSeite={5}';

function personenTabelleFilter(input) {
	var filterSeite = 1;	
	var filterEintraegeJeSeite = $("#personenTabelle").data("filter-eintraege-je-seite") ?? 10;
	
	var filterVorname = $("#personenTabelle").data("filter-vorname") ?? "";
	var filterNachname = $("#personenTabelle").data("filter-nachname") ?? "";
	var filterKennung = $("#personenTabelle").data("filter-kennung") ?? "";
	var filterArchiv = $("#personenTabelle").data("filter-archiv") ?? "";
		
	var filterFeld = $(input).data("feld");
	if(filterFeld == "vorname") {
		filterVorname = $(input).val() ?? "";
	}
	if(filterFeld == "nachname") {
		filterNachname = $(input).val() ?? "";
	}
	if(filterFeld == "kennung") {
		filterKennung = $(input).val() ?? "";
	}
	if(filterFeld == "seite") {
		filterSeite = $(input).data("seite");
	}
	if(filterFeld == "eintraegeJeSeite") {
		filterEintraegeJeSeite = $(input).data("eintraege-je-seite");
	}
	if(filterFeld == "archiv") {
		filterArchiv = $(input).val();
	}
		
	ladePersonenTabelle( filterVorname, filterNachname, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite);
}

function ladePersonenTabelle(filterVorname, filterNachname, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite) {
	var filterUrl = stringFormat(personenFilterUrl, filterVorname, filterNachname, filterKennung, filterArchiv, filterSeite, filterEintraegeJeSeite);
	$.get(filterUrl, function( data ) {
		$('#personenTabelleContainer').html($(data).find('#personenTabelleContainer').html());
		$('#personenFilterArchivSelect').selectpicker('render');
	});
}
