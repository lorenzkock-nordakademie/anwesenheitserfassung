var erfassenUrl = 'erfassen';
var ausweisZuweisenUrl = 'ausweisZuweisen';
var anwesenheitenZuTerminAktualisierenUrl = 'nachVeranstaltung?termin={0}&tabelle=true';
var erfassungStartenUrl = 'erfassungStart';


// Erfasse eine Person
function erfasse(ref, online) {

	var terminId = $(ref).data("termin-id");
	var personId = $(ref).data("person-id");

	var raumId = $("#raumSelect-"+terminId).val();

	var postData = { terminId: terminId, personId: personId, raumId: raumId == null || raumId === '' ? null : raumId, online: online};

	$.post(erfassenUrl, postData, function( data ) {

		var meldungenContainer = $(".meldungenTermin[data-termin-id='"+terminId+"']");
		meldungenContainer.append(($(data).find('#meldungen').html()));
		meldungenContainer.children().delay(3000).fadeOut(800);

		$(".terminTable[data-termin-id='"+terminId+"']").html($(data).find('.terminTable').first().html());
		$(".erfassungenTable[data-termin-id='"+terminId+"']").html($(data).find('.erfassungenTable').first().html());
	});

}
// Erfasse eine Person
function aendereErfassungStart(ref) {

	var terminId = $(ref).data("termin-id");
	var zeitpunktStart = $(".erfassungStart[data-termin-id='"+terminId+"']").val();

	var postData = { terminId: terminId, zeitpunkt: zeitpunktStart};

	$.post(erfassungStartenUrl, postData, function( data ) {
		var meldungenContainer = $(".meldungenTermin[data-termin-id='"+terminId+"']");
		meldungenContainer.append(($(data).find('#meldungen').html()));
		meldungenContainer.children().delay(3000).fadeOut(800);
		var start = $(data).find('#startZeitpunkt').html();
		$(".erfassungStart[data-termin-id='"+terminId+"']").val(start);
		if(start === "") {
			$(".erfassungButtonStartenContainer[data-termin-id='"+terminId+"']").removeClass("d-none");
		} else {
			$(".erfassungButtonStartenContainer[data-termin-id='"+terminId+"']").addClass("d-none");
		}
	});

}
// Erfasse eine Person
function starteErfassung(ref) {

	var terminId = $(ref).data("termin-id");
	var postData = { terminId: terminId };

	$.post(erfassungStartenUrl, postData, function( data ) {
		var meldungenContainer = $(".meldungenTermin[data-termin-id='"+terminId+"']");
		meldungenContainer.append(($(data).find('#meldungen').html()));
		meldungenContainer.children().delay(3000).fadeOut(800);
		$(".erfassungStart[data-termin-id='"+terminId+"']").val($(data).find('#startZeitpunkt').html());
		$(".erfassungButtonStartenContainer[data-termin-id='"+terminId+"']").addClass("d-none");
	});

}

function zuordnen(ref) {

	var terminId = $(ref).data("termin-id");
	var rfidSnr = $(ref).data("rfid-snr");	
	var zeitpunkt = $(ref).data("zeitpunkt");
	
	$("#modal-zuweisen-"+terminId+" input[name=rfidSnr]").val(rfidSnr);
	$("#modal-zuweisen-"+terminId+" input[name=zeitpunkt]").val(zeitpunkt);
	$("#modal-zuweisen-"+terminId+" input[name=fremdausweis]").prop("checked", true);
	$("#modal-zuweisen-"+terminId+" select[name=personId]").val("");
	
	$("#modal-zuweisen-"+terminId+" .selectpicker").selectpicker("val", "");
	
	$("#modal-zuweisen-"+terminId).modal('show');

}

function zuweisenSubmit(ref) {


	var terminId = $(ref).data("termin-id");
	
	$("#modal-zuweisen-"+terminId+" .meldungen-zuweisung-container").addClass("d-none");
	$("#modal-zuweisen-"+terminId+" .meldungen-zuweisung").html("");
	
	var rfidSnr = $("#modal-zuweisen-"+terminId+" input[name=rfidSnr]").val();
	var fremdausweis = $("#modal-zuweisen-"+terminId+" input[name=fremdausweis]").is(":checked");
	var personId = $("#modal-zuweisen-"+terminId+" select[name=personId]").val();
	
	if(personId == null || personId === "") {
		$("#modal-zuweisen-"+terminId+" .meldungen-zuweisung-container").removeClass("d-none");
		$("#modal-zuweisen-"+terminId+" .meldungen-zuweisung").html("Bitte Person auswählen!");
	} else {
		var postData = { rfidSnr: rfidSnr, fremdausweis: fremdausweis, personId: personId, terminId: terminId};
		$.post(ausweisZuweisenUrl, postData, function( data ) {
		
			var meldungenContainer = $(".meldungenTermin[data-termin-id='"+terminId+"']");		
			meldungenContainer.append(($(data).find('#meldungen').html()));
			meldungenContainer.children().delay(3000).fadeOut(800);    
		
			$(".terminTable[data-termin-id='"+terminId+"']").html(($(data).find('.terminTable').first().html()));
			$(".erfassungenTable[data-termin-id='"+terminId+"']").html(($(data).find('.erfassungenTable').first().html()));
			$(".nichtZugeordneteAusweiseTable[data-termin-id='"+terminId+"']").html(($(data).find('.nichtZugeordneteAusweiseTable').first().html()));
			$(".selectpicker").selectpicker("render");
			$("#modal-zuweisen-"+terminId).modal('hide');
		});
	}

}

// Aktualisieren der Teilnehmer und nicht zugeordneter Ausweise zu einem Termin
function anwesenheitenAktualisierenZuAktuellemTermin() {
	var terminId = $(".terminData:visible").data("termin-id");	
	
	
	var aktualisierenUrl = stringFormat(anwesenheitenZuTerminAktualisierenUrl, terminId);
	$.get(aktualisierenUrl, function( data ) {
		$(".terminTable[data-termin-id='"+terminId+"']").html(($(data).find('.terminTable').first().html()));
		$(".erfassungenTable[data-termin-id='"+terminId+"']").html(($(data).find('.erfassungenTable').first().html()));
		$(".nichtZugeordneteAusweiseTable[data-termin-id='"+terminId+"']").html(($(data).find('.nichtZugeordneteAusweiseTable').first().html()));
		$(".selectpicker").selectpicker("render");
	});
	
	setTimeout(function() {
		anwesenheitenAktualisierenZuAktuellemTermin();
	}, 5000);
}


// Starten der Aktualisierung eines Termins
$(document).ready(function() {
	if(document.getElementsByClassName("terminTable").length > 0) {
		setTimeout(function() {
			anwesenheitenAktualisierenZuAktuellemTermin();
		}, 5000);
	}
});
