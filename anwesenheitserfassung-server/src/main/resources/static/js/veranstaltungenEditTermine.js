function editTermin(element) {
	var terminId = $(element).data("termin-id");
	
	var url = 'termin/'+terminId;
	
	const myModalEl = new bootstrap.Modal('#editTerminModal', {
	  keyboard: true
	});
	$.get(url, function(data) {
		$("#editTerminModalContent").html(data);
		$("#terminBearbeitenRaumSelect").selectpicker("render");
		myModalEl.show();
	});
	
	
	
}