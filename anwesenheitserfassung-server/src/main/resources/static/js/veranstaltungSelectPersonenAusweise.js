function fuegePersonHinzu(selectId, tbodyId, formDataId, keinePersonenBodyId) {
	
	var selectedIndex = $('#'+selectId).prop("selectedIndex");
	var element = $('#'+selectId+' option:nth-child('+(selectedIndex+1)+')')[0];
	
	var id = $(element).data("id");
	var vorname = $(element).data("vorname");
	var nachname = $(element).data("nachname");
	var kennung = $(element).data("kennung");
	
	$('#'+tbodyId).append('<tr data-id="'+id+'" class="added-row" data-added="true" data-id="'+id+'"><td>' + kennung + '</td><td>' + nachname + '</td><td>' + vorname + '</td><td><a href="#" onclick="entfernePerson(this, \''+selectId+'\', \''+tbodyId+'\', \''+formDataId+'\', \''+keinePersonenBodyId+'\'); return false;" data-id="' + id + '" data-kennung="' + kennung + '" data-vorname="' + vorname + '" data-nachname="' + nachname + '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/><path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/></svg></a></td></tr>');
	var childsLength = $('#'+tbodyId).children().length;
	if(childsLength > 0) {
		$('#'+keinePersonenBodyId).hide();
		$('#'+tbodyId).show();
	}
	
	
	$('#'+selectId).selectpicker('val', '');
	$('#'+selectId+' option[value=\''+id+'\']' ).remove();
	$('#'+selectId).selectpicker('refresh');
	$('#'+formDataId).append('<input type="hidden" name="addPersonen[]" value="'+id+'" />');

}

function entfernePerson(aElement, selectId, tbodyId, formDataId, keinePersonenBodyId) {

	var id = $(aElement).data("id");
	var kennung = $(aElement).data("kennung");
	var vorname = $(aElement).data("vorname");
	var nachname = $(aElement).data("nachname");
	
	var row = $('#'+tbodyId+' tr[data-id=\''+id+'\']');
	var added = row.data("added");
	if(added) {
		row.remove();		
		$('#'+formDataId+' input[value=\''+id+'\']').remove();
		
		// Ergänze das Select Feld
		$('#'+selectId).append(' <option value="'+id+'" data-vorname="'+vorname+'" data-nachname="'+nachname+'" data-id="'+id+'" data-kennung="'+kennung+'" data-subtext="'+kennung+'">'+vorname+' '+nachname+'</option>');
		$('#'+selectId).selectpicker('refresh');
		
		
	} else {
		if(row.hasClass('removed-row')) {
			row.removeClass('removed-row');
			$('#'+formDataId+' input[value='+id+']').remove();
		} else {
			row.addClass('removed-row');
			$('#'+formDataId).append('<input type="hidden" name="removePersonen[]" value="'+id+'" />');
		}	
	}
	var childsLength = $('#'+tbodyId).children().length;
	if(childsLength == 0) {
		$('#'+keinePersonenBodyId).show();
		$('#'+tbodyId).hide();
	}
	
}

function fuegeAusweisHinzu(selectId, tbodyId, formDataId, keineAusweiseBodyId) {
	
	var selectedIndex = $('#'+selectId).prop("selectedIndex");
	var element = $('#'+selectId+' option:nth-child('+(selectedIndex+1)+')')[0];
	
	var snr = $(element).data("snr");
	var kennung = $(element).data("kennung");
	
	$('#'+tbodyId).append('<tr data-snr="'+snr+'" class="added-row" data-added="true" data-kennung="'+kennung+'"><td>' + snr + '</td><td>' + kennung + '</td><td><a href="#" onclick="entferneAusweis(this, \''+selectId+'\', \''+tbodyId+'\', \''+formDataId+'\', \''+keineAusweiseBodyId+'\'); return false;" data-snr="' + snr + '" data-kennung="' + kennung + '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/><path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/></svg></a></td></tr>');
	var childsLength = $('#'+tbodyId).children().length;
	if(childsLength > 0) {
		$('#'+keineAusweiseBodyId).hide();
		$('#'+tbodyId).show();
	}
	
	
	$('#'+selectId).selectpicker('val', '');
	$('#'+selectId+' option[value=\''+snr+'\']' ).remove();
	$('#'+selectId).selectpicker('refresh');
	$('#'+formDataId).append('<input type="hidden" name="addAusweise[]" value="'+snr+'" />');

}

function entferneAusweis(aElement, selectId, tbodyId, formDataId, keineAusweiseBodyId) {

	var snr = $(aElement).data("snr");
	var kennung = $(aElement).data("kennung");
	
	var row = $('#'+tbodyId+' tr[data-snr=\''+snr+'\']');
	var added = row.data("added");
	if(added) {
		row.remove();		
		$('#'+formDataId+' input[value=\''+snr+'\']').remove();
				
		// Ergänze das Select Feld
		$('#'+selectId).append(' <option value="'+snr+'" data-snr="'+snr+'" data-kennung="'+kennung+'" data-subtext="'+kennung+'">'+snr+'</option>');
		$('#'+selectId).selectpicker('refresh');
		
	} else {
		if(row.hasClass('removed-row')) {
			row.removeClass('removed-row');
			$('#'+formDataId+' input[value=\''+snr+'\']').remove();
		} else {
			row.addClass('removed-row');
			$('#'+formDataId).append('<input type="hidden" name="removeAusweise[]" value="'+snr+'" />');
		}	
	}
	var childsLength = $('#'+tbodyId).children().length;
	if(childsLength == 0) {
		$('#'+keineAusweiseBodyId).show();
		$('#'+tbodyId).hide();
	}
	
}