function stringFormat(str, ...args) {
	return str.replace(/{(\d+)}/g, (match, index) => args[index]);
}

$(".nav-toggle").click(function() {
	var containerId = $(this).data("container-id");
	var selectedTabId = $(this).data("tab-id");
	$("#"+containerId+" .tab").addClass("d-none");
	$("#"+containerId+" #"+selectedTabId).removeClass("d-none");
	
	$(this).parent().find(".nav-toggle").removeClass("active");
	$(this).addClass("active");
		
});

// Kopieren von Modals in ein zentrales Modal, welches nicht aktualisiert wird,
// damit das Modal, aus dem der Inhalt kopiert wird, nicht verworfen wird.
$(document).on('click', '.modal-fill-btn', function() {
	var targetModalId = $(this).data('target-modal-id');
	var sourceModalId = $(this).data('source-modal-id');
	$("#"+targetModalId).html($("#"+sourceModalId).html());
	$("#"+targetModalId).modal('show');
});

$( function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });
});