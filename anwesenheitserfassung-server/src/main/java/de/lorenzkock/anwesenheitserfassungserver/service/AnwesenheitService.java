package de.lorenzkock.anwesenheitserfassungserver.service;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatus;
import lombok.NonNull;

@Component
public class AnwesenheitService {

	// Values
	@Value("${anwesenheit.verspaetetabminuten:15}")
	private int verspaetetAbMinutenGlobal;

	public AnwesenheitStatus statusZuAnwesenheit(@NonNull Optional<OffsetDateTime> ersteErfassungZeitpunkt,
			@NonNull OffsetDateTime start, Optional<Integer> verspaetetAbMinuten, Boolean onlineTeilnahme) {

		int verapetetAb = verspaetetAbMinuten.orElse(verspaetetAbMinutenGlobal);

		if (ersteErfassungZeitpunkt.isEmpty())
			return AnwesenheitStatus.KEINE_ERFASSUNG;

		if (verapetetAb < 0) {
			return onlineTeilnahme ? AnwesenheitStatus.ONLINE : AnwesenheitStatus.ANWESEND;
		}

		long differenzInMinuten = start.until(ersteErfassungZeitpunkt.get(), ChronoUnit.MINUTES);
		return differenzInMinuten > verapetetAb
				? (onlineTeilnahme ? AnwesenheitStatus.ONLINE_VERSPAETET : AnwesenheitStatus.VERSPAETET)
				: (onlineTeilnahme ? AnwesenheitStatus.ONLINE : AnwesenheitStatus.ANWESEND);
	}

}
