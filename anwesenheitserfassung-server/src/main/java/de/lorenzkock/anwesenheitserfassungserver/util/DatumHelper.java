package de.lorenzkock.anwesenheitserfassungserver.util;

import liquibase.exception.DateParseException;
import org.apache.commons.lang3.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Hilfsmethoden für die Verarbeitung von Daten
 * @author Lorenz Kock
 */
public class DatumHelper {

	/**
	 * Formatiert ein Datum zu einem String im Format dd.MM.yyyy HH:mm
	 * @param date Datum, das zu formatieren ist
	 * @return Formatiertes Datum
	 */
	public static String formatDateTime(OffsetDateTime date) {
		ZonedDateTime zoned = date.toZonedDateTime().withZoneSameInstant(ZoneId.systemDefault());
		return zoned.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
	}

	/**
	 * Ermittelt aus zwei gegebenen Daten das spätere
	 * @param a Erstes Datum
	 * @param b Zweites Datum
	 * @return Späteres Datum
	 */
	public static OffsetDateTime max(OffsetDateTime a, OffsetDateTime b) {
		return a.isBefore(b) ? b : a;
	}

	/**
	 * Ermittelt ein {@link LocalDate} aus einem String
	 * @param date Datum als String
	 * @return Datum als {@link LocalDate}
	 */
	public static LocalDate localDateFromString(String date) {
		return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
	}

	/**
	 * Prüft, ob ein gegebenes Datum zwischen zwei Daten (inklusive) liegt.
	 * @param datum Datum, das geprüft wird
	 * @param start Start-Datum
	 * @param ende Ende-Datum
	 * @return Ergebnis der Prüfung
	 */
	public static boolean istDatumInZeitspanne(LocalDate datum, LocalDate start, LocalDate ende) {
		return !start.isAfter(datum) && !datum.isAfter(ende);
	}

	public static OffsetDateTime toDateTime(String dateTimeString) throws DateTimeParseException {
		if (!StringUtils.isBlank(dateTimeString)) {

			ZonedDateTime t = LocalDateTime.parse(dateTimeString, DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"))
					.atZone(ZoneId.systemDefault());
			return t.toOffsetDateTime();
		}
		return null;
	}

	public static String toDateTime(OffsetDateTime dateTime) throws DateTimeParseException {
		if (dateTime != null) {
			return DatumHelper.formatDateTime(dateTime);
		}
		return null;
	}

	public static String toExportDate(OffsetDateTime dateTime) {
		ZonedDateTime zoned = dateTime.toZonedDateTime().withZoneSameInstant(ZoneId.systemDefault());
		return zoned.format(DateTimeFormatter.ofPattern("EE dd.MM.yyyy"));
	}

}
