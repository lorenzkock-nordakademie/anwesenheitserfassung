package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Stream;

/**
 * Service zum Importieren von Veranstaltungen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportVeranstaltungenService {

    // Service
    private final VeranstaltungService veranstaltungService;

    // Repository
    private final VeranstaltungRepository veranstaltungRepository;
    private final PersonRepository personRepository;

    /**
     * Methode zum Erzeugen einer Veranstaltung
     * @param personRepository Repository zur Abfrage von Personen
     * @param veranstaltungService Service für die Überprüfung der Einmaligkeit der Kennung einer Veranstaltung
     * @param name Name der Veranstaltung
     * @param veranstaltungkennung Kennung der Veranstaltung
     * @param teilnehmende IDs von teilnehmende Personen
     * @param berechtigte IDs von berechtigten Personen
     * @param erlaubeEinAusgehend Information, ob zwischen ein- und ausgehenden Personen unterschieden werden soll.
     * @param erlaubeMehrfach Information, ob Personen mehrfach erfasst werden dürfen.
     * @param erlaubeFremdausweise Information, ob fremde Ausweise erlaubt sind
     * @param fremdausweiseBestaetigen Information, ob die Zuweisung von fremden Ausweisen bestätigt werden muss.
     * @param warnmeldungenZeile Warnmeldungen, die ergänzt werden können.
     * @return Erstellte Veranstaltung
     */
    public static Veranstaltung legeVeranstaltungAn(@NonNull PersonRepository personRepository, @NonNull VeranstaltungService veranstaltungService,
                                                    @NonNull String name, @NonNull Optional<String> veranstaltungkennung,
                                                    @NonNull Optional<String> teilnehmende, Optional<String> berechtigte,
                                                    boolean erlaubeEinAusgehend, boolean erlaubeMehrfach,
                                                    boolean erlaubeFremdausweise, boolean fremdausweiseBestaetigen,
                                                    @NonNull List<String> warnmeldungenZeile) {

        // Ermittle Teilnehmer und Berechtigte
        Set<Person> teilnehmendePersonen = new HashSet<>();
        if (teilnehmende.isPresent()) {
            List<String> teilnehmerKennungen = Stream.of(teilnehmende.get().split(",")).filter(k -> !k.isBlank()).map(String::trim).toList();
            for (String teilnehmerKennung : teilnehmerKennungen) {
                Optional<Person> personZuKennung = personRepository.findByKennung(teilnehmerKennung);
                if (personZuKennung.isPresent()) {
                    teilnehmendePersonen.add(personZuKennung.get());
                } else {
                    warnmeldungenZeile.add("Die teilnehmende Person mit der Kennung '%s' zu der Veranstaltung '%s' wurde nicht gefunden.".formatted(teilnehmerKennung, name));
                }

            }
        }
        Set<Person> berechtigtePersonen = new HashSet<>();
        if (berechtigte.isPresent()) {
            List<String> berechtigteKennungen = Stream.of(berechtigte.get().split(",")).filter(k -> !k.isBlank()).map(String::trim).toList();
            for (String berechtigteKennung : berechtigteKennungen) {
                Optional<Person> personZuKennung = personRepository.findByKennung(berechtigteKennung);
                if (personZuKennung.isPresent()) {
                    berechtigtePersonen.add(personZuKennung.get());
                } else {
                    warnmeldungenZeile.add("Die berechtigte Person mit der Kennung '%s' zu der Veranstaltung '%s' wurde nicht gefunden.".formatted(berechtigteKennung, name));
                }
            }
        }

        // Erzeuge das Objekt der Veranstaltung
        Veranstaltung veranstaltung = Veranstaltung.builder()
                .name(name)
                .erlaubeEinUndAusgehendeBuchungen(erlaubeEinAusgehend)
                .erlaubeMehrfachBuchungen(erlaubeMehrfach)
                .erlaubeFremdausweis(erlaubeFremdausweise)
                .fremdausweisBestaetigen(fremdausweiseBestaetigen)
                .kennung(veranstaltungkennung.orElse(null))
                .teilnehmendePersonen(teilnehmendePersonen.stream().toList())
                .berechtigtePersonen(berechtigtePersonen.stream().toList())
                .build();

        // Ergänze ggf. eine generierte Nummer
        BaseService.ergaenzeGenerierteKennung(veranstaltung);
        return veranstaltung;
    }

    /**
     * Importiere eine Datei mit Personen
     *
     * @param file Datei mit dem Inhalt
     * @param erstellteVeranstaltungenNachKennung Liste, um erstellte Veranstaltungen anderen Import-Schritten zur Verfügung zu stellen
     * @param erstellteVeranstaltungenNachName Liste, um erstellte Veranstaltungen anderen Import-Schritten zur Verfügung zu stellen
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException       Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importiereVeranstaltungen(@NonNull MultipartFile file, Map<String, Veranstaltung> erstellteVeranstaltungenNachKennung, Map<String, Veranstaltung> erstellteVeranstaltungenNachName) throws IllegalArgumentException, NoContentException {

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        // Validiere, ob es eine Kopfzeile mit den entsprechenden Spalten gibt
        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, true, Optional.of(List.of("name")));

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.getFirst()).toList();
        int positionName = kopfinformationen.indexOf("name");
        int positionKennung = kopfinformationen.indexOf("kennung");
        int positionErlaubeEinAusgehend = kopfinformationen.indexOf("erlaube_eingehendausgehend");
        int positionErlaubeMehrfachbuchungen = kopfinformationen.indexOf("erlaube_mehrfachbuchungen");
        int positionErlaubeFremdausweise = kopfinformationen.indexOf("erlaube_fremdausweise");
        int positionFremdausweiseBestaetigen = kopfinformationen.indexOf("fremdausweise_bestaetigen");
        int positionTeilnehmer = kopfinformationen.indexOf("teilnehmer");
        int positionBerechtigte = kopfinformationen.indexOf("berechtigte");

        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;

        // Iteriere über die einzelnen Zeilen
        for (List<String> felder : dateiInhalt.stream().skip(1).map(z -> Arrays.stream(z).toList()).toList()) {

            List<String> fehlermeldungenZeile = new ArrayList<>();
            List<String> warnmeldungenZeile = new ArrayList<>();

            // Ziehe die Informationen aus den Feldern
            String name = felder.get(positionName).trim();
            Optional<String> kennung = positionKennung >= 0 ? Optional.of(felder.get(positionKennung).trim()) : Optional.empty();
            boolean erlaubeEinAusgehende = positionErlaubeEinAusgehend >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionErlaubeEinAusgehend).trim()).orElse("false"));
            boolean erlaubeMehrfach = positionErlaubeMehrfachbuchungen >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionErlaubeMehrfachbuchungen).trim()).orElse("false"));
            boolean erlaubeFremdausweise = positionErlaubeFremdausweise >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionErlaubeFremdausweise).trim()).orElse("false"));
            boolean fremdausweiseBestaetigen = positionFremdausweiseBestaetigen >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionFremdausweiseBestaetigen).trim()).orElse("false"));
            Optional<String> teilnehmer = positionTeilnehmer >= 0 ? Optional.of(felder.get(positionTeilnehmer).trim()) : Optional.empty();
            Optional<String> berechtigte = positionBerechtigte >= 0 ? Optional.of(felder.get(positionBerechtigte).trim()) : Optional.empty();

            // Validierung der Felder
            if (StringUtils.isBlank(name)) {
                fehlermeldungenZeile.add("Der Name einer Veranstaltung ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }

            Optional<Veranstaltung> existierendeVeranstaltungZuKennung = Optional.empty();
            // Validierung der Kennung
            if (fehlermeldungenZeile.isEmpty() && !StringUtils.isBlank(kennung.orElse(null))) {
                if (kennung.get().contains(" ")) {
                    fehlermeldungenZeile.add("In der Kennung '%s' sind keine Leerzeichen erlaubt.".formatted(kennung.get()));
                } else {
                    try {
                        existierendeVeranstaltungZuKennung = veranstaltungService.pruefeKennungEinmalig(veranstaltungRepository, kennung.get(), Optional.empty());
                    } catch (IllegalArgumentException e) {
                        fehlermeldungenZeile.add("Die Kennung '%s' für die Veranstaltung '%s' ist bereits vergeben. Die Veranstaltung wurde nicht hinzugefügt.".formatted(kennung.get(), name));
                    }
                }
            }

            // Anlegen der Veranstaltung, wenn die Veranstaltung noch nicht existiert
            if (fehlermeldungenZeile.isEmpty() && existierendeVeranstaltungZuKennung.isEmpty()) {

                Veranstaltung veranstaltung = legeVeranstaltungAn(personRepository, veranstaltungService, name, kennung, teilnehmer, berechtigte,
                        erlaubeEinAusgehende, erlaubeMehrfach, erlaubeFremdausweise, fremdausweiseBestaetigen, warnmeldungenZeile);
                try {
                    // Speichern
                    veranstaltung = veranstaltungRepository.save(veranstaltung);

                    // Hinzufügen, dass die anderen Importschritte auch die Daten zur Verfügung haben
                    erstellteVeranstaltungenNachKennung.putIfAbsent(veranstaltung.getKennung(), veranstaltung);
                    erstellteVeranstaltungenNachName.putIfAbsent(veranstaltung.getName(), veranstaltung);
                    erfolgreicheImporte++;

                } catch (Exception exception) {
                    fehlermeldungenZeile.add("Beim Importieren der Veranstaltung '%s' ist ein Fehler aufgetreten.".formatted(name));
                }

            }

            fehlermeldungen.addAll(fehlermeldungenZeile);
            warnmeldungen.addAll(warnmeldungenZeile);
        }

        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.VERANSTALTUNG)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1)
                .build();

    }

}
