package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Service zum Importieren von Zuweisungen von Personen zu Veranstaltungen oder Terminen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportPersonenZuweisungService {

    // Repository
    private final VeranstaltungRepository veranstaltungRepository;
    private final TerminRepository terminRepository;
    private final PersonRepository personRepository;

    /**
     * Importiere Zuweisungen von Personen zu Terminen oder Veranstaltungen
     *
     * @param file                                Datei mit dem Inhalt
     * @param erstelltePersonenNachKennung        Liste bereits erstellter Personen
     * @param erstellteTermineNachKennung         Liste bereits erstellter Termine
     * @param erstellteVeranstaltungenNachKennung Liste bereits erstellter Veranstaltungen
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException       Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importierePersonenZuweisung(@NonNull MultipartFile file,
                                                      @NonNull Map<String, Person> erstelltePersonenNachKennung,
                                                      Map<String, Termin> erstellteTermineNachKennung,
                                                      Map<String, Veranstaltung> erstellteVeranstaltungenNachKennung) throws IllegalArgumentException, NoContentException {

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        // Validiere, ob es eine Kopfzeile mit den entsprechenden Spalten gibt
        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, true, Optional.of(List.of("kennung", "person_kennung")));

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.getFirst()).toList();
        int positionKennungTyp = kopfinformationen.indexOf("kennung_typ");
        int positionKennung = kopfinformationen.indexOf("kennung");
        int positionPersonKennung = kopfinformationen.indexOf("person_kennung");
        int positionRolle = kopfinformationen.indexOf("rolle");

        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;

        // Validiere das Dateiformat

        for (List<String> felder : dateiInhalt.stream().skip(1).map(z -> Arrays.stream(z).toList()).toList()) {

            List<String> fehlermeldungenZeile = new ArrayList<>();
            List<String> warnmeldungenZeile = new ArrayList<>();

            // Ziehe die Informationen aus den Feldern
            String kennung = felder.get(positionKennung).trim();
            String personKennung = felder.get(positionPersonKennung).trim();
            String kennungTyp = positionKennungTyp >= 0 ? felder.get(positionKennungTyp).trim() : "veranstaltung";
            String rolle = positionRolle >= 0 ? felder.get(positionRolle).trim() : "teilnehmer";

            // Validierung der sonstigen Felder
            if (StringUtils.isBlank(kennung)) {
                fehlermeldungenZeile.add("Die Kennung zu einem Termin oder einer Veranstaltung ist in einer Zuweisung leer. Sie wurde nicht durchgeführt. Zeile: %s".formatted(String.join(";", felder)));
            }
            if (StringUtils.isBlank(personKennung)) {
                fehlermeldungenZeile.add("Die Kennung zu einer Person ist in einer Zuweisung leer. Sie wurde nicht durchgeführt. Zeile: %s".formatted(String.join(";", felder)));
            }
            if (!List.of("termin", "veranstaltung").contains(kennungTyp)) {
                fehlermeldungenZeile.add("Der Typ der Kennung '%s' zu einem Termin oder einer Veranstaltung ist nicht valide (veranstaltung oder termin). Die Zuordnung wurde nicht durchgeführt. Zeile: %s".formatted(kennungTyp, String.join(";", felder)));
            }
            if (!List.of("teilnehmer", "berechtigter").contains(rolle)) {
                fehlermeldungenZeile.add("Die Rolle '%s' zu einer Person ist nicht valide (teilnehmer oder berechtigter). Die Zuordnung wurde nicht durchgeführt. Zeile: %s".formatted(rolle, String.join(";", felder)));
            }

            if (fehlermeldungenZeile.isEmpty()) {

                Optional<Person> person = Optional.ofNullable(erstelltePersonenNachKennung.get(personKennung));
                Optional<Termin> termin = Optional.empty();
                Optional<Veranstaltung> veranstaltung = Optional.empty();

                if (person.isEmpty()) {
                    person = personRepository.findByKennung(personKennung);
                }
                if (person.isEmpty()) {
                    warnmeldungenZeile.add("Die Person mit der Kennung '%s' wurde nicht gefunden.".formatted(personKennung));
                } else {

                    // Ermitteln der Veranstaltung bzw. des Termins
                    if (kennungTyp.equals("veranstaltung")) {
                        veranstaltung = Optional.ofNullable(erstellteVeranstaltungenNachKennung.get(kennung));
                        if (veranstaltung.isEmpty()) {
                            veranstaltung = veranstaltungRepository.findByKennung(kennung);
                        }
                        if (veranstaltung.isEmpty()) {
                            warnmeldungenZeile.add("Die Veranstaltung mit der Kennung '%s' wurde nicht gefunden.".formatted(kennung));
                        }
                    } else if (kennungTyp.equals("termin")) {
                        termin = Optional.ofNullable(erstellteTermineNachKennung.get(kennung));
                        if (termin.isEmpty()) {
                            termin = terminRepository.findByKennung(kennung);
                        }
                        if (termin.isEmpty()) {
                            warnmeldungenZeile.add("Der Termin mit der Kennung '%s' wurde nicht gefunden.".formatted(kennung));
                        }
                    }
                }

                try {

                    // Zuweisung zu einer Veranstaltung oder einem Termin
                    boolean durchfuehren = false;
                    if (termin.isPresent() && person.isPresent()) {

                        Termin terminZuAendern = termin.get();
                        if (rolle.equals("teilnehmer")) {
                            if (!terminZuAendern.getTeilnehmendePersonen().contains(person.get())) {
                                terminZuAendern.addTeilnehmendePerson(person.get());
                                durchfuehren = true;
                            } else {
                                warnmeldungenZeile.add("Die Person mit der Kennung '%s' ist bereits Teilnehmer des Termins".formatted(person.get().getKennung()));
                            }
                        } else if (rolle.equals("berechtigter")) {
                            if (!terminZuAendern.getBerechtigtePersonen().contains(person.get())) {
                                terminZuAendern.addBerechtigtePerson(person.get());
                                durchfuehren = true;
                            } else {
                                warnmeldungenZeile.add("Die Person mit der Kennung '%s' ist bereits Berechtigter des Termins".formatted(person.get().getKennung()));
                            }
                        }

                        // Nur durchführen, wenn Person noch nicht Teil der Liste ist
                        if (durchfuehren) {
                            terminRepository.save(terminZuAendern);
                            erfolgreicheImporte++;
                        }

                    } else if (veranstaltung.isPresent() && person.isPresent()) {

                        Veranstaltung veranstaltungZuAendern = veranstaltung.get();
                        if (rolle.equals("teilnehmer")) {
                            if (!veranstaltungZuAendern.getTeilnehmendePersonen().contains(person.get())) {
                                veranstaltungZuAendern.addTeilnehmendePerson(person.get());
                                durchfuehren = true;
                            } else {
                                warnmeldungenZeile.add("Die Person mit der Kennung '%s' ist bereits Teilnehmer der Veranstaltung".formatted(person.get().getKennung()));
                            }
                        } else if (rolle.equals("berechtigter")) {
                            if (!veranstaltungZuAendern.getBerechtigtePersonen().contains(person.get())) {
                                veranstaltungZuAendern.addBerechtigtePerson(person.get());
                                durchfuehren = true;
                            } else {
                                warnmeldungenZeile.add("Die Person mit der Kennung '%s' ist bereits Berechtigter der Veranstaltung".formatted(person.get().getKennung()));
                            }
                        }
                        // Speichern
                        if (durchfuehren) {
                            veranstaltungRepository.save(veranstaltungZuAendern);
                            erfolgreicheImporte++;
                        }

                    }
                } catch (Exception exception) {
                    fehlermeldungenZeile.add("Beim Importieren der Zuordnungen von Personen zu Veranstaltungen / Terminen ist ein Fehler aufgetreten. Zeile: %s".formatted(String.join(";", felder)));
                }

            }

            fehlermeldungen.addAll(fehlermeldungenZeile);
            warnmeldungen.addAll(warnmeldungenZeile);
        }

        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.ZUORDNUNG)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1)
                .build();

    }

}
