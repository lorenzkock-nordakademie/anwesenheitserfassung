package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Service zum Importieren von Räumen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportRaeumeService {

    // Service
    private final RaumService raumService;

    // Repository
    private final RaumRepository raumRepository;


    /**
     *
     * @param raumService Service für die Überprüfung der Einmaligkeit der Kennung eines Raums
     * @param name Name des Raums
     * @param kennung Kennung des Raums
     * @return Erstellter Raum
     */
    public static Raum legeRaumAn(@NonNull RaumService raumService,
                                  @NonNull String name, @NonNull Optional<String> kennung) {

        // Erzeuge das Objekt
        Raum raum = Raum.builder()
                .bezeichnung(name)
                .kennung(kennung.orElse(null))
                .build();

        // Ergänze ggf. eine generierte Nummer
        BaseService.ergaenzeGenerierteKennung(raum);
        return raum;
    }

    /**
     * Importiere eine Datei mit Räumen
     * @param file Datei mit dem Inhalt
     * @param erstellteRaeumeNachKennung Liste, um erstellte Räume anderen Import-Schritten zur Verfügung zu stellen
     * @param erstellteRaeumeNachName Liste, um erstellte Räume anderen Import-Schritten zur Verfügung zu stellen
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importiereRaeume(@NonNull MultipartFile file, @NonNull Map<String, Raum> erstellteRaeumeNachKennung, @NonNull Map<String, Raum> erstellteRaeumeNachName) throws IllegalArgumentException, NoContentException {

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        // Validiere, ob es eine Kopfzeile mit den entsprechenden Spalten gibt
        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, true, Optional.of(List.of("bezeichnung")));

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.getFirst()).toList();
        int positionBezeichnung = kopfinformationen.indexOf("bezeichnung");
        int positionKennung = kopfinformationen.indexOf("kennung");

        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;

        // Iteriere über die einzelnen Zeilen
        for (List<String> felder : dateiInhalt.stream().skip(1).map(z -> Arrays.stream(z).toList()).toList()) {

            List<String> fehlermeldungenZeile = new ArrayList<>();

            // Ziehe die Informationen aus den Feldern
            String bezeichnung = felder.get(positionBezeichnung).trim();
            Optional<String> kennung = positionKennung >= 0 ? Optional.of(felder.get(positionKennung).trim()) : Optional.empty();

            // Validierung der sonstigen Felder
            if (StringUtils.isBlank(bezeichnung)) {
                fehlermeldungenZeile.add("Die Bezeichnung eines Raums ist leer. Er wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }

            // Validierung der Kennung
            if (fehlermeldungenZeile.isEmpty() && !StringUtils.isBlank(kennung.orElse(null))) {
                if (kennung.get().contains(" ")) {
                    fehlermeldungenZeile.add("In der Kennung '%s' sind keine Leerzeichen erlaubt.".formatted(kennung.get()));
                } else {
                    try {
                        raumService.pruefeKennungEinmalig(raumRepository, kennung.get(), Optional.empty());
                    } catch (IllegalArgumentException e) {
                        fehlermeldungenZeile.add("Die Kennung '%s' für den Raum '%s' ist bereits vergeben.".formatted(kennung.get(), bezeichnung));
                    }
                }
            }


            if (fehlermeldungenZeile.isEmpty()) {

                Raum raum = legeRaumAn(raumService, bezeichnung, kennung);

                try {
                    // Speichern
                    raumRepository.save(raum);
                    erstellteRaeumeNachKennung.putIfAbsent(raum.getKennung(), raum);
                    erstellteRaeumeNachName.putIfAbsent(raum.getBezeichnung(), raum);
                    erfolgreicheImporte++;
                } catch (Exception exception) {
                    fehlermeldungenZeile.add("Beim Importieren des Raums '%s' ist ein Fehler aufgetreten.".formatted(bezeichnung));
                }

            }
            fehlermeldungen.addAll(fehlermeldungenZeile);

        }

        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.RAUM)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1)
                .build();

    }

}
