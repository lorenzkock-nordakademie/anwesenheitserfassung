package de.lorenzkock.anwesenheitserfassungserver.importservices;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.CisImportDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnisCollector;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;

/**
 * Service zum Importieren verschiedener Dateien
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportService {

    // Service
    private final ImportPersonenService importierePersonenService;
    private final ImportRaeumeService importiereRaeumeService;
    private final ImportAusweiseService importiereAusweiseService;
    private final ImportVeranstaltungenService importiereVeranstaltungenService;
    private final ImportTermineService importiereTermineService;
    private final ImportPersonenZuweisungService importierePersonenZuweisungService;
    private final ImportCisService importiereCisService;

    /**
     * Verarbeitet eine CSV Datei und gibt die Daten als Liste zurück
     *
     * @param file Datei, die verarbeitet werden soll
     * @return Eingelesene Daten
     */
    public static List<String[]> importCsv(MultipartFile file) {
        CSVReader csvReader = null;
        try {
            try {
                Reader reader = new InputStreamReader(file.getInputStream());

                // Parse CSV data
                csvReader = new CSVReaderBuilder(reader).withCSVParser(new CSVParserBuilder().withSeparator(';').build()).build();
                return csvReader.readAll();

            } catch (CsvException e) {
                throw new IllegalArgumentException("Die CSV-Datei '%s' kann nicht gelesen werden.".formatted(file.getOriginalFilename()));
            } finally {
                if (csvReader != null) csvReader.close();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Die CSV-Datei '%s' kann nicht gelesen werden.".formatted(file.getOriginalFilename()));
        }
    }

    /**
     * Validiert das Dateiformat einer gegebenen Datei
     *
     * @param datei  Datei, die zu validieren ist
     * @param format Erwartetes Format bspw. text/csv
     * @throws IllegalArgumentException Fehler, wenn Format nicht übereinstimmt
     */
    public static void validiereDateiFormat(@NonNull MultipartFile datei, @NonNull String format) throws IllegalArgumentException {

        if (!format.equals(datei.getContentType())) {
            throw new IllegalArgumentException("Die Datei '%s' ist nicht im csv-Format.".formatted(datei.getOriginalFilename()));
        }

    }

    /**
     * Prüft den Inhalt einer CSV Datei auf den notwendigen Inhalt
     *
     * @param dateiname             Dateiname der datei
     * @param inhalt                Inhalt der Datei
     * @param kopfzeileErforderlich Information, ob Kopfzeile erwartet wird
     * @param notwendigeSpalten     Liste von Spaltennamen, die enthalten sein sollen. Nur relevant, wenn kopfzeile erforderlich ist
     * @throws NoContentException Fehler
     */
    public static void validiereDateiInhalt(@NonNull String dateiname, @NonNull List<String[]> inhalt, boolean kopfzeileErforderlich, @NonNull Optional<List<String>> notwendigeSpalten) throws NoContentException {

        if (kopfzeileErforderlich) {
            if (inhalt.size() < 2) {
                throw new NoContentException("Die Datei '%s' hat keinen Inhalt. Mindestens eine Kopf- und eine Inhaltszeile erwartet. Zeilenlänge: %s".formatted(dateiname, inhalt.size()));
            }
        } else {
            if (inhalt.isEmpty()) {
                throw new NoContentException("Die Datei '%s' hat keinen Inhalt. Mindestens eine Inhaltszeile erwartet. Zeilenlänge: 0".formatted(dateiname));
            }
        }

        if (kopfzeileErforderlich && notwendigeSpalten.isPresent() && !notwendigeSpalten.get().isEmpty()) {
            List<String> kopfinformationen = Arrays.stream(inhalt.getFirst()).toList();
            for (String spaltenname : notwendigeSpalten.get()) {
                if (!kopfinformationen.contains(spaltenname)) {
                    throw new IllegalArgumentException("Das Feld '%s' wird in der CSV-Datei '%s' erwartet, existiert aber nicht".formatted(spaltenname, dateiname));
                }
            }
        }
    }

    /**
     * Importiert die einzelnen Dateien
     *
     * @param raeumeDatei          Datei mit Daten zu Räumen
     * @param ausweiseDatei        Datei mit Daten zu Ausweisen
     * @param personenDatei        Datei mit Daten zu Personen
     * @param veranstaltungenDatei Datei mit Daten zu Veranstaltungen
     * @param termineDatei         Datei mit Daten zu Terminen
     * @param zuordnungDatei       Datei mit Daten zu Zuordnungen von Personen zu Terminen und Veranstaltungen
     * @return Ergebnis des Imports
     */
    public ImportErgebnisCollector importiereDateien(@NonNull Optional<MultipartFile> raeumeDatei,
                                                     @NonNull Optional<MultipartFile> ausweiseDatei,
                                                     @NonNull Optional<MultipartFile> personenDatei,
                                                     @NonNull Optional<MultipartFile> veranstaltungenDatei,
                                                     @NonNull Optional<MultipartFile> termineDatei,
                                                     @NonNull Optional<MultipartFile> zuordnungDatei) {


        List<ImportErgebnis> ergebnisse = new ArrayList<>();
        List<String> fehlermeldungen = new ArrayList<>();

        // Erstelle Listen, um Daten über die einzelnen Schritte hinweg zu teilen
        Map<String, Person> erstelltePersonenNachKennung = new HashMap<>();

        Map<String, Raum> erstellteRaeumeNachKennung = new HashMap<>();
        Map<String, Raum> erstellteRaeumeNachName = new HashMap<>();

        Map<String, Ausweis> erstellteAusweiseNachKennung = new HashMap<>();
        Map<String, Ausweis> erstellteAusweiseNachSnr = new HashMap<>();

        Map<String, Veranstaltung> erstellteVeranstaltungenNachKennung = new HashMap<>();
        Map<String, Veranstaltung> erstellteVeranstaltungenNachName = new HashMap<>();
        Map<String, Termin> erstellteTermineNachKennung = new HashMap<>();

        // Importiere Räume, wenn Datei gegeben
        if (raeumeDatei.isPresent() && dateiHatInhalt(raeumeDatei)) {
            try {
                ergebnisse.add(this.importiereRaeumeService.importiereRaeume(raeumeDatei.get(), erstellteRaeumeNachKennung, erstellteRaeumeNachName));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Räume ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Importiere Ausweise, wenn Datei gegeben
        if (ausweiseDatei.isPresent() && dateiHatInhalt(ausweiseDatei)) {
            try {
                ergebnisse.add(this.importiereAusweiseService.importiereAusweise(ausweiseDatei.get(), erstelltePersonenNachKennung, erstellteAusweiseNachKennung, erstellteAusweiseNachSnr));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Ausweise ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Importiere Personen, wenn Datei gegeben
        if (personenDatei.isPresent() && dateiHatInhalt(personenDatei)) {
            try {
                ergebnisse.add(this.importierePersonenService.importierePersonen(personenDatei.get(), erstelltePersonenNachKennung, erstellteAusweiseNachKennung, erstellteAusweiseNachSnr));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Personen ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Importiere Veranstaltungen, wenn Datei gegeben
        if (veranstaltungenDatei.isPresent() && dateiHatInhalt(veranstaltungenDatei)) {
            try {
                ergebnisse.add(this.importiereVeranstaltungenService.importiereVeranstaltungen(veranstaltungenDatei.get(), erstellteVeranstaltungenNachKennung, erstellteVeranstaltungenNachName));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Veranstaltungen ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Importiere Termine, wenn Datei gegeben
        if (termineDatei.isPresent() && dateiHatInhalt(termineDatei)) {
            try {
                ergebnisse.add(this.importiereTermineService.importiereTermine(termineDatei.get(),
                        erstellteRaeumeNachKennung, erstellteRaeumeNachName,
                        erstellteVeranstaltungenNachKennung, erstellteVeranstaltungenNachName,
                        erstellteTermineNachKennung));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Termine ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Importiere Zuordnungen, wenn Datei gegeben
        if (zuordnungDatei.isPresent() && dateiHatInhalt(zuordnungDatei)) {
            try {
                ergebnisse.add(this.importierePersonenZuweisungService.importierePersonenZuweisung(zuordnungDatei.get(),
                        erstelltePersonenNachKennung, erstellteTermineNachKennung,
                        erstellteVeranstaltungenNachKennung));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Zuweisungen von Personen zu Terminen / Veranstaltungen ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Erstelle das Ergebnis
        return ImportErgebnisCollector.builder()
                .ergebnisse(ergebnisse)
                .fehlermeldungen(fehlermeldungen)
                .build();

    }

    public ImportErgebnisCollector importiereCis(@NonNull CisImportDto importDaten) {

        List<ImportErgebnis> ergebnisse = new ArrayList<>();
        List<String> fehlermeldungen = new ArrayList<>();

        // Importiere Daten aus dem CIS
        if (dateiHatInhalt(Optional.of(importDaten.getCisFile()))) {
            try {
                ergebnisse.add(this.importiereCisService.importiereCisDaten(importDaten.getCisFile(), Optional.empty(), Optional.empty(), Optional.empty()));
            } catch (Exception e) {
                fehlermeldungen.add("Beim Import der Daten aus CIS ist ein Fehler aufgetreten: %s".formatted(e.getMessage()));
            }
        }

        // Erstelle das Ergebnis
        return ImportErgebnisCollector.builder()
                .ergebnisse(ergebnisse)
                .fehlermeldungen(fehlermeldungen)
                .build();
    }

    /**
     * Prüft, ob eine Datei gegeben ist und einen Dateinamen besitzt
     *
     * @param datei Datei, die zu prüfen ist
     * @return true, wenn Datei vorhanden ist und einen Namen hat
     */
    private boolean dateiHatInhalt(Optional<MultipartFile> datei) {
        return datei.isPresent() && datei.filter(multipartFile -> !StringUtils.isBlank(Optional.of(multipartFile).map(MultipartFile::getOriginalFilename).orElse(""))).isPresent();
    }

}
