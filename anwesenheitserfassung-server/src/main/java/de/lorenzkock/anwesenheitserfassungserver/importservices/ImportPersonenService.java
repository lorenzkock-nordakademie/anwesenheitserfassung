package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Service zum Importieren von Personen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportPersonenService {

    // Service
    private final PersonenService personenService;
    private final AusweisService ausweisService;

    // Repository
    private final AusweisRepository ausweisRepository;
    private final PersonRepository personRepository;

    /**
     * Methode zum Erzeugen einer Person
     *
     * @param personenService Service zur Überprüfung der Einmaligkeit einer Kennung zu einer Person
     * @param vorname         Vorname der Person
     * @param nachname        Nachname der Person
     * @param personkennung   Kennung der Person
     * @return Erzeugte Person
     */
    public static Person legePersonAn(@NonNull PersonenService personenService, @NonNull String vorname, @NonNull String nachname, @NonNull Optional<String> personkennung) {
        Person p = Person.builder()
                .kennung(personkennung.orElse(null))
                .vorname(vorname)
                .nachname(nachname)
                .build();
        BaseService.ergaenzeGenerierteKennung(p);
        return p;
    }

    /**
     * Importiere eine Datei mit Personen
     *
     * @param file                              Datei mit dem Inhalt
     * @param zuvorErstelltePersonen            Liste, um erstellte Personen anderen Import-Schritten zur Verfügung zu stellen
     * @param zuvorErstellteAusweiseNachKennung Liste, um auf zuvor erstellte Ausweise zugreifen zu können
     * @param zuvorErstellteAusweiseNachSnr     Liste, um auf zuvor erstellte Ausweise zugreifen zu können
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException       Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importierePersonen(@NonNull MultipartFile file, Map<String, Person> zuvorErstelltePersonen,
                                             Map<String, Ausweis> zuvorErstellteAusweiseNachKennung,
                                             Map<String, Ausweis> zuvorErstellteAusweiseNachSnr) throws IllegalArgumentException, NoContentException {

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        // Validiere, ob es eine Kopfzeile mit den entsprechenden Spalten gibt
        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, true, Optional.of(List.of("vorname", "nachname")));

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.getFirst()).toList();
        int positionVorname = kopfinformationen.indexOf("vorname");
        int positionNachname = kopfinformationen.indexOf("nachname");
        int positionKennung = kopfinformationen.indexOf("kennung");
        int positionAusweiskennung = kopfinformationen.indexOf("ausweis_kennung");
        int positionAusweisRfidSnr = kopfinformationen.indexOf("ausweis_rfidsnr");

        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;

        Map<String, Person> hierErstelltePersonen = new HashMap<>();
        // Iteriere über die einzelnen Zeilen
        for (List<String> felder : dateiInhalt.stream().skip(1).map(z -> Arrays.stream(z).toList()).toList()) {

            List<String> fehlermeldungenZeile = new ArrayList<>();
            List<String> warnmeldungenZeile = new ArrayList<>();

            // Ziehe die Informationen aus den Feldern
            String vorname = felder.get(positionVorname).trim();
            String nachname = felder.get(positionNachname).trim();
            Optional<String> kennung = positionKennung >= 0 ? Optional.of(felder.get(positionKennung).trim()) : Optional.empty();
            Optional<String> ausweiskennung = positionAusweiskennung >= 0 ? Optional.of(felder.get(positionAusweiskennung).trim()) : Optional.empty();
            Optional<String> ausweisRfidSnr = positionAusweisRfidSnr >= 0 ? Optional.of(felder.get(positionAusweisRfidSnr).trim()) : Optional.empty();

            if (!StringUtils.isBlank(kennung.orElse(null)) && hierErstelltePersonen.containsKey(kennung.get())) {
                fehlermeldungenZeile.add("Die Matrikelnummer der Person wurde innerhalb der Liste doppelt vergeben. Zeile: %s".formatted(String.join(";", felder)));
            }

            // Validierung der sonstigen Felder
            if (StringUtils.isBlank(vorname)) {
                fehlermeldungenZeile.add("Der Vorname einer Person ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }
            if (StringUtils.isBlank(nachname)) {
                fehlermeldungenZeile.add("Der Nachname einer Person ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }

            Optional<Person> existierendePersonZuKennung = Optional.empty();
            // Validierung der Kennung
            if (fehlermeldungenZeile.isEmpty() && !StringUtils.isBlank(kennung.orElse(null))) {
                if (kennung.get().contains(" ")) {
                    fehlermeldungenZeile.add("In der Kennung '%s' sind keine Leerzeichen erlaubt.".formatted(kennung.get()));
                } else {
                    Optional<String> erlaubteId = Optional.ofNullable(zuvorErstelltePersonen.getOrDefault(kennung.get(), null)).map(Person::getId);
                    try {
                        existierendePersonZuKennung = personenService.pruefeKennungEinmalig(personRepository, kennung.get(), erlaubteId);
                    } catch (IllegalArgumentException e) {
                        fehlermeldungenZeile.add("Die Kennung '%s' für die Person '%s, %s' ist bereits vergeben. Die Person wurde nicht hinzugefügt.".formatted(kennung.get(), nachname, vorname));
                    }
                }
            }

            if (fehlermeldungenZeile.isEmpty() && existierendePersonZuKennung.isEmpty()) {

                boolean neuerAusweis = false;
                Optional<Ausweis> ausweisZuPerson = Optional.empty();

                // Versuche einen Ausweis zu ermitteln und die der Person zuzuweisen, bzw. diese anzulegen
                if (!StringUtils.isBlank(ausweiskennung.orElse(null))) {

                    ausweisZuPerson = Optional.ofNullable(zuvorErstellteAusweiseNachKennung.get(ausweiskennung.get()));

                    if (ausweisZuPerson.isEmpty() && !StringUtils.isBlank(ausweisRfidSnr.orElse(null))) {
                        ausweisZuPerson = Optional.ofNullable(zuvorErstellteAusweiseNachSnr.get(ausweisRfidSnr.get()));
                    }
                    if (ausweisZuPerson.isEmpty()) {
                        ausweisZuPerson = ausweisRepository.findFirstByIdKennung(ausweiskennung.get());
                    }
                    // Prüfe, ob der Ausweis existiert
                    if (ausweisZuPerson.isPresent()) {
                        // Prüfe, ob zu dem Ausweis schon eine Person zugeordnet wurde
                        if (ausweisZuPerson.get().getPerson() != null) {
                            if (StringUtils.isBlank(kennung.orElse(null)) || !ausweisZuPerson.get().getPerson().getKennung().equals(kennung.get())) {
                                ausweisZuPerson = Optional.empty();
                                warnmeldungenZeile.add("Ein Ausweis mit der Kennung '%s' für die Person '%s, %s' ist bereits einer anderen Person zugewiesen. Die Zuweisung wurde nicht vorgenommen.".formatted(ausweiskennung.get(), nachname, vorname));
                            }
                        }
                    } else {
                        // Wenn kein Ausweis gegeben ist, kann sie angelegt werden, wenn eine RFID-Seriennummer angegeben wurde
                        if (!StringUtils.isBlank(ausweisRfidSnr.orElse(null))) {
                            ausweisZuPerson = Optional.of(ImportAusweiseService.legeAusweisAn(ausweisService, ausweisRfidSnr.get(), ausweiskennung));
                            neuerAusweis = true;
                        } else {
                            warnmeldungenZeile.add("Ein Ausweis mit der Kennung '%s' für die Person '%s, %s' konnte nicht gefunden werden. Die Zuweisung wurde nicht vorgenommen.".formatted(ausweiskennung.get(), nachname, vorname));
                        }
                    }
                } else {
                    // Wenn keine Kennung angegeben wurde, prüfe, ob ein Ausweis mit einer gegebenen RFID Seriennummer angelegt werden soll
                    if (!StringUtils.isBlank(ausweisRfidSnr.orElse(null))) {
                        ausweisZuPerson = Optional.ofNullable(zuvorErstellteAusweiseNachSnr.get(ausweisRfidSnr.get()));
                        if (ausweisZuPerson.isEmpty()) {
                            ausweisZuPerson = Optional.of(ImportAusweiseService.legeAusweisAn(ausweisService, ausweisRfidSnr.get(), ausweiskennung));
                            neuerAusweis = true;
                        }
                    }
                }


                Person person = legePersonAn(this.personenService, vorname, nachname, kennung);
                hierErstelltePersonen.putIfAbsent(person.getKennung(), person);

                try {
                    // Speichern
                    person = personRepository.save(person);
                    zuvorErstelltePersonen.putIfAbsent(person.getKennung(), person);
                    erfolgreicheImporte++;

                    // Schreibe die Person an den Ausweis
                    if (ausweisZuPerson.isPresent()) {

                        // Deaktivieren alter Ausweise, wenn es ein neuer Ausweis ist
                        if (neuerAusweis) {
                            Optional<Ausweis> ausweisMitRfidSnr = this.ausweisRepository.findFirstByIdRfidSnrAndAktiv(ausweisZuPerson.get().getRfidSnr(), true);
                            if (ausweisMitRfidSnr.isPresent()) {
                                Ausweis ausweisMitSnr = ausweisMitRfidSnr.get();
                                ausweisMitSnr.setAktiv(false);
                                ausweisRepository.save(ausweisMitSnr);
                            }
                        }

                        // Speichere Zuordnung zum Ausweis
                        ausweisRepository.save(ausweisZuPerson.get().toBuilder().person(person).build());
                    }
                } catch (Exception exception) {
                    fehlermeldungenZeile.add("Beim Importieren der Person '%s, %s' ist ein Fehler aufgetreten.".formatted(nachname, vorname));
                }

            }

            fehlermeldungen.addAll(fehlermeldungenZeile);
            warnmeldungen.addAll(warnmeldungenZeile);
        }

        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.PERSON)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1)
                .build();

    }


}
