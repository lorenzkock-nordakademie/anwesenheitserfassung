package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Stream;

/**
 * Service zum Importieren von Terminen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportTermineService {

    // Service
    private final VeranstaltungService veranstaltungService;
    private final TerminService terminService;
    private final RaumService raumService;

    // Repository
    private final VeranstaltungRepository veranstaltungRepository;
    private final TerminRepository terminRepository;
    private final PersonRepository personRepository;
    private final RaumRepository raumRepository;

    // Mapper

    /**
     * Importiere eine Datei mit Personen
     *
     * @param file Datei mit dem Inhalt
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException       Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importiereTermine(@NonNull MultipartFile file,
                                            Map<String, Raum> erstellteRaeumeNachKennung,
                                            Map<String, Raum> erstellteRaeumeNachName,
                                            Map<String, Veranstaltung> erstellteVeranstaltungenNachKennung,
                                            Map<String, Veranstaltung> erstellteVeranstaltungenNachName,
                                            Map<String, Termin> erstellteTermine) throws IllegalArgumentException, NoContentException {

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        // Validiere, ob es eine Kopfzeile mit den entsprechenden Spalten gibt
        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, true, Optional.of(List.of("start", "ende")));

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.getFirst()).toList();
        int positionStart = kopfinformationen.indexOf("start");
        int positionEnde = kopfinformationen.indexOf("ende");
        int positionKennung = kopfinformationen.indexOf("kennung");
        int positionRaumKennung = kopfinformationen.indexOf("raum_kennung");
        int positionRaumName = kopfinformationen.indexOf("raum_name");
        int positionTeilnehmer = kopfinformationen.indexOf("teilnehmer");
        int positionBerechtigte = kopfinformationen.indexOf("berechtigte");
        int positionVeranstaltungKennung = kopfinformationen.indexOf("veranstaltung_kennung");
        int positionVeranstaltungName = kopfinformationen.indexOf("veranstaltung_name");
        int positionErlaubeEinAusgehend = kopfinformationen.indexOf("veranstaltung_erlaube_eingehendausgehend");
        int positionErlaubeMehrfachbuchungen = kopfinformationen.indexOf("veranstaltung_erlaube_mehrfachbuchungen");
        int positionErlaubeFremdausweise = kopfinformationen.indexOf("veranstaltung_erlaube_fremdausweise");
        int positionFremdausweiseBestaetigen = kopfinformationen.indexOf("veranstaltung_fremdausweise_bestaetigen");
        int positionVeranstaltungTeilnehmer = kopfinformationen.indexOf("veranstaltung_teilnehmer");
        int positionVeranstaltungBerechtigte = kopfinformationen.indexOf("veranstaltung_berechtigte");

        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;

        // Iteriere über die einzelnen Zeilen
        for (List<String> felder : dateiInhalt.stream().skip(1).map(z -> Arrays.stream(z).toList()).toList()) {

            List<String> fehlermeldungenZeile = new ArrayList<>();
            List<String> warnmeldungenZeile = new ArrayList<>();

            // Ziehe die Informationen aus den Feldern
            String start = felder.get(positionStart).trim();
            String ende = felder.get(positionEnde).trim();

            // Optionale Termin-Attribute
            Optional<String> kennung = positionKennung >= 0 ? Optional.of(felder.get(positionKennung).trim()) : Optional.empty();
            Optional<String> teilnehmer = positionTeilnehmer >= 0 ? Optional.of(felder.get(positionTeilnehmer).trim()) : Optional.empty();
            Optional<String> berechtigte = positionBerechtigte >= 0 ? Optional.of(felder.get(positionBerechtigte).trim()) : Optional.empty();
            Optional<String> raumKennung = positionRaumKennung >= 0 ? Optional.of(felder.get(positionRaumKennung).trim()) : Optional.empty();
            Optional<String> raumName = positionRaumName >= 0 ? Optional.of(felder.get(positionRaumName).trim()) : Optional.empty();

            // Veranstaltungsdaten Name oder Kennung notwendig
            Optional<String> veranstaltungName = positionVeranstaltungName >= 0 ? Optional.of(felder.get(positionVeranstaltungName).trim()) : Optional.empty();
            Optional<String> veranstaltungKennung = positionVeranstaltungKennung >= 0 ? Optional.of(felder.get(positionVeranstaltungKennung).trim()) : Optional.empty();

            // Optionale Veranstaltungs-Attribute
            boolean veranstaltungErlaubeEinAusgehende = positionErlaubeEinAusgehend >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionErlaubeEinAusgehend).trim()).orElse("false"));
            boolean veranstaltungErlaubeMehrfach = positionErlaubeMehrfachbuchungen >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionErlaubeMehrfachbuchungen).trim()).orElse("false"));
            boolean veranstaltungErlaubeFremdausweise = positionErlaubeFremdausweise >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionErlaubeFremdausweise).trim()).orElse("false"));
            boolean veranstaltungFremdausweiseBestaetigen = positionFremdausweiseBestaetigen >= 0 && List.of("true", "1").contains(Optional.of(felder.get(positionFremdausweiseBestaetigen).trim()).orElse("false"));
            Optional<String> teilnehmerVeranstaltung = positionVeranstaltungTeilnehmer >= 0 ? Optional.of(felder.get(positionVeranstaltungTeilnehmer).trim()) : Optional.empty();
            Optional<String> berechtigteVeranstaltung = positionVeranstaltungBerechtigte >= 0 ? Optional.of(felder.get(positionVeranstaltungBerechtigte).trim()) : Optional.empty();

            // Validierung der sonstigen Felder
            if (StringUtils.isBlank(start)) {
                fehlermeldungenZeile.add("Der Start eines Termins ist leer. Er wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }
            if (StringUtils.isBlank(ende)) {
                fehlermeldungenZeile.add("Das Ende eines Termins ist leer. Er wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }
            OffsetDateTime startZeitpunkt = null;
            OffsetDateTime endeZeitpunkt = null;
            if (fehlermeldungenZeile.isEmpty()) {
                try {
                    startZeitpunkt = DatumHelper.toDateTime(start);
                } catch (DateTimeParseException e) {
                    fehlermeldungenZeile.add("Das Start-Datum '%s' einer Veranstaltung kann nicht gelesen werden, da es im falschen Format ist. Zeile: %s".formatted(start, String.join(";", felder)));
                }
                try {
                    endeZeitpunkt = DatumHelper.toDateTime(ende);
                } catch (DateTimeParseException e) {
                    fehlermeldungenZeile.add("Das Ende-Datum '%s' einer Veranstaltung kann nicht gelesen werden, da es im falschen Format ist. Zeile: %s".formatted(ende, String.join(";", felder)));
                }
            }

            // Validierung der Kennung
            if (fehlermeldungenZeile.isEmpty() && !StringUtils.isBlank(kennung.orElse(null))) {
                if (kennung.get().contains(" ")) {
                    fehlermeldungenZeile.add("In der Kennung '%s' sind keine Leerzeichen erlaubt.".formatted(kennung.get()));
                } else {
                    try {
                        terminService.pruefeKennungEinmalig(terminRepository, kennung.get(), Optional.empty());
                    } catch (IllegalArgumentException e) {
                        fehlermeldungenZeile.add("Die Kennung '%s' für einen Termin ist bereits vergeben. Der Termin wurde nicht hinzugefügt.".formatted(kennung.get()));
                    }
                }
            }

            // Ermittle die Veranstaltung zu dem Termin
            Veranstaltung veranstaltungZuTermin = null;
            if (fehlermeldungenZeile.isEmpty()) {
                // Entweder ein Veranstaltungsname oder eine Veranstaltungs-Kennung muss gegeben werden
                if (!StringUtils.isBlank(veranstaltungKennung.orElse(null)) || !StringUtils.isBlank(veranstaltungName.orElse(null))) {

                    // Suche aus bereits erstellten Veranstaltungen

                    // Wenn eine Kennung angegeben ist, soll geprüft werden, ob die Veranstaltung bereits existiert
                    if (!StringUtils.isBlank(veranstaltungKennung.orElse(null))) {

                        // Suche zunächst in der Liste der bereits angelegten
                        veranstaltungZuTermin = erstellteVeranstaltungenNachKennung.get(veranstaltungKennung.get());

                        // Wenn eine Kennung angegeben wurde, wird davon ausgegangen, dass auch beim Import
                        // von Veranstaltungen eine Kennung verwendet wird

                        // Dann im Repository
                        if (veranstaltungZuTermin == null) {
                            veranstaltungZuTermin = veranstaltungRepository.findByKennung(veranstaltungKennung.get()).orElse(null);
                        }

                        if (veranstaltungZuTermin == null) {

                            // Wenn die Veranstaltung nicht existiert, muss sie angelegt werden.
                            // Dafür muss mindestens ein Name angegeben werden
                            if (StringUtils.isBlank(veranstaltungName.orElse(null))) {
                                fehlermeldungenZeile.add("Die Veranstaltung mit der Kennung '%s' konnte nicht gefunden werden. Sie konnte nicht angelegt werden, da der Name der Veranstaltung fehlt.".formatted(veranstaltungKennung.get()));
                            } else {
                                veranstaltungZuTermin = ImportVeranstaltungenService.legeVeranstaltungAn(personRepository, veranstaltungService,
                                        veranstaltungName.get(), veranstaltungKennung, teilnehmerVeranstaltung, berechtigteVeranstaltung,
                                        veranstaltungErlaubeEinAusgehende, veranstaltungErlaubeMehrfach,
                                        veranstaltungErlaubeFremdausweise, veranstaltungFremdausweiseBestaetigen, warnmeldungenZeile);
                            }

                        }
                    } else {

                        // Suche in der Liste von bereits angelegten mit dem Namen
                        veranstaltungZuTermin = erstellteVeranstaltungenNachName.get(veranstaltungName.get());

                        if (veranstaltungZuTermin == null) {
                            veranstaltungZuTermin = ImportVeranstaltungenService.legeVeranstaltungAn(personRepository, veranstaltungService,
                                    veranstaltungName.get(), veranstaltungKennung, teilnehmerVeranstaltung, berechtigteVeranstaltung,
                                    veranstaltungErlaubeEinAusgehende, veranstaltungErlaubeMehrfach,
                                    veranstaltungErlaubeFremdausweise, veranstaltungFremdausweiseBestaetigen, warnmeldungenZeile);
                        }
                    }
                } else {
                    fehlermeldungenZeile.add("Zu einem Termin wurde keine Veranstaltung angegeben. Entweder eine Veranstaltungs-Kennung oder ein Name für eine Veranstaltung, die angelegt werden soll, ist notwendig. Zeile: %s".formatted(String.join(";", felder)));
                }
            }

            // Ermittle den Raum zum Termin
            Optional<Raum> raumZuTermin = Optional.empty();
            if (fehlermeldungenZeile.isEmpty()) {

                // Entweder ein Veranstaltungsname oder eine Veranstaltungs-Kennung muss gegeben werden
                if (!StringUtils.isBlank(raumKennung.orElse(null)) || !StringUtils.isBlank(raumName.orElse(null))) {

                    // Suche aus bereits erstellten Räumen

                    // Wenn eine Kennung angegeben ist, soll geprüft werden, ob die Veranstaltung bereits existiert
                    if (!StringUtils.isBlank(raumKennung.orElse(null))) {

                        // Suche zunächst in der Liste der bereits angelegten
                        raumZuTermin = Optional.ofNullable(erstellteRaeumeNachKennung.get(raumKennung.get()));

                        // Dann im Repository
                        if (raumZuTermin.isEmpty()) {
                            raumZuTermin = raumRepository.findByKennung(raumKennung.get());
                        }

                        if (raumZuTermin.isEmpty()) {
                            // Wenn der Raum nicht existiert, wird er angegeben, wenn ein Name gegeben wird
                            if (!StringUtils.isBlank(raumName.orElse(null))) {
                                raumZuTermin = Optional.of(ImportRaeumeService.legeRaumAn(raumService,
                                        raumName.get(), raumKennung));
                            } else {
                                warnmeldungenZeile.add("Ein Raum für die Kennung '%s' kann nicht angelegt werden, da der Name leer ist.".formatted(raumKennung.get()));
                            }
                        }
                    } else {

                        // Suche in der Liste von bereits angelegten mit dem Namen
                        raumZuTermin = Optional.ofNullable(erstellteRaeumeNachName.get(raumName.get()));

                        if (raumZuTermin.isEmpty()) {
                            raumZuTermin = Optional.of(ImportRaeumeService.legeRaumAn(raumService,
                                    raumName.get(), raumKennung));
                        }
                    }
                }
            }


            if (fehlermeldungenZeile.isEmpty() && veranstaltungZuTermin != null) {


                // Ermittle Teilnehmer und Berechtigte
                Set<Person> teilnehmendePersonen = new HashSet<>();
                if (teilnehmer.isPresent()) {
                    List<String> teilnehmerKennungen = Stream.of(teilnehmer.get().split(",")).filter(k -> !k.isBlank()).map(String::trim).toList();
                    for (String teilnehmerKennung : teilnehmerKennungen) {
                        Optional<Person> personZuKennung = personRepository.findByKennung(teilnehmerKennung);
                        if (personZuKennung.isPresent()) {
                            teilnehmendePersonen.add(personZuKennung.get());
                        } else {
                            warnmeldungenZeile.add("Die teilnehmende Person mit der Kennung '%s' zu einem Termin wurde nicht gefunden.".formatted(teilnehmerKennung));
                        }

                    }
                }
                Set<Person> berechtigtePersonen = new HashSet<>();
                if (teilnehmer.isPresent()) {
                    List<String> berechtigteKennungen = Stream.of(berechtigte.get().split(",")).filter(k -> !k.isBlank()).map(String::trim).toList();
                    for (String berechtigteKennung : berechtigteKennungen) {
                        Optional<Person> personZuKennung = personRepository.findByKennung(berechtigteKennung);
                        if (personZuKennung.isPresent()) {
                            berechtigtePersonen.add(personZuKennung.get());
                        } else {
                            warnmeldungenZeile.add("Die berechtigte Person mit der Kennung '%s' zu einem Termin wurde nicht gefunden.".formatted(berechtigteKennung));
                        }
                    }
                }

                // Wenn eine neue Veranstaltung angelegt wurde, speichere diese
                if (StringUtils.isBlank(veranstaltungZuTermin.getId())) {
                    veranstaltungZuTermin = veranstaltungRepository.save(veranstaltungZuTermin);
                    erstellteVeranstaltungenNachKennung.putIfAbsent(veranstaltungZuTermin.getKennung(), veranstaltungZuTermin);
                    erstellteVeranstaltungenNachName.putIfAbsent(veranstaltungZuTermin.getKennung(), veranstaltungZuTermin);
                }

                // Wenn ein neuer Raum angelegt wurde, speichere diesen
                if (raumZuTermin.isPresent() && StringUtils.isBlank(raumZuTermin.get().getId())) {
                    raumZuTermin = Optional.of(raumRepository.save(raumZuTermin.get()));
                    erstellteRaeumeNachKennung.put(raumZuTermin.get().getKennung(), raumZuTermin.get());
                    erstellteRaeumeNachName.put(raumZuTermin.get().getBezeichnung(), raumZuTermin.get());
                }

                Termin termin = Termin.builder()
                        .start(startZeitpunkt)
                        .ende(endeZeitpunkt)
                        .kennung(kennung.orElse(null))
                        .teilnehmendePersonen(teilnehmendePersonen.stream().toList())
                        .berechtigtePersonen(berechtigtePersonen.stream().toList())
                        .veranstaltung(veranstaltungZuTermin)
                        .raeume(raumZuTermin.map(List::of).orElse(Collections.emptyList()))
                        .build();

                BaseService.ergaenzeGenerierteKennung(termin);

                try {
                    // Speichern
                    termin = terminRepository.save(termin);
                    erfolgreicheImporte++;

                    erstellteTermine.putIfAbsent(termin.getKennung(), termin);

                } catch (Exception exception) {
                    fehlermeldungenZeile.add("Beim Importieren eines Termins ist ein Fehler aufgetreten. Zeile: %s".formatted(String.join(";", felder)));
                }

            }

            fehlermeldungen.addAll(fehlermeldungenZeile);
            warnmeldungen.addAll(warnmeldungenZeile);
        }

        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.TERMIN)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1)
                .build();

    }

}
