package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Service zum Importieren von einer Datei aus dem CIS
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportCisService {

    // Service
    private final VeranstaltungService veranstaltungService;
    private final TerminService terminService;
    private final PersonenService personenService;
    // Repository
    private final VeranstaltungRepository veranstaltungRepository;
    private final PersonRepository personRepository;
    private final TerminRepository terminRepository;
    private final RaumRepository raumRepository;

    @Value("${import.cis.datum.regexp}")
    private String datumRegex;
    @Value("${import.cis.veranstaltung.regexp}")
    private String veranstaltungNameRegex;
    @Value("${import.cis.datum.default.start:06:00}")
    private String veranstaltungDefaultStart;
    @Value("${import.cis.datum.default.ende:22:00}")
    private String veranstaltungDefaultEnde;
    @Value("${import.cis.spalte.name.kennung:kennung}")
    private String spalteBezeichnungKennung;
    @Value("${import.cis.spalte.name.vorname:vorname}")
    private String spalteBezeichnungVorname;
    @Value("${import.cis.spalte.name.nachname:nachname}")
    private String spalteBezeichnungNachname;

    /**
     * Importiere eine Datei aus dem CIS
     *
     * @param file Datei mit dem Inhalt
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException       Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importiereCisDaten(@NonNull MultipartFile file, @NonNull Optional<String> veranstaltungNameUi,
                                             @NonNull Optional<String> veranstaltungKennungUi, @NonNull Optional<String> raumIdUi) throws IllegalArgumentException, NoContentException {


        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, false, Optional.empty());

        // Ermitteln, in welcher Zeile die Kopfinformationen stehen
        int zeileKopfinformationen = 0;
        Optional<Integer> konfigurationsZeile = Optional.empty();
        if (Arrays.stream(dateiInhalt.getFirst()).toList().getFirst().equals("Veranstaltung")) {
            konfigurationsZeile = Optional.of(0);
        }

        if (konfigurationsZeile.isPresent()) {
            zeileKopfinformationen = 1;
        }

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.get(zeileKopfinformationen)).toList();
        int positionNachname = kopfinformationen.indexOf(spalteBezeichnungNachname);
        int positionVorname = kopfinformationen.indexOf(spalteBezeichnungVorname);
        int positionMatrikelnummer = kopfinformationen.indexOf(spalteBezeichnungKennung);
        Set<Integer> positionenTerminDatum = new HashSet<>();
        for (int i = 0; i < kopfinformationen.size(); i++) {
            String spalte = kopfinformationen.get(i);
            if (spalte.matches(datumRegex)) {
                positionenTerminDatum.add(i);
            }
        }

        String veranstaltungName = veranstaltungNameUi.orElse(null);
        Optional<String> veranstaltungKennung = veranstaltungKennungUi;
        Optional<String> veranstaltungRaumKennung = Optional.empty();
        Optional<String> veranstaltungTerminStart = Optional.empty();
        Optional<String> veranstaltungTerminEnde = Optional.empty();

        // Allgemeine Daten
        if (konfigurationsZeile.isPresent()) {
            List<String> konfigurationsZeileInhalt = Arrays.stream(dateiInhalt.getFirst()).toList();

            veranstaltungName = konfigurationsZeileInhalt.get(1);
            if (konfigurationsZeileInhalt.size() > 2) {
                veranstaltungKennung = Optional.ofNullable(StringUtils.getIfEmpty(konfigurationsZeileInhalt.get(2), () -> null));
                if (konfigurationsZeileInhalt.size() > 3) {
                    veranstaltungTerminStart = Optional.ofNullable(StringUtils.getIfEmpty(konfigurationsZeileInhalt.get(3), () -> null));
                    if (konfigurationsZeileInhalt.size() > 4) {
                        veranstaltungTerminEnde = Optional.ofNullable(StringUtils.getIfEmpty(konfigurationsZeileInhalt.get(4), () -> null));
                        if (konfigurationsZeileInhalt.size() > 5) {
                            veranstaltungRaumKennung = Optional.ofNullable(StringUtils.getIfEmpty(konfigurationsZeileInhalt.get(5), () -> null));
                        }
                    }
                }
            }

        } else {
            // Versuche Daten aus dem Dateinamen zu lesen
            String dateiname = file.getOriginalFilename();
            Pattern veranstaltungPattern = Pattern.compile(veranstaltungNameRegex);
            Matcher matcher = veranstaltungPattern.matcher(dateiname);
            if (matcher.find()) {
                veranstaltungName = matcher.namedGroups().containsKey("name") ? matcher.group("name") : null;
                veranstaltungKennung = Optional.ofNullable(matcher.namedGroups().containsKey("kennung") ? matcher.group("kennung") : null);
            }
        }


        // Ermitteln der Veranstaltung
        Veranstaltung veranstaltung = null;
        if (veranstaltungKennung.isPresent()) {

            // Versuche die Veranstaltung in der Datenbank zu finden
            veranstaltung = veranstaltungRepository.findByKennung(veranstaltungKennung.get()).orElse(null);

            // Wenn die Veranstaltung nicht exisitiert, lege sie an
            if (veranstaltung == null && veranstaltungName != null) {
                veranstaltung = ImportVeranstaltungenService.legeVeranstaltungAn(personRepository, veranstaltungService, veranstaltungName, veranstaltungKennung, Optional.empty(), Optional.empty(), true, true, true, true, Collections.emptyList());
            }
        }

        // Ermitteln des Raums
        Optional<Raum> raum = Optional.empty();
        if(raumIdUi.isPresent()) {
            raum = raumRepository.findById(raumIdUi.get());
            if(raum.isEmpty()) {
                warnmeldungen.add("Der angegeben Raum aus der Oberfläche kann nicht gefunden werden");
            }
        }
        if(raum.isEmpty() && veranstaltungRaumKennung.isPresent()) {
            raum = raumRepository.findByKennung(veranstaltungRaumKennung.get());
            if(raum.isEmpty()) {
                warnmeldungen.add("Der angegeben Raum mit der Kennung '%s' kann nicht gefunden werden.".formatted(veranstaltungRaumKennung.get()));
            }
        }

        if (veranstaltung == null) {
            fehlermeldungen.add("Die Veranstaltung kann nicht importiert werden, da kein Name für die Veranstaltung ermittelt werden kann.");
        }

        if (fehlermeldungen.isEmpty()) {


            Pattern terminDatumPattern = Pattern.compile(datumRegex);
            // Ermittle die Termine
            List<Termin> termineDerVeranstaltung = new ArrayList<>();
            int i = 1;
            for (Integer terminSpalte : positionenTerminDatum) {
                String spaltentitel = kopfinformationen.get(terminSpalte);

                Matcher matcher = terminDatumPattern.matcher(spaltentitel);
                if (matcher.find()) {
                    String datumString = matcher.group("datum");
                    Optional<String> startString = Optional.ofNullable(matcher.namedGroups().containsKey("start") ? matcher.group("start") : null);
                    Optional<String> endeString = Optional.ofNullable(matcher.namedGroups().containsKey("ende") ? matcher.group("ende") : null);

                    String datumStartString = "%s %s".formatted(datumString, startString.orElse(veranstaltungTerminStart.orElse(this.veranstaltungDefaultStart)));
                    String datumEndeString = "%s %s".formatted(datumString, endeString.orElse(veranstaltungTerminEnde.orElse(this.veranstaltungDefaultEnde)));

                    OffsetDateTime datumStart = DatumHelper.toDateTime(datumStartString);
                    OffsetDateTime datumEnde = DatumHelper.toDateTime(datumEndeString);

                    final int terminKennungSuffix = i;
                    Termin termin = Termin.builder()
                            .start(datumStart)
                            .ende(datumEnde)
                            .veranstaltung(veranstaltung)
                            .raeume(raum.map(List::of).orElse(Collections.emptyList()))
                            .build();

                    BaseService.ergaenzeGenerierteKennung(termin);
                    termineDerVeranstaltung.add(termin);
                    i++;
                }

            }

            Map<String, Person> teilnehmer = new HashMap<>();
            // Iteriere über die einzelnen und Lese die Personen
            for (List<String> felder : dateiInhalt.stream().skip(zeileKopfinformationen + 1).map(z -> Arrays.stream(z).toList()).toList()) {

                List<String> fehlermeldungenZeile = new ArrayList<>();
                List<String> warnmeldungenZeile = new ArrayList<>();

                // Ziehe die Informationen aus den Feldern
                String vorname = felder.get(positionVorname).trim();
                String nachname = felder.get(positionNachname).trim();
                String kennung = felder.get(positionMatrikelnummer).trim();

                // Validierung der Felder
                if (StringUtils.isBlank(vorname)) {
                    fehlermeldungenZeile.add("Der Vorname einer Person ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
                }
                if (StringUtils.isBlank(nachname)) {
                    fehlermeldungenZeile.add("Der Nachname einer Person ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
                }
                if (StringUtils.isBlank(kennung)) {
                    fehlermeldungenZeile.add("Die Matrikelnummer einer Person ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
                }

                if (fehlermeldungenZeile.isEmpty()) {
                    Person person = personRepository.findByKennung(kennung).orElse(null);
                    if (person == null) {
                        if (!teilnehmer.containsKey(kennung)) {
                            person = ImportPersonenService.legePersonAn(personenService, vorname, nachname, Optional.of(kennung));
                        } else {
                            warnmeldungenZeile.add("Die Person '%s, %s' mit der Matrikelnummer '%s' kann nicht hinzugefügt werden. Die Kennung wird doppelt vergeben.".formatted(nachname, vorname, kennung));
                        }
                    }
                    if (person != null) {
                        teilnehmer.put(person.getKennung(), person);
                    }
                }

                fehlermeldungen.addAll(fehlermeldungenZeile);
                warnmeldungen.addAll(warnmeldungenZeile);
            }


            List<Person> teilnehmendePersonen = new ArrayList<>();
            for (Person person : teilnehmer.values()) {
                try {
                    if (StringUtils.isBlank(person.getId())) {
                        person = personRepository.save(person);
                    }
                    final String personKennung = person.getKennung();
                    // Wenn noch kein Teilnehmer der Veranstaltung, füge hinzu
                    if (veranstaltung.getTeilnehmendePersonen().stream().map(Person::getKennung).filter(k -> k.equals(personKennung)).findFirst().isEmpty()) {
                        teilnehmendePersonen.add(person);
                        erfolgreicheImporte++;
                    } else {
                        warnmeldungen.add("Die Person '%s, %s' mit der Matrikelnummer '%s' ist bereits Teilnehmer der Veranstaltung.".formatted(person.getNachname(), person.getVorname(), person.getKennung()));
                    }
                } catch (Exception e) {
                    warnmeldungen.add("Die Person '%s, %s' mit der Matrikelnummer '%s' kann nicht hinzugefügt werden.".formatted(person.getNachname(), person.getVorname(), person.getKennung()));
                }
            }
            try {
                teilnehmendePersonen.addAll(veranstaltung.getTeilnehmendePersonen());
                veranstaltung.setTeilnehmendePersonen(teilnehmendePersonen);
                veranstaltung = veranstaltungRepository.save(veranstaltung);

                for (Termin termin : termineDerVeranstaltung) {
                    if(terminRepository.findFirstByStartAndEndeAndVeranstaltungAndRaeume(termin.getStart(), termin.getEnde(), veranstaltung, null).isEmpty()) {
                        try {
                            termin.setVeranstaltung(veranstaltung);
                            terminRepository.save(termin);
                        } catch (Exception e) {
                            warnmeldungen.add("Der Termin mit dem Start '%s' kann nicht gespeichert werden.".formatted(DatumHelper.toDateTime(termin.getStart())));
                        }
                    } else {
                        warnmeldungen.add("Der Termin mit dem Start '%s', dem Ende '%s' zu dieser Veranstaltung und dem Raum gibt es bereits.".formatted(DatumHelper.toDateTime(termin.getStart()), DatumHelper.toDateTime(termin.getEnde())));
                    }
                }

            } catch (Exception e) {
                fehlermeldungen.add("Die Veranstaltung kann nicht gespeichert werden.");
            }

        }


        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.CIS)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1 - zeileKopfinformationen)
                .build();

    }

}
