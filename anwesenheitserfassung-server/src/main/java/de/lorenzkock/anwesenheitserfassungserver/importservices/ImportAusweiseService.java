package de.lorenzkock.anwesenheitserfassungserver.importservices;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisId;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NoContentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Service zum Importieren von Ausweisen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ImportAusweiseService {

    // Service
    private final AusweisService ausweisService;
    private final PersonenService personenService;

    // Repository
    private final AusweisRepository ausweisRepository;
    private final PersonRepository personRepository;

    /**
     * Erzeugt einen Ausweis
     * @param ausweisService Service zur Überprüfung der Einmaligkeit der Kennung
     * @param rfidSnr Seriennummer des Ausweises
     * @param ausweiskennung Kennung des Ausweises
     * @return Erzeugter Ausweis
     */
    public static Ausweis legeAusweisAn(@NonNull AusweisService ausweisService, @NonNull String rfidSnr, @NonNull Optional<String> ausweiskennung) {
        Ausweis i = Ausweis.builder()
                .id(AusweisId.builder().rfidSnr(rfidSnr).kennung(ausweiskennung.orElse(null)).build())
                .aktiv(true)
                .fremdausweis(false)
                .build();
        BaseService.ergaenzeGenerierteKennung(i);
        return i;
    }

    /**
     * Importieren von Ausweisen aus einer Datei
     *
     * @param file                             Datei mit den Daten
     * @param erstelltePersonen                Liste, um auf zuvor erstellte Personen zugreifen zu können
     * @param erstellteAusweiseNachKennung Liste, um erstellte Ausweise anderen Import-Schritten zur Verfügung zu stellen
     * @param erstellteAusweiseNachSnr     Liste, um erstellte Ausweise anderen Import-Schritten zur Verfügung zu stellen
     * @return Ergebnis des Imports
     * @throws IllegalArgumentException Fehler aufgrund fehlender oder falscher Daten
     * @throws NoContentException       Fehler aufgrund von fehlendem Inhalt
     */
    public ImportErgebnis importiereAusweise(@NonNull MultipartFile file, @NonNull Map<String, Person> erstelltePersonen,
                                             Map<String, Ausweis> erstellteAusweiseNachKennung,
                                             Map<String, Ausweis> erstellteAusweiseNachSnr) throws IllegalArgumentException, NoContentException {

        // Validiere das Datei-Format
        ImportService.validiereDateiFormat(file, "text/csv");

        // Lade den Inhalt der CSV-Datei
        List<String[]> dateiInhalt = ImportService.importCsv(file);

        // Validiere, ob es eine Kopfzeile mit den entsprechenden Spalten gibt
        ImportService.validiereDateiInhalt(file.getOriginalFilename(), dateiInhalt, true, Optional.of(List.of("rfidsnr")));

        // Lese die Position der Spalten
        List<String> kopfinformationen = Arrays.stream(dateiInhalt.getFirst()).toList();
        int positionRfidSnr = kopfinformationen.indexOf("rfidsnr");
        int positionKennung = kopfinformationen.indexOf("kennung");
        int positionPersonkennung = kopfinformationen.indexOf("person_kennung");
        int positionPersonVorname = kopfinformationen.indexOf("person_vorname");
        int positionPersonNachname = kopfinformationen.indexOf("person_nachname");

        List<String> fehlermeldungen = new ArrayList<>();
        List<String> warnmeldungen = new ArrayList<>();
        int erfolgreicheImporte = 0;


        // Iteriere über die einzelnen Zeilen
        for (List<String> felder : dateiInhalt.stream().skip(1).map(z -> Arrays.stream(z).toList()).toList()) {

            List<String> fehlermeldungenZeile = new ArrayList<>();
            List<String> warnmeldungenZeile = new ArrayList<>();

            // Ziehe die Informationen aus den Feldern
            String rfidSnr = felder.get(positionRfidSnr).trim();
            Optional<String> kennung = positionKennung >= 0 ? Optional.of(felder.get(positionKennung).trim()) : Optional.empty();
            Optional<String> personkennung = positionPersonkennung >= 0 ? Optional.of(felder.get(positionPersonkennung).trim()) : Optional.empty();
            Optional<String> personVorname = positionPersonVorname >= 0 ? Optional.of(felder.get(positionPersonVorname).trim()) : Optional.empty();
            Optional<String> personNachname = positionPersonNachname >= 0 ? Optional.of(felder.get(positionPersonNachname).trim()) : Optional.empty();

            // Validierung der sonstigen Felder
            if (StringUtils.isBlank(rfidSnr)) {
                fehlermeldungenZeile.add("Die RFID-Seriennummer eines Ausweises ist leer. Sie wurde nicht importiert. Zeile: %s".formatted(String.join(";", felder)));
            }

            // Validierung der Kennung
            if (fehlermeldungenZeile.isEmpty() && !StringUtils.isBlank(kennung.orElse(null))) {
                if (kennung.get().contains(" ")) {
                    fehlermeldungenZeile.add("In der Kennung '%s' sind keine Leerzeichen erlaubt.".formatted(kennung.get()));
                } else {
                    try {
                        ausweisService.pruefeKennungEinmalig(ausweisRepository, kennung.get(), Optional.empty());
                    } catch (IllegalArgumentException e) {
                        fehlermeldungenZeile.add("Die Kennung '%s' für den Ausweis mit der Seriennummer '%s' ist bereits vergeben. Der Ausweis wurde nicht hinzugefügt.".formatted(kennung.get(), rfidSnr));
                    }
                }
            }

            if (fehlermeldungenZeile.isEmpty()) {

                Optional<Person> person = Optional.empty();
                // Versuche einen Ausweis zu ermitteln und die der Person zuzuweisen
                if (!StringUtils.isBlank(personkennung.orElse(null))) {
                    person = personRepository.findByKennung(personkennung.get());
                    // Prüfe, ob der Ausweis existiert
                    if (person.isEmpty()) {
                        if (!StringUtils.isBlank(personVorname.orElse(null)) && !StringUtils.isBlank(personNachname.orElse(null))) {
                            person = Optional.of(ImportPersonenService.legePersonAn(personenService, personVorname.get(), personNachname.get(), personkennung));
                        } else {
                            warnmeldungenZeile.add("Die Person zu dem Ausweis mit der RFID Seriennummer '%s' konnte nicht gefunden werden. Die Zuweisung wurde nicht vorgenommen.".formatted(rfidSnr));
                        }
                    }
                } else {
                    // Wenn keine Kennung angegeben wurde, prüfe, ob eine Person angelegt werden soll
                    if (!StringUtils.isBlank(personVorname.orElse(null)) && !StringUtils.isBlank(personNachname.orElse(null))) {
                        person = Optional.of(ImportPersonenService.legePersonAn(personenService, personVorname.get(), personNachname.get(), personkennung));
                    }
                }

                Ausweis ausweis = legeAusweisAn(ausweisService, rfidSnr, kennung);
                BaseService.ergaenzeGenerierteKennung(ausweis);

                // Deaktivieren alter Ausweise
                Optional<Ausweis> ausweisMitSnrOpt = this.ausweisRepository.findFirstByIdRfidSnrAndAktiv(rfidSnr, true);
                if (ausweisMitSnrOpt.isPresent()) {
                    Ausweis ausweisMitSnr = ausweisMitSnrOpt.get();
                    ausweisMitSnr.setAktiv(false);
                    ausweisRepository.save(ausweisMitSnr);
                }

                try {
                    ausweis = ausweisRepository.save(ausweis);
                    erstellteAusweiseNachKennung.putIfAbsent(ausweis.getKennung(), ausweis);
                    erstellteAusweiseNachSnr.put(ausweis.getRfidSnr(), ausweis);

                    erfolgreicheImporte++;

                    // Schreibe die Person an den Ausweis
                    if (person.isPresent()) {
                        Person p = person.get();
                        p.getAusweise().add(ausweis);
                        p = this.personRepository.save(p);

                        erstelltePersonen.putIfAbsent(p.getKennung(), p);

                        ausweis.setPerson(p);
                        ausweisRepository.save(ausweis);
                    }
                } catch (Exception exception) {
                    fehlermeldungenZeile.add("Beim Importieren des Ausweises mit der Seriennummer '%s' ist ein Fehler aufgetreten.".formatted(rfidSnr));
                }

            }

            fehlermeldungen.addAll(fehlermeldungenZeile);
            warnmeldungen.addAll(warnmeldungenZeile);
        }

        // Zurückgeben des Ergebnisses
        return ImportErgebnis.builder()
                .objektTyp(ImportErgebnis.ImportObjekt.AUSWEIS)
                .warnmeldungen(warnmeldungen)
                .fehlermeldungen(fehlermeldungen)
                .erfolgreicheImporte(erfolgreicheImporte)
                .anzahlGesamt(dateiInhalt.size() - 1)
                .build();

    }


}
