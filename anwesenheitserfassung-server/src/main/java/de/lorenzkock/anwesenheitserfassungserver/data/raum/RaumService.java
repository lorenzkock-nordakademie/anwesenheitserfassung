package de.lorenzkock.anwesenheitserfassungserver.data.raum;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum.RaumBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Service für die Verarbeitung von Räumen
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class RaumService extends BaseService {

    final private RaumRepository raumRepo;

    /**
     * Ermittelt eine Liste von Räumen zu einem gegebenen Filter
     *
     * @param bezeichnung Bezeichnung des Raums
     * @param kennung      Kennung des Raums
     * @param archiv      Archiv
     * @return Liste von ermittelten Räumen
     */
    public List<Raum> getRaeumeMitFilter(@NonNull final Optional<String> bezeichnung, @NonNull final Optional<String> kennung, @NonNull final Optional<Boolean> archiv) {
        return raumRepo.findAll()
                .stream()
                .filter(r -> StringUtils.isBlank(bezeichnung.orElse(null))
                        || r.getBezeichnung().toLowerCase().contains(bezeichnung.get().toLowerCase().trim()))
                .filter(r -> StringUtils.isBlank(kennung.orElse(null))
                        || r.getKennung().toLowerCase().contains(kennung.get().toLowerCase().trim()))
                .filter(r -> archiv.isEmpty() || archiv.get() == r.isArchiv())
                .sorted(Comparator.comparing(Raum::getBezeichnung))
                .toList();
    }

    /**
     * Ergänze einen Raum um die gegebenen Daten
     *
     * @param raum        Raum, der ergänzt werden soll
     * @param bezeichnung Bezeignung für den Raum
     * @param kennung      Kennung für den Raum
     * @return Angepasster Raum
     * @throws IllegalArgumentException Fehler
     */
    public Raum ergaenzeRaum(@NonNull Raum raum, @NonNull String bezeichnung, @NonNull Optional<String> kennung)
            throws IllegalArgumentException {
        RaumBuilder<?, ?> builder = raum.toBuilder().bezeichnung(bezeichnung);

        if (!StringUtils.isBlank(kennung.orElse(null))) {
            builder.kennung(kennung.get());
        }
        return builder.build();
    }

}
