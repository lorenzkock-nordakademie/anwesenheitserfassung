package de.lorenzkock.anwesenheitserfassungserver.data.raum;

import de.lorenzkock.anwesenheitserfassungserver.data.EntityMitKennungRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaumRepository extends EntityMitKennungRepository<Raum, String> {

}
