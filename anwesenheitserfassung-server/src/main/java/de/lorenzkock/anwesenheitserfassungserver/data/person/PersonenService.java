package de.lorenzkock.anwesenheitserfassungserver.data.person;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person.PersonBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Service für die Verarbeitung von Personen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class PersonenService extends BaseService {

    // Repository
    final private PersonRepository personRepo;

    /**
     * Ermittle eine Liste von Personen anhand eines gegebenen Filters
     *
     * @param vorname  Vorname der Person
     * @param nachname Nachname der Person
     * @param kennung   Kennung der Person
     * @param archiv   Information, ob archivierte Personen ermittelt werden sollen
     * @return Liste von Personen
     */
    public List<Person> getPersonenMitFilter(@NonNull final Optional<String> vorname, @NonNull final Optional<String> nachname,
                                             @NonNull final Optional<String> kennung, @NonNull final Optional<Boolean> archiv) {
        return personRepo.findAll()
                .stream()
                .filter(p -> StringUtils.isBlank(vorname.orElse(null))
                        || p.getVorname().toLowerCase().contains(vorname.get().toLowerCase().trim()))
                .filter(p -> StringUtils.isBlank(nachname.orElse(null))
                        || p.getNachname().toLowerCase().contains(nachname.get().toLowerCase().trim()))
                .filter(p -> StringUtils.isBlank(kennung.orElse(null))
                        || p.getKennung().toLowerCase().contains(kennung.get().toLowerCase().trim()))
                .filter(p -> archiv.isEmpty() || archiv.get() == p.isArchiv())
                .sorted(Comparator.comparing(Person::getNachname).thenComparing(Person::getVorname))
                .toList();
    }

    /**
     * Ermittelt alle Personen, die keinen Benutzer zugewiesen haben
     *
     * @return Liste von Personen ohne Benutzer
     */
    public List<Person> getAktivePersonenOhneBenutzer() {
        return personRepo.findByBenutzerAndArchiv(null, Boolean.FALSE);
    }

    /**
     * Ermittle die Person zu einem Benutzer
     *
     * @param benutzer Benutzer, zu dem die Person ermittelt werden soll
     * @return Ermittelte Person
     */
    public Optional<Person> getPersonZuBenutzer(@NonNull Benutzer benutzer) {
        return personRepo.findByBenutzer(benutzer);
    }

    /**
     * Ergänze die Daten zu einer Person
     *
     * @param person   Person, die ergänzt werden soll
     * @param vorname  Vorname der Person
     * @param nachname Nachname der Person
     * @param kennung   Kennung der Person
     * @throws IllegalArgumentException Fehler
     */
    public Person ergaenzePerson(@NonNull Person person, @NonNull String vorname, @NonNull String nachname, @NonNull Optional<String> kennung)
            throws IllegalArgumentException {

        PersonBuilder<?, ?> personBuilder = person.toBuilder();
        personBuilder.vorname(vorname);
        personBuilder.nachname(nachname);
        if (!StringUtils.isBlank(kennung.orElse(null))) {
            personBuilder.kennung(kennung.get());
        }
        return personBuilder.build();

    }


}
