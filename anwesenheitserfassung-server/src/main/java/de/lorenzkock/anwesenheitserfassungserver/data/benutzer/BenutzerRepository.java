package de.lorenzkock.anwesenheitserfassungserver.data.benutzer;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BenutzerRepository extends JpaRepository<Benutzer, String> {
	Optional<Benutzer> findByBenutzernameIgnoreCase(String benutzername);

}
