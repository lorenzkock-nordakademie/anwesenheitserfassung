package de.lorenzkock.anwesenheitserfassungserver.data.person;

import java.util.List;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungserver.data.EntityMitKennungRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;

@Repository
public interface PersonRepository extends EntityMitKennungRepository<Person, String> {

	List<Person> findByBenutzerAndArchiv(Benutzer benutzer, Boolean archiv);

	Optional<Person> findByBenutzer(Benutzer benutzer);

}
