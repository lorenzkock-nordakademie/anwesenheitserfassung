package de.lorenzkock.anwesenheitserfassungserver.data.benutzer;

import java.util.Optional;

public enum Berechtigung {

    AUSWEIS_BESTAETIGEN,
    ANWESENHEIT_NACHTRAGEN,
    ALLE_ANWESENHEITEN_EINSEHEN,
    VERWALTUNG_PERSONEN,
    VERWALTUNG_AUSWEISE,
    VERWALTUNG_VERANSTALTUNGEN,
    VERWALTUNG_RAEUME,
    VERWALTUNG_GERAET,
    SCHNITTSTELLE_EXTERN,
    SCHNITTSTELLE_INTERN,
    ADMINISTRATION;

    public static Optional<Berechtigung> valueOfOpt(String rolle) {
        try {
            return Optional.of(Berechtigung.valueOf(rolle));
        } catch (Exception e) {
            return Optional.empty();
        }

    }

}
