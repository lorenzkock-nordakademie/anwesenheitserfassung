package de.lorenzkock.anwesenheitserfassungserver.data.ausweis;

import de.lorenzkock.anwesenheitserfassungserver.data.IdEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.KennungEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungAusweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Comparator;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@Entity(name = "ausweis")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Ausweis implements IdEntity<AusweisId>, KennungEntity<AusweisId> {

    @EmbeddedId
    @EqualsAndHashCode.Include
    private AusweisId id;

    @Column(name = "aktiv")
    private boolean aktiv;

    @Column(name = "fremdausweis")
    private boolean fremdausweis;

    @JoinColumn(name = "person_id")
    @ManyToOne
    private Person person;

    @OneToMany(mappedBy = "ausweis")
    private List<ErfassungAusweis> erfassungen;

    @ManyToMany(mappedBy = "berechtigteAusweise")
    private List<Veranstaltung> berechtigteVeranstaltungen;

    @ManyToMany(mappedBy = "teilnehmendeAusweise")
    private List<Veranstaltung> teilnehmendeVeranstaltungen;

    @ManyToMany(mappedBy = "berechtigteAusweise")
    private List<Termin> berechtigteTermine;

    @ManyToMany(mappedBy = "teilnehmendeAusweise")
    private List<Termin> teilnehmendeTermine;

    @ManyToOne
    private Ausweis eigenausweis;

    @OneToMany(mappedBy = "eigenausweis")
    private List<Ausweis> fremdausweise;

    public String getKennung() {
        return this.getId().getKennung();
    }

    public void setKennung(String kennung) {
        this.getId().setKennung(kennung);
    }

    public String getRfidSnr() {
        return this.getId().getRfidSnr();
    }

    public void setRfidSnr(String rfidSnr) {
        this.getId().setRfidSnr(rfidSnr);
    }

    public List<Veranstaltung> getTeilnehmendeVeranstaltungenSorted() {
        return this.teilnehmendeVeranstaltungen.stream().sorted(Comparator.comparing(Veranstaltung::getName)).toList();
    }

    public List<Veranstaltung> getBerechtigteVeranstaltungenSorted() {
        return this.berechtigteVeranstaltungen.stream().sorted(Comparator.comparing(Veranstaltung::getName)).toList();
    }

    public List<Termin> getTeilnehmendeTermineSorted() {
        return this.teilnehmendeTermine.stream()
                .sorted(Comparator.comparing(Termin::getStart).thenComparing(Termin::getEnde))
                .toList();
    }

    public List<Termin> getBerechtigteTermineSorted() {
        return this.teilnehmendeTermine.stream()
                .sorted(Comparator.comparing(Termin::getStart).thenComparing(Termin::getEnde))
                .toList();
    }

}
