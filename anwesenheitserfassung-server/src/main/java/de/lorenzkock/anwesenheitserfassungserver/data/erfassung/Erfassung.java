package de.lorenzkock.anwesenheitserfassungserver.data.erfassung;

import java.time.OffsetDateTime;

import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@SuperBuilder
public class Erfassung {

	private OffsetDateTime zeitpunkt;

	private Person person;

	private Ausweis ausweis;

	private Termin termin;

	boolean mitKarte;

	boolean onlineAnwesend;

	private Raum raum;

}
