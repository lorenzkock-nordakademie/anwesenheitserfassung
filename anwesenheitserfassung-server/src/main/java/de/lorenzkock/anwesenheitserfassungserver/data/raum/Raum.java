package de.lorenzkock.anwesenheitserfassungserver.data.raum;

import de.lorenzkock.anwesenheitserfassungserver.data.ArchivEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.IdEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.KennungEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@Entity(name = "raum")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Raum implements KennungEntity<String>, ArchivEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NonNull
    @Column(name = "bezeichnung")
    private String bezeichnung;

    @Column(name = "kennung")
    private String kennung;

    @Column(name = "archiv")
    private boolean archiv;

    @ManyToMany(mappedBy = "raeume")
    @Builder.Default
    private List<Termin> zugeordneteTermine = new ArrayList<>();

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Raum other = (Raum) obj;
        return Objects.equals(id, other.id);
    }

}