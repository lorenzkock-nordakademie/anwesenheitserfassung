package de.lorenzkock.anwesenheitserfassungserver.data.benutzer;

import de.lorenzkock.anwesenheitserfassungserver.data.IdEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.StringListRoleConverter;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@Entity(name = "benutzer")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Benutzer implements IdEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NonNull
    @Column(name = "benutzername")
    private String benutzername;

    @NonNull
    @Column(name = "passwort")
    private String passwort;

    @Column(name = "berechtigungen")
    @Convert(converter = StringListRoleConverter.class)
    private List<Berechtigung> berechtigungen;

    @OneToOne
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    private Person person;

}
