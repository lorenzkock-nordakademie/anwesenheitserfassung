package de.lorenzkock.anwesenheitserfassungserver.data;

/**
 * Interface für Entitäten, mit einer ID
 * @author Lorenz Kock
 */
public interface IdEntity<ID> {

    ID getId();

}
