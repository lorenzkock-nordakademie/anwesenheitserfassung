package de.lorenzkock.anwesenheitserfassungserver.data.person;

import de.lorenzkock.anwesenheitserfassungserver.data.ArchivEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.KennungEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungPerson;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.*;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@Entity(name = "person")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Person implements KennungEntity<String>, ArchivEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    @EqualsAndHashCode.Include
    private String id;

    @NonNull
    @Column(name = "vorname")
    private String vorname;

    @NonNull
    @Column(name = "nachname")
    private String nachname;

    @Column(name = "kennung")
    private String kennung;

    @Column(name = "archiv")
    private boolean archiv;

    @ManyToMany(mappedBy = "berechtigtePersonen")
    @Builder.Default
    private List<Veranstaltung> berechtigteVeranstaltungen = new ArrayList<>();

    @ManyToMany(mappedBy = "teilnehmendePersonen")
    @Builder.Default
    private List<Veranstaltung> teilnehmendeVeranstaltungen = new ArrayList<>();

    @ManyToMany(mappedBy = "berechtigtePersonen")
    @Builder.Default
    private List<Termin> berechtigteTermine = new ArrayList<>();

    @ManyToMany(mappedBy = "teilnehmendePersonen")
    @Builder.Default
    private List<Termin> teilnehmendeTermine = new ArrayList<>();

    @OneToMany(mappedBy = "person")
    @Builder.Default
    private List<Ausweis> ausweise = new ArrayList<>();

    @OneToMany(mappedBy = "person")
    @Builder.Default
    private List<ErfassungPerson> erfassungenZuPerson = new ArrayList<>();

    @OneToOne(mappedBy = "person")
    private Benutzer benutzer;

    public List<Ausweis> getAusweiseSorted() {
        return this.ausweise.stream()
                .sorted(Comparator.comparing(Ausweis::getRfidSnr)
                        .thenComparing(Ausweis::isAktiv)
                        .reversed()
                        .thenComparing(Ausweis::getKennung))
                .toList();
    }

    public List<Veranstaltung> getTeilnehmendeVeranstaltungenSorted() {
        return this.teilnehmendeVeranstaltungen.stream().sorted(Comparator.comparing(Veranstaltung::getName)).toList();
    }

    public List<Veranstaltung> getBerechtigteVeranstaltungenSorted() {
        return this.berechtigteVeranstaltungen.stream().sorted(Comparator.comparing(Veranstaltung::getName)).toList();
    }

    public List<Termin> getTeilnehmendeTermineSorted() {
        return this.teilnehmendeTermine.stream()
                .sorted(Comparator.comparing(Termin::getStart).thenComparing(Termin::getEnde))
                .toList();
    }

    public List<Termin> getBerechtigteTermineSorted() {
        return this.berechtigteTermine.stream()
                .sorted(Comparator.comparing(Termin::getStart).thenComparing(Termin::getEnde))
                .toList();
    }

    public List<Termin> getAlleBerechtigenTermine() {

        // Alle direkten Termine
        Set<Termin> termine = new HashSet<>(this.getBerechtigteTermine());

        // Alle Termine über berechtigte Veranstaltungen
        this.getBerechtigteVeranstaltungen().stream().map(Veranstaltung::getTermine).forEach(termine::addAll);

        return termine.stream().toList();
    }

    public String getName() {
        return String.format("%s %s", this.vorname, this.nachname);
    }

}