package de.lorenzkock.anwesenheitserfassungserver.data;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@SuperBuilder
@Getter
@NoArgsConstructor
public class ImportErgebnis {

    private ImportObjekt objektTyp;

    @Builder.Default
    private List<String> fehlermeldungen = new ArrayList<>();

    @Builder.Default
    private List<String> warnmeldungen = new ArrayList<>();

    private int anzahlGesamt;
    private int erfolgreicheImporte;

    public String getSuccessString() {
        return "Für den Typ '%s' wurden %s von %s Einträgen erfolgreich importiert. Es gibt %s Fehler und %s Hinweise."
                .formatted(getObjektTyp().getBezeichnung(), erfolgreicheImporte,
                        anzahlGesamt, getFehlermeldungen().size(), getWarnmeldungen().size());
    }

    @Getter
    public enum ImportObjekt {

        PERSON("Person"),
        RAUM("Raum"),
        AUSWEIS("Ausweis"),
        VERANSTALTUNG("Veranstaltung"),
        TERMIN("Termin"),
        ZUORDNUNG("Zuordnungen Personen"),
        CIS("CIS");

        private final String bezeichnung;

        ImportObjekt(String bezeichnung) {
            this.bezeichnung = bezeichnung;
        }

    }


}
