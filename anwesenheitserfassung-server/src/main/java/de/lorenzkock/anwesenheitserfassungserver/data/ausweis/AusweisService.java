package de.lorenzkock.anwesenheitserfassungserver.data.ausweis;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.enums.AusweisZuPersonZuweisenValidierungsErgebnis;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für die Verarbeitung von Ausweisdaten
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class AusweisService extends BaseService {

    // Repositories
    final private AusweisRepository ausweisRepository;
    final private PersonRepository personRepository;

    /**
     * Ermittelt alle Ausweise mit einem gegebenen Filter
     *
     * @param rfidSnr         	Zu berücksichtigende Seriennummern von Ausweisen
     * @param kennung         	Zu berücksichtigende Kennungen von Ausweisen
     * @param personIds       	Zu berücksichtigende IDs von Personen
     * @param personKennungen 	Zu berücksichtigende Kennungen von Personen
     * @param nurAktive      	Information, ob nur aktive Ausweise ermittelt werden sollen
     * @param nurEigenausweise	Information, ob nur aktive Ausweise ermittelt werden sollen
     * @return Liste der Ausweise mit dem Filter
     */
    public List<Ausweis> findeAusweiseNachFilter(final String rfidSnr, final String kennung,
                                                       final List<String> personIds, final List<String> personKennungen, final Boolean nurAktive, final Boolean nurEigenausweise) {

        return ausweisRepository.findAll()
                .stream()
                .filter(i -> StringUtils.isBlank(rfidSnr)
                        || i.getId().getRfidSnr().toLowerCase().contains(rfidSnr.toLowerCase()))
                .filter(i -> StringUtils.isBlank(kennung)
                        || i.getId().getKennung().toLowerCase().contains(kennung.toLowerCase()))
                .filter(i -> personIds == null || personIds.isEmpty()
                        || (i.getPerson() != null && personIds.contains(i.getPerson().getId()))
                        || (personIds.contains("-") && i.getPerson() == null))
                .filter(i -> personKennungen == null || personKennungen.isEmpty()
                        || (i.getPerson() != null && personKennungen.contains(i.getPerson().getKennung()))
                        || (personKennungen.contains("-") && i.getPerson() == null))
                .filter(i -> nurAktive == null || nurAktive == i.isAktiv())
                .filter(i -> nurEigenausweise == null || i.isFremdausweis() != nurEigenausweise)
                .sorted(Comparator.comparing(Ausweis::getKennung))
                .toList();
    }

    /**
     * Findet den aktuellen Ausweis zu einer RFID Seriennummer
     *
     * @param rfidSnr RFID Seriennummer
     * @return Aktueller Ausweis mit der Seriennummer
     */
    public Optional<Ausweis> getAktuellenAusweisMitSnr(@NonNull final String rfidSnr) {
        return ausweisRepository.findFirstByIdRfidSnrAndAktiv(rfidSnr, true);
    }

    /**
     * Iteriert über die Fremdausweise eines gegebenen Ausweises und setzt zu denen jeweils
     * die Person gleich der Person des Eigenausweises
     *
     * @param ausweis Eigenausweis, zu der die Fremdausweise verarbeitet werden sollen
     */
    public void pruefeFremdausweisePersonZuordnung(@NonNull Ausweis ausweis) {
        if (ausweis.getPerson() != null) {
            // Setze zu allen Fremdausweise zu dem Ausweis die gleiche Person
            ausweisRepository.saveAll(ausweis.getFremdausweise()
                    .stream()
                    .filter(i -> i.getPerson() == null)
                    .peek(i -> i.setPerson(ausweis.getPerson()))
                    .toList());
        }
    }

    /**
     * Erstelle einen Fremdausweis anhand einer gegebenen Seriennummer
     *
     * @param rfidSnr Seriennummer des Fremdausweises
     * @return Erstellter Fremdausweis
     * @throws IllegalArgumentException Fehler
     */
    public Ausweis fuegeFremdausweisHinzu(@NonNull String rfidSnr) throws IllegalArgumentException {

        // Prüfe, ob genau dieser Ausweis schön existiert
        ausweisRepository.findFirstByIdRfidSnr(rfidSnr).ifPresent(i -> {
            throw new IllegalArgumentException("Ein Ausweis zu der RFID-Snr '%s' existiert bereits.".formatted(rfidSnr));
        });

        // Anlage des Ausweises
        Ausweis ausweisZuErstellen = Ausweis.builder()
                .id(AusweisId.builder().rfidSnr(rfidSnr).build())
                .fremdausweis(true)
                .aktiv(true)
                .build();

        // Ggf. Kennung erzeugen, wenn keine gesetzt
        BaseService.ergaenzeGenerierteKennung(ausweisZuErstellen);

        return ausweisRepository.save(ausweisZuErstellen);

    }


    /**
     * Weist einem nicht zugewiesenen Ausweis eine Person zu
     *
     * @param rfidSrn         Seriennummer, der eine Person zugewiesen werden soll
     * @param personId        ID der Person, die zugewiesen werden soll
     * @param fremdausweis Information, ob der Ausweis ein Fremdausweis ist
     * @return Ergebnis der Verarbeitung
     */
    public AusweisZuPersonZuweisenValidierungsErgebnis ausweisZuweisen(@NonNull String rfidSrn, @NonNull String personId,
                                                                       boolean fremdausweis) {

        // Ermittle die Person die zugeordnet werden soll
        Person person = personRepository.findById(personId).orElse(null);
        if (person == null) {
            return AusweisZuPersonZuweisenValidierungsErgebnis.NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN;
        }

        // Ermittle den Ausweis
        Ausweis ausweis = ausweisRepository.findFirstByIdRfidSnrAndAktiv(rfidSrn, true).orElse(null);
        if (ausweis == null) {
            return AusweisZuPersonZuweisenValidierungsErgebnis.NICHT_ERFOLGREICH_AUSWEIS_NICHT_GEFUNDEN;
        }

        if (ausweis.getEigenausweis() != null || ausweis.getPerson() != null) {
            return AusweisZuPersonZuweisenValidierungsErgebnis.NICHT_ERFOLGREICH_AUSWEIS_BEREITS_ZUGEWIESEN;
        }

        ausweisRepository.save(ausweis.toBuilder().person(person).fremdausweis(fremdausweis).build());
        return AusweisZuPersonZuweisenValidierungsErgebnis.ERFOLGREICH;

    }

    /**
     * Prüft bei der Zuweisung einer Person zu einem Ausweis, ob zuvor eine andere Person zugewiesen war.
     * Ansonsten wird zu Zuweisung durchgeführt
     *
     * @param ausweis    Ausweis, zu dem die Person zugewiesen werden soll
     * @param personId      ID der Person, die zugewiesen werden soll
     * @param personKennung Kennung der Person, die zugewiesen werden soll
     */
    public void pruefePersonBeiBearbeitenUndWeiseZu(@NonNull Ausweis ausweis, @NonNull Optional<String> personId, @NonNull Optional<String> personKennung) {
        if (!StringUtils.isBlank(personId.orElse(null)) || !StringUtils.isBlank(personKennung.orElse(null))) {
            if (ausweis.getPerson() == null) {
                // Zuweisung der Person, keine Personenkennung gegeben, da durch UI immer ID
                // gegeben ist, wenn eine Person ausgewählt ist
                this.personZuweisen(ausweis, personId, personKennung);
            } else {
                // Wenn die Personen nicht übereinstimmen, wurde eine neue Person ausgewählt.
                // Dies ist nicht zulässig.
                if ((!StringUtils.isBlank(personId.orElse(null)) && !ausweis.getPerson().getId().equals(personId.get()))
                        || (!StringUtils.isBlank(personKennung.orElse(null))
                        && !ausweis.getPerson().getKennung().equals(personKennung.get()))) {
                    throw new IllegalArgumentException(
                            "Diesem Ausweis ist bereits eine Person zugewiesen. Bitte neuen Ausweis anlegen.");
                }
            }
        }
    }

    /**
     * Weist eine Person einem Ausweis zu
     *
     * @param ausweis    Ausweis, dem die Person zugewiesen werden soll
     * @param personId      ID der Person, die dem Ausweis zugewiesen werden soll
     * @param personKennung Kennung der Person, die dem Ausweis zugewiesen werden soll
     * @throws IllegalArgumentException Fehler
     */
    public void personZuweisen(@NonNull Ausweis ausweis, @NonNull Optional<String> personId, @NonNull Optional<String> personKennung)
            throws IllegalArgumentException {
        if (!StringUtils.isBlank(personId.orElse(null))) {
            final Person person = this.personRepository.findById(personId.get())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte keine Person mit der ID '%s' gefunden werden.".formatted(personId)));
            ausweis.setPerson(person);
        }
        if (!StringUtils.isBlank(personKennung.orElse(null))) {
            final Person person = this.personRepository.findByKennung(personKennung.get())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte keine Person mit der Kennung '%s' gefunden werden.".formatted(personKennung)));
            ausweis.setPerson(person);
        }
    }

}
