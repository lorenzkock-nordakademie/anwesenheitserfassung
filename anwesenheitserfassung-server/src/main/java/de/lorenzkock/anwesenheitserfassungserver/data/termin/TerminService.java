package de.lorenzkock.anwesenheitserfassungserver.data.termin;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.OffsetBasedPageRequest;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.*;

/**
 * Service für die Verarbeitung von Terminen
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class TerminService extends BaseService {

    // Repository
    final private TerminRepository terminRepo;
    final private RaumRepository raumRepository;
    final private PersonRepository personRepository;
    final private VeranstaltungRepository veranstaltungRepository;

    // Mapper
    final private PersonMapper personMapper;

    /*
     * Allgemeine CRUD Methoden
     */

    /**
     * Ermittle alle Termine zu einem Benutzer und der damit verbundenen Person
     *
     * @param benutzer Benutzer, zu dem die Termine ermittelt werden sollen
     * @return Termine des Benuzers
     */
    public List<Termin> getAlleTermineFuerBenutzer(Benutzer benutzer) {
        // Ermittle die Peerson
        Optional<Person> person = personRepository.findByBenutzer(benutzer);

        // Ermittle die Termine der Person
        return person.map(Person::getAlleBerechtigenTermine).orElse(Collections.emptyList());
    }

    /**
     * Filtere Termine nach ihren Eigenschaften
     *
     * @param termine          Liste von Terminen, die zu filtern sind
     * @param veranstaltungIds Liste von IDs zu Veranstaltungen
     * @param raumIds          Liste von IDs zu Räumen
     * @param vergangene       Information, welche Termine eingebunden werden sollen -1 = alle, 1 = Nur vergangene, 0 = Nur nicht vergangene
     * @param zeitraum         Datum, das im Zeitraum des Termins liegen muss
     * @return Liste von Terminen
     */
    public List<Termin> filterTermine(@NonNull List<Termin> termine, @NonNull List<String> veranstaltungIds,
                                      @NonNull List<String> raumIds, @NonNull Integer vergangene,
                                      @NonNull Optional<String> zeitraum) {

        Boolean vergangeneBool = switch (vergangene) {
            case -1 -> null;
            case 1 -> Boolean.TRUE;
            default -> Boolean.FALSE;
        };

        return termine.stream()
                .filter(termin -> veranstaltungIds.isEmpty() || veranstaltungIds.contains(termin.getVeranstaltung().getId()))
                .filter(termin -> raumIds.isEmpty() || (termin.getRaeume().stream().map(Raum::getId).anyMatch(raumIds::contains)))
                .filter(termin -> vergangeneBool == null || vergangeneBool == termin.getEnde().isBefore(OffsetDateTime.now()))
                .filter(termin -> zeitraum.isEmpty() || (DatumHelper.istDatumInZeitspanne(DatumHelper.localDateFromString(zeitraum.get()), termin.getStart().toLocalDate(), termin.getEnde().toLocalDate())))
                .sorted(Comparator.comparing(Termin::getStart))
                .toList();
    }

    /**
     * Löscht einen Termin zu einer Kennung. Es dürfen keine Erfassungen zu dem Termin existieren.
     *
     * @param terminKennung Kennung des Termins
     * @return Veranstaltung, zu der der Termin gehörte
     */
    public Veranstaltung entferneTerminMitKennung(@NonNull String terminKennung) {
        Termin terminZuLoeschen = terminRepo.findByKennung(terminKennung)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Termin mit der Kennung '%s' gefunden.".formatted(terminKennung)));
        return entferneTermin(terminZuLoeschen);
    }

    /**
     * Löscht einen Termin zu einer ID. Es dürfen keine Erfassungen zu dem Termin existieren.
     *
     * @param terminId ID des Termins
     * @return Veranstaltung, zu der der Termin gehörte
     */
    public Veranstaltung entferneTerminMitId(@NonNull String terminId) {
        Termin terminZuLoeschen = terminRepo.findById(terminId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Termin mit der ID '%s' gefunden.".formatted(terminId)));
        return entferneTermin(terminZuLoeschen);
    }

    /**
     * Löscht einen Termin. Es dürfen keine Erfassungen zu dem Termin existieren.
     *
     * @param termin Termin, der gelöscht werden soll
     * @return Veranstaltung, zu der der Termin gehörte
     */
    public Veranstaltung entferneTermin(@NonNull Termin termin) {
        // Löschen nur erlaubt, wenn keine Erfassungen dazu existieren
        if (termin.getAusweisErfassungen().isEmpty() && termin.getPersonErfassungen().isEmpty()) {
            final String veranstaltungId = termin.getVeranstaltung().getId();
            this.terminRepo.delete(termin);
            return veranstaltungRepository.findById(veranstaltungId).get();
        } else {
            throw new IllegalStateException("Zu dem Termin existieren bereits Erfassungen.");
        }
    }

    /**
     * Ergänze einen Termin um die gegebenen Daten
     *
     * @param termin        Termin, der ergänzt werden soll
     * @param start         Start des Termins. Unverändert, wenn leer
     * @param ende          Ende des Termins. Unverändert, wenn leer
     * @param kennung       Kennung des Termins. Unverändert, wenn leer
     * @param raumIds       IDs der Räume zu dem Termin
     * @param raumKennungen Kennungen der Räume zu dem Termin
     * @return Ergänzter Termin
     * @throws IllegalArgumentException
     */
    public void ergaenzeTermin(Termin termin, Optional<OffsetDateTime> start, Optional<OffsetDateTime> ende,
                               Optional<String> kennung, List<String> raumIds, List<String> raumKennungen)
            throws IllegalArgumentException {

        start.ifPresent(termin::setStart);
        ende.ifPresent(termin::setEnde);

        if (!StringUtils.isBlank(kennung.orElse(null))) {
            termin.setKennung(kennung.get());
        }

        // Zuweisung des Raums
        this.raeumeZuweisen(termin, raumIds, raumKennungen);
    }

    /**
     * Ermittle alle Termine, die nicht abgeschlossen sind mit Filter zu Raum, einer maximalen Anzahl sowie einem Offset
     *
     * @param raumId    ID des Raums, in dem der Termin stattdinden soll
     * @param limitOpt  Maximale Anzahl an Terminen. Default 100
     * @param offsetOpt Anzahl an Terminen, die übersprungen werden sollen. Default 0
     * @return Liste an Terminen zu dem Filter
     */
    public List<Termin> getAktiveNichtAbgeschlosseneTermineMitFilter(@NonNull Optional<String> raumId,
                                                                     @NonNull Optional<Integer> limitOpt,
                                                                     @NonNull Optional<Integer> offsetOpt) {
        int limit = limitOpt.orElse(100);
        int offset = offsetOpt.orElse(0);
        if (raumId.isEmpty()) {
            return terminRepo
                    .findByEndeGreaterThan(OffsetDateTime.now(),
                            new OffsetBasedPageRequest(limit, offset, Sort.by(Sort.Direction.ASC, "start", "ende")))
                    .toList();
        } else {
            Raum raum = raumRepository.findById(raumId.get())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Der Raum mit der ID '%s' wurde nicht gefunden.".formatted(raumId)));
            return terminRepo
                    .findByRaeumeAndEndeGreaterThan(raum, OffsetDateTime.now(),
                            new OffsetBasedPageRequest(limit, offset, Sort.by(Sort.Direction.ASC, "start", "ende")))
                    .toList();
        }
    }

    /**
     * Ermittle alle Termine, die aktuell stattfinden. Optional kann ein Raum angegeben werden.
     *
     * @param raumId Optional: ID des Raums, zu dem Termine ermittelt werden sollen
     * @return Liste von Terminen
     */
    public List<Termin> getAktuelleTermineMitFilter(@NonNull Optional<String> raumId) {
        if (raumId.isEmpty()) {
            return terminRepo.findByStartLessThanAndEndeGreaterThan(OffsetDateTime.now(), OffsetDateTime.now());
        } else {
            Raum raum = raumRepository.findById(raumId.get())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Der Raum mit der ID '%s' wurde nicht gefunden.".formatted(raumId.get())));
            return terminRepo.findByRaeumeAndStartLessThanAndEndeGreaterThan(raum, OffsetDateTime.now(),
                    OffsetDateTime.now());
        }
    }

    /*
     * Hilfs-Methoden
     */

    /**
     * Weist einen Raum einem Termin zu
     *
     * @param termin        Termin, dem der Raum zugewiesen werden soll
     * @param raumIds       IDs der Räume
     * @param raumKennungen Kennungen der Räume
     * @throws IllegalArgumentException Fehler
     */
    public void raeumeZuweisen(@NonNull Termin termin, List<String> raumIds, List<String> raumKennungen) throws IllegalArgumentException {
        // Wenn raumId und raumKennung null sind, soll Raum des Termins nicht bearbeitet
        // werden.
        if (raumIds == null && raumKennungen == null)
            return;
        if (raumIds != null && !raumIds.isEmpty()) {
            List<Raum> raeume = new ArrayList<>();
            for (String raumId : raumIds) {
                final Raum raum = this.raumRepository.findById(raumId)
                        .orElseThrow(() -> new NoSuchElementException(
                                "Es konnte kein Raum mit der ID '%s' gefunden werden.".formatted(raumId)));
                raeume.add(raum);
            }

            termin.setRaeume(raeume);
            return;
        }
        if (raumKennungen != null && !raumKennungen.isEmpty()) {
            List<Raum> raeume = new ArrayList<>();
            for (String raumKennung : raumKennungen) {
                final Raum raum = this.raumRepository.findByKennung(raumKennung)
                        .orElseThrow(() -> new NoSuchElementException(
                                "Es konnte kein Raum mit der Kennung '%s' gefunden werden.".formatted(raumKennung)));
                raeume.add(raum);
            }

            termin.setRaeume(raeume);
            return;
        }
        // Entferne die Räume, wenn der Raum leer ist
        termin.setRaeume(Collections.emptyList());
    }

    /**
     * Weist eine Veranstaltung einem Termin zu
     *
     * @param termin               Termin, dem der Raum zugewiesen werden soll
     * @param veranstaltungId      ID der Veranstaltung
     * @param veranstaltungKennung Kennung der Veranstaltung
     * @throws IllegalArgumentException Fehler
     */
    public void veranstaltungZuweisen(@NonNull Termin termin, String veranstaltungId, String veranstaltungKennung)
            throws IllegalArgumentException {
        if (!StringUtils.isBlank(veranstaltungId)) {
            final Veranstaltung veranstaltung = this.veranstaltungRepository.findById(veranstaltungId)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden."
                                    .formatted(veranstaltungId)));
            termin.setVeranstaltung(veranstaltung);
        }
        if (!StringUtils.isBlank(veranstaltungKennung)) {
            final Veranstaltung veranstaltung = this.veranstaltungRepository.findByKennung(veranstaltungKennung)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden."
                                    .formatted(veranstaltungKennung)));
            termin.setVeranstaltung(veranstaltung);
        }
    }

    /**
     * Prüft, ob das Start-Datum vor dem Ende-Datum liegt
     *
     * @param start Start Datum
     * @param ende  Ende Datum
     * @throws IllegalArgumentException Fehler, wenn dies nicht der Fall ist
     */
    public void pruefeZeitraum(@NonNull OffsetDateTime start, @NonNull OffsetDateTime ende) throws IllegalArgumentException {
        if (ende.isBefore(start)) {
            throw new IllegalArgumentException("Das Ende darf nicht vor dem Start liegen");
        }
    }
}
