package de.lorenzkock.anwesenheitserfassungserver.data.ausweis;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Embeddable
@EqualsAndHashCode
public class AusweisId implements Serializable {

    @Serial
    private static final long serialVersionUID = 4219072581440615285L;

    @Column(name = "rfid_snr")
    private String rfidSnr;

    @Column(name = "kennung")
    private String kennung;

}
