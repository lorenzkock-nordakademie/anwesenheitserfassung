package de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung;

import de.lorenzkock.anwesenheitserfassungserver.data.ArchivEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.KennungEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@Entity(name = "veranstaltung")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Veranstaltung implements KennungEntity<String>, ArchivEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "kennung")
    private String kennung;

    @NonNull
    @Column(name = "name")
    private String name;

    @Column(name = "archiv")
    private boolean archiv;

    @Column(name = "erlaube_fremdausweis")
    private boolean erlaubeFremdausweis;

    @Column(name = "fremdausweis_bestaetigen")
    private boolean fremdausweisBestaetigen;

    @Column(name = "ein_ausgehende_buchungen")
    private boolean erlaubeEinUndAusgehendeBuchungen;

    @Column(name = "mehrfach_buchungen")
    private boolean erlaubeMehrfachBuchungen;

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "berechtigte_person_veranstaltung", joinColumns = @JoinColumn(name = "veranstaltung_id"), inverseJoinColumns = @JoinColumn(name = "person_id"))
    private List<Person> berechtigtePersonen = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "teilnehmende_person_veranstaltung", joinColumns = @JoinColumn(name = "veranstaltung_id"), inverseJoinColumns = @JoinColumn(name = "person_id"))
    private List<Person> teilnehmendePersonen = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "berechtigte_ausweis_veranstaltung", joinColumns = @JoinColumn(name = "veranstaltung_id"), inverseJoinColumns = {
            @JoinColumn(name = "ausweis_rfid_snr", referencedColumnName = "rfid_snr"),
            @JoinColumn(name = "ausweis_kennung", referencedColumnName = "kennung")})
    private List<Ausweis> berechtigteAusweise = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "teilnehmende_ausweis_veranstaltung", joinColumns = @JoinColumn(name = "veranstaltung_id"), inverseJoinColumns = {
            @JoinColumn(name = "ausweis_rfid_snr", referencedColumnName = "rfid_snr"),
            @JoinColumn(name = "ausweis_kennung", referencedColumnName = "kennung")})
    private List<Ausweis> teilnehmendeAusweise = new ArrayList<>();

    @Builder.Default
    @OneToMany(mappedBy = "veranstaltung")
    private List<Termin> termine = new ArrayList<>();

    public List<Termin> getTermineSorted() {
        return this.termine.stream().sorted(Comparator.comparing(Termin::getStart).thenComparing(Termin::getEnde)).toList();
    }

    public void addTeilnehmendePerson(@NonNull Person person) {
        if(!this.teilnehmendePersonen.stream().map(Person::getKennung).toList().contains(person)) {
            this.teilnehmendePersonen = new ArrayList<>(this.teilnehmendePersonen);
            this.teilnehmendePersonen.add(person);
        }
    }

    public void addBerechtigtePerson(@NonNull Person person) {
        if(!this.berechtigtePersonen.stream().map(Person::getKennung).toList().contains(person)) {
            this.berechtigtePersonen = new ArrayList<>(this.berechtigtePersonen);
            this.berechtigtePersonen.add(person);
        }
    }

    /*
     * Methoden zum Hinzufügen von Berechtigten und Teilnehmenden
     */

    public void fuegeBerechtigtePersonenHinzu(@NonNull List<Person> personen) {

        if (this.berechtigtePersonen == null) {
            this.berechtigtePersonen = personen;
        } else {
            this.berechtigtePersonen
                    .addAll(personen.stream().filter(p -> !this.berechtigtePersonen.contains(p)).toList());
        }

    }

    public void fuegeBerechtigteAusweiseHinzu(@NonNull List<Ausweis> ausweise) {

        if (this.berechtigteAusweise == null) {
            this.berechtigteAusweise = ausweise;
        } else {
            this.berechtigteAusweise
                    .addAll(ausweise.stream().filter(i -> !this.berechtigteAusweise.contains(i)).toList());
        }

    }

    public void fuegeTeilnehmendePersonenHinzu(@NonNull List<Person> personen) {

        if (this.teilnehmendePersonen == null) {
            this.teilnehmendePersonen = personen;
        } else {
            this.teilnehmendePersonen
                    .addAll(personen.stream().filter(p -> !this.teilnehmendePersonen.contains(p)).toList());
        }

    }

    public void fuegeTeilnehmendeAusweiseHinzu(@NonNull List<Ausweis> ausweise) {

        if (this.teilnehmendeAusweise == null) {
            this.teilnehmendeAusweise = ausweise;
        } else {
            this.teilnehmendeAusweise
                    .addAll(ausweise.stream().filter(i -> !this.teilnehmendeAusweise.contains(i)).toList());
        }

    }

    /*
     * Methoden zum Entfernen von Berechtigten und Teilnehmenden
     */

    public void entferneBerechtigtePersonen(@NonNull List<Person> personen) {

        if (this.berechtigtePersonen != null) {
            this.berechtigtePersonen
                    .removeAll(personen.stream().filter(p -> this.berechtigtePersonen.contains(p)).toList());
        }

    }

    public void entferneBerechtigteAusweise(@NonNull List<Ausweis> ausweise) {

        if (this.berechtigteAusweise != null) {
            this.berechtigteAusweise
                    .removeAll(ausweise.stream().filter(i -> this.berechtigteAusweise.contains(i)).toList());
        }

    }

    public void entferneTeilnehmendePersonen(@NonNull List<Person> personen) {

        if (this.teilnehmendePersonen != null) {
            this.teilnehmendePersonen
                    .removeAll(personen.stream().filter(p -> this.teilnehmendePersonen.contains(p)).toList());
        }

    }

    public void entferneTeilnehmendeAusweise(@NonNull List<Ausweis> ausweise) {

        if (this.teilnehmendeAusweise != null) {
            this.teilnehmendeAusweise
                    .removeAll(ausweise.stream().filter(i -> this.teilnehmendeAusweise.contains(i)).toList());
        }

    }

}
