package de.lorenzkock.anwesenheitserfassungserver.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@Getter
@NoArgsConstructor
public class ImportErgebnisCollector {

    private List<String> fehlermeldungen;

    private List<ImportErgebnis> ergebnisse;

}
