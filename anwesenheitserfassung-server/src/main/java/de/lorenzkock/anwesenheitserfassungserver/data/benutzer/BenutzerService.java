package de.lorenzkock.anwesenheitserfassungserver.data.benutzer;

import de.lorenzkock.anwesenheitserfassungserver.config.security.AppUserPrincipal;
import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für den Zugriff und die Verarbeitung von Benutzern
 *
 * @author Lorenz Kock
 */
@Service
@RequiredArgsConstructor
public class BenutzerService extends BaseService implements UserDetailsService {

    // Repositories
    final private BenutzerRepository benutzerRepo;
    final private PersonRepository personRepo;

    /**
     * Ermittelt eine Liste von Benutzern auf Grundlage des gegebenen Filters
     *
     * @param benutzername Benutzername
     * @return Liste von Benutzern zum Filter
     */
    public List<Benutzer> getBenutzerMitFilter(final @NonNull Optional<String> benutzername) {
        return benutzerRepo.findAll()
                .stream()
                .filter(b -> benutzername.isEmpty() || b.getBenutzername().equals(benutzername.get()))
                .sorted(Comparator.comparing(Benutzer::getBenutzername))
                .toList();
    }

    /**
     * Ermittle einen Benutzer zu einem Benutzernamen (case insensitive)
     *
     * @param benutzername Benutzername des Benutzers
     * @return Benutzer
     */
    public Optional<Benutzer> getBenutzerMitBenutzername(@NonNull final String benutzername) {
        return benutzerRepo.findByBenutzernameIgnoreCase(benutzername);
    }

    /**
     * Lösche einen Benutzer
     *
     * @param id ID des Benutzers
     * @throws NoSuchElementException Fehler, wenn Benutzer nicht gefunden wurde
     */
    public void loescheBenutzer(@NonNull String id) throws NoSuchElementException {

        benutzerRepo.findById(id)
                .orElseThrow(() -> new NoSuchElementException(
                        "Ein Benutzer mit der ID '%s' existiert nicht.".formatted(id)));

        benutzerRepo.deleteById(id);

    }

    /**
     * Fügt eine Person zu einem Benutzer hinzu
     *
     * @param benutzer Benutzer, zu dem die Person hinzugefügt werden soll
     * @param personId Person, die hinzugefügt werden soll
     */
    public void fuegePersonHinzu(@NonNull Benutzer benutzer, String personId) {
        if (!StringUtils.isBlank(personId)) {
            Person person = personRepo.findById(personId)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Person mit der ID '%s' wurde nicht gefunden.".formatted(personId)));
            if (person.getBenutzer() != null && !person.getBenutzer().getId().equals(benutzer.getId())) {
                throw new IllegalArgumentException("Die Person ist bereits einem anderen Benutzer zugewiesen.");
            }
            benutzer.setPerson(person);
        } else {
            benutzer.setPerson(null);
        }

    }


    /**
     * Ermittle die Daten eines Benutzers anhand des Benutzernamens
     *
     * @param benutzername Benutzername des Benutzers, der gelesen werden soll
     * @return Details des Benutzers
     * @throws UsernameNotFoundException Fehler, wenn Benutzer nicht gefunden wurde
     */
    @Override
    public UserDetails loadUserByUsername(@NonNull String benutzername) throws UsernameNotFoundException {

        Benutzer benutzer = benutzerRepo.findByBenutzernameIgnoreCase(benutzername)
                .orElseThrow(() -> new UsernameNotFoundException(
                        "Der Benutzer '%s' wurde nicht gefunden.".formatted(benutzername)));
        return new AppUserPrincipal(benutzer);
    }

}
