package de.lorenzkock.anwesenheitserfassungserver.data.erfassung;

import java.time.OffsetDateTime;

import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@Entity(name = "erfassung_ausweis")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ErfassungAusweis {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.UUID)
	private String id;

	@NonNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "termin_id")
	private Termin termin;

	@NonNull
	@ManyToOne(optional = false)
	private Ausweis ausweis;

	@Column(name = "zeitpunkt")
	private OffsetDateTime zeitpunkt;

	@ManyToOne
	private Raum raum;

}
