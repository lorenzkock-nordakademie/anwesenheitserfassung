package de.lorenzkock.anwesenheitserfassungserver.data.erfassung;

import java.time.OffsetDateTime;

import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@Entity(name = "erfassung_person")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ErfassungPerson {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.UUID)
	private String id;

	@NonNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "termin_id")
	private Termin termin;

	@NonNull
	@ManyToOne(optional = false)
	private Person person;

	@Column(name = "zeitpunkt")
	private OffsetDateTime zeitpunkt;

	@ManyToOne
	private Raum raum;

	private boolean onlineTeilnahme;

}
