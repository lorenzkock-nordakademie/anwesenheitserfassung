package de.lorenzkock.anwesenheitserfassungserver.data.termin;

import de.lorenzkock.anwesenheitserfassungserver.data.KennungEntity;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungAusweis;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungPerson;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@Entity(name = "termin")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Termin implements KennungEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "kennung")
    private String kennung;

    @Column(name = "start")
    private OffsetDateTime start;

    @Column(name = "ende")
    private OffsetDateTime ende;

    @Column(name = "erfassung_start")
    private OffsetDateTime erfassungStart;

    @ManyToOne(optional = false)
    @JoinColumn(name = "veranstaltung_id")
    private Veranstaltung veranstaltung;

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "raum_termin", joinColumns = @JoinColumn(name = "termin_id"), inverseJoinColumns = @JoinColumn(name = "raum_id"))
    private List<Raum> raeume = new ArrayList<>();

    @Builder.Default
    @OneToMany(mappedBy = "termin")
    private List<ErfassungAusweis> ausweisErfassungen = new ArrayList<>();

    @Builder.Default
    @OneToMany(mappedBy = "termin")
    private List<ErfassungPerson> personErfassungen = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "berechtigte_person_termin", joinColumns = @JoinColumn(name = "termin_id"), inverseJoinColumns = @JoinColumn(name = "person_id"))
    private List<Person> berechtigtePersonen = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "teilnehmende_person_termin", joinColumns = @JoinColumn(name = "termin_id"), inverseJoinColumns = @JoinColumn(name = "person_id"))
    private List<Person> teilnehmendePersonen = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "berechtigte_ausweis_termin", joinColumns = @JoinColumn(name = "termin_id"), inverseJoinColumns = {
            @JoinColumn(name = "ausweis_rfid_snr", referencedColumnName = "rfid_snr"),
            @JoinColumn(name = "ausweis_kennung", referencedColumnName = "kennung")})
    private List<Ausweis> berechtigteAusweise = new ArrayList<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "teilnehmende_ausweis_termin", joinColumns = @JoinColumn(name = "termin_id"), inverseJoinColumns = {
            @JoinColumn(name = "ausweis_rfid_snr", referencedColumnName = "rfid_snr"),
            @JoinColumn(name = "ausweis_kennung", referencedColumnName = "kennung")})
    private List<Ausweis> teilnehmendeAusweise = new ArrayList<>();

    public String getStartFormatted() {
        return "%s Uhr".formatted(DatumHelper.formatDateTime(start));
    }

    public String getEndeFormatted() {
        return "%s Uhr".formatted(DatumHelper.formatDateTime(ende));
    }

    public String getErfassungStartFormatted() {
        if(erfassungStart == null) return "";
        return "%s".formatted(DatumHelper.formatDateTime(erfassungStart));
    }

    public String getZeitraumFormatted() {
        return "%s - %s Uhr".formatted(DatumHelper.formatDateTime(start), DatumHelper.formatDateTime(ende));
    }

    public String getRaeumeString() {
        if (raeume == null || raeume.isEmpty())
            return "-";
        return String.join(", ", raeume.stream().map(Raum::getBezeichnung).toList());
    }

    public boolean getKannLoeschen() {
        return this.ausweisErfassungen.size() + this.personErfassungen.size() == 0;
    }


    public void addTeilnehmendePerson(@NonNull Person person) {
        if (!this.teilnehmendePersonen.stream().map(Person::getKennung).toList().contains(person)) {
            this.teilnehmendePersonen = new ArrayList<>(this.teilnehmendePersonen);
            this.teilnehmendePersonen.add(person);
        }
    }

    public void addBerechtigtePerson(@NonNull Person person) {
        if (!this.berechtigtePersonen.stream().map(Person::getKennung).toList().contains(person)) {
            this.berechtigtePersonen = new ArrayList<>(this.berechtigtePersonen);
            this.berechtigtePersonen.add(person);
        }
    }

    /*
     * Hilfsmethoden
     */

    /**
     * Gibt eine Liste von allen teilnehmenden Ausweisen. Beinhaltet ebenso die
     * Ausweise über die teilnehmenden Personen sowie die zur Veranstaltung.
     *
     * @return Liste von teilnehmenden Ausweisen
     */
    public List<Ausweis> getAlleTeilnehmendenAusweise(boolean mitAusweisenZuTeilnehmendenPersonen) {
        Set<Ausweis> ausweise = new HashSet<>();
        ausweise.addAll(this.getVeranstaltung().getTeilnehmendeAusweise());
        ausweise.addAll(this.getTeilnehmendeAusweise());
        if(mitAusweisenZuTeilnehmendenPersonen) {
            this.getVeranstaltung()
                    .getTeilnehmendePersonen()
                    .forEach(p -> ausweise.addAll(p.getAusweise()));
            this.getTeilnehmendePersonen().forEach(p -> ausweise.addAll(p.getAusweise()));
        }
        return ausweise.stream().toList();
    }

    /**
     * Gibt eine Liste von allen berechtigten Ausweisen zurück. Beinhaltet ebenso
     * die Ausweise über die berechtigten Personen sowie die zur Veranstaltung.
     *
     * @return Liste von berechtigten Ausweisen
     */
    public List<Ausweis> getAlleBerechtigtenAusweise() {
        Set<Ausweis> ausweise = new HashSet<>();
        ausweise.addAll(this.getVeranstaltung().getBerechtigteAusweise());
        ausweise.addAll(this.getBerechtigteAusweise());
        this.getVeranstaltung()
                .getBerechtigtePersonen()
                .forEach(p -> ausweise.addAll(p.getAusweise()));
        this.getBerechtigtePersonen().forEach(p -> ausweise.addAll(p.getAusweise()));
        return ausweise.stream().toList();
    }

    /**
     * Gibt eine Liste von allen teilnehmenden Personen. Beinhaltet ebenso die
     * Personen über die teilnehmenden Ausweise sowie die zur Veranstaltung.
     *
     * @return Liste von teilnehmenden Ausweisen
     */
    public List<Person> getAlleTeilnehmendenPersonen() {
        Set<Person> personen = new HashSet<>();
        personen.addAll(this.getVeranstaltung().getTeilnehmendePersonen());
        personen.addAll(this.getTeilnehmendePersonen());
        this.getVeranstaltung()
                .getTeilnehmendeAusweise()
                .stream()
                .filter(i -> i.getPerson() != null)
                .forEach(i -> personen.add(i.getPerson()));
        this.getTeilnehmendeAusweise()
                .stream()
                .filter(i -> i.getPerson() != null)
                .forEach(i -> personen.add(i.getPerson()));
        return personen.stream().toList();
    }

}
