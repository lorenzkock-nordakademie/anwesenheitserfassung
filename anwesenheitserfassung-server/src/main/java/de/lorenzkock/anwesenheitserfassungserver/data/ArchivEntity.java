package de.lorenzkock.anwesenheitserfassungserver.data;

/**
 * Interface für Entitäten, die archiviert werden können
 * @author Lorenz Kock
 */
public interface ArchivEntity {
    void setArchiv(boolean archiv);

}
