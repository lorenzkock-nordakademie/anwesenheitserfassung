package de.lorenzkock.anwesenheitserfassungserver.data;

/**
 * Interface für Entitäten, mit einer Kennung
 * @author Lorenz Kock
 */
public interface KennungEntity<ID> extends IdEntity<ID> {

    String getKennung();
    void setKennung(String kennung);

}
