package de.lorenzkock.anwesenheitserfassungserver.data;

import lombok.NonNull;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Klasse zur Realisierung von seitenbasierten Datenbankanfragen
 */
public class OffsetBasedPageRequest implements Pageable {
    private final int limit;
    private final int offset;

    private final Sort sort;

    public OffsetBasedPageRequest(int limit, int offset, Sort sort) {
        if (limit < 1) {
            throw new IllegalArgumentException("Limit must not be less than one!");
        }
        if (offset < 0) {
            throw new IllegalArgumentException("Offset index must not be less than zero!");
        }
        this.limit = limit;
        this.offset = offset;
        this.sort = sort;
    }

    @Override
    public int getPageNumber() {
        return offset / limit;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public @NonNull Sort getSort() {
        return sort;
    }

    @Override
    public @NonNull Pageable next() {
        return new OffsetBasedPageRequest(getPageSize(), (int) (getOffset() + getPageSize()), getSort());
    }

    public Pageable previous() {
        return hasPrevious() ? new OffsetBasedPageRequest(getPageSize(), (int) (getOffset() - getPageSize()), getSort())
                : this;
    }

    @Override
    public @NonNull Pageable previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    @Override
    public @NonNull Pageable first() {
        return new OffsetBasedPageRequest(getPageSize(), 0, getSort());
    }

    @Override
    public boolean hasPrevious() {
        return offset > limit;
    }

    @Override
    public @NonNull Pageable withPage(int pageNumber) {
        if (pageNumber < 1) {
            throw new IllegalArgumentException("PageNumber must not be less than one!");
        }
        return new OffsetBasedPageRequest(getPageSize(), (pageNumber - 1) * getPageSize(), getSort());
    }

}