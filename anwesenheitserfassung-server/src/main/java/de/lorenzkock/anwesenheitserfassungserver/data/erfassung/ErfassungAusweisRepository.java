package de.lorenzkock.anwesenheitserfassungserver.data.erfassung;

import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ErfassungAusweisRepository extends JpaRepository<ErfassungAusweis, String> {

    List<ErfassungAusweis> findAllByTermin(final Termin termin);

}
