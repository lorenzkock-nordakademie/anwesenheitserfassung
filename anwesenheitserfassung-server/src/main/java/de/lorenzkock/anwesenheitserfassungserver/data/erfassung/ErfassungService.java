package de.lorenzkock.anwesenheitserfassungserver.data.erfassung;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErfassenErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitAktion;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatusDetail;
import de.lorenzkock.anwesenheitserfassungserver.enums.ErgebnisMehrfachErfassung;
import de.lorenzkock.anwesenheitserfassungserver.enums.PersonErfassungValidierungErgebnis;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Service für die Verarbeitung von Erfassungs-Daten
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ErfassungService {

	// Repositories
	final private ErfassungAusweisRepository erfassungAusweisRepository;
	final private ErfassungPersonRepository erfassungPersonRepository;
	final private TerminRepository terminRepository;
	final private PersonRepository personRepository;
	final private RaumRepository raumRepository;

	/*
	 * Methoden zum Lesen von Erfassungen
	 */

	/**
	 * Suche Erfassungen anhand eines Filters zu einer gegebenen ID eines Termins
	 *
	 * @param terminId           ID des Termins, zu dem die Erfassungen gesucht
	 *                           werden sollen
	 * @param personId           Optional: ID einer Person
	 * @param personKennung      Optional: Kennung einer Person
	 * @param ausweisRfidSnr     Optional: Seriennummer eines Ausweises
	 * @param ausweisRfidKennung Optional: Kennung eines Ausweises
	 * @return Liste von Erfassungen
	 */
	public List<Erfassung> findeErfassungenNachFilterMitTerminId(@NonNull final String terminId,
			@NonNull final Optional<String> personId, @NonNull final Optional<String> personKennung,
			@NonNull final Optional<String> ausweisRfidSnr, @NonNull final Optional<String> ausweisRfidKennung) {

		// Ermittle den Termin
		final Termin termin = terminRepository.findById(terminId)
				.orElseThrow(() -> new NoSuchElementException(
						"Es konnte kein Termin mit der ID '%s' gefunden werden.".formatted(terminId)));
		return findeErfassungenZuTerminNachFilter(termin, personId, personKennung, ausweisRfidSnr, ausweisRfidKennung);
	}

	/**
	 * Suche Erfassungen anhand eines Filters zu einer gegebenen Kennung eines
	 * Termins
	 *
	 * @param terminKennung      Kennung des Termins, zu dem die Erfassungen gesucht
	 *                           werden sollen
	 * @param personId           Optional: ID einer Person
	 * @param personKennung      Optional: Kennung einer Person
	 * @param ausweisRfidSnr     Optional: Seriennummer eines Ausweises
	 * @param ausweisRfidKennung Optional: Kennung eines Ausweises
	 * @return Liste von Erfassungen
	 */
	public List<Erfassung> findeErfassungenNachFilterMitTerminKennung(@NonNull final String terminKennung,
			@NonNull final Optional<String> personId, @NonNull final Optional<String> personKennung,
			@NonNull final Optional<String> ausweisRfidSnr, @NonNull final Optional<String> ausweisRfidKennung) {

		// Ermittle den Termin
		final Termin termin = terminRepository.findByKennung(terminKennung)
				.orElseThrow(() -> new NoSuchElementException(
						"Es konnte kein Termin mit der Kennung '%s' gefunden werden.".formatted(terminKennung)));
		return findeErfassungenZuTerminNachFilter(termin, personId, personKennung, ausweisRfidSnr, ausweisRfidKennung);
	}

	/**
	 * Suche Erfassungen anhand eines Filters zu einem gegebenen Termin
	 *
	 * @param termin             Termin, zu dem die Erfassungen gesucht werden
	 *                           sollen
	 * @param personId           Optional: ID einer Person
	 * @param personKennung      Optional: Kennung einer Person
	 * @param ausweisRfidSnr     Optional: Seriennummer eines Ausweises
	 * @param ausweisRfidKennung Optional: Kennung eines Ausweises
	 * @return Liste von Erfassungen
	 */
	private List<Erfassung> findeErfassungenZuTerminNachFilter(@NonNull final Termin termin,
			final Optional<String> personId, final Optional<String> personKennung,
			final Optional<String> ausweisRfidSnr, final Optional<String> ausweisRfidKennung) {

		// Ermittle zunächst die Erfassungen nach Ausweisen
		Set<ErfassungAusweis> erfassungenZuAusweisen = erfassungAusweisRepository.findAllByTermin(termin)
				.stream()
				.filter(e -> ausweisRfidSnr.isEmpty()
						|| ausweisRfidSnr.get().equals(e.getAusweis().getId().getRfidSnr()))
				.filter(e -> ausweisRfidKennung.isEmpty()
						|| ausweisRfidKennung.get().equals(e.getAusweis().getId().getKennung()))
				.filter(e -> personId.isEmpty() || (e.getAusweis().getPerson() != null
						&& personId.get().equals(e.getAusweis().getPerson().getId())))
				.filter(e -> personKennung.isEmpty() || (e.getAusweis().getPerson() != null
						&& personKennung.get().equals(e.getAusweis().getPerson().getKennung())))
				.collect(Collectors.toSet());

		// Ermittle zunächst die Erfassungen nach Ausweisen
		Set<ErfassungPerson> erfassungenZuPersonen = Collections.emptySet();
		if (ausweisRfidKennung.isEmpty() && ausweisRfidSnr.isEmpty()) {
			erfassungenZuPersonen = erfassungPersonRepository.findAllByTermin(termin)
					.stream()
					.filter(e -> personId.isEmpty() || personId.get().equals(e.getPerson().getId()))
					.filter(e -> personKennung.isEmpty() || personKennung.get().equals(e.getPerson().getKennung()))
					.collect(Collectors.toSet());
		}

		return this.mergeErfassungenVonAusweisenUndPersonen(erfassungenZuPersonen, erfassungenZuAusweisen);

	}

	/**
	 * Suche Erfassungen anhand eines Filters
	 *
	 * @param personId             Optional: ID einer Person
	 * @param personKennung        Optional: Kennung einer Person
	 * @param terminId             Optional: ID eines Termins
	 * @param terminKennung        Optional: Kennung eines Termins
	 * @param veranstaltungId      Optional: ID eines Termins
	 * @param veranstaltungKennung Optional: Kennung eines Termins
	 * @param ausweisRfidSnr       Optional: Seriennummer eines Ausweises
	 * @param ausweisRfidKennung   Optional: Kennung eines Ausweises
	 * @return Liste von Erfassungen
	 */
	public List<Erfassung> findeErfassungenZuTerminNachFilter(@NonNull final Optional<String> personId,
			@NonNull final Optional<String> personKennung, @NonNull final Optional<String> terminId,
			@NonNull final Optional<String> terminKennung, @NonNull final Optional<String> veranstaltungId,
			@NonNull final Optional<String> veranstaltungKennung, @NonNull final Optional<String> ausweisRfidSnr,
			@NonNull final Optional<String> ausweisRfidKennung) {

		List<Erfassung> erfassungen = this.mergeErfassungenVonAusweisenUndPersonen(
				new HashSet<>(erfassungPersonRepository.findAll()),
				new HashSet<>(erfassungAusweisRepository.findAll()));

		return erfassungen.stream()
				.filter(e -> filterTermin(e, terminId, terminKennung))
				.filter(e -> filterVeranstaltung(e, veranstaltungId, veranstaltungKennung))
				.filter(e -> filterPerson(e, personId, personKennung))
				.filter(e -> filterAusweis(e, ausweisRfidSnr, ausweisRfidKennung))
				.toList();

	}

	private boolean filterTermin(@NonNull Erfassung erfassung, @NonNull Optional<String> terminId,
			@NonNull Optional<String> terminKennung) {
		if (terminId.isEmpty() && terminKennung.isEmpty())
			return true;
		return terminId.map(s -> erfassung.getTermin().getId().equals(s))
				.orElseGet(() -> erfassung.getTermin().getKennung().equals(terminKennung.get()));
	}

	private boolean filterVeranstaltung(@NonNull Erfassung erfassung, @NonNull Optional<String> veranstaltungId,
			@NonNull Optional<String> veranstaltungKennung) {
		if (veranstaltungId.isEmpty() && veranstaltungKennung.isEmpty())
			return true;
		return veranstaltungId.map(s -> erfassung.getTermin().getVeranstaltung().getId().equals(s))
				.orElseGet(
						() -> erfassung.getTermin().getVeranstaltung().getKennung().equals(veranstaltungKennung.get()));
	}

	private boolean filterPerson(@NonNull Erfassung erfassung, @NonNull Optional<String> personId,
			@NonNull Optional<String> personKennung) {
		if (personId.isEmpty() && personKennung.isEmpty())
			return true;
		if (erfassung.getPerson() != null) {
			return personId.map(s -> erfassung.getPerson().getId().equals(s))
					.orElseGet(() -> erfassung.getPerson().getKennung().equals(personKennung.get()));
		} else if (erfassung.getAusweis() != null) {
			if (erfassung.getAusweis().getPerson() != null) {
				return personId.map(s -> erfassung.getAusweis().getPerson().getId().equals(s))
						.orElseGet(() -> erfassung.getAusweis().getPerson().getKennung().equals(personKennung.get()));
			} else {
				return false;
			}
		}
		return false;
	}

	private boolean filterAusweis(@NonNull Erfassung erfassung, @NonNull Optional<String> ausweisRfidSnr,
			@NonNull Optional<String> ausweisKennung) {
		if (ausweisRfidSnr.isEmpty() && ausweisKennung.isEmpty())
			return true;
		if (erfassung.getAusweis() == null)
			return false;
		return ausweisRfidSnr.map(s -> erfassung.getAusweis().getRfidSnr().equals(s))
				.orElseGet(() -> erfassung.getAusweis().getKennung().equals(ausweisKennung.get()));
	}

	/**
	 * Ermittle die Erfassungen zu einer Person und einer Veranstaltung, inkl.
	 * Erfassungen über Ausweise
	 *
	 * @param person          Person, zu der Erfassungen ermittelt werden sollen
	 * @param veranstaltungId Optional: ID der Veranstaltung
	 * @return Liste von Erfassungen zu dem Ausweis
	 */
	public List<Erfassung> getErfassungenZuPersonUndVeranstaltung(@NonNull Person person,
			@NonNull Optional<String> veranstaltungId) {

		Set<ErfassungAusweis> erfassungenNachAusweisen = new HashSet<>();
		Set<ErfassungPerson> erfassungenNachPersonen = new HashSet<>(person.getErfassungenZuPerson());
		for (Ausweis i : person.getAusweise()) {
			erfassungenNachAusweisen.addAll(i.getErfassungen());
		}

		if (veranstaltungId.isPresent()) {
			erfassungenNachPersonen = erfassungenNachPersonen.stream()
					.filter(e -> e.getTermin().getVeranstaltung().getId().equals(veranstaltungId.get()))
					.collect(Collectors.toSet());
			erfassungenNachAusweisen = erfassungenNachAusweisen.stream()
					.filter(e -> e.getTermin().getVeranstaltung().getId().equals(veranstaltungId.get()))
					.collect(Collectors.toSet());
		}

		return mergeErfassungenVonAusweisenUndPersonen(erfassungenNachPersonen, erfassungenNachAusweisen);

	}

	/**
	 * Ermittle die Erfassungen zu einer Person und einem Termin, inkl. Erfassungen
	 * über Ausweise
	 *
	 * @param person   Person, zu der Erfassungen ermittelt werden sollen
	 * @param terminId Optional: ID des Termins
	 * @return Liste von Erfassungen zu dem Ausweis
	 */
	public List<Erfassung> getErfassungenZuPersonUndTermin(@NonNull Person person, @NonNull Optional<String> terminId) {

		Set<ErfassungAusweis> erfassungenNachAusweisen = new HashSet<>();
		Set<ErfassungPerson> erfassungenNachPersonen = new HashSet<>(person.getErfassungenZuPerson());
		for (Ausweis i : person.getAusweise()) {
			erfassungenNachAusweisen.addAll(i.getErfassungen());
		}

		if (terminId.isPresent()) {
			erfassungenNachPersonen = erfassungenNachPersonen.stream()
					.filter(e -> e.getTermin().getId().equals(terminId.get()))
					.collect(Collectors.toSet());
			erfassungenNachAusweisen = erfassungenNachAusweisen.stream()
					.filter(e -> e.getTermin().getId().equals(terminId.get()))
					.collect(Collectors.toSet());
		}

		return mergeErfassungenVonAusweisenUndPersonen(erfassungenNachPersonen, erfassungenNachAusweisen);

	}

	/**
	 * Ermittle die Erfassungen zu einem Ausweise und einer Veranstaltung
	 *
	 * @param ausweis         Ausweis, zu der Erfassungen ermittelt werden sollen
	 * @param veranstaltungId Optional: ID der Veranstaltung
	 * @return Liste von Erfassungen zu dem Ausweis
	 */
	public List<Erfassung> getErfassungenZuAusweisUndVeranstaltung(@NonNull Ausweis ausweis,
			@NonNull Optional<String> veranstaltungId) {

		Set<ErfassungAusweis> erfassungenZumAusweis = new HashSet<>(ausweis.getErfassungen());

		// Wenn eine ID zur Veranstaltung gegeben wird, sollen nur Erfassungen zu dem
		// Termin ermittelt werden
		if (veranstaltungId.isPresent()) {
			erfassungenZumAusweis = erfassungenZumAusweis.stream()
					.filter(e -> e.getTermin().getVeranstaltung().getId().equals(veranstaltungId.get()))
					.collect(Collectors.toSet());
		}

		return mergeErfassungenVonAusweisenUndPersonen(Collections.emptySet(), erfassungenZumAusweis);

	}

	/**
	 * Ermittle die Erfassungen zu einem Ausweise und einem Termin
	 *
	 * @param ausweis  Ausweis, zu dem Erfassungen ermittelt werden sollen
	 * @param terminId Optional: ID des Termins
	 * @return Liste von Erfassungen zu dem Ausweis
	 */
	public List<Erfassung> getErfassungenZuAusweisUndTermin(@NonNull Ausweis ausweis,
			@NonNull Optional<String> terminId) {

		Set<ErfassungAusweis> erfassungenZuAusweis = new HashSet<>(ausweis.getErfassungen());

		// Wenn eine ID zu einem Termin gegeben wird, sollen nur Erfassungen zu dem
		// Termin ermittelt werden
		if (terminId.isPresent()) {
			erfassungenZuAusweis = erfassungenZuAusweis.stream()
					.filter(e -> e.getTermin().getId().equals(terminId.get()))
					.collect(Collectors.toSet());
		}

		return mergeErfassungenVonAusweisenUndPersonen(Collections.emptySet(), erfassungenZuAusweis);

	}

	/**
	 * Ermittle die Erfassungen zu einem Termin
	 *
	 * @param termin Termin, dessen Erfassungen gefragt sind
	 * @return Liste von Erfassungen zum Termin
	 */
	public List<Erfassung> getErfassungenZuTermin(@NonNull Termin termin) {

		return this.mergeErfassungenVonAusweisenUndPersonen(new HashSet<>(termin.getPersonErfassungen()),
				new HashSet<>(termin.getAusweisErfassungen()));

	}

	/*
	 * Speichern der Erfassungen in der Datenbank
	 */

	/**
	 * Speichern einer Erfassung zu einem Ausweise
	 *
	 * @param termin    Termin zu dem die Erfassung gespeichert werden soll
	 * @param ausweis   Ausweis, der erfasst werden soll
	 * @param zeitpunkt Zeitpunkt der Erfassung
	 */
	public void speichereErfassung(@NonNull Termin termin, @NonNull Ausweis ausweis, @NonNull OffsetDateTime zeitpunkt,
			@NonNull Optional<Raum> raum) {
		ErfassungAusweis erfassung = ErfassungAusweis.builder()
				.termin(termin)
				.ausweis(ausweis)
				.zeitpunkt(zeitpunkt)
				.raum(raum.orElse(null))
				.build();
		this.erfassungAusweisRepository.save(erfassung);
	}

	/**
	 * Speichern einer Erfassung zu einer Person
	 *
	 * @param termin    Termin zu dem die Erfassung gespeichert werden soll
	 * @param person    Person, die erfasst werden soll
	 * @param zeitpunkt Zeitpunkt der Erfassung
	 * @return Erstellte Erfassung
	 */
	public ErfassungPerson speichereErfassung(Termin termin, Person person, OffsetDateTime zeitpunkt,
			@NonNull Optional<Raum> raum, @NonNull Boolean online) {
		ErfassungPerson erfassung = ErfassungPerson.builder()
				.termin(termin)
				.person(person)
				.zeitpunkt(zeitpunkt)
				.raum(raum.orElse(null))
				.onlineTeilnahme(online)
				.build();
		return this.erfassungPersonRepository.save(erfassung);
	}

	/*
	 * Verarbeiten von Erfassungs-Anfragen
	 */

	/**
	 * Verarbeiten der Erfassung zu einer Person
	 *
	 * @param personId  Person, die erfasst wird
	 * @param terminId  Termin, zu der die Erfassung durchgeführt wird
	 * @param zeitpunkt Zeitpunkt der Erfassung
	 * @return Ergebnis der Verarbeitung
	 */
	public PersonErfassenErgebnisDto erfassungZuPersonVerarbeiten(@NonNull String personId, @NonNull String terminId,
			@NonNull OffsetDateTime zeitpunkt, @NonNull Optional<String> raumId, @NonNull Boolean online) {

		// Ermittle den Termin der Erfassung
		Termin termin = terminRepository.findById(terminId).orElse(null);
		if (termin == null) {
			return PersonErfassenErgebnisDto.builder()
					.ergebnis(PersonErfassungValidierungErgebnis.NICHT_ERFOLGREICH_TERMIN_UNBEKANNT)
					.build();
		}

		// Ermittle die Person der Erfassung
		Person person = personRepository.findById(personId).orElse(null);
		if (person == null) {
			return PersonErfassenErgebnisDto.builder()
					.ergebnis(PersonErfassungValidierungErgebnis.NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN)
					.build();
		}

		Optional<Raum> raumDerErfassung = StringUtils.isBlank(raumId.orElse(null)) ? Optional.empty()
				: raumRepository.findById(raumId.get());

		// Bestimme zunächst die teilnehmenden Personen, um die erfasste abgleichen
		// zu können
		List<Person> teilnehmendePersonen = termin.getAlleTeilnehmendenPersonen();

		// Wenn die Person enthalten ist, verarbeite diese Erfassung
		if (teilnehmendePersonen.contains(person)) {

			int anzahlErfassungenZuTerminUndPerson = this
					.getErfassungenZuPersonUndTermin(person, Optional.of(termin.getId()))
					.size();

			ErgebnisMehrfachErfassung ergebnisValidierung = this.validiereAufMehrfachBuchungen(
					anzahlErfassungenZuTerminUndPerson, termin.getVeranstaltung().isErlaubeEinUndAusgehendeBuchungen(),
					termin.getVeranstaltung().isErlaubeMehrfachBuchungen());

			ErfassungPerson erfassung = null;
			if (ergebnisValidierung.isErfolgreich()) {
				erfassung = this.speichereErfassung(termin, person, zeitpunkt, raumDerErfassung, online);
				person.getErfassungenZuPerson().add(erfassung);
			}

			PersonErfassungValidierungErgebnis validierungErgebnis = switch (ergebnisValidierung) {
			case ERFOLGREICH -> PersonErfassungValidierungErgebnis.ERFOLGREICH;
			case ERFOLGREICH_AUSGEHEND -> PersonErfassungValidierungErgebnis.ERFOLGREICH_AUSGEHEND;
			case ERFOLGREICH_AUSGEHEND_WIEDERKEHREND ->
				PersonErfassungValidierungErgebnis.ERFOLGREICH_AUSGEHEND_WIEDERKEHREND;
			case ERFOLGREICH_MEHRFACH -> PersonErfassungValidierungErgebnis.ERFOLGREICH_MEHRFACH;
			case ERFOLGREICH_WIEDERKEHREND -> PersonErfassungValidierungErgebnis.ERFOLGREICH_WIEDERKEHREND;
			case NICHT_ERFOLGREICH_MEHRFACH -> PersonErfassungValidierungErgebnis.NICHT_ERFOLGREICH_MEHRFACH;
			};

			return this.personErfassungErgebnis(validierungErgebnis, person, erfassung);

		} else { // Wenn die Person nicht enthalten ist, gib einen Fehler zurück
			return this.personErfassungErgebnis(PersonErfassungValidierungErgebnis.NICHT_ERFOLGREICH_KEIN_TEILNEHMER,
					person, null);
		}
	}

	/**
	 * Methode zum Ändern des Start-Zeitpunkts der Erfassungen zu einem Termin
	 *
	 * @param zeitpunkt Zeitpunkt der Erfassungen, wenn leer wird der Zeitpunkt
	 *                  gelöscht
	 * @param terminId  ID des Termins
	 * @return Geänderter Termin
	 */
	public Termin aendereErfassungStart(@NonNull Optional<OffsetDateTime> zeitpunkt, @NonNull String terminId) {

		// Ermittle den Termin
		final Termin termin = terminRepository.findById(terminId)
				.orElseThrow(() -> new NoSuchElementException(
						"Es konnte kein Termin mit der ID '%s' gefunden werden.".formatted(terminId)));

		termin.setErfassungStart(zeitpunkt.orElse(null));
		return terminRepository.save(termin);
	}

	/**
	 * Stelle das Ergebnis der Verarbeitung zur Erfassung einer person zusammen
	 *
	 * @param ergebnis  Ergebnis der Verarbeitung
	 * @param person    Person, die erfasst wurde
	 * @param erfassung Erstellte Erfassung
	 * @return Ergebnis der Verarbeitung
	 */
	private PersonErfassenErgebnisDto personErfassungErgebnis(@NonNull PersonErfassungValidierungErgebnis ergebnis,
			Person person, ErfassungPerson erfassung) {
		return PersonErfassenErgebnisDto.builder().ergebnis(ergebnis).erfassung(erfassung).person(person).build();
	}

	/**
	 * Validiere, ob auf Grundlage der existierenden Anzahl von Erfassung eine
	 * Erfassung durchgeführt werden kann
	 *
	 * @param anzahlErfassungen                       Anzahl der Erfassungen, die zu
	 *                                                berücksichtigen sind
	 * @param veranstaltungUnterscheidungEinAusgehend Information, ob in der
	 *                                                Veranstaltung zwischen ein-
	 *                                                und ausgehenden Erfassungen
	 *                                                unterschieden wird
	 * @param veranstaltungErlaubeMehrfach            Information, ob in der
	 *                                                Veranstaltung mehrfache
	 *                                                Erfassungen zugelassen sind
	 * @return Ergebnis der Validierung
	 */
	public ErgebnisMehrfachErfassung validiereAufMehrfachBuchungen(int anzahlErfassungen,
			boolean veranstaltungUnterscheidungEinAusgehend, boolean veranstaltungErlaubeMehrfach) {

		// Wenn die Liste leer ist, kann in jedem Fall eine Erfassung erfolgen
		if (anzahlErfassungen == 0) {
			return ErgebnisMehrfachErfassung.ERFOLGREICH;
		} else { // Ansonsten müssen weitere Bedingungen geprüft werden

			// Behandle das Verhalten bei ein- und ausgehenden Buchungen
			if (veranstaltungUnterscheidungEinAusgehend) {
				// Wenn erst eine Buchung existiert, ist dies die Ausgehende
				if (anzahlErfassungen == 1) {
					return ErgebnisMehrfachErfassung.ERFOLGREICH_AUSGEHEND;
				} else {

					// Wenn mehrere Erfassungen bereits existieren muss geprüft werden, ob
					// Mehrfacherfassungen zulässig sind
					if (veranstaltungErlaubeMehrfach) {
						// Ermittle, ob es sich um eine ein- oder ausgehende Erfassung handelt
						return anzahlErfassungen % 2 == 0 ? ErgebnisMehrfachErfassung.ERFOLGREICH_WIEDERKEHREND
								: ErgebnisMehrfachErfassung.ERFOLGREICH_AUSGEHEND_WIEDERKEHREND;
					} else { // Sind Mehrfachbuchungen nicht erlaubt, ist die Erfassung nicht erfolgreich
						return ErgebnisMehrfachErfassung.NICHT_ERFOLGREICH_MEHRFACH;
					}
				}
			} else {
				// Wenn mehrere Erfassungen zulässig sind, verarbeite diese in jedem Fall
				if (veranstaltungErlaubeMehrfach) {
					return ErgebnisMehrfachErfassung.ERFOLGREICH_MEHRFACH;
				} else { // Wenn weder Mehrfachbuchungen noch ein- und ausgehende erlaubt sind, wird die
					// Erfassung abgelehnt
					return ErgebnisMehrfachErfassung.NICHT_ERFOLGREICH_MEHRFACH;
				}
			}
		}
	}

	/**
	 * Ermittle den Anwesenheitsstatus zu einer Person oder einem Ausweis auf
	 * Grundlage einer Anzahl von Erfassungen
	 *
	 * @param anzahlErfassungen       Anzahl der Erfassungen, die zu berücksichtigen
	 *                                sind
	 * @param erlaubeEinUndAusgehende Information, ob zwischen ein- und ausgehenden
	 *                                Erfassungen unterschieden wird
	 * @return Ermittelter Status
	 */
	public AnwesenheitStatusDetail statusZuAnzahlErfassungen(int anzahlErfassungen, boolean erlaubeEinUndAusgehende) {

		// Wenn die Liste leer ist, erfolgte bisher keine Erfassung
		if (anzahlErfassungen == 0) {
			return AnwesenheitStatusDetail.NICHT_ERFASST;
		} else if (anzahlErfassungen == 1) {
			return AnwesenheitStatusDetail.ANWESEND;
		} else { // Ansonsten müssen weitere Bedingungen geprüft werden

			// Behandle das Verhalten bei ein- und ausgehenden Buchungen
			if (erlaubeEinUndAusgehende) {
				if (anzahlErfassungen == 2) {
					return AnwesenheitStatusDetail.ABWESEND;
				} else {
					if (anzahlErfassungen % 2 == 0) {
						return AnwesenheitStatusDetail.ABWESEND_WIDERKEHREND;
					} else {
						return AnwesenheitStatusDetail.ANWESEND_WIEDERKEHREND;
					}
				}
			} else {
				return AnwesenheitStatusDetail.ANWESEND_MEHRFACH;
			}
		}
	}

	/**
	 * Ermittle die mögliche Aktion zu einer Person oder einem Ausweis auf Grundlage
	 * einer Anzahl von Erfassungen
	 *
	 * @param anzahlErfassungen        Anzahl der Erfassungen, die zu
	 *                                 berücksichtigen sind
	 * @param erlaubeEinUndAusgehende  Information, ob zwischen ein- und ausgehenden
	 *                                 Erfassungen unterschieden wird
	 * @param erlaubeMehrfachbuchungen Information, ob Erfassungen zu einem Termin
	 *                                 mehrfach durchgeführt werden dürfen
	 * @return Ermittelte Aktion
	 */
	public AnwesenheitAktion aktionZuAnzahlErfassungen(int anzahlErfassungen, boolean erlaubeEinUndAusgehende,
			boolean erlaubeMehrfachbuchungen) {
		if (erlaubeEinUndAusgehende) {
			if (anzahlErfassungen == 0) {
				return AnwesenheitAktion.WIEDERKEHREN;
			} else if (anzahlErfassungen == 1) {
				return AnwesenheitAktion.VERLASSEN;
			} else {
				if (erlaubeMehrfachbuchungen) {
					if (anzahlErfassungen % 2 == 0) {
						return AnwesenheitAktion.WIEDERKEHREN;
					} else {
						return AnwesenheitAktion.VERLASSEN;
					}
				} else {
					return null;
				}
			}
		} else {
			if (anzahlErfassungen == 0) {
				return AnwesenheitAktion.ERFASSEN;
			} else if (erlaubeMehrfachbuchungen) {
				return AnwesenheitAktion.ERFASSEN_MEHRFACH;
			} else {
				return null;
			}
		}
	}

	/**
	 * Füge die Erfassungen zu Personen und zu Ausweisen zusammen
	 *
	 * @param personenErfassungen ErfassungenZuPersonen
	 * @param ausweiseErfassungen Erfassungen zu Ausweisen
	 * @return Liste von Erfassungen
	 */
	public List<Erfassung> mergeErfassungenVonAusweisenUndPersonen(@NonNull Set<ErfassungPerson> personenErfassungen,
			Set<ErfassungAusweis> ausweiseErfassungen) {

		List<Erfassung> erfassungen = new ArrayList<>();

		for (ErfassungPerson p : personenErfassungen) {
			erfassungen.add(Erfassung.builder()
					.termin(p.getTermin())
					.zeitpunkt(p.getZeitpunkt())
					.person(p.getPerson())
					.mitKarte(false)
					.raum(p.getRaum())
					.onlineAnwesend(p.isOnlineTeilnahme())
					.build());
		}
		for (ErfassungAusweis i : ausweiseErfassungen) {
			erfassungen.add(Erfassung.builder()
					.termin(i.getTermin())
					.zeitpunkt(i.getZeitpunkt())
					.ausweis(i.getAusweis())
					.person(i.getAusweis().getPerson())
					.mitKarte(true)
					.raum(i.getRaum())
					.onlineAnwesend(false)
					.build());
		}

		return erfassungen.stream().sorted(Comparator.comparing(Erfassung::getZeitpunkt)).toList();

	}

}
