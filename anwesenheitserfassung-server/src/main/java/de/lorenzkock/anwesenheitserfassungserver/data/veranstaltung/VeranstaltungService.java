package de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung.VeranstaltungBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class VeranstaltungService extends BaseService {

    final private VeranstaltungRepository veranstaltungRepo;

    /*
     * Allgemeine CRUD Methoden
     */

    /**
     * Ermittle eine Liste von Veranstaltungen zu einem Filter
     *
     * @param id      ID der Veranstaltung
     * @param kennung Kennung der Veranstaltung
     * @param name    Name der Veranstaltung
     * @param archiv  Archivierte Veranstaltung
     * @return Liste von ermittelten Veranstaltungen
     */
    public List<Veranstaltung> getVeranstaltungenMitFilter(final @NonNull Optional<String> id, final @NonNull Optional<String> kennung,
                                                           final @NonNull Optional<String> name, final @NonNull Optional<Boolean> archiv) {
        return veranstaltungRepo.findAll()
                .stream()
                .filter(v -> StringUtils.isBlank(name.orElse(null)) || v.getName().toLowerCase().contains(name.get().toLowerCase()))
                .filter(v -> StringUtils.isBlank(id.orElse(null)) || v.getId().toLowerCase().contains(id.get().toLowerCase()))
                .filter(v -> StringUtils.isBlank(kennung.orElse(null)) || v.getKennung().toLowerCase().contains(kennung.get().toLowerCase()))
                .filter(v -> archiv.isEmpty() || archiv.get() == v.isArchiv())
                .sorted(Comparator.comparing(Veranstaltung::getName))
                .toList();
    }

    /**
     * Ergänze eine Veranstaltung, die Veranstaltung wird nicht gespeichert
     *
     * @param veranstaltung            Zu ergänzende Veranstaltung
     * @param name                     Name der Veranstaltung
     * @param erlaubeEinAusgehend      Information, ob zwischen ein- und ausgehenden Personen unterschieden werden soll
     * @param erlaubeMehrfach          Information, ob mehrfache Erfassungen erlaubt sind
     * @param erlaubeFremdausweise     Information, ob Fremdausweise zugelassen sind
     * @param fremdausweiseBestaetigen Information, ob Fremdausweise von Berechtigten Personen bestätigt werden müssen
     * @param kennung                  Kennung der Veranstaltung
     * @return Ergänzte Veranstaltung
     */
    public Veranstaltung ergaenzeVeranstaltung(@NonNull Veranstaltung veranstaltung, @NonNull String name, boolean erlaubeEinAusgehend,
                                               boolean erlaubeMehrfach,
                                               boolean erlaubeFremdausweise,
                                               boolean fremdausweiseBestaetigen,
                                               @NonNull Optional<String> kennung) {

        VeranstaltungBuilder<?, ?> builder = veranstaltung.toBuilder()
                .name(name)
                .erlaubeEinUndAusgehendeBuchungen(erlaubeEinAusgehend)
                .erlaubeMehrfachBuchungen(erlaubeMehrfach)
                .erlaubeFremdausweis(erlaubeFremdausweise)
                .fremdausweisBestaetigen(fremdausweiseBestaetigen);

        if (!StringUtils.isBlank(kennung.orElse(null))) {
            builder.kennung(kennung.get());
        }
        return builder.build();
    }

    /**
     * Fügt eine Liste von Ausweisen und Personen als Teilnehmer zu einer Veranstaltung hinzu
     *
     * @param veranstaltung Veranstaltung, zu der die Personen und Ausweisen hinzugefügt werden sollen
     * @param ausweise      Ausweise, die hinzugefügt werden sollen
     * @param personen      Personen, die hinzugefügt werden sollen
     * @return Erweiterte Veranstaltung
     */
    public Veranstaltung fuegeTeilnehmendePersonenUndAusweiseHinzu(@NonNull Veranstaltung veranstaltung,
                                                                   @NonNull List<Ausweis> ausweise,
                                                                   @NonNull List<Person> personen) {

        // Füge die Personen und Ausweise aus dem View Model der Veranstaltung hinzu
        veranstaltung.fuegeTeilnehmendeAusweiseHinzu(ausweise);
        veranstaltung.fuegeTeilnehmendePersonenHinzu(personen);

        // Speichere die Veranstaltung
        return veranstaltungRepo.save(veranstaltung);

    }

    /**
     * Entfernt eine Liste von Ausweisen und Personen als Berechtigte von einer Veranstaltung
     *
     * @param veranstaltung Veranstaltung, von der die Personen und Ausweisen entfernt werden sollen
     * @param ausweise      Ausweise, die entfernt werden sollen
     * @param personen      Personen, die entfernt werden sollen
     * @return Erweiterte Veranstaltung
     */
    public Veranstaltung entferneBerechtigePersonenUndAusweise(@NonNull Veranstaltung veranstaltung,
                                                               @NonNull List<Ausweis> ausweise,
                                                               @NonNull List<Person> personen) {

        // Füge die Personen und Ausweise der Veranstaltung hinzu
        veranstaltung.entferneBerechtigteAusweise(ausweise);
        veranstaltung.entferneBerechtigtePersonen(personen);

        // Speichere die Veranstaltung
        return veranstaltungRepo.save(veranstaltung);

    }

    /**
     * Entfernt eine Liste von Ausweisen und Personen als Teilnehmer von einer Veranstaltung
     *
     * @param veranstaltung Veranstaltung, von der die Personen und Ausweisen entfernt werden sollen
     * @param ausweise      Ausweise, die entfernt werden sollen
     * @param personen      Personen, die entfernt werden sollen
     * @return Erweiterte Veranstaltung
     */
    public Veranstaltung entferneTeilnehmendePersonenUndAusweise(@NonNull Veranstaltung veranstaltung,
                                                                 @NonNull List<Ausweis> ausweise,
                                                                 @NonNull List<Person> personen) {

        // Füge die Personen und Ausweise der Veranstaltung hinzu
        veranstaltung.entferneTeilnehmendeAusweise(ausweise);
        veranstaltung.entferneTeilnehmendePersonen(personen);

        // Speichere die Veranstaltung
        return veranstaltungRepo.save(veranstaltung);

    }

    /**
     * Fügt eine Liste von Ausweisen und Personen als Berechtigte zu einer Veranstaltung hinzu
     *
     * @param veranstaltung Veranstaltung, zu der die Personen und Ausweisen hinzugefügt werden sollen
     * @param ausweise  Ausweise, die hinzugefügt werden sollen
     * @param personen      Personen, die hinzugefügt werden sollen
     * @return Erweiterte Veranstaltung
     */
    public Veranstaltung fuegeBerechtigePersonenUndAusweiseHinzu(@NonNull Veranstaltung veranstaltung,
                                                                 @NonNull List<Ausweis> ausweise,
                                                                 @NonNull List<Person> personen) {

        // Füge die Personen und Ausweise der Veranstaltung hinzu
        veranstaltung.fuegeBerechtigteAusweiseHinzu(ausweise);
        veranstaltung.fuegeBerechtigtePersonenHinzu(personen);

        // Speichere die Veranstaltung
        return veranstaltungRepo.save(veranstaltung);

    }

}
