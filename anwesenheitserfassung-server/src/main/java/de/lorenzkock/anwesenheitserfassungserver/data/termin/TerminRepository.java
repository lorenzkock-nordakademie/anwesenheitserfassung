package de.lorenzkock.anwesenheitserfassungserver.data.termin;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungserver.data.EntityMitKennungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;

@Repository
public interface TerminRepository extends EntityMitKennungRepository<Termin, String> {

	Page<Termin> findByRaeumeAndEndeGreaterThan(Raum raum, OffsetDateTime ende, Pageable pageable);

	List<Termin> findByRaeumeAndStartLessThanAndEndeGreaterThan(Raum raum, OffsetDateTime start, OffsetDateTime ende);

	Page<Termin> findByEndeGreaterThan(OffsetDateTime ende, Pageable pageable);

	Optional<Termin> findFirstByStartAndEndeAndVeranstaltungAndRaeume(OffsetDateTime start, OffsetDateTime ende, Veranstaltung veranstaltung, Raum raum);

	List<Termin> findByStartLessThanAndEndeGreaterThan(OffsetDateTime ende, OffsetDateTime start);

}
