package de.lorenzkock.anwesenheitserfassungserver.data.ausweis;

import de.lorenzkock.anwesenheitserfassungserver.data.EntityMitKennungRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AusweisRepository extends EntityMitKennungRepository<Ausweis, AusweisId> {

    Optional<Ausweis> findFirstByIdRfidSnrAndAktiv(String rfidSnr, boolean aktiv);

    Optional<Ausweis> findFirstByIdRfidSnr(String rfidSnr);

    Optional<Ausweis> findFirstByIdKennungAndAktiv(String kennung, boolean aktiv);

    Optional<Ausweis> findFirstByIdKennung(String kennung);

    Optional<Ausweis> findFirstByIdRfidSnrAndIdKennung(String rfidSnr, String kennung);

    List<Ausweis> findAllByFremdausweis(boolean fremdausweis);

    @Override
    @Query("SELECT a FROM ausweis a WHERE a.id.kennung = ?1")
    Optional<Ausweis> findByKennung(String kennung);

}
