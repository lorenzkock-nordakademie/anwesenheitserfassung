package de.lorenzkock.anwesenheitserfassungserver.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

/**
 * Interfaces für ein Repository für Entitäten mit Kennungen
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface EntityMitKennungRepository<T extends KennungEntity, ID> extends JpaRepository<T, ID> {

    /**
     * Findet eine Entität bei einer Kennung
     * @param kennung Kennung der Entität
     * @return Entität
     */
    Optional<T> findByKennung(String kennung);

}
