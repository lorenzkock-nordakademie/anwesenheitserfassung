package de.lorenzkock.anwesenheitserfassungserver.data;

import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Berechtigung;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Converter, um eine Liste von Rollen zu einem String und andersherum zu konvertieren
 */
@Converter
public class StringListRoleConverter implements AttributeConverter<List<Berechtigung>, String> {

    private static final String SPLIT_CHAR = ";";

    /**
     * Konvertiere eine Liste von Rollen zu einem String mit einem Trennzeichen
     *
     * @param rollen Die Rollen, die zu einem String konvertiert werden sollen
     * @return String mit Rollen, separiert durch Trennzeichen
     */
    @Override
    public String convertToDatabaseColumn(final List<Berechtigung> rollen) {
        return rollen == null ? "" : rollen.stream().map(Berechtigung::name).collect(Collectors.joining(SPLIT_CHAR));
    }

    /**
     * Konvertiere einen String mit Trennzeichen zu einer Liste von Rollen
     *
     * @param rollen String mit Rollen
     * @return Liste von Rollen
     */
    @Override
    public List<Berechtigung> convertToEntityAttribute(final String rollen) {
        return StringUtils.isBlank(rollen) ? List.of()
                : Stream.of(rollen.split(SPLIT_CHAR)).map(Berechtigung::valueOf).toList();
    }

}