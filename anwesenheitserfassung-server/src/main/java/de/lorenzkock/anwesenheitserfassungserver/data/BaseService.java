package de.lorenzkock.anwesenheitserfassungserver.data;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

/**
 * Abstrakter Service mit häufig genutzten Methoden
 *
 * @author Lorenz Kock
 */
public abstract class BaseService {

    /**
     * Ermittelt eine Instanz aus einem gegebenen Repository zu einer ID
     *
     * @param repository Repository
     * @param id         ID der Instanz
     * @param <T>        Typ der Klasse
     * @param <ID>       Typ der ID der Klasse
     * @return Ermittelte Instanz
     */
    public <T extends IdEntity<ID>, ID> Optional<T> getMitID(@NonNull JpaRepository<T, ID> repository, @NonNull final ID id) {
        return repository.findById(id);
    }

    /**
     * Ermittelt eine Instanz aus einem gegebenen Repository zu einer Kennung
     *
     * @param repository Repository
     * @param kennung     Kennung der Instanz
     * @param <T>        Typ der Klasse
     * @param <ID>       Typ der ID der Klasse
     * @return Ermittelte Instanz
     */
    public <T extends KennungEntity<ID>, ID> Optional<T> getMitKennung(@NonNull EntityMitKennungRepository<T, ID> repository, @NonNull final String kennung) {
        return repository.findByKennung(kennung);
    }

    /**
     * Prüft, ob bereits eine Instanz zu der Kennung existiert.
     *
     * @param repository Repository
     * @param kennung     Kennung, die zu prüfen ist
     * @param erlaubteId ID, die diese Kennung haben darf
     * @param <T>        Typ der Klasse
     * @param <ID>       Typ der ID der Klasse
     */
    public <T extends KennungEntity<ID>, ID> Optional<T> pruefeKennungEinmalig(@NonNull EntityMitKennungRepository<T, ID> repository, String kennung, @NonNull Optional<ID> erlaubteId) throws IllegalArgumentException {
        if (!StringUtils.isBlank(kennung)) {
            Optional<T> entityMitKennung = repository.findByKennung(kennung);
            if(entityMitKennung.isPresent()) {
                if (erlaubteId.isEmpty() || !entityMitKennung.get().getId().equals(erlaubteId.get())) {
                    throw new IllegalArgumentException(
                            "Eine Instanz mit der Kennung '%s' existiert bereits.".formatted(kennung));
                }
                return Optional.of(entityMitKennung.get());
            }
        }
        return Optional.empty();
    }

    /**
     * Ergänze zu einer gegebenen Instanz die Kennung, wenn keine gesetzt ist
     *
     * @param entity Instanz
     * @param <T>    Typ der Instanz
     */
    public static <T extends KennungEntity<ID>, ID> void ergaenzeGenerierteKennung(T entity) {
        if (StringUtils.isBlank(entity.getKennung())) {
            entity.setKennung(UUID.randomUUID().toString());
        }
    }

    /**
     * Archiviert eine Instanz mit der gegebenen ID
     *
     * @param repository Repository zum Zugriff
     * @param id         ID der Instanz
     * @param <T>        Typ der Instanz
     * @param <ID>       Typ der ID der Instanz
     * @throws NoSuchElementException Fehler, wenn Instanz nicht gefunden wurde
     */
    public <T extends ArchivEntity, ID> void archiviere(@NonNull JpaRepository<T, ID> repository, @NonNull final ID id) throws NoSuchElementException {

        T entityZuArchivieren = repository.findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("Es wurde keine Instanz mit der ID '%s' gefunden.".formatted(id)));

        entityZuArchivieren.setArchiv(true);

        repository.save(entityZuArchivieren);

    }

}
