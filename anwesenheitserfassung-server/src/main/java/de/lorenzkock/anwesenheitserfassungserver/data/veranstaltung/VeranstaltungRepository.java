package de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung;

import de.lorenzkock.anwesenheitserfassungserver.data.EntityMitKennungRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VeranstaltungRepository extends EntityMitKennungRepository<Veranstaltung, String> {

}
