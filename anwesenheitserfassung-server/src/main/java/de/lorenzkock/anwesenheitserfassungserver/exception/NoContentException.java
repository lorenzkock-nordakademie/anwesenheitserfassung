package de.lorenzkock.anwesenheitserfassungserver.exception;

public class NoContentException extends Exception {

    public NoContentException(String message) {
        super(message);
    }
}
