package de.lorenzkock.anwesenheitserfassungserver.exception;

public class CsvImportException extends Exception {

    public CsvImportException(String message) {
        super(message);
    }
}
