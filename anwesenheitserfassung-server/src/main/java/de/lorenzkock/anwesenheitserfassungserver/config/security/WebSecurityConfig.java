package de.lorenzkock.anwesenheitserfassungserver.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;

import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Berechtigung;
import lombok.RequiredArgsConstructor;

/**
 * Konfiguration der Zugriffe auf die einzelnen Endpunkte
 * @author Lorenz Kock
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig {

	final private UserDetailsService userDetailsService;

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.authorizeHttpRequests((requests) -> requests
				.requestMatchers("/css/**", "/js/**", "/webjars/**", "/swagger-ui/**", "/v3/api-docs/**")
				.permitAll()
				.requestMatchers("/api/intern/**")
				.hasAnyRole(Berechtigung.SCHNITTSTELLE_INTERN.name())
				.requestMatchers("/api/**")
				.hasAnyRole(Berechtigung.SCHNITTSTELLE_EXTERN.name())
				.requestMatchers("/verwaltung/personen/**")
				.hasAnyRole(Berechtigung.VERWALTUNG_PERSONEN.name(), Berechtigung.ADMINISTRATION.name())
				.requestMatchers("/verwaltung/veranstaltungen/**")
				.hasAnyRole(Berechtigung.VERWALTUNG_VERANSTALTUNGEN.name(), Berechtigung.ADMINISTRATION.name())
				.requestMatchers("/verwaltung/ausweise/**")
				.hasAnyRole(Berechtigung.VERWALTUNG_AUSWEISE.name(), Berechtigung.ADMINISTRATION.name())
				.requestMatchers("/verwaltung/anwesenheiten/**")
				.authenticated()
				.requestMatchers("/verwaltung/raeume/**")
				.hasAnyRole(Berechtigung.VERWALTUNG_RAEUME.name(), Berechtigung.ADMINISTRATION.name())
				.requestMatchers("/verwaltung/benutzer/**", "/verwaltung/konfiguration/**")
				.hasAnyRole(Berechtigung.ADMINISTRATION.name())
				.requestMatchers("/favicon.ico", "/resources/**", "/error").permitAll()
				.anyRequest()
				.authenticated())
				.formLogin((form) -> form.loginPage("/login").permitAll().failureUrl("/login-error").permitAll())
				.logout((l) -> l.logoutUrl("/logout"))
				.httpBasic(Customizer.withDefaults())
				.csrf(AbstractHttpConfigurer::disable)
				.logout(LogoutConfigurer::permitAll)
				.userDetailsService(userDetailsService);

		return http.build();
	}

}
