package de.lorenzkock.anwesenheitserfassungserver.config.security;

import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;

/**
 * Objekt, welches die für Spring notwendigen Informationen zum Benutzer bereitstellt.
 * @param benutzer
 */
public record AppUserPrincipal(Benutzer benutzer) implements UserDetails {

    @Serial
    private static final long serialVersionUID = 4120739445024862599L;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return benutzer.getBerechtigungen()
                .stream()
                .map(r -> new SimpleGrantedAuthority("ROLE_%s".formatted(r.name())))
                .toList();
    }

    @Override
    public String getPassword() {
        return benutzer.getPasswort();
    }

    @Override
    public String getUsername() {
        return benutzer.getBenutzername();
    }

    /*
     * Folgende Eigenschaften sind standardmäßig auf true gesetzt.
     * Eine Implementierung ist notwendig, wenn Benutzer ablaufen sollen.
     */

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
