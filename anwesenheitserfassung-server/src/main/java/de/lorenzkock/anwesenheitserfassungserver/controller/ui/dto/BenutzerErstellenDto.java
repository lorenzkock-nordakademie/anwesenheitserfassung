package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BenutzerErstellenDto {

	String benutzername;
	String passwort;
	List<String> berechtigungen;
	String personId;

}
