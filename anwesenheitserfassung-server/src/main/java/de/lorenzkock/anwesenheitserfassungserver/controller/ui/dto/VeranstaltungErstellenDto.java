package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VeranstaltungErstellenDto {

	String name;
	String kennung;

	String raumId;

	boolean fremdausweisBestaetigen;
	boolean erlaubeFremdausweis;
	boolean erlaubeEinUndAusgehendeBuchungen;
	boolean erlaubeMehrfachBuchungen;

}
