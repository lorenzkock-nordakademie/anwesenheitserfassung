package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.api.AusweisApiIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.AusweisVerknuepfenErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.AusweisVerknuepfenViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.BerechtigungenViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.EigenausweisMitPersonVerknuepfenViewModelIntern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

/**
 * Dieser Controller stellt für die interne API Endpunkte für Ausweise bereit
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/intern")
public class AusweisInternRestController implements AusweisApiIntern {

    // Service
    private final AusweisService ausweisService;

    // Mapper
    private final AusweisMapper ausweisMapper;

    // Controller Service
    private final AusweisInternRestControllerService controllerService;

    /**
     * Ermittelt die Berechtigungen zu einem Ausweis
     *
     * @param rfidSnr Seriennummer eines RFID-Ausweises (required)
     * @return Berechtigungen zu dem gegebenen Ausweis
     */
    @Override
    public ResponseEntity<BerechtigungenViewModelIntern> getBerechtigungenZuAusweis(@NonNull String rfidSnr) {

        // Ermittle den aktuellen Ausweis mit der Seriennummer
        Ausweis ausweis = ausweisService.getAktuellenAusweisMitSnr(rfidSnr)
                .orElseThrow(() -> new NoSuchElementException(
                        "Kein aktiver Ausweis zu der RFID-Seriennummer '%s' gefunden.".formatted(rfidSnr)));

        // Ermittle die Berechtigungen im Mapping
        return ResponseEntity.ok(ausweisMapper.toBerechtigungViewModel(ausweis));
    }

    /**
     * Endpunkt zum Verknüpfen eines fremden Ausweises mit einer bekannten
     *
     * @param ausweisVerknuepfenViewModelIntern (required) Informationen zur Verknüpfung
     * @return Ergebnis der Verknüpfung
     */
    @Override
    public ResponseEntity<AusweisVerknuepfenErgebnisViewModelIntern> verknuepfeFremdAusweis(
            @NonNull AusweisVerknuepfenViewModelIntern ausweisVerknuepfenViewModelIntern) {
        // Verknüpfe den Ausweis und gib das Ergebnis zurück
        return ResponseEntity.ok(controllerService.verknuepfeFremdausweise(ausweisVerknuepfenViewModelIntern));
    }

    /**
     * Endpunkt zum Verknüpfen eines eigenen Ausweises mit einer bekannten Person
     *
     * @param eigenausweisMitPersonVerknuepfenViewModelIntern (required) Informationen zur Verknüpfung
     * @return Ergebnis der Verknüpfung
     */
	@Override
	public ResponseEntity<EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern> verknuepfeEigenAusweis(
			@NonNull EigenausweisMitPersonVerknuepfenViewModelIntern eigenausweisMitPersonVerknuepfenViewModelIntern) {
		
		// Verknüpfe den Ausweis und gib das Ergebnis zurück
		return ResponseEntity.ok(controllerService.verknuepfeEigenausweisMitPerson(eigenausweisMitPersonVerknuepfenViewModelIntern));
	}

}
