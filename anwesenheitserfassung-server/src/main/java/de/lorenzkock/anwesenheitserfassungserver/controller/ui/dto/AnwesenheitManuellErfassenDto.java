package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AnwesenheitManuellErfassenDto {

	String personId;
	String terminId;
	String raumId;
	Boolean online;

}
