package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.RaumMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.RaumApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.FehlerViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumViewModelExtern;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Verarbeitet Anfragen der externen API zu Räumen
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class RaumRestController extends BaseRestController implements RaumApiExtern {

    // Service
    private final RaumService raumService;

    // Mapper
    private final RaumMapper raumMapper;

    // Repository
    private final RaumRepository raumRepository;

    // Controller Service
    private final RaumRestControllerService controllerService;

    @Override
    public ResponseEntity<List<RaumViewModelExtern>> getRaeume(String bezeichnung, String kennung) {

        List<RaumViewModelExtern> raeumeVM = raumService.getRaeumeMitFilter(Optional.ofNullable(bezeichnung), Optional.ofNullable(kennung), Optional.empty())
                .stream()
                .map(raumMapper::toViewModelExtern)
                .toList();

        return ResponseEntity.ok(raeumeVM);
    }

    @Override
    public ResponseEntity<RaumViewModelExtern> addRaum(RaumErstellenViewModelExtern personViewModel) {

        Raum person = controllerService.fuegeRaumHinzu(personViewModel);

        RaumViewModelExtern vm = raumMapper.toViewModelExtern(person);
        return ResponseEntity.ok(vm);
    }

    @Override
    public ResponseEntity<RaumViewModelExtern> editRaum(RaumBearbeitenViewModelExtern raumViewModel) {

        Raum raum = controllerService.bearbeiteRaum(raumViewModel);

        RaumViewModelExtern vm = raumMapper.toViewModelExtern(raum);
        return ResponseEntity.ok(vm);
    }

    @Override
    public ResponseEntity<RaumViewModelExtern> getRaumById(String id) {
        return ResponseEntity.ok(raumService.getMitID(raumRepository, id)
                .map(raumMapper::toViewModelExtern)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte kein Raum mit der ID '%s' gefunden werden.".formatted(id))));
    }

    @Override
    public ResponseEntity<RaumViewModelExtern> getRaumByKennung(String kennung) {
        return ResponseEntity.ok(raumService.getMitKennung(raumRepository, kennung)
                .map(raumMapper::toViewModelExtern)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte kein Raum mit der Kennung '%s' gefunden werden.".formatted(kennung))));
    }

    /**
     * Fehlerhandling
     * @param ex Abzufangender Fehler
     * @return ViewModel mit dem Fehler
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(Exception ex) {
        return handleError(ex, RaumRestController.class);
    }

}
