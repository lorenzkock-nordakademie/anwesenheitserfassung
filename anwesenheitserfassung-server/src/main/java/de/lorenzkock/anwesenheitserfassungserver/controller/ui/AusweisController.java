package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisUmwandelnDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisId;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Controller behandelt die Anfragen zur Verwaltung von Ausweisen
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/ausweise")
public class AusweisController extends BaseController {

	// Services
	final AusweisService ausweisService;
	final PersonenService personService;

	// Mapper
	final AusweisMapper ausweisMapper;

	// Repository
	final AusweisRepository ausweisRepository;

	// Controller Service
	final AusweisControllerService controllerService;

	/**
	 * Endpunkt zum Anzeigen aller Ausweise zu einem Filter
	 */
	@GetMapping
	public String ausweisenMitFilterGet(@RequestParam(required = false) String rfidSnr,
			@RequestParam(required = false) String kennung, @RequestParam(required = false) List<String> personen,
			@RequestParam(required = false, defaultValue = "1") Integer aktiv, @RequestParam(required = false, defaultValue = "1") Integer eigenausweis,
			@RequestParam(required = false) Integer seite, @RequestParam(required = false) Integer eintraegeJeSeite,
			Model model) {

		// Setzt die Liste von Ausweisen für die ermittelte Seite in das Model
		this.modelAusweisListe(model, rfidSnr, kennung, personen, aktiv, eigenausweis, seite, eintraegeJeSeite);

		return "verwaltung/ausweise";
	}

	/**
	 * Endpunkt zum Verarbeiten des Erstellens eines Ausweises
	 */
	@GetMapping("/erstellen")
	public String ausweisErstellenGet(Model model) {
		model.addAttribute("ausweisErstellen", new AusweisErstellenDto());
		this.modelPersonenListe(model);
		return "verwaltung/ausweise";
	}

	/**
	 * Endpunkt zum Anzeigen eines gewählten Ausweises
	 */
	@GetMapping("/{snr}/{kennung}")
	public String ausweisBearbeitenGet(@PathVariable String snr, @PathVariable String kennung, Model model) {

		try {
			// Ermitteln des Ausweises
			Ausweis ausweis = ausweisService.getMitID(ausweisRepository, AusweisId.builder().rfidSnr(snr).kennung(kennung).build())
					.orElseThrow(() -> new NoSuchElementException(
							"Es konnte keinen Ausweis mit der RFID-Snr. '%s' und Kennung '%s' gefunden werden."
									.formatted(snr, kennung)));


			// Setzen des Models für das Frontend
			model.addAttribute("ausweis", ausweis);
			model.addAttribute("ausweisBearbeiten", ausweisMapper.toBearbeitenDto(ausweis));

			// Wenn dem Ausweis noch keine Person zugewiesen wurde, kann dies noch erfolgen. Also eine Liste
			// möglicher Personen ergänzen
			if (ausweis.getPerson() == null) {
				this.modelPersonenListe(model);
			}

		} catch (NoSuchElementException e) {
			// Fehler Handling, wenn ein Fehler aufgetreten ist
			model.addAttribute("error", "Der Ausweis wurde nicht gefunden");
			this.modelAusweisListe(model);
		}
		return "verwaltung/ausweise";
	}


	/**
	 * Endpunkt zum Verarbeiten des Erstellens eines Ausweises
	 */
	@PostMapping("/erstellen")
	public String ausweisErstellenPost(AusweisErstellenDto dto, Model model) {

		try {
			// Verarbeiten des Erstellens eines Ausweises
			Ausweis ausweis = controllerService.fuegeAusweisHinzu(dto);
			return "redirect:/verwaltung/ausweise/%s/%s?createSuccess".formatted(ausweis.getRfidSnr(),
					ausweis.getKennung());
		} catch (IllegalArgumentException e) {
			// Fehler Handling, wenn beim Erstellen ein Fehler aufgetreten ist
			model.addAttribute("error", e.getMessage());
			model.addAttribute("ausweisErstellen", dto);
			return "verwaltung/ausweise";
		}

	}

	/**
	 * Endpunkt zum Verarbeiten der Bearbeitung eines Ausweises
	 */
	@PostMapping("/zuEigenausweisUmwandeln")
	public String ausweisZuEigenausweisUmwandelnPost(AusweisUmwandelnDto dto, Model model) {

		try {
			// Verarbeiten des Bearbeitens eines Ausweises
			Ausweis ausweis = controllerService.wanldeAusweisUm(dto);
			return "redirect:/verwaltung/ausweise/%s/%s?editSuccess".formatted(ausweis.getRfidSnr(),
					ausweis.getKennung());
		} catch (IllegalArgumentException e) {
			return "redirect:/verwaltung/ausweise/bearbeiten/%s/%s?editError=%s".formatted(dto.getRfidSnr(),
					dto.getKennung(), e.getMessage());
		}

	}

	/**
	 * Endpunkt zum Umwandeln eines Ausweises von Fremdausweis zu Eigenausweis
	 */
	@PostMapping("/{rfidSnr}/{kennung}")
	public String ausweisBearbeitenPost(AusweisBearbeitenDto dto, Model model) {

		try {
			// Verarbeiten des Bearbeitens eines Ausweises
			Ausweis ausweis = controllerService.bearbeiteAusweis(dto);
			return "redirect:/verwaltung/ausweise/%s/%s?editSuccess".formatted(ausweis.getRfidSnr(),
					ausweis.getKennung());
		} catch (IllegalArgumentException e) {
			// Fehler Handling, wenn beim Erstellen ein Fehler aufgetreten ist
			model.addAttribute("error", e.getMessage());
			model.addAttribute("ausweisBearbeiten", dto);
			return "verwaltung/ausweise";
		}

	}

	/*
	 * Methoden zum Aufbauen der Liste von Ausweisen
	 */

	/**
	 * Erstellt die Standard-Liste ohne Filter
	 * @param model Das zu ergänzende Model
	 */
	private void modelAusweisListe(Model model) {
		this.modelAusweisListe(model, null, null, null, 0, 0, null, null);
	}

	/**
	 * Stellt die anzuzeigende Liste von Ausweisen zusammen
	 * @param model Das zu ergänzende Model
	 * @param rfidSnr Filter der Seriennummer des Ausweises
	 * @param kennung Filter der Kennung des Ausweises
	 * @param personenIds Filter einzubeziehender IDs von Personen
	 * @param nurAktive Nur aktive Ausweise anzeigen
	 * @param seite Seite
	 * @param eintraegeJeSeite Einträge je Seite
	 */
	private void modelAusweisListe(Model model, String rfidSnr, String kennung, List<String> personenIds,
								   Integer nurAktive, Integer nurEigenausweise, Integer seite, Integer eintraegeJeSeite) {

		Boolean nurAktiveBool = switch (nurAktive) {
			case -1 -> null;
			case 1 -> Boolean.TRUE;
			default -> Boolean.FALSE;
		};
		
		Boolean nurEigenausweiseBool = switch (nurEigenausweise) {
			case -1 -> null;
			case 1 -> Boolean.TRUE;
			default -> Boolean.FALSE;
		};

		// Lade alle Ausweise zu dem Filter
		List<Ausweis> alleAusweiseMitFilter = ausweisService.findeAusweiseNachFilter(rfidSnr,
				kennung, personenIds, null, nurAktiveBool, nurEigenausweiseBool);

		// Bestimme die Ausweise aus der Liste aller für die gegebene Seite
		List<Ausweis> alleAusweiseFuerSeite = this.ermittleNavigation(model, alleAusweiseMitFilter, seite, eintraegeJeSeite);

		// Setzen des Models für das Frontend
		model.addAttribute("ausweise", alleAusweiseFuerSeite);

		// Setzen des Models für den Filter
		model.addAttribute("filterKennung", kennung == null ? "" : kennung);
		model.addAttribute("filterRfidSnr", rfidSnr == null ? "" : rfidSnr);
		model.addAttribute("filterPersonen", personenIds == null ? List.of() : personenIds);
		model.addAttribute("filterAktiv", nurAktive);
		model.addAttribute("filterEigenausweis", nurEigenausweise);

		// Setzen der Personen Liste für den Filter
		model.addAttribute("personenListe", personService.getPersonenMitFilter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));

	}

	/*
	 * Personen zum Model hinzufügen für Auswahl von Personen
	 */
	private void modelPersonenListe(Model model) {
		model.addAttribute("personen", personService.getPersonenMitFilter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE)));
	}

}
