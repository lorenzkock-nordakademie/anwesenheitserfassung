package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import de.lorenzkock.anwesenheitserfassungserver.enums.ErfassungsModus;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ErfassungStartAendernDto {

	String zeitpunkt;

	String terminId;

}
