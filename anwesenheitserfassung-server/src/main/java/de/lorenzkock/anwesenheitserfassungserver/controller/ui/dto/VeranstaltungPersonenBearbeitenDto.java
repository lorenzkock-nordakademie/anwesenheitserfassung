package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VeranstaltungPersonenBearbeitenDto {

	List<String> removePersonen;
	List<String> addPersonen;

}
