package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.CisImportDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.KonfigurationImportDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnisCollector;
import de.lorenzkock.anwesenheitserfassungserver.importservices.ImportService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.util.Optional;

/**
 * Dieser Service stellt Funktionalität für den KonfigurationController dar und stellt eine Zwischenebene
 * zu den Services dar.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class KonfigurationControllerService {

    // Service
    final ImportService importService;

    /**
     * Importiere die Dateien aus dem DTO
     *
     * @param dto DTO mit den Dateien
     * @return Ergebnis des Imports
     */
    public ImportErgebnisCollector importiere(@NonNull KonfigurationImportDto dto) {

        return this.importService.importiereDateien(Optional.ofNullable(dto.getRaeumeFile()),
                Optional.ofNullable(dto.getAusweiseFile()),
                Optional.ofNullable(dto.getPersonenFile()),
                Optional.ofNullable(dto.getVeranstaltungenFile()),
                Optional.ofNullable(dto.getTermineFile()),
                Optional.ofNullable(dto.getZurodnungenFile()));

    }

    /**
     * Importiere die Dateien aus dem DTO
     *
     * @param dto DTO mit den Dateien
     * @return Ergebnis des Imports
     */
    public ImportErgebnisCollector importiereCis(@NonNull CisImportDto dto) {

        return this.importService.importiereCis(dto);

    }

}
