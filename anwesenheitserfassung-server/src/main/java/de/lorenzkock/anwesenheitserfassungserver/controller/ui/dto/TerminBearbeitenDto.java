package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TerminBearbeitenDto {
    String id;
    String kennung;
    List<String> raumIds;
    String start;
    String ende;
}
