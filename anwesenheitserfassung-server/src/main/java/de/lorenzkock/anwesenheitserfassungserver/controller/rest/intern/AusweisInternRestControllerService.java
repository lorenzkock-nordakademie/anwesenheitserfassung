package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisId;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.AusweisVerknuepfenErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.AusweisVerknuepfenViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.EigenausweisMitPersonVerknuepfenViewModelIntern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Dient als Vermittler zwischen dem {@link AusweisInternRestController} und den benötigten Services.
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class AusweisInternRestControllerService {

    // Repository
    private final AusweisRepository ausweisRepository;
    
    // Repository
    private final PersonRepository personRepository;

    /**
     * Führt eine Verknüfung eines fremden Ausweises zu einem eigenen Ausweis durch
     *
     * @param verknuepfung Daten zu der Verknüpfung
     * @return Ergebnis der Verarbeitung
     */
    public AusweisVerknuepfenErgebnisViewModelIntern verknuepfeFremdausweise(
            @NonNull AusweisVerknuepfenViewModelIntern verknuepfung) {

        // Validiere zunächst das View Model
        this.validiereViewModelFachlich(verknuepfung);

        // Versuche der fremde Ausweis zu finden
        Optional<Ausweis> fremdausweisOpt = this.ausweisRepository
                .findFirstByIdRfidSnrAndAktiv(verknuepfung.getRfidSnrFremd(), true);

        // Wenn der fremde Ausweis nicht gefunden wurde, gib einen Fehler zurück
        if (fremdausweisOpt.isEmpty()) {
            return verknuepfungErgebnis(
                    AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.FREMDAUSWEIS_NICHT_GEFUNDEN);
        }

        Ausweis fremdausweis = fremdausweisOpt.get();

        // Wenn der fremde Ausweis kein fremder Ausweis ist, gib einen Fehler zurück
        if (!fremdausweis.isFremdausweis()) {
            return verknuepfungErgebnis(
                    AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.FREMDAUSWEIS_IST_EIGENAUSWEIS);
        }

        // Wenn der fremde Ausweis bereits zugeordnet ist, gib einen Fehler zurück
        if (fremdausweis.getEigenausweis() != null) {
            return verknuepfungErgebnis(
                    AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.FREMDAUSWEIS_BEREITS_VERKNUEPFT);
        }

        // Versuche den eigenen Ausweis zu finden
        Optional<Ausweis> eigenausweisOpt = this.ausweisRepository
                .findFirstByIdRfidSnrAndAktiv(verknuepfung.getRfidSnrEigen(), true);

        // Wenn der eigene Ausweis nicht gefunden wurde, gib einen Fehler zurück
        if (eigenausweisOpt.isEmpty()) {
            return verknuepfungErgebnis(
                    AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.EIGENAUSWEIS_NICHT_GEFUNDEN);
        }

        Ausweis eigenerausweis = eigenausweisOpt.get();

        // Wenn der eigene Ausweis kein eigener Ausweis ist, gib einen Fehler zurück
        if (eigenerausweis.isFremdausweis()) {
            return verknuepfungErgebnis(
                    AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.EIGENAUSWEIS_IST_FREMDAUSWEIS);
        }

        // Führe die Verknüfung durch
        fremdausweis.setEigenausweis(eigenerausweis);
        fremdausweis.setPerson(eigenerausweis.getPerson());
        this.ausweisRepository.save(fremdausweis);

        return verknuepfungErgebnis(AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH);
    }
    
    /**
     * Führt eine Verknüfung eines eigenen Ausweises zu einer bekannten Person durch
     *
     * @param verknuepfung Daten zu der Verknüpfung
     * @return Ergebnis der Verarbeitung
     */
    public EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern verknuepfeEigenausweisMitPerson(
            @NonNull EigenausweisMitPersonVerknuepfenViewModelIntern verknuepfung) {

        // Validiere zunächst das View Model
        this.validiereViewModelFachlich(verknuepfung);

        // Versuche der fremde Ausweis zu finden
        Optional<Ausweis> eigenausweisOpt = this.ausweisRepository
                .findFirstByIdRfidSnrAndAktiv(verknuepfung.getRfidSnrEigen(), true);
        
        Optional<Person> person = personRepository.findById(verknuepfung.getPersonId());
        
        if(person.isEmpty()) {
        	return verknuepfungErgebnis(EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.PERSON_NICHT_GEFUNDEN);
        }
        
        if(eigenausweisOpt.isEmpty()) {
        	
        	// Erstelle den Ausweis
        	
        	Ausweis ausweis = Ausweis.builder()
        			.aktiv(true)
        			.id(AusweisId.builder().rfidSnr(verknuepfung.getRfidSnrEigen()).kennung(UUID.randomUUID().toString()).build())
        			.person(person.get())
        			.fremdausweis(false)
        			.build();
        	
        	ausweisRepository.save(ausweis);
        	
        	return verknuepfungErgebnis(EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH);
        	
        } else {
        	
        	if(eigenausweisOpt.get().isFremdausweis()) {
        		return verknuepfungErgebnis(EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.EIGENAUSWEIS_IST_FREMDAUSWEIS);
        	} else {
        		
        		if(eigenausweisOpt.get().getPerson() != null && eigenausweisOpt.get().getPerson().getId().equals(verknuepfung.getPersonId())) {
            		return verknuepfungErgebnis(EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.EIGENAUSWEIS_BEREITS_VERKNUEPFT);        			
        		} else {
        			
        			// Wenn eine Person hinterlegt ist, muss der existierende Ausweis deaktiviert werden
        			if(eigenausweisOpt.get().getPerson() != null) {
        				Ausweis eigenausweis = eigenausweisOpt.get();
        				eigenausweis.setAktiv(false);
        				ausweisRepository.save(eigenausweis);
            			
            			// Lege den neuen Ausweis an
        				Ausweis ausweis = Ausweis.builder()
        	        			.aktiv(true)
        	        			.id(AusweisId.builder().rfidSnr(verknuepfung.getRfidSnrEigen()).kennung(UUID.randomUUID().toString()).build())
        	        			.person(person.get())
        	        			.fremdausweis(false)
        	        			.build();
        	        	ausweisRepository.save(ausweis);
        	        	
        	        	return verknuepfungErgebnis(EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.EIGENAUSWEIS_NEU_VERKNUEPFT);
        			} else {
        				Ausweis eigenausweis = eigenausweisOpt.get();
        				eigenausweis.setPerson(person.get());
        				ausweisRepository.save(eigenausweis);
        	        	return verknuepfungErgebnis(EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH);
        			}        			
        		}
        	}        	
        }        
    }

    /**
     * Baut das Objekt mit dem Ergebnis zur Verknüfung
     *
     * @param ergebnis Ergebnis der Verarbeitung
     * @return Objekt mit dem Ergebnis
     */
    private AusweisVerknuepfenErgebnisViewModelIntern verknuepfungErgebnis(
            @NonNull AusweisVerknuepfenErgebnisViewModelIntern.ErgebnisEnum ergebnis) {
        AusweisVerknuepfenErgebnisViewModelIntern ergebnisContainer = new AusweisVerknuepfenErgebnisViewModelIntern();
        ergebnisContainer.setErgebnis(ergebnis);
        return ergebnisContainer;
    }
    

    /**
     * Baut das Objekt mit dem Ergebnis zur Verknüfung
     *
     * @param ergebnis Ergebnis der Verarbeitung
     * @return Objekt mit dem Ergebnis
     */
    private EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern verknuepfungErgebnis(
            @NonNull EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern.ErgebnisEnum ergebnis) {
    	EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern ergebnisContainer = new EigenausweisMitPersonVerknuepfenErgebnisViewModelIntern();
        ergebnisContainer.setErgebnis(ergebnis);
        return ergebnisContainer;
    }


    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelFachlich(final @NonNull AusweisVerknuepfenViewModelIntern viewModel) {
        if (StringUtils.isBlank(viewModel.getRfidSnrEigen()))
            throw new IllegalArgumentException("Die RFID-Snr des Eigenausweises darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getRfidSnrFremd()))
            throw new IllegalArgumentException("Die RFID-Snr des Eigenausweises darf nicht leer sein.");
    }

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelFachlich(final @NonNull EigenausweisMitPersonVerknuepfenViewModelIntern viewModel) {
        if (StringUtils.isBlank(viewModel.getRfidSnrEigen()))
            throw new IllegalArgumentException("Die RFID-Snr des Eigenausweises darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getPersonId()))
            throw new IllegalArgumentException("Die ID der Person darf nicht leer sein.");
    }

}
