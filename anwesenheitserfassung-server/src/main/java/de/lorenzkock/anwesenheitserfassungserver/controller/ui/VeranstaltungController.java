package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.*;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.TerminMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.VeranstaltungMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Dieser Controller behandelt die Anfragen zur Verwaltung von Veranstaltungen und Terminen
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/veranstaltungen")
public class VeranstaltungController extends BaseController {

    // Services
    final VeranstaltungService veranstaltungService;
    final TerminService terminService;
    final PersonenService personService;
    final AusweisService ausweisService;
    final RaumService raumService;

    // Mapper
    final VeranstaltungMapper veranstaltungMapper;
    final TerminMapper terminMapper;

    // Repository
    final TerminRepository terminRepository;
    final VeranstaltungRepository veranstaltungRepository;

    // Controller Service
    final VeranstaltungenControllerServiceVeranstaltungen controllerServiceVeranstaltungen;
    final VeranstaltungenControllerServiceTermine controllerServiceTermine;

    /**
     * Endpunkt zum Anzeigen aller Veranstaltungen zu einem Filter
     */
    @GetMapping
    public String veranstaltungenGet(@RequestParam(required = false) String name,
                                     @RequestParam(required = false) String kennung, @RequestParam(required = false) Integer seite,
                                     @RequestParam(required = false, defaultValue = "0") Integer archiv,
                                     @RequestParam(required = false) Integer eintraegeJeSeite, Model model) {

        // Setzt die Liste von Veranstaltungen für die ermittelte Seite in das Model
        this.modelVeranstaltungListe(model, name, kennung, archiv, seite, eintraegeJeSeite);

        return "verwaltung/veranstaltungen";
    }

    /**
     * Endpunkt zum Erstellen einer Veranstaltung
     */
    @GetMapping("/erstellen")
    public String veranstaltungErstellenGet(Model model) {

        // Leeres DTO, wird im Frontend gefüllt
        model.addAttribute("veranstaltungErstellen", new VeranstaltungErstellenDto());

        // Liste von Räumen für die Erstellung von Terminen
        model.addAttribute("raeumeListe", raumService.getRaeumeMitFilter(Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE)));

        return "verwaltung/veranstaltungen";
    }


    /**
     * Endpunkt zum Verarbeiten des Erstellens einer Veranstaltung
     */
    @PostMapping("/erstellen")
    public String veranstaltungErstellenPost(VeranstaltungErstellenDto dto, Model model) {

        try {
            // Veranstaltung erstellen
            Veranstaltung veranstaltung = controllerServiceVeranstaltungen.fuegeVeranstaltungHinzu(dto);
            return "redirect:/verwaltung/veranstaltungen/%s?createSuccess".formatted(veranstaltung.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungErstellen", dto);

            return "verwaltung/veranstaltungen";

        }

    }

    /**
     * Endpunkt zum Bearbeiten einer Veranstaltung
     */
    @GetMapping("/{id}")
    public String veranstaltungBearbeitenGet(@PathVariable String id, Model model) {

        try {
            // Ermitteln der Veranstaltung
            Veranstaltung veranstaltung = veranstaltungService.getMitID(veranstaltungRepository, id)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(id)));

            // Ermitteln möglicher teilnehmender Personen, die noch nicht an der Veranstaltung Teilnehmen
            List<Person> nichtTeilnehmendePersonen = personService
                    .getPersonenMitFilter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE))
                    .stream()
                    .filter(p -> !veranstaltung.getTeilnehmendePersonen().contains(p))
                    .toList();
            model.addAttribute("nichtTeilnehmendePersonen", nichtTeilnehmendePersonen);

            // Ermitteln möglicher berechtigter Personen, die noch nicht zu der Veranstaltung berechtigt sind
            List<Person> nichtBerechtigtePersonen = personService
                    .getPersonenMitFilter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE))
                    .stream()
                    .filter(p -> !veranstaltung.getBerechtigtePersonen().contains(p))
                    .toList();
            model.addAttribute("nichtBerechtigtePersonen", nichtBerechtigtePersonen);

            // Ermitteln möglicher teilnehmender Ausweise, die noch nicht an der Veranstaltung Teilnehmen
            List<Ausweis> nichtTeilnehmendeAusweise = ausweisService
                    .findeAusweiseNachFilter(null, null, null, null, null, true)
                    .stream()
                    .filter(Ausweis::isAktiv)
                    .filter(i -> !veranstaltung.getTeilnehmendeAusweise().contains(i))
                    .toList();
            model.addAttribute("nichtTeilnehmendeAusweise", nichtTeilnehmendeAusweise);

            // Ermitteln möglicher berechtigter Ausweise, die noch nicht zu der Veranstaltung berechtigt sind
            List<Ausweis> nichtBerechtigteAusweise = ausweisService
                    .findeAusweiseNachFilter(null, null, null, null, null, true)
                    .stream()
                    .filter(Ausweis::isAktiv)
                    .filter(i -> !veranstaltung.getBerechtigteAusweise().contains(i))
                    .toList();
            model.addAttribute("nichtBerechtigteAusweise", nichtBerechtigteAusweise);

            // Ermittle die Räume für die Auswahl bei der Anlage neuer Termine
            List<Raum> raumeFuerAuswahl = new ArrayList<>(raumService.getRaeumeMitFilter(Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE)));

            // Setzen des Models für das Frontend
            model.addAttribute("veranstaltungBearbeiten", veranstaltungMapper.toBearbeitenDto(veranstaltung));
            model.addAttribute("veranstaltung", veranstaltung);
            model.addAttribute("raeumeListe", raumeFuerAuswahl);
            model.addAttribute("terminErstellen", TerminErstellenDto.builder().build());

        } catch (NoSuchElementException e) {
            // Fehlerhandling, wenn die Veranstaltung nicht gefunden wurde
            model.addAttribute("error", "Die Veranstaltung wurde nicht gefunden");
            this.modelVeranstaltungListe(model);
        }
        return "verwaltung/veranstaltungen";

    }

    /**
     * Endpunkt zum Verarbeiten des Bearbeitens einer Veranstaltung
     */
    @PostMapping("/{id}")
    public String veranstaltungBearbeitenPost(VeranstaltungBearbeitenDto dto, Model model) {

        try {
            // Bearbeiten der Veranstaltung
            Veranstaltung veranstaltung = controllerServiceVeranstaltungen.bearbeiteVeranstaltung(dto);
            return "redirect:/verwaltung/veranstaltungen/%s?editSuccess".formatted(veranstaltung.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungBearbeiten", dto);
            return "verwaltung/veranstaltungen";
        }

    }


    /**
     * Endpunkt zum Bearbeiten der teilnehmenden Personen einer Veranstaltung
     */
    @PostMapping("/{id}/teilnehmendeBearbeiten/personen")
    public String veranstaltungBearbeiteTeilnehmendePersonenPost(@PathVariable String id,
                                                                 VeranstaltungPersonenBearbeitenDto dto, Model model) {
        try {
            // Bearbeite teilnehmende Personen zu Veranstaltung
            Veranstaltung veranstaltung = controllerServiceVeranstaltungen.bearbeiteTeilnehmendePersonen(id, dto);
            return "redirect:/verwaltung/veranstaltungen/%s?editSuccess".formatted(veranstaltung.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungErstellen", dto);

            return "verwaltung/veranstaltungen";
        }
    }

    /**
     * Endpunkt zum Bearbeiten der berechtigten Personen einer Veranstaltung
     */
    @PostMapping("/{id}/berechtigteBearbeiten/personen")
    public String veranstaltungBearbeiteBerechtigtePersonenPost(@PathVariable String id,
                                                                VeranstaltungPersonenBearbeitenDto dto, Model model) {
        try {
            // Bearbeite berechtigte Personen zu Veranstaltung
            Veranstaltung veranstaltung = controllerServiceVeranstaltungen.bearbeiteBerechtigtePersonen(id, dto);
            return "redirect:/verwaltung/veranstaltungen/%s?editSuccess".formatted(veranstaltung.getId());
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungErstellen", dto);

            return "verwaltung/veranstaltungen";
        }
    }

    /**
     * Endpunkt zum Bearbeiten der teilnehmenden Ausweise einer Veranstaltung
     */
    @PostMapping("/{id}/teilnehmendeBearbeiten/ausweise")
    public String veranstaltungBearbeiteTeilnehmendeAusweisePost(@PathVariable String id,
                                                                     VeranstaltungAusweiseBearbeitenDto dto, Model model) {
        try {
            // Bearbeite teilnehmende Ausweise zu Veranstaltung
            Veranstaltung veranstaltung = controllerServiceVeranstaltungen.bearbeiteTeilnehmendeAusweise(id, dto);
            return "redirect:/verwaltung/veranstaltungen/%s?editSuccess".formatted(veranstaltung.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungErstellen", dto);

            return "verwaltung/veranstaltungen";
        }
    }

    /**
     * Endpunkt zum Bearbeiten der berechtigten Ausweise einer Veranstaltung
     */
    @PostMapping("/{id}/berechtigteBearbeiten/ausweise")
    public String veranstaltungBearbeiteBerechtigteAusweisePost(@PathVariable String id,
                                                                    VeranstaltungAusweiseBearbeitenDto dto, Model model) {
        try {
            // Bearbeite berechtigte Ausweise zu Veranstaltung
            Veranstaltung veranstaltung = controllerServiceVeranstaltungen.bearbeiteBerechtigteAusweise(id, dto);
            return "redirect:/verwaltung/veranstaltungen/%s?editSuccess".formatted(veranstaltung.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungErstellen", dto);

            return "verwaltung/veranstaltungen";
        }
    }

    /**
     * Endpunkt zum Archivieren einer Veranstaltung
     */
    @GetMapping("/{id}/archivieren")
    public String veranstaltungArchivierenGet(@PathVariable String id, Model model) {

        try {
            // Veranstaltung archivieren
            veranstaltungService.archiviere(veranstaltungRepository, id);
            return "redirect:/verwaltung/veranstaltungen?deleteSuccess";
        } catch (NoSuchElementException e) {
            // Fehlerhandling, wenn Veranstaltung nicht gefunden wurde
            model.addAttribute("error", e.getMessage());
        } catch (Exception e) {
            // Fehlerhandling, wenn Veranstaltung nicht archiviert werden kann
            model.addAttribute("error", "Fehler beim Archivieren der Veranstaltung");
        }

        // Setzen des Models für das Frontend
        this.modelVeranstaltungListe(model);
        return "verwaltung/veranstaltungen";

    }

    /**
     * Endpunkt zum Verarbeiten des Erstellens eines Termins
     */
    @PostMapping("{id}/termin")
    public String terminErstellenPost(@PathVariable("id") String veranstaltungId, TerminErstellenDto dto, Model model) {

        try {
            controllerServiceTermine.fuegeTerminHinzu(veranstaltungId, dto);
            return "redirect:/verwaltung/veranstaltungen/%s?createTerminSuccess".formatted(veranstaltungId);
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("veranstaltungErstellen", dto);

            return "verwaltung/veranstaltungen";

        }

    }

    /**
     * Endpunkt zum Bearbeiten eines Termins (gibt nur einen Ausschnitt, der vom Frontend des Bearbeitens einer Veranstaltung eingebunden wird)
     */
    @GetMapping("/termin/{editTerminId}")
    public String terminBearbeitenGet(@PathVariable String editTerminId, Model model) {

        try {
            // Ermitteln des Termins
            Termin termin = terminService.getMitID(terminRepository, editTerminId)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte kein Termin mit der ID '%s' gefunden werden.".formatted(editTerminId)));

            // Setzen des Models für das Frontend
            model.addAttribute("terminBearbeiten", terminMapper.toBearbeitenDto(termin));
            model.addAttribute("veranstaltungId", termin.getVeranstaltung().getId());

            // Räume für Auswahl ermitteln und setzen
            List<Raum> raumeFuerAuswahl = new ArrayList<>(raumService.getRaeumeMitFilter(Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE)));
            // Wenn der gewählte Raum bereits archiviert ist, muss er ergänzt werden
            if (termin.getRaeume() != null) {
                for(Raum raum : termin.getRaeume()) {
                    if (!raumeFuerAuswahl.contains(raum)) {
                        raumeFuerAuswahl.add(raum);
                    }
                }
            }
            model.addAttribute("raeumeListe", raumeFuerAuswahl);

        } catch (NoSuchElementException e) {
            // Fehlerhandling, wenn Termin nicht gefunden
            model.addAttribute("error", "Der Termin wurde nicht gefunden");
        }
        return "verwaltung/termine";

    }

    /**
     * Endpunkt zum Verarbeiten des Bearbeitens eines Termins
     */
    @PostMapping("{id}/termin/bearbeiten")
    public String terminBearbeitenPost(@PathVariable("id") String veranstaltungId, TerminBearbeitenDto dto) {

        try {
            // Bearbeiten eines Termins
            controllerServiceTermine.bearbeiteTermin(dto);
            return "redirect:/verwaltung/veranstaltungen/%s?editTerminSuccess".formatted(veranstaltungId);
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            return "redirect:/verwaltung/veranstaltungen/%s?editTerminError=%s".formatted(veranstaltungId,
                    e.getMessage());

        }

    }

    /**
     * Endpunkt zum Löschen eines Termins
     */
    @GetMapping("{veranstaltungId}/termin/{terminId}/loeschen")
    public String terminLoeschenGet(@PathVariable String veranstaltungId, @PathVariable String terminId, Model model) {

        try {
            // Löschen des Termins
            terminService.entferneTerminMitId(terminId);
            return "redirect:/verwaltung/veranstaltungen/%s?deleteTerminSuccess".formatted(veranstaltungId);
        } catch (NoSuchElementException | IllegalStateException e) {
            // Fehlerhandling, wenn Termin nicht gefunden oder Erfassungen existieren
            model.addAttribute("error", e.getMessage());
            return "redirect:/verwaltung/veranstaltungen/%s?deleteTerminError=%s".formatted(veranstaltungId,
                    e.getMessage());
        } catch (Exception e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", "Fehler beim Löschen des Termins.");
            return "redirect:/verwaltung/veranstaltungen/%s?deleteTerminError=%s".formatted(veranstaltungId,
                    "Fehler beim Löschen des Termins.");
        }

    }

    /*
     * Methoden zum Aufbauen der Liste von Veranstaltungen
     */

    /**
     * Erstellt die Standard-Liste ohne Filter
     *
     * @param model Das zu ergänzende Model
     */
    private void modelVeranstaltungListe(Model model) {
        this.modelVeranstaltungListe(model, null, null, 0, null, null);
    }

    /**
     * Stellt die anzuzeigende Liste von Veranstaltungen zusammen
     *
     * @param model            Das zu ergänzende Model
     * @param name             Filter des Namens einer Veranstaltung
     * @param kennung           Filter der Kennung einer Veranstaltung
     * @param archiv           Archivierte Personen anzeigen
     * @param seite            Seite
     * @param eintraegeJeSeite Einträge je Seite
     */
    private void modelVeranstaltungListe(Model model, String name, String kennung, Integer archiv, Integer seite,
                                         Integer eintraegeJeSeite) {


        Boolean archivBool = switch (archiv) {
            case -1 -> null;
            case 1 -> Boolean.TRUE;
            default -> Boolean.FALSE;
        };
        // Lade alle Veranstaltungen zu dem Filter
        List<Veranstaltung> alleVeranstaltungenMitFilter = veranstaltungService.getVeranstaltungenMitFilter(Optional.empty(),
                Optional.ofNullable(kennung), Optional.ofNullable(name), Optional.ofNullable(archivBool));

        // Bestimme die Veranstaltungen aus der Liste aller für die gegebene Seite
        List<Veranstaltung> veranstaltungenFuerSeite = this.ermittleNavigation(model, alleVeranstaltungenMitFilter, seite, eintraegeJeSeite);

        // Setzen des Models für das Frontend
        model.addAttribute("veranstaltungen", veranstaltungenFuerSeite);

        // Setzen des Models für den Filter
        model.addAttribute("filterArchiv", archiv);
        model.addAttribute("filterKennung", kennung == null ? "" : kennung);
        model.addAttribute("filterName", name == null ? "" : name);
    }
}
