package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.*;

import java.time.OffsetDateTime;
import java.util.Optional;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ErfassungStartAendernErgebnisDto {

	@Builder.Default
	Optional<OffsetDateTime> zeitpunkt = Optional.empty();

	@Builder.Default
	Optional<String> fehler = Optional.empty();

}
