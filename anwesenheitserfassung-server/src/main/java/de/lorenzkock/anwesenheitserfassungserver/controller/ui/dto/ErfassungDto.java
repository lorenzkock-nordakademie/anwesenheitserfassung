package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.time.OffsetDateTime;

import de.lorenzkock.anwesenheitserfassungserver.enums.ErfassungsModus;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ErfassungDto {

	OffsetDateTime zeitpunkt;
	ErfassungsModus modus;
	PersonDto person;
	AusweisDto ausweis;
	String raumBezeichnung;
	Boolean onlineAnwesend;

	public String getZeitpunktFormatted() {
		return DatumHelper.formatDateTime(zeitpunkt);
	}

}
