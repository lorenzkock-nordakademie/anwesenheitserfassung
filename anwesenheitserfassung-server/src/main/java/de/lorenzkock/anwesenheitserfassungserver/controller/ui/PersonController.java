package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Controller zum Verarbeiten von Anfragen zur Verwaltung von Personen über das UI
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/personen")
public class PersonController extends BaseController {

    // Service
    final PersonenService personService;

    // Mapper
    final PersonMapper personMapper;

    // Mapper
    final PersonRepository personRepository;

    // ControllerService
    final PersonControllerService controllerService;

    /**
     * Liste von Personen zu der Seite und Einträge je Seite mit dem gegebenen Filter
     */
    @GetMapping
    public String personenGet(@RequestParam(required = false) String vorname,
                              @RequestParam(required = false) String nachname, @RequestParam(required = false) String kennung,
                              @RequestParam(required = false, defaultValue = "0") Integer archiv,
                              @RequestParam(required = false) Integer seite, @RequestParam(required = false) Integer eintraegeJeSeite,
                              Model model) {

        // Ermitteln der anzuzeigenden Elemente und Hinzufügen zum Model
        this.modelPersonListe(model, vorname, nachname, kennung, archiv, seite, eintraegeJeSeite);
        return "verwaltung/personen";

    }

    /**
     * Endpunkt zum Erstellen einer Person
     */
    @GetMapping("/erstellen")
    public String personErstellenGet(Model model) {
        // Leeres DTO was über das Formular im Frontend gefüllt wird
        model.addAttribute("personErstellen", new PersonErstellenDto());
        return "verwaltung/personen";
    }

    /**
     * Endpunkt zum Anzeigen und Bearbeiten einer Person
     */
    @GetMapping("/{id}")
    public String personBearbeitenGet(@PathVariable String id, Model model) {

        try {
            // Ermitteln der Person
            Person person = personService.getMitID(personRepository, id)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte keine Person mit der ID '%s' gefunden werden.".formatted(id)));

            // Setzen des Models für das Frontend
            model.addAttribute("personBearbeiten", personMapper.toBearbeitenDto(person));
            model.addAttribute("person", person);
        } catch (NoSuchElementException e) {
            // Fehlerhandling, wenn Person nicht existiert
            model.addAttribute("error", e.getMessage());
            this.modelPersonListe(model);
        }
        return "verwaltung/personen";

    }

    /**
     * Endpunkt zum Archivieren einer Person
     */
    @GetMapping("/{id}/archivieren")
    public String personArchivierenGet(@PathVariable String id, Model model) {

        try {
            // Archiviert eine Person
            personService.archiviere(personRepository, id);
            return "redirect:/verwaltung/personen?deleteSuccess";
        } catch (NoSuchElementException e) {
            // Fehlerhandling, wenn Person nicht existiert
            model.addAttribute("error", e.getMessage());
        } catch (Exception e) {
            // Fehlerhandling, wenn Person nicht archiviert werden kann
            model.addAttribute("error", "Fehler beim Archivieren der Person");
        }

        // Setzen des Models für das Frontend
        this.modelPersonListe(model);
        return "verwaltung/personen";

    }

    /**
     * Endpunkt zum Erstellen einer Person
     */
    @PostMapping("/erstellen")
    public String personErstellenPost(PersonErstellenDto dto, Model model) {

        try {
            // Hinzufügen der Person
            Person person = controllerService.fuegePersonHinzu(dto);
            return "redirect:/verwaltung/personen/%s?createSuccess".formatted(person.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("personErstellen", dto);
            return "verwaltung/personen";
        }

    }

    /**
     * Endpunkt zum Verarbeiten des Bearbeitens einer Person
     */
    @PostMapping("/{id}")
    public String personBearbeitenPost(PersonBearbeitenDto dto, Model model) {

        try {
            // Bearbeiten der Person
            Person person = controllerService.bearbeitePerson(dto);
            return "redirect:/verwaltung/personen/%s?editSuccess".formatted(person.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("personBearbeiten", dto);
            return "verwaltung/personen";
        }

    }

    /*
     * Methoden zum Aufbauen der Liste von Ausweisen
     */

    /**
     * Erstellt die Standard-Liste ohne Filter
     * @param model Das zu ergänzende Model
     */
    private void modelPersonListe(Model model) {
        this.modelPersonListe(model, null, null, null, 0, null, null);
    }

    /**
     * Stellt die anzuzeigende Liste von Personen zusammen
     * @param model Das zu ergänzende Model
     * @param vorname Filter des Vornamens der Person
     * @param nachname Filter des Nachnamens der Person
     * @param kennung Filter der Kennung der Person
     * @param archiv Archivierte Personen anzeigen
     * @param seite Seite
     * @param eintraegeJeSeite Einträge je Seite
     */
    private void modelPersonListe(Model model, String vorname, String nachname, String kennung, Integer archiv,
                                  Integer seite, Integer eintraegeJeSeite) {

        // Lade alle Personen zu dem Filter
        Boolean archivBool = switch (archiv) {
            case -1 -> null;
            case 1 -> Boolean.TRUE;
            default -> Boolean.FALSE;
        };

        // Lade alle Personen zu dem Filter
        List<Person> allePersonenMitFilter = personService.getPersonenMitFilter(Optional.ofNullable(vorname), Optional.ofNullable(nachname), Optional.ofNullable(kennung), Optional.ofNullable(archivBool));

        // Bestimme die Personen aus der Liste aller für die gegebene Seite
        List<Person> allePersonenFuerSeite = this.ermittleNavigation(model, allePersonenMitFilter, seite, eintraegeJeSeite);

        // Setzen des Models für das Frontend
        model.addAttribute("personen", allePersonenFuerSeite);

        // Setzen des Models für den Filter
        model.addAttribute("filterArchiv", archiv);
        model.addAttribute("filterKennung", kennung == null ? "" : kennung);
        model.addAttribute("filterVorname", vorname == null ? "" : vorname);
        model.addAttribute("filterNachname", nachname == null ? "" : nachname);
    }

}
