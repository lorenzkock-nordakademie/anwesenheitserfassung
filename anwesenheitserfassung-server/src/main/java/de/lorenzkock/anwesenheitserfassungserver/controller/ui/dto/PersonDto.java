package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.diff.StringsComparator;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PersonDto {

	@EqualsAndHashCode.Include
	String id;
	String kennung;
	String vorname;
	String nachname;

	public String getName() {
		return "%s, %s".formatted(nachname, vorname);
	}

}
