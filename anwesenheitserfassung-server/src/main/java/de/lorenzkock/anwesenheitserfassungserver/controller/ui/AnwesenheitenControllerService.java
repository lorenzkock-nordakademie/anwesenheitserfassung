package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisZuPersonZuweisenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisZuPersonZuweisenErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungKomprimiertDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungStartAendernDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungStartAendernErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.NichtZugewiesenerAusweisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErfassenErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TeilnehmerAnwesenheitAusweisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TeilnehmerAnwesenheitPersonDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TerminAnwesenheitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungAnwesenheitenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.Erfassung;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungAusweis;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.enums.AusweisZuPersonZuweisenValidierungsErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.ErfassungMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Dieser Service stellt Funktionalität für den AnwesenheitenController dar und
 * stellt eine Zwischenebene zu den Services dar.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class AnwesenheitenControllerService {

	// Repositories
	final private VeranstaltungRepository veranstaltungRepository;
	final private TerminRepository terminRepository;

	// Mapper
	final private ErfassungMapper erfassungMapper;
	final private AusweisMapper ausweisMapper;
	final private PersonMapper personMapper;

	// Services
	final private ErfassungService erfassungService;
	final private AusweisService ausweisService;

	/**
	 * Verarbeite die Erfassung zu einer Person, einem Termin sowie einem Zeitpunkt.
	 *
	 * @param personId  ID der Person. Wirft {@link NullPointerException}, wenn
	 *                  null.
	 * @param terminId  ID des Termins. Wirft {@link NullPointerException}, wenn
	 *                  null.
	 * @param zeitpunkt Zeitpunkt der Erfassung. Wirft {@link NullPointerException},
	 *                  wenn null.
	 * @return Ergebnis der Verarbeitung
	 */
	public PersonErfassenErgebnisDto erfassungZuPersonVerarbeiten(@NonNull String personId, @NonNull String terminId,
			@NonNull OffsetDateTime zeitpunkt, @NonNull Optional<String> raumId, Boolean online) {
		return this.erfassungService.erfassungZuPersonVerarbeiten(personId, terminId, zeitpunkt, raumId, online);
	}

	/**
	 * Methode zum Verarbeiten einer Anfrage zum Ändern des Startzeitpunkts eines
	 * Termins
	 * 
	 * @param dto Daten der Änderung
	 * @return Fehlermeldung, wenn ein Fehler auftritt. Ansonsten leeres Optional.
	 */
	public ErfassungStartAendernErgebnisDto erfassungStartZeitpunktAendern(@NonNull ErfassungStartAendernDto dto) {

		// Wenn String null setze auf jetzt
		Optional<OffsetDateTime> zeitpunkt = Optional.of(OffsetDateTime.now());
		if (dto.getZeitpunkt() != null) {
			// Wenn der String leer ist, lösche das Datum
			if (StringUtils.isBlank(dto.getZeitpunkt())) {
				zeitpunkt = Optional.empty();
			} else {
				// Ansonsten versuche das Datum zu parsen
				try {
					zeitpunkt = Optional.ofNullable(DatumHelper.toDateTime(dto.getZeitpunkt()));
				} catch (DateTimeParseException ex) {
					return ErfassungStartAendernErgebnisDto.builder()
							.fehler(Optional.of("Das Datum ist im falschen Format."))
							.build();
				}
			}
		}

		this.erfassungService.aendereErfassungStart(zeitpunkt, dto.getTerminId());
		return ErfassungStartAendernErgebnisDto.builder().zeitpunkt(zeitpunkt).build();
	}

	/**
	 * Verarbeitet ein {@link AusweisZuPersonZuweisenDto} Objekt, um einen nicht
	 * zugewiesenen Ausweis einer Person zuzuweisen.
	 *
	 * @param dto Instanz von {@link AusweisZuPersonZuweisenDto} mit den
	 *            Informationen zur Zuweisung. Wirft {@link NullPointerException},
	 *            wenn null.
	 * @return Ergebnis der Zuweisung als {@link AusweisZuPersonZuweisenErgebnisDto}
	 */
	public AusweisZuPersonZuweisenErgebnisDto ausweisZuweisen(@NonNull AusweisZuPersonZuweisenDto dto) {

		AusweisZuPersonZuweisenValidierungsErgebnis ergebnis = this.ausweisService.ausweisZuweisen(dto.getRfidSnr(),
				dto.getPersonId(), dto.isFremdausweis());
		return AusweisZuPersonZuweisenErgebnisDto.builder().ergebnis(ergebnis).build();
	}

	/**
	 * Ermittelt alle Anwesenheiten von teilnehmenden Personen und Ausweise zu einer
	 * Termin-ID
	 *
	 * @param terminId ID des Termins, zu dem die Anwesenheiten ermittelt werden
	 *                 sollen. Wirft {@link NullPointerException}, wenn null.
	 * @return {@link TerminAnwesenheitenDto} Aggregator für teilnehmende Personen
	 *         und deren Anwesenheiten, teilnehmende Ausweise und deren
	 *         Anwesenheiten sowie nicht zugeordneten Ausweisen
	 */
	public TerminAnwesenheitenDto anwesenheitenUndNichtZugewieseneAusweiseZuTermin(@NonNull String terminId) {

		// Ermittelt den Termin
		Termin termin = terminRepository.findById(terminId)
				.orElseThrow(() -> new NoSuchElementException(
						"Der Termin mit der ID '%s' wurde nicht gefunden".formatted(terminId)));

		boolean veranstaltungMehrfachErfassungenErlaubt = termin.getVeranstaltung().isErlaubeMehrfachBuchungen();
		boolean veranstaltungUnterscheidungEinAusgehend = termin.getVeranstaltung()
				.isErlaubeEinUndAusgehendeBuchungen();

		// Ermitteln der Anwesenheiten von zu dem Termin oder der Veranstaltung
		// teilnehmenden Personen (auch über Ausweise)
		List<TeilnehmerAnwesenheitPersonDto> teilnehmendePersonen = new ArrayList<>();
		for (Person person : termin.getAlleTeilnehmendenPersonen()) {

			// Ermittle alle Erfassungen zu der Person und dem Termin, um Informationen wie
			// den Status oder mögliche Aktionen zu generieren
			List<Erfassung> erfassungen = this.erfassungService.getErfassungenZuPersonUndTermin(person,
					Optional.of(termin.getId()));
			teilnehmendePersonen
					.add(this.buildAndAddTeilnehmerAnwesenheitDtoForPerson(veranstaltungMehrfachErfassungenErlaubt,
							veranstaltungUnterscheidungEinAusgehend, person, erfassungen));

		}

		// Ermitteln der Anwesenheiten von zu dem Termin oder der Veranstaltung
		// teilnehmenden Ausweise
		List<TeilnehmerAnwesenheitAusweisDto> teilnehmendeAusweise = new ArrayList<>();
		for (Ausweis ausweis : termin.getAlleTeilnehmendenAusweise(false)) {

			// Ermittle alle Erfassungen zu dem Ausweis und dem Termin, um Informationen wie
			// den Status oder mögliche Aktionen zu generieren
			List<Erfassung> erfassungen = this.erfassungService.getErfassungenZuAusweisUndTermin(ausweis,
					Optional.of(termin.getId()));
			teilnehmendeAusweise
					.add(buildAndAddTeilnehmerAnwesenheitDtoForAusweis(veranstaltungMehrfachErfassungenErlaubt,
							veranstaltungUnterscheidungEinAusgehend, ausweis, erfassungen));
		}

		List<Erfassung> erfassungenZuTermin = erfassungService.getErfassungenZuTermin(termin);
		List<ErfassungDto> erfassungenZuTerminSorted = erfassungenZuTermin.stream()
				.sorted(Comparator.comparing(Erfassung::getZeitpunkt).reversed())
				.map(erfassungMapper::toDto)
				.toList();

		// Zusammenstellen des Aggregators
		return TerminAnwesenheitenDto.builder()
				.termin(termin)
				.teilnehmendePersonen(teilnehmendePersonen)
				.teilnehmendeAusweise(teilnehmendeAusweise)
				.erfassungen(erfassungenZuTerminSorted)
				// Ermittle die nicht zugewiesenen Ausweise zu dem Termin
				.nichtZugewieseneAusweise(this.ermittleNichtZugewieseneAusweise(termin))
				.build();
	}

	/**
	 * Ermittelt die Anwesenheiten zu einer Veranstaltung und den Terminen der
	 * Veranstaltung. Außerdem werden die nicht zugewiesenen Ausweise zu den
	 * jeweiligen Terminen der Veranstaltung ermittelt.
	 *
	 * @param veranstaltungId ID der Veranstaltung. Wirft
	 *                        {@link NullPointerException}, wenn null.
	 * @return Aggregator, der die Teilnehmer (Personen und Ausweise) zu der
	 *         Veranstaltung und zu den Terminen enthält.
	 */
	public VeranstaltungAnwesenheitenDto anwesenheitenZuVeranstaltung(@NonNull String veranstaltungId) {

		// Ermittelt die Veranstaltung
		Veranstaltung veranstaltung = veranstaltungRepository.findById(veranstaltungId)
				.orElseThrow(() -> new NoSuchElementException(
						"Die Veranstaltung mit der ID '%s' wurde nicht gefunden".formatted(veranstaltungId)));

		boolean veranstaltungMehrfachErfassungenErlaubt = veranstaltung.isErlaubeMehrfachBuchungen();
		boolean veranstaltungUnterscheidungEinAusgehend = veranstaltung.isErlaubeEinUndAusgehendeBuchungen();

		// Ermitteln der Anwesenheiten zu den Teilnehmenden Personen (auch über
		// Ausweise)
		List<TeilnehmerAnwesenheitPersonDto> teilnehmendePersonen = new ArrayList<>();
		for (Person person : veranstaltung.getTeilnehmendePersonen()) {

			// Ermittle alle Erfassungen zu der Person und der Veranstaltung, um
			// Informationen wie den Status oder mögliche Aktionen zu generieren
			List<Erfassung> erfassungen = this.erfassungService.getErfassungenZuPersonUndVeranstaltung(person,
					Optional.of(veranstaltung.getId()));
			teilnehmendePersonen
					.add(this.buildAndAddTeilnehmerAnwesenheitDtoForPerson(veranstaltungMehrfachErfassungenErlaubt,
							veranstaltungUnterscheidungEinAusgehend, person, erfassungen));
		}

		// Ermitteln der Anwesenheiten zu den Ausweisen
		List<TeilnehmerAnwesenheitAusweisDto> teilnehmendeAusweise = new ArrayList<>();
		for (Ausweis ausweis : veranstaltung.getTeilnehmendeAusweise()) {

			// Ermittle alle Erfassungen zu dem Ausweis und der Veranstaltung, um
			// Informationen wie den Status oder mögliche Aktionen zu generieren
			List<Erfassung> erfassungen = this.erfassungService.getErfassungenZuAusweisUndVeranstaltung(ausweis,
					Optional.of(veranstaltung.getId()));
			teilnehmendeAusweise
					.add(buildAndAddTeilnehmerAnwesenheitDtoForAusweis(veranstaltungMehrfachErfassungenErlaubt,
							veranstaltungUnterscheidungEinAusgehend, ausweis, erfassungen));
		}

		return VeranstaltungAnwesenheitenDto.builder()
				.veranstaltung(veranstaltung)
				.teilnehmendePersonen(teilnehmendePersonen)
				.teilnehmendeAusweise(teilnehmendeAusweise)
				// Ermitteln der Anwesenheiten zu den einzelnen Terminen
				.termine(veranstaltung.getTermine()
						.stream()
						.map(Termin::getId)
						.map(this::anwesenheitenUndNichtZugewieseneAusweiseZuTermin)
						.toList())
				.build();

	}

	/**
	 * Stellt das DTO mit den Informationen zur Anwesenheit zu dem Ausweis zusammen
	 *
	 * @param veranstaltungMehrfachErfassungenErlaubt Information, ob zu der
	 *                                                Veranstaltung Ausweise
	 *                                                mehrfach erfasst werden können
	 * @param veranstaltungUnterscheidungEinAusgehend Information, ob bei der
	 *                                                Veranstaltung zwischen ein-
	 *                                                und ausgehenden Personen
	 *                                                unterschieden werden soll
	 * @param ausweis                                 Der Ausweis zu der die
	 *                                                Anwesenheiten ermittelt werden
	 * @param erfassungen                             Die Erfassungen zu dem Ausweis
	 */
	private TeilnehmerAnwesenheitAusweisDto buildAndAddTeilnehmerAnwesenheitDtoForAusweis(
			boolean veranstaltungMehrfachErfassungenErlaubt, boolean veranstaltungUnterscheidungEinAusgehend,
			Ausweis ausweis, List<Erfassung> erfassungen) {
		// Mappe die Erfassungen
		List<ErfassungDto> erfassungenDtos = erfassungen.stream().map(erfassungMapper::toDto).toList();

		// Baue das AnwesenheitenDto
		return TeilnehmerAnwesenheitAusweisDto.builder()
				.ausweis(ausweisMapper.toDto(ausweis))
				.erfassungen(erfassungenDtos)
				// Komprimiere die Erfassungen ggf. zu kommen oder zu kommen und Gehen
				// Kombinationen
				.erfassungenKomprimiert(
						this.komprimiereErfassungen(erfassungenDtos, veranstaltungUnterscheidungEinAusgehend))
				// Ermittle den Status der Anwesenheit (z.B. Abwesend, Anwesend, Mehrfach
				// anwesend, ...)
				.status(this.erfassungService.statusZuAnzahlErfassungen(erfassungen.size(),
						veranstaltungUnterscheidungEinAusgehend))
				// Ermittle die möglichen Aktionen zu der Person (z.B. Erfassen, Wiederkehren,
				// Ausgehen, erneut Erfassen)
				.aktion(this.erfassungService.aktionZuAnzahlErfassungen(erfassungen.size(),
						veranstaltungUnterscheidungEinAusgehend, veranstaltungMehrfachErfassungenErlaubt))
				.build();
	}

	/**
	 * Stellt das DTO mit den Informationen zur Anwesenheit zu der Person zusammen
	 *
	 * @param veranstaltungMehrfachErfassungenErlaubt Information, ob zu der
	 *                                                Veranstaltung Ausweise
	 *                                                mehrfach erfasst werden können
	 * @param veranstaltungUnterscheidungEinAusgehend Information, ob bei der
	 *                                                Veranstaltung zwischen ein-
	 *                                                und ausgehenden Personen
	 *                                                unterschieden werden soll
	 * @param person                                  Die Person zu der die
	 *                                                Anwesenheiten ermittelt werden
	 * @param erfassungen                             Die Erfassungen zu dem Ausweis
	 */
	private TeilnehmerAnwesenheitPersonDto buildAndAddTeilnehmerAnwesenheitDtoForPerson(
			boolean veranstaltungMehrfachErfassungenErlaubt, boolean veranstaltungUnterscheidungEinAusgehend,
			Person person, List<Erfassung> erfassungen) {
		// Mappe die Erfassungen
		List<ErfassungDto> erfassungenDtos = erfassungen.stream().map(erfassungMapper::toDto).toList();
		return TeilnehmerAnwesenheitPersonDto.builder()
				.person(personMapper.toDto(person))
				.erfassungen(erfassungenDtos)
				// Komprimiere die Erfassungen ggf. zu kommen oder zu kommen und gehen
				// Kombinationen
				.erfassungenKomprimiert(
						this.komprimiereErfassungen(erfassungenDtos, veranstaltungUnterscheidungEinAusgehend))
				// Ermittle den Status der Anwesenheit (z.B. Abwesend, Anwesend, Mehrfach
				// anwesend, ...)
				.status(this.erfassungService.statusZuAnzahlErfassungen(erfassungen.size(),
						veranstaltungUnterscheidungEinAusgehend))
				// Ermittle die möglichen Aktionen zu der Person (z.B. Erfassen, Wiederkehren,
				// Ausgehen, erneut Erfassen)
				.aktion(this.erfassungService.aktionZuAnzahlErfassungen(erfassungen.size(),
						veranstaltungUnterscheidungEinAusgehend, veranstaltungMehrfachErfassungenErlaubt))
				.build();
	}

	/**
	 * Ermittelt Ausweise, die weder einer Person, noch einem anderen Ausweis
	 * zugewiesen sind in Bezug zu einem Termin
	 *
	 * @param termin Termin aus dem die nicht zugeordneten Ausweisen ermittelt
	 *               werden. Wirft {@link NullPointerException} wenn null ist.
	 * @return {@link List< NichtZugewiesenerAusweisDto >} Liste nicht zugewiesener
	 *         Ausweise
	 */
	private List<NichtZugewiesenerAusweisDto> ermittleNichtZugewieseneAusweise(@NonNull Termin termin) {

		// Bestimme die Erfassungen mit nicht zugeordneten Ausweisen
		List<ErfassungAusweis> erfassungen = termin.getAusweisErfassungen()
				.stream()
				.filter(erfassung -> (erfassung.getAusweis().getPerson() == null
						&& erfassung.getAusweis().getEigenausweis() == null))
				.toList();

		// Bestimme zu jedem Ausweis den spätesten Zeitpunkt
		Map<Ausweis, OffsetDateTime> ausweiseMitSpaetestemZeitpunkt = erfassungen.stream()
				.collect(Collectors.toMap(ErfassungAusweis::getAusweis, ErfassungAusweis::getZeitpunkt,
						DatumHelper::max));

		// Mapping zu Liste von Dtos
		List<NichtZugewiesenerAusweisDto> returnList = new ArrayList<>();
		for (Map.Entry<Ausweis, OffsetDateTime> ausweis : ausweiseMitSpaetestemZeitpunkt.entrySet()) {
			returnList.add(NichtZugewiesenerAusweisDto.builder()
					.ausweis(ausweisMapper.toDto(ausweis.getKey()))
					.letzterZeitpunkt(ausweis.getValue())
					.build());
		}

		return returnList;
	}

	/**
	 * Erstellt eine Liste an komprimierten Erfassungen. Wenn zur gegebenen
	 * Veranstaltung ein- und ausgehende Erfassungen aktiviert sind, wird für eine
	 * Person und Termin eine Liste an Zeiträumen bestehend aus kommen und Gehen
	 * gebildet. Wenn der Modus nicht aktiv ist, werden nur kommen-Zeiten erzeugt.
	 *
	 * @param erfassungen            Alle zu berücksichtigen Erfassungen. Wenn null
	 *                               wird {@link NullPointerException} geworfen.
	 * @param einUndAusgehendErlaubt Definiert, ob ein- und ausgehende Personen
	 *                               erfasst werden.
	 * @return Liste von Erfassungen bestehend aus kommen und Gehen
	 */
	private List<ErfassungKomprimiertDto> komprimiereErfassungen(@NonNull List<ErfassungDto> erfassungen,
			boolean einUndAusgehendErlaubt) {

		// Sortiere zunächst die Erfassungen nach dem Zeitpunkt aufsteigend
		List<ErfassungDto> erfassungenSorted = erfassungen.stream()
				.sorted(Comparator.comparing(ErfassungDto::getZeitpunkt))
				.toList();

		// Unterschiedlicher Herangehensweise, wenn ein- und ausgehende Erfassungen
		// erlaubt sind
		if (einUndAusgehendErlaubt) {

			// Ermittle kommen und Gehen Zeiten, jeder 2. Eintrag ist eine gehen Zeit
			List<ErfassungKomprimiertDto> returnList = new ArrayList<>();
			for (int i = 0; i < erfassungen.size(); i = i + 2) {
				ErfassungDto ausgehendeErfassung = null;
				if (i + 1 < erfassungen.size()) {
					ausgehendeErfassung = erfassungen.get(i + 1);
				}
				returnList.add(ErfassungKomprimiertDto.of(erfassungen.get(i), ausgehendeErfassung));
			}
			return returnList;
		} else {
			// Erstelle eine Liste, in der jede Zeit eine kommen Zeit ist, da nicht
			// unterschieden wird.
			return erfassungenSorted.stream().map(ErfassungKomprimiertDto::of).toList();
		}
	}

	/**
	 * Ermittle die IDs von Terminen, zu denen eine Person als berechtigte Person
	 * zugeordnet ist (Auch über Veranstaltungen).
	 *
	 * @param person Person, zu der die berechtigten Termine ermittelt werden sollen
	 * @return Liste von IDs zu berechtigten Terminen.
	 */
	public List<String> getBerechtigteTerminIdsZuPerson(Optional<Person> person) {
		return person.map(value -> value.getAlleBerechtigenTermine().stream().map(Termin::getId).toList())
				.orElse(Collections.emptyList());

	}

}
