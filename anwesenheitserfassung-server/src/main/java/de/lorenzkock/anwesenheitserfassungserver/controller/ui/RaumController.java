package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.RaumBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.RaumErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.mapper.RaumMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Controller behandelt die Anfragen zur Verwaltung von Räumen
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/raeume")
public class RaumController extends BaseController {

    // Service
    final RaumService raumService;

    // Service
    final RaumRepository raumRepository;

    // Mapper
    final RaumMapper raumMapper;

    // Mapper
    final RaumControllerService controllerService;

    /**
     * Endpunkt zum Anzeigen aller Räume zu einem Filter
     */
    @GetMapping
    public String raeumeGet(@RequestParam(required = false) String bezeichnung,
                            @RequestParam(required = false) String kennung,
                            @RequestParam(required = false, defaultValue = "0") Integer archiv,
                            @RequestParam(required = false) Integer seite, @RequestParam(required = false) Integer eintraegeJeSeite,
                            Model model) {

        // Setzt die Liste von Räumen für die ermittelte Seite in das Model
        this.modelRaumListe(model, bezeichnung, kennung, archiv, seite, eintraegeJeSeite);

        return "verwaltung/raeume";
    }

    /**
     * Endpunkt zum Erstellen eines Raumes
     */
    @GetMapping("/erstellen")
    public String raumErstellenGet(Model model) {
        model.addAttribute("raumErstellen", new RaumErstellenDto());
        return "verwaltung/raeume";
    }

    /**
     * Endpunkt zum Anzeigen eines gewählten Raums
     */
    @GetMapping("/{id}")
    public String raumBearbeitenGet(@PathVariable String id, Model model) {

        try {
            // Ermitteln des Raums
            Raum raum = raumService.getMitID(raumRepository, id)
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es konnte kein Raum mit der ID '%s' gefunden werden.".formatted(id)));

            // Setzen des Models für das Frontend
            model.addAttribute("raumBearbeiten", raumMapper.toBearbeitenDto(raum));
            model.addAttribute("raum", raum);
        } catch (NoSuchElementException e) {
            // Fehler Handling, wenn Raum nicht gefunden wurde
            model.addAttribute("error", e.getMessage());
            this.modelRaumListe(model);
        }
        return "verwaltung/raeume";

    }

    /**
     * Endpunkt zum Archivieren eines Raums
     */
    @GetMapping("/{id}/archivieren")
    public String raumArchivierenGet(@PathVariable String id, Model model) {

        try {
            // Archiviert ein Raum
            raumService.archiviere(raumRepository, id);
            return "redirect:/verwaltung/raeume?deleteSuccess";
        } catch (NoSuchElementException e) {
            // Fehlerhandling, wenn Raum nicht existiert
            model.addAttribute("error", e.getMessage());
        } catch (Exception e) {
            // Fehlerhandling, wenn Raum nicht archiviert werden kann
            model.addAttribute("error", "Fehler beim Archivieren des Raums");
        }

        // Setzen des Models für das Frontend
        this.modelRaumListe(model);
        return "verwaltung/raeume";

    }

    /**
     * Endpunkt zum Erstellen eines Raums
     */
    @PostMapping("/erstellen")
    public String raumErstellenPost(RaumErstellenDto dto, Model model) {

        try {
            // Hinzufügen des Raums
            Raum raum = controllerService.fuegeRaumHinzu(dto);
            return "redirect:/verwaltung/raeume/%s?createSuccess".formatted(raum.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("raumErstellen", dto);
            return "verwaltung/raeume";
        }

    }

    /**
     * Endpunkt zum Verarbeiten des Bearbeitens eines Raums
     */
    @PostMapping("/{id}")
    public String raumBearbeitenPost(RaumBearbeitenDto dto, Model model) {

        try {
            // Bearbeiten des Raums
            Raum raum = controllerService.bearbeiteRaum(dto);
            return "redirect:/verwaltung/raeume/%s?editSuccess".formatted(raum.getId());
        } catch (IllegalArgumentException e) {
            // Fehlerhandling, wenn Eingaben nicht in Ordnung sind
            model.addAttribute("error", e.getMessage());
            model.addAttribute("raumBearbeiten", dto);
            return "verwaltung/raeume";
        }

    }

    /*
     * Methoden zum Aufbauen der Liste von Räumen
     */

    /**
     * Erstellt die Standard-Liste ohne Filter
     * @param model Das zu ergänzende Model
     */
    private void modelRaumListe(Model model) {
        this.modelRaumListe(model, null, null, 0, null, null);
    }

    /**
     * Stellt die anzuzeigende Liste von Personen zusammen
     * @param model Das zu ergänzende Model
     * @param bezeichnung Filter der Bezeichnung eines Raums
     * @param kennung Filter der Kennung des Raums
     * @param archiv Archivierte Personen anzeigen
     * @param seite Seite
     * @param eintraegeJeSeite Einträge je Seite
     */
    private void modelRaumListe(Model model, String bezeichnung, String kennung, Integer archiv, Integer seite,
                                Integer eintraegeJeSeite) {

        Boolean archivBool = switch (archiv) {
            case -1 -> null;
            case 1 -> Boolean.TRUE;
            default -> Boolean.FALSE;
        };

        // Lade alle Räume zu dem Filter
        List<Raum> alleRaeumeMitFilter = raumService.getRaeumeMitFilter(Optional.ofNullable(bezeichnung), Optional.ofNullable(kennung), Optional.ofNullable(archivBool));

        // Bestimme die Raeume aus der Liste aller für die gegebene Seite
        List<Raum> alleRaeumeFuerSeite = this.ermittleNavigation(model, alleRaeumeMitFilter, seite, eintraegeJeSeite);

        // Setzen des Models für das Frontend
        model.addAttribute("raeume", alleRaeumeFuerSeite);

        // Setzen des Models für den Filter
        model.addAttribute("filterArchiv", archiv);
        model.addAttribute("filterKennung", kennung == null ? "" : kennung);
        model.addAttribute("filterBezeichnung", bezeichnung == null ? "" : bezeichnung);
    }

}
