package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.CisImportDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.KonfigurationImportDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ImportErgebnisCollector;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller zum Verarbeiten der Konfiguration in der UI
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/konfiguration")
public class KonfigurationController extends BaseController {

    // ControllerService
    private final KonfigurationControllerService controllerService;

    /**
     * Endpunkt für Startseite der Konfiguration
     */
    @GetMapping
    public String konfigurationGet(Model model) {

        this.model(model);

        return "verwaltung/konfiguration";
    }

    /**
     * Endpunkt für die Verarbeitung des Imports
     */
    @PostMapping("import")
    public String importPost(KonfigurationImportDto dto, Model model) {

        ImportErgebnisCollector ergebnis = this.controllerService.importiere(dto);
        model.addAttribute("importErgebnis", ergebnis);

        this.model(model);

        return "verwaltung/konfiguration";
    }

    /**
     * Endpunkt für die Verarbeitung des Imports
     */
    @PostMapping("import/cis")
    public String importCisPost(CisImportDto dto, Model model) {

        ImportErgebnisCollector ergebnis = this.controllerService.importiereCis(dto);
        model.addAttribute("importErgebnis", ergebnis);

        this.model(model);

        return "verwaltung/konfiguration";
    }

    /**
     * Leitet nach den Neuladen eines Posts auf die Ursprungsseite
     */
    @GetMapping("import/**")
    public String redirectGet() {

        return "redirect:/verwaltung/konfiguration";

    }

    /**
     * Fügt dem Model eine leere UploadConfig hinzu
     * @param model Model, dem die UploadConfig hinzugefügt wurde.
     */
    private void model(Model model) {
        model.addAttribute("uploadConfig", new KonfigurationImportDto());
        model.addAttribute("uploadCisConfig", new CisImportDto());
    }


}
