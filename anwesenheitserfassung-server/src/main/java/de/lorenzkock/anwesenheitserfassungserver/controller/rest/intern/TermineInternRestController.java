package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.TerminMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.api.TerminApiIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.PersonZuTerminViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.TerminDetailViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.TerminListViewModelIntern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Controller stellt für die interne API Endpunkte für das Abrufen von Terminen bereit
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/intern")
public class TermineInternRestController implements TerminApiIntern {

    // Service
    private final TerminService terminService;

    // Mapper
    private final TerminMapper terminMapper;
    private final PersonMapper personMapper;

    // Repository
    private final TerminRepository terminRepository;

    /**
     * Endpunkt zum Laden von Terminen, die nicht vorüber sind.
     *
     * @param raumId ID des Raums (optional)
     * @param limit  Maximale Anzahl an Terminen (optional)
     * @param offset Anzahl an Terminen, die übersprungen werden sollen (optional)
     * @return Liste von Terminen, die noch nicht vorüber sind
     */
    @Override
    public ResponseEntity<List<TerminListViewModelIntern>> getAktiveTermine(String raumId, Integer limit,
                                                                            Integer offset) {

        // Ermitteln der Termine, die noch nicht vorüber sind
        List<Termin> termine = terminService.getAktiveNichtAbgeschlosseneTermineMitFilter(Optional.ofNullable(raumId),
                Optional.ofNullable(limit), Optional.ofNullable(offset));

        // Mapping und Zurückgeben
        return ResponseEntity.ok(termine.stream().map(terminMapper::toListViewModelIntern).toList());
    }

    /**
     * Laden von Terminen, die aktuell stattfinden
     *
     * @param raumId ID des Raums (optional)
     * @return Liste von Terminen, die aktuell stattfinden
     */
    @Override
    public ResponseEntity<List<TerminListViewModelIntern>> getAktuelleTermine(String raumId) {

        // Ermitteln der Termine die aktuell stattfinden
        List<Termin> termine = terminService.getAktuelleTermineMitFilter(Optional.ofNullable(raumId));

        // Mapping und Zurückgeben
        return ResponseEntity.ok(termine.stream().map(terminMapper::toListViewModelIntern).toList());
    }

    /**
     * Endpunkt zum Abrufen von Detailinformationen eines einzelnen Termins
     *
     * @param id ID des Termins (required)
     * @return Termin-Details
     */
    @Override
    public ResponseEntity<TerminDetailViewModelIntern> getTerminById(@NonNull String id) {

        // Ermitteln des Termins
        Termin termin = terminService.getMitID(terminRepository, id)
                .orElseThrow(() -> new NoSuchElementException("Kein Termin mit der ID '%s' gefunden.".formatted(id)));

        // Mapping und Zurückgeben
        return ResponseEntity.ok(terminMapper.toDetailViewModelIntern(termin));
    }

    /**
     * Endpunkt zum Abrufen der Teilnehmer eines Termins
     *
     * @param id ID des Termins (required)
     * @return Liste von teilnehmenden Personen zu einem Termin
     */
    @Override
    public ResponseEntity<List<PersonZuTerminViewModelIntern>> getTeilnehmerByTermin(@NonNull String id) {

        // Ermitteln des Termins
        Termin termin = terminService.getMitID(terminRepository, id)
                .orElseThrow(() -> new NoSuchElementException("Kein Termin mit der ID '%s' gefunden.".formatted(id)));

        // Ermitteln der Personen inkl. Status und möglichen Aktionen
        List<PersonZuTerminViewModelIntern> liste = termin.getAlleTeilnehmendenPersonen()
                .stream().map(p -> this.personMapper.toPersonTerminViewModel(p, termin)).toList();
        return ResponseEntity.ok(liste);
    }

}
