package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitAktion;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatusDetail;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class TeilnehmerAnwesenheitPersonDto {

	PersonDto person;

	@Builder.Default
	List<ErfassungDto> erfassungen = new ArrayList<>();

	@Builder.Default
	List<ErfassungKomprimiertDto> erfassungenKomprimiert = new ArrayList<>();

	AnwesenheitStatusDetail status;

	AnwesenheitAktion aktion;

	public int sortByPersonName(TeilnehmerAnwesenheitPersonDto dto) {
		int nachname = this.person.getNachname().compareToIgnoreCase(dto.getPerson().getNachname());
		if (nachname == 0) {
			return this.person.getVorname().compareToIgnoreCase(dto.getPerson().getVorname());
		}
		return nachname;
	}

	public Optional<OffsetDateTime> getErsteErfassung() {
		return erfassungen.stream().map(ErfassungDto::getZeitpunkt).sorted().findFirst();
	}

	public Boolean istOnlineAnwesenheit() {
		return erfassungen.stream().map(ErfassungDto::getOnlineAnwesend).anyMatch(Boolean::booleanValue);
	}

}
