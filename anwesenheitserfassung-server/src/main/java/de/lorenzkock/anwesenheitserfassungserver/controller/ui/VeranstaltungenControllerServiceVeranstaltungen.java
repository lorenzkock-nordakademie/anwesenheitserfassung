package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungAusweiseBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungPersonenBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.VeranstaltungMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Service stellt Funktionalität für den VeranstaltungsController dar und stellt eine Zwischenebene
 * zu den Services dar. Fokus auf Veranstaltungen.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class VeranstaltungenControllerServiceVeranstaltungen {

    // Repositories
    final private VeranstaltungRepository veranstaltungRepository;
    final private AusweisRepository ausweisRepository;
    final private PersonRepository personRepository;

    // Mapper
    final private VeranstaltungMapper veranstaltungMapper;
    // Services
    final private VeranstaltungService veranstaltungService;


    /**
     * Fügt eine Veranstaltung hinzu
     *
     * @param dto DTO mit den Daten
     * @return Hinzugefügte Veranstaltung
     * @throws IllegalArgumentException Fehler
     */
    public Veranstaltung fuegeVeranstaltungHinzu(@NonNull VeranstaltungErstellenDto dto) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einer Veranstaltung mit der Kennung gesucht, existiert
        // bereits eine,
        // wird ein Fehler geworfen.
        this.veranstaltungService.pruefeKennungEinmalig(veranstaltungRepository, dto.getKennung(), Optional.empty());

        // Anlage der Veranstaltung
        Veranstaltung veranstaltungZuErstellen;
        try {
            veranstaltungZuErstellen = veranstaltungMapper.fromDto(dto);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Ein Datums-Feld wurde nicht im richtigen Format angegeben.");
        }

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(veranstaltungZuErstellen);

        return veranstaltungRepository.save(veranstaltungZuErstellen);
    }


    /**
     * Bearbeite eine Veranstaltung
     *
     * @param dto DTO mit den Daten
     * @return Bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung bearbeiteVeranstaltung(@NonNull VeranstaltungBearbeitenDto dto)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        Veranstaltung veranstaltungZuBearbeiten = veranstaltungRepository.findById(dto.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde keine Veranstaltung mit der ID '%s' gefunden.".formatted(dto.getId())));

        // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
        // existiert.
        this.veranstaltungService.pruefeKennungEinmalig(veranstaltungRepository, dto.getKennung(), Optional.of(dto.getId()));

        // Ergänze Veranstaltung mit den Daten aus DTO
        veranstaltungZuBearbeiten = veranstaltungService.ergaenzeVeranstaltung(veranstaltungZuBearbeiten, dto.getName(), dto.isErlaubeEinUndAusgehendeBuchungen(),
                dto.isErlaubeMehrfachBuchungen(), dto.isErlaubeFremdausweis(), dto.isFremdausweisBestaetigen(), Optional.ofNullable(dto.getKennung()));

        return veranstaltungRepository.save(veranstaltungZuBearbeiten);

    }

    /*
     * Methoden zum Bearbeiten von Personen und Ausweisen
     */

    /**
     * Bearbeite die teilnehmenden Personen
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param dto             DTO mit den Daten
     * @return Bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung bearbeiteTeilnehmendePersonen(@NonNull String veranstaltungId, @NonNull VeranstaltungPersonenBearbeitenDto dto)
            throws NoSuchElementException, IllegalArgumentException {

        final Veranstaltung veranstaltung = this.veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));

        veranstaltung.fuegeTeilnehmendePersonenHinzu(extrahiereHinzuzufuegendePersonen(dto));
        veranstaltung.entferneTeilnehmendePersonen(extrahiereZuEntfernendePersonen(dto));

        return veranstaltungRepository.save(veranstaltung);

    }

    /**
     * Bearbeite die berechtigten Personen
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param dto             DTO mit den Daten
     * @return Bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung bearbeiteBerechtigtePersonen(@NonNull String veranstaltungId, @NonNull VeranstaltungPersonenBearbeitenDto dto)
            throws NoSuchElementException, IllegalArgumentException {

        final Veranstaltung veranstaltung = this.veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));

        veranstaltung.fuegeBerechtigtePersonenHinzu(extrahiereHinzuzufuegendePersonen(dto));
        veranstaltung.entferneBerechtigtePersonen(extrahiereZuEntfernendePersonen(dto));

        return veranstaltungRepository.save(veranstaltung);

    }

    /**
     * Bearbeite die berechtigten Ausweisen
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param dto             DTO mit den Daten
     * @return Bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung bearbeiteBerechtigteAusweise(@NonNull String veranstaltungId,
                                                      @NonNull VeranstaltungAusweiseBearbeitenDto dto) throws NoSuchElementException, IllegalArgumentException {

        final Veranstaltung veranstaltung = this.veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));

        veranstaltung.fuegeBerechtigteAusweiseHinzu(extrahiereHinzuzufuegendeAusweise(dto));
        veranstaltung.entferneBerechtigteAusweise(extrahiereZuEntfernendeAusweise(dto));

        return veranstaltungRepository.save(veranstaltung);

    }

    /**
     * Bearbeite die teilnehmenden Ausweisen
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param dto             DTO mit den Daten
     * @return Bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung bearbeiteTeilnehmendeAusweise(@NonNull String veranstaltungId,
                                                       @NonNull VeranstaltungAusweiseBearbeitenDto dto) throws NoSuchElementException, IllegalArgumentException {

        final Veranstaltung veranstaltung = this.veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));

        veranstaltung.fuegeTeilnehmendeAusweiseHinzu(extrahiereHinzuzufuegendeAusweise(dto));
        veranstaltung.entferneTeilnehmendeAusweise(extrahiereZuEntfernendeAusweise(dto));

        return veranstaltungRepository.save(veranstaltung);

    }

    /*
     * Methoden zum Hinzufügen von berechtigten Personen und Ausweisen zu einer
     * Veranstaltung
     */

    /**
     * Extrahiere aus dem DTO die Personen, die zum Hinzufügen ausgewählt wurden
     *
     * @param dto DTO mit den Daten
     * @return Liste von ermittelten Personen
     */
    private List<Person> extrahiereHinzuzufuegendePersonen(@NonNull VeranstaltungPersonenBearbeitenDto dto) {
        if (dto.getAddPersonen() != null) {
            return dto.getAddPersonen()
                    .stream()
                    .map(personRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList();
        }
        return List.of();
    }

    /**
     * Extrahiere aus dem DTO die Personen, die zum Entfernen ausgewählt wurden
     *
     * @param dto DTO mit den Daten
     * @return Liste von ermittelten Personen
     */
    private List<Person> extrahiereZuEntfernendePersonen(@NonNull VeranstaltungPersonenBearbeitenDto dto) {
        if (dto.getRemovePersonen() != null) {
            return dto.getRemovePersonen()
                    .stream()
                    .map(personRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList();
        }
        return List.of();
    }

    /**
     * Extrahiere aus dem DTO die Ausweise, die zum Entfernen ausgewählt wurden
     *
     * @param dto DTO mit den Daten
     * @return Liste von ermittelten Ausweisen
     */
    private List<Ausweis> extrahiereZuEntfernendeAusweise(@NonNull VeranstaltungAusweiseBearbeitenDto dto) {
        if (dto.getRemoveAusweise() != null) {
            return dto.getRemoveAusweise()
                    .stream()
                    .map(rfidSnr -> ausweisRepository.findFirstByIdRfidSnrAndAktiv(rfidSnr, true))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList();
        }
        return List.of();
    }

    /**
     * Extrahiere aus dem DTO die Ausweise, die zum Hinzufügen ausgewählt wurden
     *
     * @param dto DTO mit den Daten
     * @return Liste von ermittelten Ausweisen
     */
    private List<Ausweis> extrahiereHinzuzufuegendeAusweise(@NonNull VeranstaltungAusweiseBearbeitenDto dto) {
        if (dto.getAddAusweise() != null) {
            return dto.getAddAusweise()
                    .stream()
                    .map(rfidSnr -> ausweisRepository.findFirstByIdRfidSnrAndAktiv(rfidSnr, true))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList();
        }
        return List.of();
    }

    /*
     * Validierungsmethoden für DTOs (UI)
     */

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull VeranstaltungErstellenDto dto) {
        if (StringUtils.isBlank(dto.getName())) {
            throw new IllegalArgumentException("Der Name der Veranstaltung darf nicht leer sein.");
        }

    }

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull VeranstaltungBearbeitenDto dto) {
        if (StringUtils.isBlank(dto.getName())) {
            throw new IllegalArgumentException("Der Name der Veranstaltung darf nicht leer sein.");
        }

    }
}
