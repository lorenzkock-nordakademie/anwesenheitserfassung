package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErfassenErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.Erfassung;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.enums.ErgebnisMehrfachErfassung;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.ErfassungMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitAusweisErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitAusweisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitPersonErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitPersonViewModelIntern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Dient als Vermittler zwischen dem {@link ErfassungInternRestController} und
 * den benötigten Services.
 *
 * @author Lorenz Kock
 */
@Component
@RequiredArgsConstructor
public class ErfassungInternRestControllerService {

	// Mapper
	final private ErfassungMapper erfassungMapper;
	final private PersonMapper personMapper;
	final private AusweisMapper ausweisMapper;

	// Services
	private final ErfassungService erfassungService;
	final private AusweisService ausweisService;

	// Repositories
	final private TerminRepository terminRepository;
	final private AusweisRepository ausweisRepository;
	final private RaumRepository raumRepository;

	/*
	 * Methoden zum Verarbeiten von Erfassungen aus der internen Schnittstelle
	 */

	/**
	 * Verarbeiten der Erfassung mit einem Ausweis / Ausweis
	 *
	 * @param viewModel Daten zu der Erfassung
	 * @return Ergebnis der Verarbeitung
	 */
	public ErfassungErstellenMitAusweisErgebnisViewModelIntern erfassungVerarbeiten(
			@NonNull ErfassungErstellenMitAusweisViewModelIntern viewModel) {

		// Ermittle den Termin der Erfassung und gib den Status zurück, wenn der Termin
		// nicht bekannt ist
		Termin termin = terminRepository.findById(viewModel.getTerminId()).orElse(null);
		if (termin == null) {
			return ergebnis(
					ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_TERMIN_UNBEKANNT,
					Optional.empty());
		}

		Optional<Raum> raumDerErfassung = StringUtils.isBlank(viewModel.getRaumId()) ? Optional.empty()
				: raumRepository.findById(viewModel.getRaumId());

		// Ermittle den Ausweis der Erfassung
		Optional<Ausweis> ausweis = ausweisRepository.findFirstByIdRfidSnrAndAktiv(viewModel.getRfidSnr(), true);
		if (ausweis.isPresent()) {
			// Verarbeite die Erfassung anhand des bekannten Ausweises
			return this.erfassungVerarbeitenBekannterAusweis(termin, ausweis.get(), viewModel.getZeitpunkt(),
					raumDerErfassung);
		} else {
			// Verarbeite die Erfassung mit einem fremden Ausweis
			return this.erfassungVerarbeitenUnbekannterAusweis(termin, viewModel.getRfidSnr(), viewModel.getZeitpunkt(),
					viewModel.getFremdAusweis(), raumDerErfassung);
		}

	}

	/**
	 * Verarbeiten der Erfassung mit einer Person durch manuelle Auswahl
	 *
	 * @param viewModel Daten der Erfassung
	 * @return Ergebnis der Verarbeitung
	 */
	public ErfassungErstellenMitPersonErgebnisViewModelIntern erfassungVerarbeiten(
			@NonNull ErfassungErstellenMitPersonViewModelIntern viewModel) {

		// Verarbeite die Erfassung und erhalte das Ergebnis
		PersonErfassenErgebnisDto erfassungErgebnis = this.erfassungService.erfassungZuPersonVerarbeiten(
				viewModel.getPersonId(), viewModel.getTerminId(), viewModel.getZeitpunkt(),
				Optional.ofNullable(viewModel.getRaumId()), false);

		// Mappe das Ergebnis zu einem Status und gib es zurück
		return ergebnis(erfassungMapper.toPersonErgebnisEnum(erfassungErgebnis.getErgebnis()),
				Optional.ofNullable(erfassungErgebnis.getPerson()));

	}

	/*
	 * Verarbeite die Anfragen aus der internen Schnittstelle
	 */

	/**
	 * Verarbeite eine Erfassung zu einem bekannten Ausweis
	 *
	 * @param termin    Termin, zu dem die Erfassung erstellt werden soll
	 * @param ausweis   Ausweis, mit dem die Erfassung erstellt werden soll
	 * @param zeitpunkt Zeitpunkt der Erfassung
	 * @return Ergebnis der Erfassung
	 */
	private ErfassungErstellenMitAusweisErgebnisViewModelIntern erfassungVerarbeitenBekannterAusweis(
			@NonNull Termin termin, @NonNull Ausweis ausweis, @NonNull OffsetDateTime zeitpunkt,
			@NonNull Optional<Raum> raum) {

		ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum ergebnis;
		// Wenn es sich um eine berechtigte Person handelt, soll der Zeitpunkt
		// festgehalten werden,
		// denn nach ihm richten sich die Verspätungen der Teilnehmer
		if (termin.getAlleBerechtigtenAusweise().contains(ausweis)) {

			// Aber nur, wenn noch kein Zeitpunkt gesetzt wurde
			if (termin.getErfassungStart() == null) {
				termin.setErfassungStart(OffsetDateTime.now());
				this.terminRepository.save(termin);
				ergebnis = ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFASSUNG_GESTARTET;
			} else {
				ergebnis = ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFASSUNG_BEREITS_GESTARTET;
			}
		} else {

			// Prüfe, ob dies ein bekannter Fremdausweis ist und ob Fremdausweise erlaubt
			// sein sollen
			if (ausweis.isFremdausweis() && !termin.getVeranstaltung().isErlaubeFremdausweis()) {
				ergebnis = ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_KEINE_FREMDAUSWEISE;
			} else {
				// Bestimme zunächst die teilnehmenden Ausweise, um den erfassten abgleichen zu
				// können
				List<Ausweis> teilnehmendeAusweise = termin.getAlleTeilnehmendenAusweise(true);

				// Wenn der Ausweis enthalten ist, verarbeite diese Erfassung
				if ((ausweis.isFremdausweis() && ausweis.getEigenausweis() == null && ausweis.getPerson() == null)
						|| teilnehmendeAusweise.contains(ausweis)) {

					List<Erfassung> erfassungenZuAusweis = this.erfassungService
							.getErfassungenZuAusweisUndTermin(ausweis, Optional.of(termin.getId()));

					// Prüfe Zeit seit der letzten Erfassung
					Optional<Erfassung> letzteErfassung = erfassungenZuAusweis.stream()
							.max(Comparator.comparing(Erfassung::getZeitpunkt));
					if (letzteErfassung.isPresent() && letzteErfassung.get()
							.getZeitpunkt()
							.until(OffsetDateTime.now(), ChronoUnit.SECONDS) < 60) {
						ergebnis = ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_ZEIT;
					} else {
						ErgebnisMehrfachErfassung ergebnisValidierungMehrfach = this.erfassungService
								.validiereAufMehrfachBuchungen(erfassungenZuAusweis.size(),
										termin.getVeranstaltung().isErlaubeEinUndAusgehendeBuchungen(),
										termin.getVeranstaltung().isErlaubeMehrfachBuchungen());
						if (ergebnisValidierungMehrfach.isErfolgreich()) {
							this.erfassungService.speichereErfassung(termin, ausweis, zeitpunkt, raum);
						}
						ergebnis = erfassungMapper.toAusweisErgebnisEnum(ergebnisValidierungMehrfach);
					}
				} else { // Wenn der Ausweis nicht enthalten ist, gib einen Fehler zurück
					ergebnis = ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_KEIN_TEILNEHMER;
				}
			}
		}

		return ergebnis(ergebnis, Optional.of(ausweis));

	}

	/**
	 * Verarbeite eine Erfassung zu einem unbekannten Ausweis
	 *
	 * @param termin       Termin, zu dem die Erfassung erstellt werden soll
	 * @param rfidSnr      Seriennummer zu dem unbekannten Ausweis
	 * @param zeitpunkt    Zeitpunkt der Erfassung
	 * @param fremdausweis Information, ob es sich um einen Fremdausweis handeln
	 *                     soll
	 * @return Ergebnis der Erfassung
	 */
	private ErfassungErstellenMitAusweisErgebnisViewModelIntern erfassungVerarbeitenUnbekannterAusweis(
			@NonNull Termin termin, @NonNull String rfidSnr, @NonNull OffsetDateTime zeitpunkt, boolean fremdausweis,
			@NonNull Optional<Raum> raum) {

		// Prüfen, ob ein Fremausweis erwartet wird
		if (fremdausweis) {
			// Bei der Verarbeitung von unbekannten Ausweisen muss zunächst geprüft werden,
			// ob eine Verarbeitung von Fremden Ausweisen für die Veranstaltung zulässig
			// ist
			if (termin.getVeranstaltung().isErlaubeFremdausweis()) {

				// Erstelle den Fremdausweis
				Ausweis erstellterFremdausweis = ausweisService.fuegeFremdausweisHinzu(rfidSnr);

				// Speichere die Erfassung
				this.erfassungService.speichereErfassung(termin, erstellterFremdausweis, zeitpunkt, raum);

				// Gib das Ergebnis als Erfolgreich zurück
				return ergebnis(ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH,
						Optional.of(erstellterFremdausweis));
			} else {
				// Gib den Fehler zurück, dass keine Fremdausweise zugelassen sind
				return ergebnis(
						ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_KEINE_FREMDAUSWEISE,
						Optional.empty());
			}
		} else {
			// Wenn ein bekannter Ausweis erwartet wird, es jedoch keine gefunden wird, wird
			// ein Fehler zurückgegeben
			return ergebnis(
					ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_UNBEKANNTER_AUSWEIS,
					Optional.empty());
		}

	}

	/**
	 * Erstelle das ErgebnisViewModel aus den gegebenen Informationen zur
	 * Verarbeitung von Erfassungen zu Ausweisen
	 *
	 * @param ergebnis Ergebnis der Verarbeitung
	 * @param ausweis  Ggf. ermittelter Ausweis
	 * @return Zusammengestelltes Ergebnis
	 */
	private ErfassungErstellenMitAusweisErgebnisViewModelIntern ergebnis(
			@NonNull ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum ergebnis,
			@NonNull Optional<Ausweis> ausweis) {

		ErfassungErstellenMitAusweisErgebnisViewModelIntern ergebnisVM = new ErfassungErstellenMitAusweisErgebnisViewModelIntern();
		ergebnisVM.setErgebnis(ergebnis);

		ausweis.ifPresent(i -> {
			ergebnisVM.setAusweisTyp(ausweisMapper.bestimmeAusweisTyp(i));
			ergebnisVM.setAusweis(ausweisMapper.toViewModelIntern(i));
		});

		return ergebnisVM;
	}

	/**
	 * Erstelle das ErgebnisViewModel aus den gegebenen Informationen zur
	 * Verarbeitung von Erfassungen zu Personen
	 *
	 * @param ergebnis Ergebnis der Verarbeitung
	 * @param person   Ggf. ermittelte Person
	 * @return Zusammengestelltes Ergebnis
	 */
	private ErfassungErstellenMitPersonErgebnisViewModelIntern ergebnis(
			@NonNull ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum ergebnis,
			@NonNull Optional<Person> person) {

		ErfassungErstellenMitPersonErgebnisViewModelIntern ergebnisVM = new ErfassungErstellenMitPersonErgebnisViewModelIntern();
		ergebnisVM.setErgebnis(ergebnis);
		person.ifPresent(p -> ergebnisVM.setPerson(personMapper.toViewModelIntern(p)));

		return ergebnisVM;
	}

}
