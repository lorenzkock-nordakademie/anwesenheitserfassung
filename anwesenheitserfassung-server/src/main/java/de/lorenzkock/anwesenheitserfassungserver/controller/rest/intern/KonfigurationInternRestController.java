package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.lorenzkock.anwesenheitserfassungserver.rest.intern.api.KonfigurationApiIntern;
import lombok.RequiredArgsConstructor;

/**
 * Dieser Controller stellt für die interne API einen Endpunkt bereit, an dem die API-Konfifuration getestet werden kann
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/intern")
public class KonfigurationInternRestController implements KonfigurationApiIntern {

	/**
	 * Endpunkt zum Testen der API-Konfiguration
	 * @return Leeres Ergebnis
	 */
	@Override
	public ResponseEntity<Void> testKonfiguration() {
		return ResponseEntity.ok().build();
	}

}
