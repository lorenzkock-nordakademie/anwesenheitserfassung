package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AnwesenheitenExportDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TeilnehmerAnwesenheitPersonDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TerminAnwesenheitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungAnwesenheitenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.BenutzerService;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatus;
import de.lorenzkock.anwesenheitserfassungserver.mapper.BenutzerMapper;
import de.lorenzkock.anwesenheitserfassungserver.service.AnwesenheitService;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Controller Service zum Verarbeiten der Anfragen zum Export
 */
@Component
@RequiredArgsConstructor
public class ExportControllerService {

	// Service
	final BenutzerService benutzerService;
	final AnwesenheitService anwesenheitService;

	// Mapper
	final BenutzerMapper benutzerMapper;

	// Weitere Hilfsklassen
	final private AnwesenheitenControllerService anwesenheitenControllerService;
	final private Environment environment;

	/**
	 * Exportiert die Anwesenheiten zu einer ausgewählten Veranstaltung
	 *
	 * @param veranstaltungId ID der Veranstaltung, die zu exportieren ist
	 * @return Objekt mit den Daten zu der Veranstaltung für den Dateinamen sowie
	 *         den Daten für die CSV Datei
	 */
	public AnwesenheitenExportDto exportiereAnwesenheitenFuerVeranstaltung(@NonNull String veranstaltungId) {

		VeranstaltungAnwesenheitenDto anwesenheiten = anwesenheitenControllerService
				.anwesenheitenZuVeranstaltung(veranstaltungId);

		// Sortierte Termine mit deren Anwesenheiten
		List<TerminAnwesenheitenDto> termine = anwesenheiten.getTermine()
				.stream()
				.sorted(Comparator.comparing(t -> t.getTermin().getStart()))
				.toList();

		byte[] data = this.erstelleCsvInhalt(termine);

		// Erstelle das DTO mit allen relevanten Informationen
		return AnwesenheitenExportDto.builder()
				.exportDaten(data)
				.verantaltungName(anwesenheiten.getVeranstaltung().getName())
				.veranstaltungKennung(anwesenheiten.getVeranstaltung().getKennung())
				.build();

	}

	/**
	 * Exportiert die Anwesenheiten zu einem ausgewählten Termin
	 *
	 * @param terminId ID des Termins, der zu exportieren ist
	 * @return Objekt mit den Daten zu der Veranstaltung für den Dateinamen sowie
	 *         den Daten für die CSV Datei
	 */
	public AnwesenheitenExportDto exportiereAnwesenheitenFuerTermin(@NonNull String terminId) {

		TerminAnwesenheitenDto anwesenheiten = anwesenheitenControllerService
				.anwesenheitenUndNichtZugewieseneAusweiseZuTermin(terminId);

		byte[] data = this.erstelleCsvInhalt(List.of(anwesenheiten));

		// Erstelle das DTO mit allen relevanten Informationen
		return AnwesenheitenExportDto.builder()
				.exportDaten(data)
				.verantaltungName(anwesenheiten.getTermin().getVeranstaltung().getName())
				.veranstaltungKennung(anwesenheiten.getTermin().getVeranstaltung().getKennung())
				.build();

	}

	/**
	 * Formatiert eine Liste von Terminen mit den jeweiligen Anwesenheiten zu einer
	 * CSV Datei
	 *
	 * @param termine Termine, die in der Datei enthalten sein sollen, inkl. der
	 *                Teilnehmer
	 * @return ByteArray mit den Daten der CSV Datei
	 */
	private byte[] erstelleCsvInhalt(@NonNull List<TerminAnwesenheitenDto> termine) {

		// Zeilen der CSV Datei
		List<List<String>> lines = new ArrayList<>();

		// Bestimme alle Teilnehmer
		final Set<PersonDto> personenZuAllenTerminen = new HashSet<>();
		termine.stream()
				.map(TerminAnwesenheitenDto::getTeilnehmendePersonen)
				.forEach(list -> list.stream()
						.map(TeilnehmerAnwesenheitPersonDto::getPerson)
						.forEach(personenZuAllenTerminen::add));

		List<PersonDto> personenZuAllenTerminenSorted = personenZuAllenTerminen.stream()
				.sorted(Comparator.comparing(PersonDto::getNachname).thenComparing(PersonDto::getVorname))
				.toList();

		Map<String, Map<String, TeilnehmerAnwesenheitPersonDto>> anwesenheitenZuPersonenUndTerminen = new HashMap<>();

		// Definiere die Titelzeile
		List<String> headline = new ArrayList<>(List.of("Name", "Vorname", "Matrikelnummer"));
		for (TerminAnwesenheitenDto termin : termine) {
			headline.add(DatumHelper.toExportDate(termin.getTermin().getStart()));

			Map<String, TeilnehmerAnwesenheitPersonDto> anwesenheitenZuPersonen = new HashMap<>();
			for (TeilnehmerAnwesenheitPersonDto anwesenheitZuTeilnehmer : termin.getTeilnehmendePersonen()) {
				anwesenheitenZuPersonen.put(anwesenheitZuTeilnehmer.getPerson().getId(), anwesenheitZuTeilnehmer);
			}
			anwesenheitenZuPersonenUndTerminen.put(termin.getTermin().getId(), anwesenheitenZuPersonen);
		}
		lines.add(headline);

		// Bestimme die Zeilen für alle Teilnehmenden
		for (PersonDto person : personenZuAllenTerminenSorted) {
			List<String> line = new ArrayList<>();
			line.add(person.getNachname());
			line.add(person.getVorname());
			line.add(person.getKennung());

			for (TerminAnwesenheitenDto termin : termine) {
				Optional<TeilnehmerAnwesenheitPersonDto> anwesenheitZuPersonUndTermin = Optional.ofNullable(
						anwesenheitenZuPersonenUndTerminen.get(termin.getTermin().getId()).get(person.getId()));

				// Ermittle den Status der Person zu dem Termin anhand der ersten Erfassung und
				// der Referenzzeit, die durch
				// den tatsächlichen Start des Termins definiert wird. Ist dieser leer, ist die
				// Startzeit des Termins die Referenz
				String statusZuTerminUndPerson = anwesenheitZuPersonUndTermin.map(a -> {
					AnwesenheitStatus status = anwesenheitService.statusZuAnwesenheit(a.getErsteErfassung(),
							termin.getTermin().getStart(), Optional.empty(), a.istOnlineAnwesenheit());

					// Lese den String zum Status aus den Properties, verwende ansonsten den
					// Default-String

					return environment.getProperty(
							String.format("export.status.beschreibung.%s", status.name().toLowerCase()),
							status.getDefaultBeschreibung());
				}).orElse("-");
				line.add(statusZuTerminUndPerson);
			}

			lines.add(line);
		}

		// Schreibe die Daten als CSV
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputStreamWriter streamWriter = new OutputStreamWriter(stream);
		try (ICSVWriter writer = new CSVWriterBuilder(streamWriter).withSeparator(';').build()) {
			for (List<String> line : lines) {
				writer.writeNext(line.toArray(new String[0]));
			}
			streamWriter.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return stream.toByteArray();
	}

}
