package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VeranstaltungBearbeitenDto {

	String id;

	String name;
	String kennung;

	boolean fremdausweisBestaetigen;
	boolean erlaubeFremdausweis;
	boolean erlaubeEinUndAusgehendeBuchungen;
	boolean erlaubeMehrfachBuchungen;

}
