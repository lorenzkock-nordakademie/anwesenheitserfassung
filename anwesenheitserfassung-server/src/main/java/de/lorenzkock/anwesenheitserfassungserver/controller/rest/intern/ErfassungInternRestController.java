package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import de.lorenzkock.anwesenheitserfassungserver.rest.intern.api.ErfassungApiIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitAusweisErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitAusweisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitPersonErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitPersonViewModelIntern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Dieser Controller stellt für die interne API Endpunkte zum Erstellen von Anwesenheitserfassungen bereit
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/intern")
public class ErfassungInternRestController implements ErfassungApiIntern {

    // Controller Service
    private final ErfassungInternRestControllerService erfassungControllerService;

    /**
     * Erfassen einer Anwesenheit mittels Ausweis
     *
     * @param erfassungErstellenMitAusweisViewModelIntern (required) Informationen zur Erfassung
     * @return Ergebnis der Verarbeitung
     */
    @Override
    public ResponseEntity<ErfassungErstellenMitAusweisErgebnisViewModelIntern> erstelleErfassungMittelsAusweis(
            @NonNull ErfassungErstellenMitAusweisViewModelIntern erfassungErstellenMitAusweisViewModelIntern) {
        // Verarbeiten der Erfassung
        return ResponseEntity
                .ok(erfassungControllerService.erfassungVerarbeiten(erfassungErstellenMitAusweisViewModelIntern));
    }

    /**
     * Erfassen einer Anwesenheit mittels Übergabe der Person
     *
     * @param erfassungErstellenMitPersonViewModelIntern (required) Informationen zur Erfassung
     * @return Ergebnis der Verarbeitung
     */
    @Override
    public ResponseEntity<ErfassungErstellenMitPersonErgebnisViewModelIntern> erstelleErfassungMittelsPerson(
            @NonNull ErfassungErstellenMitPersonViewModelIntern erfassungErstellenMitPersonViewModelIntern) {
        // Verarbeiten der Erfassung
        return ResponseEntity
                .ok(erfassungControllerService.erfassungVerarbeiten(erfassungErstellenMitPersonViewModelIntern));
    }

}
