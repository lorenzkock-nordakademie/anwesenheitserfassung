package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonErstellenViewModelExtern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für die Verarbeitung von ViewModels aus der externen API
 */
@Component
@RequiredArgsConstructor
public class PersonRestControllerService {

    // Mapper
    private final PersonMapper personMapper;

    // Service
    private final PersonenService personenService;

    // Repository
    private final PersonRepository personRepository;

    /**
     * Verarbeite das Erstellen eine Person aus der externen Schnittstelle
     *
     * @param viewModel ViewModel mit den Daten
     * @return Erstellte Person
     * @throws NoSuchElementException   Fehler, wenn Person nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Person fuegePersonHinzu(@NonNull PersonErstellenViewModelExtern viewModel) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einer Person mit der Kennung gesucht, existiert bereits
        // eine, wird ein Fehler geworfen.
        this.personenService.pruefeKennungEinmalig(personRepository, viewModel.getKennung(), Optional.empty());

        // Anlage der Person
        Person personZuErstellen = personMapper.fromViewModelExtern(viewModel);

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(personZuErstellen);
        return personRepository.save(personZuErstellen);

    }

    /**
     * Verarbeite das Bearbeiten eine Person aus der externen Schnittstelle
     *
     * @param viewModel ViewModel mit den Daten
     * @return Bearbeitete Person
     * @throws NoSuchElementException   Fehler, wenn Person nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Person bearbeitePerson(@NonNull PersonBearbeitenViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine ID gesetzt ist, bearbeite die Person anhand der ID
        if (!StringUtils.isBlank(viewModel.getId())) {

            Person personZuBearbeiten = personRepository.findById(viewModel.getId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde keine Person mit der ID '%s' gefunden.".formatted(viewModel.getId())));

            // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
            // existiert.
            this.personenService.pruefeKennungEinmalig(personRepository, viewModel.getKennung(), Optional.of(viewModel.getId()));

            // Bearbeite die Daten
            personZuBearbeiten = this.personenService.ergaenzePerson(personZuBearbeiten, viewModel.getVorname(), viewModel.getNachname(), Optional.of(viewModel.getKennung()));

            return personRepository.save(personZuBearbeiten);

        }

        // Wenn eine Kennung gesetzt ist, bearbeite die Person anhand der Kennung
        if (!StringUtils.isBlank(viewModel.getKennung())) {

            // Suche die Person mit der Kennung
            Person personZuBearbeiten = personRepository.findByKennung(viewModel.getKennung())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde keine Person mit der Kennung '%s' gefunden.".formatted(viewModel.getKennung())));

            // Bearbeite die Daten
            personZuBearbeiten = this.personenService.ergaenzePerson(personZuBearbeiten, viewModel.getVorname(), viewModel.getNachname(), Optional.of(viewModel.getKennung()));

            return personRepository.save(personZuBearbeiten);

        }

        throw new IllegalArgumentException("Es wurde weder eine ID noch eine Kennung angegeben.");

    }

    /*
     * Validierungsmethoden für ViewModelExtern (Externe API)
     */

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull PersonErstellenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getVorname()))
            throw new IllegalArgumentException("Der Vorname der Person darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getNachname()))
            throw new IllegalArgumentException("Der Nachname der Person darf nicht leer sein.");
    }

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull PersonBearbeitenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getVorname()))
            throw new IllegalArgumentException("Der Vorname der Person darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getNachname()))
            throw new IllegalArgumentException("Der Nachname der Person darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getId()) && StringUtils.isBlank(viewModel.getKennung()))
            throw new IllegalArgumentException("Entweder die ID oder die Kennung muss angegeben werden.");
    }

}
