package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.BenutzerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.BenutzerBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.BenutzerErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.BenutzerService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.BenutzerMapper;
import lombok.RequiredArgsConstructor;

/**
 * Controller zum Verarbeiten von Anfragen zur Verwaltung von Benutzern über das UI
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/benutzer")
public class BenutzerController extends BaseController {

	// Services
	final private BenutzerService benutzerService;
	final private PersonenService personenService;

	// Mapper
	final private BenutzerMapper benutzerMapper;

	// Repository
	final private BenutzerRepository benutzerRepository;

	// ControllerService
	final private BenutzerControllerService controllerService;

	/**
	 * Endpunkt zur Auflistung aller Benutzer
	 */
	@GetMapping
	public String benutzerGet(Model model) {

		this.modelBenutzerListe(model);

		return "verwaltung/benutzer";
	}

	/**
	 * Endpunkt für das Formular zum Erstellen eines Benutzers
	 */
	@GetMapping("/erstellen")
	public String personErstellenGet(Model model) {

		// Setzen des Models für das Frontend
		model.addAttribute("benutzerErstellen", new BenutzerErstellenDto());

		// Mögliche Personen für das Auswahl-Feld, die noch nicht einem Benutzer zugewiesen wurden
		model.addAttribute("personenList", personenService.getAktivePersonenOhneBenutzer());

		return "verwaltung/benutzer";
	}

	/**
	 * Endpunkt zum Anzeigen und Bearbeiten eines gewählten Benutzers
	 */
	@GetMapping("/{id}")
	public String personBearbeitenGet(@PathVariable String id, Model model) {

		try {
			// Ermitteln des Benutzers
			Benutzer benutzer = benutzerService.getMitID(benutzerRepository, id)
					.orElseThrow(() -> new NoSuchElementException(
							"Es konnte kein Benutzer mit der ID '%s' gefunden werden.".formatted(id)));

			// Mögliche Personen für das Auswahl-Feld, die noch nicht einem Benutzer zugewiesen wurden
			List<Person> personenFuerListe = personenService.getAktivePersonenOhneBenutzer();

			// Ergänzen der bereits gewählten Person, wenn diese sonst nicht in der Liste wäre
			if (benutzer.getPerson() != null && !personenFuerListe.contains(benutzer.getPerson())) {
				personenFuerListe.add(benutzer.getPerson());
			}

			// Setzen des Models für das Frontend
			model.addAttribute("benutzerBearbeiten", benutzerMapper.toBearbeitenDto(benutzer));
			model.addAttribute("benutzer", benutzer);
			model.addAttribute("personenList", personenFuerListe);

		} catch (NoSuchElementException e) {
			// Verarbeiten des Fehlers, wenn der Benutzer nicht gefunden wurde
			model.addAttribute("error", e.getMessage());
			this.modelBenutzerListe(model);
		}
		return "verwaltung/benutzer";

	}

	/**
	 * Endpunkt zum Löschen eines Benutzers
	 */
	@GetMapping("/{id}/loeschen")
	public String benutzerLoeschenGet(@PathVariable String id, Model model) {

		try {
			// Durchführen der Löschung
			benutzerService.loescheBenutzer(id);
			return "redirect:/verwaltung/benutzer?deleteSuccess";
		} catch (NoSuchElementException e) {
			// Verarbeiten des Fehlers, wenn der Benutzer nicht gefunden wurde
			model.addAttribute("error", e.getMessage());
		} catch (Exception e) {
			// Verarbeiten des Fehlers, wenn einer Aufgetreten ist
			model.addAttribute("error", "Fehler beim Löschen des Benutzers");
		}

		// Setzen des Models für das Frontend
		this.modelBenutzerListe(model);
		return "verwaltung/benutzer";

	}

	/**
	 * Endpunkt zum Verarbeiten des Erstellens eines Benutzers
	 */
	@PostMapping("/erstellen")
	public String benutzerErstellenPost(BenutzerErstellenDto dto, Model model) {
		model.addAttribute("error", null);
		try {
			// Verarbeiten des Hinzufügens / Erstellens
			Benutzer benutzer = controllerService.fuegeBenutzerHinzu(dto);
			return "redirect:/verwaltung/benutzer/%s?createSuccess".formatted(benutzer.getId());
		} catch (IllegalArgumentException e) {
			// Fehler Handling, wenn beim Erstellen ein Fehler aufgetreten ist
			model.addAttribute("error", e.getMessage());
			model.addAttribute("benutzerErstellen", dto);
			model.addAttribute("personenList", personenService.getAktivePersonenOhneBenutzer());
			return "verwaltung/benutzer";
		}

	}

	/**
	 * Endpunkt zum Verarbeiten des Bearbeitens eines Benutzers
	 */
	@PostMapping("/{id}")
	public String benutzerBearbeitenPost(@PathVariable String id, BenutzerBearbeitenDto dto, Model model) {

		try {
			// Verarbeiten des Bearbeitens des Benutzers
			Benutzer benutzer = controllerService.bearbeiteBenutzer(dto);
			return "redirect:/verwaltung/benutzer/%s?editSuccess".formatted(benutzer.getId());
		} catch (IllegalArgumentException e) {
			// Fehler Handling, wenn beim Erstellen ein Fehler aufgetreten ist
			Optional<Benutzer> benutzer = benutzerService.getMitID(benutzerRepository, id);
			if (benutzer.isEmpty()) {
				return "redirect:/verwaltung/benutzer";
			} else {
				model.addAttribute("error", e.getMessage());
				model.addAttribute("benutzerBearbeiten", dto);
				model.addAttribute("benutzer", benutzer.get());
				return "verwaltung/benutzer";
			}
		}

	}

	/**
	 * Stellt die Liste an Benutzern zusammen, die in der Tabelle angezeigt werden sollen
	 * @param model Model das um die Benutzer ergänzt werden soll
	 */
	private void modelBenutzerListe(Model model) {

		// Ermittle die anzuzeigenden Benutzer
		List<Benutzer> benutzer = benutzerService.getBenutzerMitFilter(Optional.empty());

		// Setzen des Models für das Frontend
		model.addAttribute("benutzerList", benutzer);
	}

}
