package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TerminBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TerminErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.TerminMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import java.time.format.DateTimeParseException;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Service stellt Funktionalität für den VeranstaltungsController dar und stellt eine Zwischenebene
 * zu den Services dar. Fokus auf Termine.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class VeranstaltungenControllerServiceTermine {

    // Repositories
    final private TerminRepository terminRepository;

    // Mapper
    final private TerminMapper terminMapper;

    // Services
    final private TerminService terminService;

    /**
     * Fügt einen Termin zu einer Veranstaltung hinzu
     *
     * @param veranstaltungId ID der Veranstaltung, zu der der Termin hinzugefügt werden soll
     * @param dto             Daten des Termins
     * @throws IllegalArgumentException Fehler
     */
    public void fuegeTerminHinzu(@NonNull String veranstaltungId, @NonNull TerminErstellenDto dto) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einer Veranstaltung mit der Kennung gesucht, existiert
        // bereits eine,
        // wird ein Fehler geworfen.
        this.terminService.pruefeKennungEinmalig(terminRepository, dto.getKennung(), Optional.empty());

        // Anlage des Termins
        Termin terminZuErstellen;
        try {
            terminZuErstellen = terminMapper.fromDto(dto);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Ein Datums-Feld wurde nicht im richtigen Format angegeben.");
        }

        // Prüfe, ob der Start vor dem Ende liegt
        this.terminService.pruefeZeitraum(terminZuErstellen.getStart(), terminZuErstellen.getEnde());

        // Zuweisung des Raums, keine Raumkennung gegeben, da durch UI immer ID
        // gegeben ist, wenn ein Raum ausgewählt ist
        this.terminService.raeumeZuweisen(terminZuErstellen, dto.getRaumIds(), null);

        // Zuweisung der Veranstaltung
        this.terminService.veranstaltungZuweisen(terminZuErstellen, veranstaltungId, null);

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(terminZuErstellen);

        terminRepository.save(terminZuErstellen);
    }

    /**
     * Bearbeite einen Termin
     *
     * @param dto Daten des Termins
     * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wurde
     * @throws IllegalArgumentException Fehler
     */
    public void bearbeiteTermin(@NonNull TerminBearbeitenDto dto) throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        Termin terminZuBearbeiten = terminRepository.findById(dto.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Termin mit der ID '%s' gefunden.".formatted(dto.getId())));

        // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
        // existiert.
        this.terminService.pruefeKennungEinmalig(terminRepository, dto.getKennung(), Optional.of(terminZuBearbeiten.getId()));

        // Ergänze Termin mit den Daten aus DTO
        try {
            this.terminService.ergaenzeTermin(terminZuBearbeiten, Optional.ofNullable(terminMapper.toDateTime(dto.getStart())),
                    Optional.ofNullable(terminMapper.toDateTime(dto.getEnde())), Optional.ofNullable(dto.getKennung()),
                    dto.getRaumIds(), null);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Das angegebene Datum hat ein falsches Format.");
        }

        // Prüfe, ob der Start vor dem Ende liegt
        this.terminService.pruefeZeitraum(terminZuBearbeiten.getStart(), terminZuBearbeiten.getEnde());

        // Speichere den Termin
        terminRepository.save(terminZuBearbeiten);

    }


    /*
     * Validierungsmethoden für DTOs (UI)
     */

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull TerminErstellenDto dto) {
        validiereStartUndEnde(dto.getStart(), dto.getEnde());
    }

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull TerminBearbeitenDto dto) {
        validiereStartUndEnde(dto.getStart(), dto.getEnde());
    }

    /**
     * Validiert den Start und das Ende
     *
     * @param start Start des Termins
     * @param ende  Ende des Termins
     */
    private void validiereStartUndEnde(@NonNull String start, @NonNull String ende) {
        if (StringUtils.isBlank(start)) {
            throw new IllegalArgumentException("Der Start-Zeitpunkt darf nicht leer sein.");
        }
        if (StringUtils.isBlank(ende)) {
            throw new IllegalArgumentException("Der Ende-Zeitpunkt darf nicht leer sein.");
        }
        if (!start.matches("\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}")) {
            throw new IllegalArgumentException("Der Start-Zeitpunkt ist nicht valide");
        }
        if (!ende.matches("\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}")) {
            throw new IllegalArgumentException("Der Ende-Zeitpunkt ist nicht valide");
        }
    }
}
