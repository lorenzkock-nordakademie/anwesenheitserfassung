package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungService;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.ErfassungMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.VeranstaltungMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.VeranstaltungApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Verarbeitet Anfragen der externen API zu Veranstaltungen
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class VeranstaltungRestController extends BaseRestController implements VeranstaltungApiExtern {

    // Services
    private final VeranstaltungService veranstaltungService;
    private final TerminService terminService;

    // Mapper
    private final VeranstaltungMapper veranstaltungMapper;

    // Repositories
    private final VeranstaltungRepository veranstaltungRepository;

    // Controller Services
    private final VeranstaltungRestControllerServiceVeranstaltung controllerServiceVeranstaltung;
    private final VeranstaltungRestControllerServiceTermin controllerServiceTermin;

    @Override
    public ResponseEntity<List<VeranstaltungViewModelExtern>> getVeranstaltungen(String id, String kennung,
                                                                                 String name) {
        final List<Veranstaltung> veranstaltungen = veranstaltungService.getVeranstaltungenMitFilter(Optional.ofNullable(id),
                Optional.ofNullable(kennung), Optional.ofNullable(name), Optional.empty());
        return ResponseEntity.ok(veranstaltungen.stream().map(veranstaltungMapper::toViewModelExtern).toList());
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> addVeranstaltung(
            VeranstaltungErstellenViewModelExtern veranstaltungErstellenViewModelExtern) {
        Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .fuegeVeranstaltungHinzu(veranstaltungErstellenViewModelExtern);
        return ResponseEntity.ok(veranstaltungMapper.toViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> editVeranstaltung(
            VeranstaltungBearbeitenViewModelExtern veranstaltungBearbeitenViewModelExtern) {
        Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .bearbeiteVeranstaltung(veranstaltungBearbeitenViewModelExtern);
        return ResponseEntity.ok(veranstaltungMapper.toViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> getVeranstaltungById(String id) {
        return ResponseEntity.ok(veranstaltungService.getMitID(veranstaltungRepository, id)
                .map(veranstaltungMapper::toViewModelExtern)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(id))));
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> getVeranstaltungByKennung(String kennung) {
        return ResponseEntity.ok(veranstaltungService.getMitKennung(veranstaltungRepository, kennung)
                .map(veranstaltungMapper::toViewModelExtern)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden.".formatted(kennung))));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> getTeilnehmerZuVeranstaltungenById(String id) {
        return veranstaltungService.getMitID(veranstaltungRepository, id)
                .map(veranstaltungMapper::teilnehmendePersonenUndAusweiseToViewModelExtern)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(id)));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> getTeilnehmerZuVeranstaltungenByKennung(
            String kennung) {
        return veranstaltungService.getMitKennung(veranstaltungRepository, kennung)
                .map(veranstaltungMapper::teilnehmendePersonenUndAusweiseToViewModelExtern)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden.".formatted(kennung)));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> getBerechtigteZuVeranstaltungenById(String id) {
        return veranstaltungService.getMitID(veranstaltungRepository, id)
                .map(veranstaltungMapper::berechtigtePersonenUndAusweiseToViewModelExtern)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(id)));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> getBerechtigteZuVeranstaltungenByKennung(
            String kennung) {
        return veranstaltungService.getMitKennung(veranstaltungRepository, kennung)
                .map(veranstaltungMapper::berechtigtePersonenUndAusweiseToViewModelExtern)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden.".formatted(kennung)));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> berechtigeZuVeranstaltungMitIdHinzufuegen(String id,
                                                                                                        PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .fuegeBerechtigtePersonenUndAusweiseMitVeranstaltungIdHinzu(id,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.berechtigtePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> berechtigeZuVeranstaltungMitKennungHinzufuegen(
            String kennung,
            PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .fuegeBerechtigtePersonenUndAusweiseMitVeranstaltungKennungHinzu(kennung,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.berechtigtePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> teilnehmendeZuVeranstaltungMitIdHinzufuegen(String id,
                                                                                                          PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .fuegeTeilnehmendePersonenUndAusweiseMitVeranstaltungIdHinzu(id,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.teilnehmendePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> teilnehmendeZuVeranstaltungMitKennungHinzufuegen(
            String kennung,
            PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .fuegeTeilnehmendePersonenUndAusweiseMitVeranstaltungKennungHinzu(kennung,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.teilnehmendePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> berechtigteZuVeranstaltungMitIdEntfernen(String id,
                                                                                                       PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .entferneBerechtigtePersonenUndAusweiseMitVeranstaltungId(id,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.berechtigtePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> berechtigteZuVeranstaltungMitKennungEntfernen(
            String kennung,
            PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .entferneBerechtigtePersonenUndAusweiseMitVeranstaltungKennung(kennung,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.berechtigtePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> teilnehmendeZuVeranstaltungMitIdEntfernen(String id,
                                                                                                        PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .entferneTeilnehmendePersonenUndAusweiseMitVeranstaltungId(id,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.teilnehmendePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<PersonenUndAusweiseViewModelExtern> teilnehmendeZuVeranstaltungMitKennungEntfernen(
            String kennung,
            PersonenUndAusweisKennungenUndIdsViewModelExtern personenUndAusweisKennungenUndIdsViewModelExtern) {
        final Veranstaltung veranstaltung = controllerServiceVeranstaltung
                .entferneTeilnehmendePersonenUndAusweiseMitVeranstaltungKennung(kennung,
                        personenUndAusweisKennungenUndIdsViewModelExtern);
        return ResponseEntity
                .ok(this.veranstaltungMapper.teilnehmendePersonenUndAusweiseToViewModelExtern(veranstaltung));
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> terminZuVeranstaltungBearbeiten(
            TerminBearbeitenViewModelExtern terminBearbeitenViewModelExtern) {
        Termin termin = this.controllerServiceTermin.bearbeiteTermin(terminBearbeitenViewModelExtern);
        return ResponseEntity.ok(veranstaltungMapper.toViewModelExtern(termin.getVeranstaltung()));

    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> terminZuVeranstaltungHinzufuegen(
            TerminErstellenViewModelExtern terminErstellenViewModelExtern) {
        Termin termin = this.controllerServiceTermin.fuegeTerminHinzu(terminErstellenViewModelExtern);
        return ResponseEntity.ok(veranstaltungMapper.toViewModelExtern(termin.getVeranstaltung()));
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> terminZuVeranstaltungMitIdLoeschen(String id) {
        return ResponseEntity.ok(veranstaltungMapper.toViewModelExtern(this.terminService.entferneTerminMitId(id)));
    }

    @Override
    public ResponseEntity<VeranstaltungViewModelExtern> terminZuVeranstaltungMitKennungLoeschen(String kennung) {
        return ResponseEntity
                .ok(veranstaltungMapper.toViewModelExtern(this.terminService.entferneTerminMitKennung(kennung)));
    }

    /**
     * Fehlerhandling
     *
     * @param ex Abzufangender Fehler
     * @return ViewModel mit dem Fehler
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(Exception ex) {
        return handleError(ex, VeranstaltungRestController.class);
    }

}
