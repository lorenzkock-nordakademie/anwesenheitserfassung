package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.FehlerViewModelExtern;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.OffsetDateTime;
import java.util.NoSuchElementException;
import java.util.UUID;

@Log4j2
public class BaseRestController {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(NoSuchElementException ex) {
        var fehler = new FehlerViewModelExtern();
        fehler.message(ex.getMessage());
        fehler.status(HttpStatus.NOT_FOUND.value());
        fehler.setTimestamp(OffsetDateTime.now());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(fehler);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(IllegalArgumentException ex) {
        var fehler = new FehlerViewModelExtern();
        fehler.message(ex.getMessage());
        fehler.status(HttpStatus.BAD_REQUEST.value());
        fehler.setTimestamp(OffsetDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(fehler);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(MethodArgumentNotValidException ex) {
        var fehler = new FehlerViewModelExtern();
        fehler.message(ex.getMessage());
        fehler.status(HttpStatus.BAD_REQUEST.value());
        fehler.setTimestamp(OffsetDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(fehler);
    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<FehlerViewModelExtern> handleError() {
        var fehler = new FehlerViewModelExtern();
        fehler.message("Anfrage nicht lesbar. Falsches Format");
        fehler.status(HttpStatus.BAD_REQUEST.value());
        fehler.setTimestamp(OffsetDateTime.now());
        return
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body(fehler);
    }

    public ResponseEntity<FehlerViewModelExtern> handleError(Exception ex,
                                                             Class<?> classWithError) {
        final String logId = UUID.randomUUID().toString();
        log.error("Fehler in %s abgefangen. Log-ID: %s".formatted(classWithError.getName(), logId), ex);
        var fehler = new FehlerViewModelExtern();
        fehler.message("Ein Server-Fehler ist aufgetreten. Log-ID ist: %s".formatted(logId));
        fehler.status(HttpStatus.INTERNAL_SERVER_ERROR.value());
        fehler.setTimestamp(OffsetDateTime.now());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(fehler);
    }
}
