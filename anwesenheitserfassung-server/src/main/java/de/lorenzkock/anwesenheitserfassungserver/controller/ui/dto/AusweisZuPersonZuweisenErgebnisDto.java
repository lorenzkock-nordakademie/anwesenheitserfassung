package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import de.lorenzkock.anwesenheitserfassungserver.enums.AusweisZuPersonZuweisenValidierungsErgebnis;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class AusweisZuPersonZuweisenErgebnisDto {

	AusweisZuPersonZuweisenValidierungsErgebnis ergebnis;

}
