package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

/**
 * Controller zum Verarbeiten des Logins
 * @author Lorenz Kock
 */
@Controller
@RequestMapping
public class LoginController extends BaseController {

	/**
	 * Startseite
	 */
	@GetMapping("/")
	public String homeGet() {
		return "home";
	}

	/**
	 * Login-Seite
	 */
	@GetMapping("/login")
	public String loginGet() {

		// Wenn Benutze existiert, leite ihn zu Startseite
		if (getBenutzer().isPresent()) {
			return "redirect:/";
		}
		return "login";
	}

	/**
	 * Verarbeiten eines Fehlers beim Login
	 */
	@GetMapping("/login-error")
	public String login(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession(false);
		String errorMessage = null;
		if (session != null) {
			// Ermitteln des letzten Fehlers und Ausgabe
			AuthenticationException ex = (AuthenticationException) session
					.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
			if (ex != null) {
				errorMessage = ex.getMessage();
			}
		}

		// Ergänzen des Fehlers zum Model
		model.addAttribute("errorMessage", errorMessage);
		return "login";
	}

}
