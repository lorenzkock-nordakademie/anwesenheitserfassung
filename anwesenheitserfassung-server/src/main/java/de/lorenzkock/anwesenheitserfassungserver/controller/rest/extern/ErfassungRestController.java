package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.Erfassung;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.ErfassungMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.AnwesenheitenApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.ErfassungenApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuTerminViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuVeranstaltungViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.ErfassungViewModelExtern;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Verarbeitet Anfragen der externen API zu Erfassungen
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ErfassungRestController implements ErfassungenApiExtern {

    // Services
    private final ErfassungService erfassungService;

    // Mapper
    private final ErfassungMapper erfassungMapper;

    @Override
    public ResponseEntity<List<ErfassungViewModelExtern>> getErfassungen(String personId, String personKennung, String rfidSnr, String rfidKennung, String veranstaltungId, String veranstaltungKennung, String terminId, String terminKennung) {

        List<Erfassung> erfassungen = erfassungService.findeErfassungenZuTerminNachFilter(Optional.ofNullable(StringUtils.getIfEmpty(personId, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(personKennung, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(terminId, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(terminKennung, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(veranstaltungId, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(veranstaltungKennung, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(rfidSnr, null)),
                Optional.ofNullable(StringUtils.getIfEmpty(rfidKennung, null)));


        return ResponseEntity.ok(erfassungen.stream().sorted(Comparator.comparing(Erfassung::getZeitpunkt)).map(erfassungMapper::toViewModelExtern).toList());
    }


}
