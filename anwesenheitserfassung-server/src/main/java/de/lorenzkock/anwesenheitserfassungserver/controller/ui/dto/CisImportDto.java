package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
public class CisImportDto {

	MultipartFile cisFile;
	String veranstaltungName;
	String veranstaltungKennung;
	String raumId;

}
