package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.BenutzerBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.BenutzerErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.BenutzerRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.BenutzerService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.BenutzerMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.NoSuchElementException;

/**
 * Controller Service zum Verarbeiten der Anfragen aus der UI
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/benutzer")
public class BenutzerControllerService extends BaseController {


    // Service
    final BenutzerService benutzerService;

    // Mapper
    final BenutzerMapper benutzerMapper;

    // Repositories
    final private BenutzerRepository benutzerRepo;

    // Weitere Hilfsklassen
    final private PasswordEncoder passwordEncoder;

    /**
     * Fügt einen Benutzer hinzu
     *
     * @param dto Daten des Benutzers
     * @return Erstellter Benutzer
     * @throws IllegalArgumentException Fehler
     */
    public Benutzer fuegeBenutzerHinzu(BenutzerErstellenDto dto)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere DTO fachlich
        this.validiereDtoFachlich(dto);

        // Prüfe, ob der Benutzername bereits vergeben ist.
        this.benutzerService.getBenutzerMitBenutzername(dto.getBenutzername()).ifPresent((b) -> {
            throw new IllegalArgumentException(
                    "Ein Benutzer mit dem Benutzernamen '%s' existiert bereits.".formatted(dto.getBenutzername()));
        });

        // Anlage des Benutzers
        Benutzer benutzer = Benutzer.builder()
                .benutzername(dto.getBenutzername())
                .passwort(passwordEncoder.encode(dto.getPasswort()))
                .berechtigungen(benutzerMapper.mapBerechtigungen(dto.getBerechtigungen()))
                .build();

        // Füge die Person zu dem Benutzer hinzu
        this.benutzerService.fuegePersonHinzu(benutzer, dto.getPersonId());

        return benutzerRepo.save(benutzer);

    }

    /**
     * Bearbeiten eines Benutzers
     *
     * @param dto Daten des Benutzers
     * @return Bearbeiteter Benutzer
     * @throws NoSuchElementException   Fehler, wenn Benutzer nicht gefunden wurde
     * @throws IllegalArgumentException Fehler
     */
    public Benutzer bearbeiteBenutzer(BenutzerBearbeitenDto dto)
            throws NoSuchElementException, IllegalArgumentException {

        Benutzer benutzerZuBearbeiten = benutzerRepo.findById(dto.getId())
                .orElseThrow(() -> new IllegalArgumentException(
                        "Ein Benutzer mit der ID '%s' existiert nicht.".formatted(dto.getId())));

        // Validiere DTO fachlich
        this.validiereDtoFachlich(dto);

        // Prüfe, ob der Benutzername bereits vergeben ist.
        this.benutzerService.getBenutzerMitBenutzername(dto.getBenutzername())
                .filter(b -> !b.getId().equals(dto.getId()))
                .ifPresent((b) -> {
                    throw new NoSuchElementException("Ein Benutzer mit dem Benutzernamen '%s' existiert bereits."
                            .formatted(dto.getBenutzername()));
                });

        // Anlage des Benutzers
        Benutzer.BenutzerBuilder<?, ?> builder = benutzerZuBearbeiten.toBuilder();

        // Bearbeite Benutzernamen, bzw. übernimm den alten
        builder.benutzername(dto.getBenutzername());

        // Füge die Berechtigung hinzu. Dafür Mapping von String zu Enum
        builder.berechtigungen(benutzerMapper.mapBerechtigungen(dto.getBerechtigungen()));

        // Wenn ein Passwort gesetzt wurde, wird dies geändert
        if (!StringUtils.isBlank(dto.getPasswort())) {
            builder.passwort(passwordEncoder.encode(dto.getPasswort()));
        }

        benutzerZuBearbeiten = builder.build();

        // Füge die Person zu dem Benutzer hinzu
        this.benutzerService.fuegePersonHinzu(benutzerZuBearbeiten, dto.getPersonId());

        return benutzerRepo.save(benutzerZuBearbeiten);

    }

    /*
     * Validierungsmethoden für DTO (UI)
     */

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final BenutzerErstellenDto dto) {
        // Prüfe, ob der Benutzername leer ist
        if (StringUtils.isBlank(dto.getBenutzername())) {
            throw new IllegalArgumentException("Der Benutzername darf nicht leer sein.");
        }

        // Prüfe, ob der Benutzername Leerzeichen enthält
        if (StringUtils.contains(dto.getBenutzername(), " ")) {
            throw new IllegalArgumentException("Der Benutzername darf keine Leerzeichen enthalten.");
        }

        // Prüfe, ob das Passwort leer ist
        if (StringUtils.isBlank(dto.getPasswort())) {
            throw new IllegalArgumentException("Das Password darf nicht leer sein.");
        }

        // Prüfe, ob das Passwort Leerzeichen enthält
        if (StringUtils.contains(dto.getPasswort(), " ")) {
            throw new IllegalArgumentException("Das Password darf keine Leerzeichen enthalten.");
        }

    }

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final BenutzerBearbeitenDto dto) {
        // Prüfe, ob der Benutzername leer ist
        if (StringUtils.isBlank(dto.getBenutzername())) {
            throw new IllegalArgumentException("Der Benutzername darf nicht leer sein.");
        }

        // Prüfe, ob der Benutzername Leerzeichen enthält
        if (StringUtils.contains(dto.getBenutzername(), " ")) {
            throw new IllegalArgumentException("Der Benutzername darf keine Leerzeichen enthalten.");
        }

        // Prüfe, ob das Passwort Leerzeichen enthält
        if (StringUtils.contains(dto.getPasswort(), " ")) {
            throw new IllegalArgumentException("Das Password darf keine Leerzeichen enthalten.");
        }

    }

}
