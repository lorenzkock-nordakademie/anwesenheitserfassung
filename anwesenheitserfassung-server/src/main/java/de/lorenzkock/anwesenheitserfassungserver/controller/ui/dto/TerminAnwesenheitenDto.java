package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.util.ArrayList;
import java.util.List;

import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class TerminAnwesenheitenDto {

	Termin termin;

	@Builder.Default
	List<TeilnehmerAnwesenheitPersonDto> teilnehmendePersonen = new ArrayList<>();

	@Builder.Default
	List<TeilnehmerAnwesenheitAusweisDto> teilnehmendeAusweise = new ArrayList<>();

	@Builder.Default
	List<NichtZugewiesenerAusweisDto> nichtZugewieseneAusweise = new ArrayList<>();

	@Builder.Default
	List<ErfassungDto> erfassungen = new ArrayList<>();

	public List<TeilnehmerAnwesenheitPersonDto> getTeilnehmendePersonenSorted() {
		return this.teilnehmendePersonen.stream().sorted(TeilnehmerAnwesenheitPersonDto::sortByPersonName).toList();
	}

}
