package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.VeranstaltungMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonenUndAusweisKennungenUndIdsViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.VeranstaltungBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.VeranstaltungErstellenViewModelExtern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für die Verarbeitung von ViewModels zu Veranstaltungen aus der externen API
 */
@Component
@RequiredArgsConstructor
public class VeranstaltungRestControllerServiceVeranstaltung {

    // Mapper
    private final VeranstaltungMapper veranstaltungMapper;

    // Service
    private final VeranstaltungService veranstaltungService;

    // Repository
    private final VeranstaltungRepository veranstaltungRepository;
    final private AusweisRepository ausweisRepository;
    final private PersonRepository personRepository;


    /**
     * Erstelle eine Veranstaltung
     *
     * @param viewModel ViewModel mit den Daten
     * @return Erstellte Veranstaltung
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung fuegeVeranstaltungHinzu(@NonNull VeranstaltungErstellenViewModelExtern viewModel)
            throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einer Veranstaltung mit der Kennung gesucht, existiert
        // bereits eine,
        // wird ein Fehler geworfen.
        this.veranstaltungService.pruefeKennungEinmalig(veranstaltungRepository, viewModel.getKennung(), Optional.empty());

        // Anlage der Veranstaltung
        Veranstaltung veranstaltungZuErstellen = veranstaltungMapper.fromViewModelExtern(viewModel);

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(veranstaltungZuErstellen);

        return veranstaltungRepository.save(veranstaltungZuErstellen);

    }

    /**
     * Bearbeite eine Veranstaltung
     *
     * @param viewModel ViewModel mit den Daten
     * @return Bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn die Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung bearbeiteVeranstaltung(@NonNull VeranstaltungBearbeitenViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine ID gesetzt ist, bearbeite die Veranstaltung anhand der ID
        if (!StringUtils.isBlank(viewModel.getId())) {

            Veranstaltung veranstaltungZuBearbeiten = veranstaltungRepository.findById(viewModel.getId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde keine Veranstaltung mit der ID '%s' gefunden.".formatted(viewModel.getId())));

            // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
            // existiert.
            this.veranstaltungService.pruefeKennungEinmalig(veranstaltungRepository, viewModel.getKennung(), Optional.of(veranstaltungZuBearbeiten.getId()));

            // Ergänze Daten zu der Veranstaltung
            veranstaltungZuBearbeiten = veranstaltungService.ergaenzeVeranstaltung(veranstaltungZuBearbeiten, viewModel.getName(),
                    Optional.ofNullable(viewModel.getErlaubeEinUndAusgehendeBuchungen()).orElse(veranstaltungZuBearbeiten.isErlaubeEinUndAusgehendeBuchungen()),
                    Optional.ofNullable(viewModel.getErlaubeMehrfachBuchungen()).orElse(veranstaltungZuBearbeiten.isErlaubeMehrfachBuchungen()),
                    Optional.ofNullable(viewModel.getErlaubeFremdausweis()).orElse(veranstaltungZuBearbeiten.isErlaubeFremdausweis()),
                    Optional.ofNullable(viewModel.getFremdausweisBestaetigen()).orElse(veranstaltungZuBearbeiten.isFremdausweisBestaetigen()),
                    Optional.ofNullable(viewModel.getKennung()));

            return veranstaltungRepository.save(veranstaltungZuBearbeiten);

        }

        // Wenn eine Kennung gesetzt ist, bearbeite die Veranstaltung anhand der Kennung
        if (!StringUtils.isBlank(viewModel.getKennung())) {

            // Suche die Veranstaltung mit der Kennung
            Veranstaltung veranstaltungZuBearbeiten = veranstaltungRepository.findByKennung(viewModel.getKennung())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde keine Veranstaltung mit der Kennung '%s' gefunden."
                                    .formatted(viewModel.getKennung())));

            // Ergänze Daten zu der Veranstaltung
            veranstaltungZuBearbeiten = veranstaltungService.ergaenzeVeranstaltung(veranstaltungZuBearbeiten, viewModel.getName(),
                    Optional.ofNullable(viewModel.getErlaubeEinUndAusgehendeBuchungen()).orElse(veranstaltungZuBearbeiten.isErlaubeEinUndAusgehendeBuchungen()),
                    Optional.ofNullable(viewModel.getErlaubeMehrfachBuchungen()).orElse(veranstaltungZuBearbeiten.isErlaubeMehrfachBuchungen()),
                    Optional.ofNullable(viewModel.getErlaubeFremdausweis()).orElse(veranstaltungZuBearbeiten.isErlaubeFremdausweis()),
                    Optional.ofNullable(viewModel.getFremdausweisBestaetigen()).orElse(veranstaltungZuBearbeiten.isFremdausweisBestaetigen()),
                    Optional.of(viewModel.getKennung()));

            return veranstaltungRepository.save(veranstaltungZuBearbeiten);

        }

        throw new IllegalArgumentException("Es wurde weder eine ID noch eine Kennung angegeben.");

    }


    /*
     * Methoden zum Hinzufügen von teilnehmenden Personen und Ausweisen zu einer
     * Veranstaltung
     */

    /**
     * Fügt teilnehmende Personen und Ausweise zu einer Veranstaltung anhand der ID hinzu
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param viewModel       ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung fuegeTeilnehmendePersonenUndAusweiseMitVeranstaltungIdHinzu(@NonNull String veranstaltungId,
                                                                                     @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));
        return this.veranstaltungService.fuegeTeilnehmendePersonenUndAusweiseHinzu(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /**
     * Fügt teilnehmende Personen und Ausweise zu einer Veranstaltung anhand der Kennung hinzu
     *
     * @param veranstaltungKennung Kennung der Veranstaltung
     * @param viewModel            ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung fuegeTeilnehmendePersonenUndAusweiseMitVeranstaltungKennungHinzu(String veranstaltungKennung,
                                                                                          PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitKennung(veranstaltungRepository, veranstaltungKennung).orElseThrow(
                () -> new NoSuchElementException("Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden."
                        .formatted(veranstaltungKennung)));
        return this.veranstaltungService.fuegeTeilnehmendePersonenUndAusweiseHinzu(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /*
     * Methoden zum Entfernen von teilnehmenden Personen und Ausweisen zu einer
     * Veranstaltung
     */

    /**
     * Entfernt berechtigte Personen und Ausweise von einer Veranstaltung anhand der ID hinzu
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param viewModel       ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung entferneTeilnehmendePersonenUndAusweiseMitVeranstaltungId(@NonNull String veranstaltungId,
                                                                                   @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));
        return this.veranstaltungService.entferneTeilnehmendePersonenUndAusweise(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /**
     * Entfernt berechtigte Personen und Ausweise von einer Veranstaltung anhand der Kennung hinzu
     *
     * @param veranstaltungKennung Kennung der Veranstaltung
     * @param viewModel            ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung entferneTeilnehmendePersonenUndAusweiseMitVeranstaltungKennung(@NonNull String veranstaltungKennung,
                                                                                        @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitKennung(veranstaltungRepository, veranstaltungKennung).orElseThrow(
                () -> new NoSuchElementException("Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden."
                        .formatted(veranstaltungKennung)));
        return this.veranstaltungService.entferneTeilnehmendePersonenUndAusweise(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /*
     * Methoden zum Hinzufügen von berechtigten Personen und Ausweisen zu einer
     * Veranstaltung
     */


    /**
     * Fügt berechtigte Personen und Ausweise zu einer Veranstaltung anhand der ID hinzu
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param viewModel       ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung fuegeBerechtigtePersonenUndAusweiseMitVeranstaltungIdHinzu(@NonNull String veranstaltungId,
                                                                                    @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));

        // Füge die Personen und Ausweise aus dem View Model der Veranstaltung hinzu
        return this.veranstaltungService.fuegeBerechtigePersonenUndAusweiseHinzu(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /**
     * Fügt berechtigte Personen und Ausweise zu einer Veranstaltung anhand der Kennung hinzu
     *
     * @param veranstaltungKennung Kennung der Veranstaltung
     * @param viewModel            ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung fuegeBerechtigtePersonenUndAusweiseMitVeranstaltungKennungHinzu(@NonNull String veranstaltungKennung,
                                                                                         @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitKennung(veranstaltungRepository, veranstaltungKennung).orElseThrow(
                () -> new NoSuchElementException("Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden."
                        .formatted(veranstaltungKennung)));

        // Füge die Personen und Ausweise aus dem View Model der Veranstaltung hinzu
        return this.veranstaltungService.fuegeBerechtigePersonenUndAusweiseHinzu(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));


    }

    /*
     * Methoden zum Entfernen von berechtigten Personen und Ausweisen zu einer
     * Veranstaltung
     */


    /**
     * Entfernt berechtigte Personen und Ausweise von einer Veranstaltung anhand der ID
     *
     * @param veranstaltungId ID der Veranstaltung
     * @param viewModel       ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung entferneBerechtigtePersonenUndAusweiseMitVeranstaltungId(@NonNull String veranstaltungId,
                                                                                  @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Veranstaltung mit der ID '%s' gefunden werden.".formatted(veranstaltungId)));
        return this.veranstaltungService.entferneBerechtigePersonenUndAusweise(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /**
     * Entfernt berechtigte Personen und Ausweise von einer Veranstaltung anhand der ID
     *
     * @param veranstaltungKennung Kennung der Veranstaltung
     * @param viewModel            ViewModel mit den Daten
     * @return Ergänzte Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden wurde
     * @throws IllegalArgumentException Andere Fehler
     */
    public Veranstaltung entferneBerechtigtePersonenUndAusweiseMitVeranstaltungKennung(@NonNull String veranstaltungKennung,
                                                                                       @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        final Veranstaltung veranstaltung = veranstaltungService.getMitKennung(veranstaltungRepository, veranstaltungKennung).orElseThrow(
                () -> new NoSuchElementException("Es konnte keine Veranstaltung mit der Kennung '%s' gefunden werden."
                        .formatted(veranstaltungKennung)));
        return this.veranstaltungService.entferneBerechtigePersonenUndAusweise(veranstaltung, extrahiereAusweise(viewModel), extrahierePersonen(viewModel));

    }

    /*
     * Hilfsmethoden
     */

    /**
     * Extrahiere die Ausweise aus dem ViewModel mit Kennungen und Seriennummern
     *
     * @param viewModel ViewModel mit den Daten
     * @return Liste von ermittelten Ausweisen
     */
    private List<Ausweis> extrahiereAusweise(@NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel) {
        List<Ausweis> ausweise = new ArrayList<>();
        if (viewModel.getAusweisKennungen() != null) {
            viewModel.getAusweisKennungen()
                    .stream()
                    .map(in -> ausweisRepository.findFirstByIdKennungAndAktiv(in, true))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(ausweise::add);
        }
        if (viewModel.getAusweisRfidSnrn() != null) {
            viewModel.getAusweisRfidSnrn()
                    .stream()
                    .map(snr -> ausweisRepository.findFirstByIdRfidSnrAndAktiv(snr, true))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(ausweise::add);
        }
        return ausweise;
    }

    /**
     * Extrahiere die Personen aus dem ViewModel mit Kennungen und IDs
     *
     * @param viewModel ViewModel mit den Daten
     * @return Liste von ermittelten Personen
     */
    private List<Person> extrahierePersonen(@NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel) {
        List<Person> personen = new ArrayList<>();
        if (viewModel.getPersonKennungen() != null) {
            viewModel.getPersonKennungen()
                    .stream()
                    .map(personRepository::findByKennung)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(personen::add);
        }
        if (viewModel.getPersonIds() != null) {
            viewModel.getPersonIds()
                    .stream()
                    .map(personRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(personen::add);
        }
        return personen;
    }

    /*
     * Validierungsmethoden für ViewModelExtern (Externe API)
     */


    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull VeranstaltungErstellenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getName()))
            throw new IllegalArgumentException("Der Name der Veranstaltung darf nicht leer sein.");
        if (viewModel.getRaumIds() != null && !viewModel.getRaumIds().isEmpty() && viewModel.getRaumKennungen() != null && !viewModel.getRaumKennungen().isEmpty())
            throw new IllegalArgumentException("Es dürfen entweder nur Kennungen oder IDs für Räume gegeben werden.");
    }


    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull VeranstaltungBearbeitenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getName()))
            throw new IllegalArgumentException("Der Name der Veranstaltung darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getId()) && StringUtils.isBlank(viewModel.getKennung()))
            throw new IllegalArgumentException("Entweder die ID oder die Kennung muss angegeben werden.");
        if (viewModel.getRaumIds() != null && !viewModel.getRaumIds().isEmpty() && viewModel.getRaumKennungen() != null && !viewModel.getRaumKennungen().isEmpty())
            throw new IllegalArgumentException("Es dürfen entweder nur Kennungen oder IDs für Räume gegeben werden.");
    }


    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull PersonenUndAusweisKennungenUndIdsViewModelExtern viewModel) {

        // Prüfen, ob sowohl IDs als auch Kennungen geliefert werden, dies ist nicht
        // erlaubt.
        if ((viewModel.getPersonIds() != null && !viewModel.getPersonIds().isEmpty())
                && (viewModel.getPersonKennungen() != null && !viewModel.getPersonKennungen().isEmpty()))
            throw new IllegalArgumentException(
                    "Es dürfen entweder nur IDs oder nur Kennungen von Personen geliefert werden.");
        if ((viewModel.getAusweisRfidSnrn() != null && !viewModel.getAusweisRfidSnrn().isEmpty())
                && (viewModel.getAusweisKennungen() != null && !viewModel.getAusweisKennungen().isEmpty()))
            throw new IllegalArgumentException(
                    "Es dürfen entweder nur RFID-Seriennummern oder nur Kennungen von Ausweisen geliefert werden.");

    }

}
