package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.util.ArrayList;
import java.util.List;

import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitAktion;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatusDetail;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class TeilnehmerAnwesenheitAusweisDto {

	AusweisDto ausweis;

	@Builder.Default
	List<ErfassungDto> erfassungen = new ArrayList<>();

	@Builder.Default
	List<ErfassungKomprimiertDto> erfassungenKomprimiert = new ArrayList<>();

	AnwesenheitStatusDetail status;

	AnwesenheitAktion aktion;

}
