package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.time.OffsetDateTime;

import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class NichtZugewiesenerAusweisDto {

	AusweisDto ausweis;

	OffsetDateTime letzterZeitpunkt;

	public String getLetzterZeitpunktFormatted() {
		return DatumHelper.formatDateTime(this.letzterZeitpunkt);
	}

}
