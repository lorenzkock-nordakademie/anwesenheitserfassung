package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.stereotype.Component;

import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.Erfassung;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatus;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AnwesenheitenMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.ErfassungMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.TerminMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.VeranstaltungMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitAusweisViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitPersonViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuTerminMitVeranstaltungViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuTerminViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuVeranstaltungViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.service.AnwesenheitService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Service für die Verarbeitung von ViewModels aus der externen API
 */
@Component
@RequiredArgsConstructor
public class AnwesenheitRestControllerService {

	// Mapper
	private final AusweisMapper ausweisMapper;
	private final TerminMapper terminMapper;
	private final VeranstaltungMapper veranstaltungMapper;
	private final AnwesenheitenMapper anwesenheitenMapper;
	private final ErfassungMapper erfassungMapper;
	private final PersonMapper personMapper;

	// Service
	private final ErfassungService erfassungService;
	private final AnwesenheitService anwesenheitService;

	// Repository
	private final TerminRepository terminRepository;
	private final VeranstaltungRepository veranstaltungRepository;

	/**
	 * Ermittle die Anwesenheiten zu einem Termin
	 *
	 * @param terminId ID des Termins
	 * @return View Model mit den Daten
	 * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wurde.
	 * @throws IllegalArgumentException Fehler
	 */
	public AnwesenheitenZuTerminMitVeranstaltungViewModelExtern anwesenheitenZuTerminMitId(@NonNull String terminId)
			throws NoSuchElementException, IllegalArgumentException {
		return this.anwesenheitenZuTermin(Optional.of(terminId), Optional.empty());
	}

	/**
	 * Ermittle die Anwesenheiten zu einem Termin
	 *
	 * @param terminKennung Kennung des Termins
	 * @return View Model mit den Daten
	 * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wurde.
	 * @throws IllegalArgumentException Fehler
	 */
	public AnwesenheitenZuTerminMitVeranstaltungViewModelExtern anwesenheitenZuTerminMitKennung(
			@NonNull String terminKennung) throws NoSuchElementException, IllegalArgumentException {
		return this.anwesenheitenZuTermin(Optional.empty(), Optional.of(terminKennung));
	}

	/**
	 * Ermittle die Anwesenheiten zu einer Veranstaltung
	 *
	 * @param veranstaltungId ID der Veranstaltung
	 * @return View Model mit den Daten
	 * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wurde.
	 * @throws IllegalArgumentException Fehler
	 */
	public AnwesenheitenZuVeranstaltungViewModelExtern anwesenheitenZuVeranstaltungMitId(
			@NonNull String veranstaltungId) throws NoSuchElementException, IllegalArgumentException {
		return this.anwesenheitenZuVeranstaltung(Optional.of(veranstaltungId), Optional.empty());
	}

	/**
	 * Ermittle die Anwesenheiten zu einer Veranstaltung
	 *
	 * @param veranstaltungKennung Kennung der Veranstaltung
	 * @return View Model mit den Daten
	 * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wurde.
	 * @throws IllegalArgumentException Fehler
	 */
	public AnwesenheitenZuVeranstaltungViewModelExtern anwesenheitenZuVeranstaltungMitKennung(
			@NonNull String veranstaltungKennung) throws NoSuchElementException, IllegalArgumentException {
		return this.anwesenheitenZuVeranstaltung(Optional.empty(), Optional.of(veranstaltungKennung));
	}

	/**
	 * Ermittle die Anwesenheiten zu einem Termin
	 *
	 * @param terminId      ID des Termins
	 * @param terminKennung Kennung des Termins
	 * @return Anwesenheiten zu einer Veranstaltung
	 * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wurde.
	 * @throws IllegalArgumentException Fehler
	 */
	private AnwesenheitenZuTerminMitVeranstaltungViewModelExtern anwesenheitenZuTermin(
			@NonNull Optional<String> terminId, @NonNull Optional<String> terminKennung)
			throws NoSuchElementException, IllegalArgumentException {

		if (terminId.isEmpty() && terminKennung.isEmpty()) {
			throw new IllegalArgumentException(
					"Entweder die Termin-ID oder die Kennung eines Termins muss gegeben sein.");
		}

		// Versuche den Termin zu finden
		Termin termin = terminId.map(terminRepository::findById)
				.orElse(terminKennung.flatMap(terminRepository::findByKennung))
				.orElseThrow(() -> new NoSuchElementException(("Der geforderte Termin kann nicht gefunden werden.")));

		// Ermitteln der Anwesenheiten von zu dem Termin oder der Veranstaltung
		// teilnehmenden Personen (auch über Ausweise)
		List<AnwesenheitPersonViewModelExtern> teilnehmendePersonen = new ArrayList<>();
		for (Person person : termin.getAlleTeilnehmendenPersonen()) {

			// Ermittle alle Erfassungen zu der Person und dem Termin, um Informationen wie
			// den Status oder mögliche Aktionen zu generieren
			List<Erfassung> erfassungen = this.erfassungService.getErfassungenZuPersonUndTermin(person,
					Optional.of(termin.getId()));
			teilnehmendePersonen
					.add(this.buildAndAddAnwesenheitViewModelForPerson(termin, person, erfassungen, Optional.empty()));

		}

		// Ermitteln der Anwesenheiten von zu dem Termin oder der Veranstaltung
		// teilnehmenden Ausweise
		List<AnwesenheitAusweisViewModelExtern> teilnehmendeAusweise = new ArrayList<>();
		for (Ausweis ausweis : termin.getAlleTeilnehmendenAusweise(false)) {

			// Ermittle alle Erfassungen zu dem Ausweis und dem Termin, um Informationen wie
			// den Status oder mögliche Aktionen zu generieren
			List<Erfassung> erfassungen = this.erfassungService.getErfassungenZuAusweisUndTermin(ausweis,
					Optional.of(termin.getId()));
			teilnehmendeAusweise
					.add(buildAndAddAnwesenheitViewModelForAusweis(termin, ausweis, erfassungen, Optional.empty()));
		}

		// Baue das Ergebnis zusammen
		AnwesenheitenZuTerminMitVeranstaltungViewModelExtern anwesenheitenZuTermin = new AnwesenheitenZuTerminMitVeranstaltungViewModelExtern();
		anwesenheitenZuTermin.setTermin(terminMapper.toViewModelExtern(termin));
		anwesenheitenZuTermin.setVeranstaltung(veranstaltungMapper.toViewModelExtern(termin.getVeranstaltung()));
		anwesenheitenZuTermin.setAnwesenheitenAusweise(teilnehmendeAusweise);
		anwesenheitenZuTermin.setAnwesenheitenPersonen(teilnehmendePersonen);

		return anwesenheitenZuTermin;

	}

	/**
	 * Ermittle die Anwesenheiten zu einer Veranstaltung und den damit verbundenen
	 * Terminen
	 *
	 * @param veranstaltungId      ID der Veranstaltung
	 * @param veranstaltungKennung Kennung der Veranstaltung
	 * @return Anwesenheiten zu einer Veranstaltung
	 * @throws NoSuchElementException   Fehler, wenn Veranstaltung nicht gefunden
	 *                                  wurde.
	 * @throws IllegalArgumentException Fehler
	 */
	private AnwesenheitenZuVeranstaltungViewModelExtern anwesenheitenZuVeranstaltung(
			@NonNull Optional<String> veranstaltungId, @NonNull Optional<String> veranstaltungKennung)
			throws NoSuchElementException, IllegalArgumentException {

		if (veranstaltungId.isEmpty() && veranstaltungKennung.isEmpty()) {
			throw new IllegalArgumentException(
					"Entweder die Termin-ID oder die Kennung eines Termins muss gegeben sein.");
		}

		// Versuche den Termin zu finden
		Veranstaltung veranstaltung = veranstaltungId.map(veranstaltungRepository::findById)
				.orElse(veranstaltungKennung.flatMap(veranstaltungRepository::findByKennung))
				.orElseThrow(
						() -> new NoSuchElementException(("Die geforderte Veranstaltung kann nicht gefunden werden.")));

		List<AnwesenheitenZuTerminViewModelExtern> anwesenheitenZuTerminen = veranstaltung.getTermine()
				.stream()
				.map(Termin::getId)
				.map(this::anwesenheitenZuTerminMitId)
				.map(anwesenheitenMapper::convertAnwesenheitenZuTermin)
				.toList();

		// Baue das Ergebnis zusammen
		AnwesenheitenZuVeranstaltungViewModelExtern anwesenheitenZuTermin = new AnwesenheitenZuVeranstaltungViewModelExtern();
		anwesenheitenZuTermin.setVeranstaltung(veranstaltungMapper.toViewModelExtern(veranstaltung));
		anwesenheitenZuTermin.setTermine(anwesenheitenZuTerminen);

		return anwesenheitenZuTermin;

	}

	/**
	 * Erstelle das ViewModel zu einem Termin, einem Ausweis, einer Liste von
	 * Erfassungen
	 *
	 * @param termin              Termin der Anwesenheiten
	 * @param ausweis             Teilnehmender Ausweis zu dem Termin
	 * @param erfassungen         Liste von Erfassungen zu der Person
	 * @param verspaetetAbMinuten Optional: Minuten, ab der die Anwesenheit
	 *                            verspäetet ist, ansonsten wird Wert aus
	 *                            Konfiguration gelesen
	 * @return View Model mit den Informationen
	 */
	private AnwesenheitAusweisViewModelExtern buildAndAddAnwesenheitViewModelForAusweis(@NonNull Termin termin,
			@NonNull Ausweis ausweis, @NonNull List<Erfassung> erfassungen,
			@NonNull Optional<Integer> verspaetetAbMinuten) {

		Optional<Erfassung> ersteErfassung = erfassungen.stream().min(Comparator.comparing(Erfassung::getZeitpunkt));

		AnwesenheitStatus status = anwesenheitService.statusZuAnwesenheit(ersteErfassung.map(Erfassung::getZeitpunkt),
				Optional.ofNullable(termin.getErfassungStart()).orElse(termin.getStart()), verspaetetAbMinuten,
				ersteErfassung.map(Erfassung::isOnlineAnwesend).orElse(Boolean.FALSE));

		AnwesenheitAusweisViewModelExtern viewModel = new AnwesenheitAusweisViewModelExtern();
		viewModel.setAusweis(ausweisMapper.toViewModelExtern(ausweis));
		viewModel.setErfassungen(erfassungen.stream().map(erfassungMapper::toViewModelExtern).toList());
		viewModel.setErsteErfassung(ersteErfassung.map(Erfassung::getZeitpunkt).orElse(null));
		viewModel.setAnwesenheitStatus(anwesenheitenMapper.anwesenheitStatusAusweis(status));

		return viewModel;
	}

	/**
	 * Erstelle das ViewModel zu einem Termin, einer Person, einer Liste von
	 * Erfassungen
	 *
	 * @param termin              Termin der Anwesenheiten
	 * @param person              Teilnehmende Person zu dem Termin
	 * @param erfassungen         Liste von Erfassungen zu der Person
	 * @param verspaetetAbMinuten Optional: Minuten, ab der die Anwesenheit
	 *                            verspäetet ist, ansonsten wird Wert aus
	 *                            Konfiguration gelesen
	 * @return View Model mit den Informationen
	 */
	private AnwesenheitPersonViewModelExtern buildAndAddAnwesenheitViewModelForPerson(@NonNull Termin termin,
			@NonNull Person person, @NonNull List<Erfassung> erfassungen,
			@NonNull Optional<Integer> verspaetetAbMinuten) {

		Optional<Erfassung> ersteErfassung = erfassungen.stream().min(Comparator.comparing(Erfassung::getZeitpunkt));

		AnwesenheitStatus status = anwesenheitService.statusZuAnwesenheit(ersteErfassung.map(Erfassung::getZeitpunkt),
				Optional.ofNullable(termin.getErfassungStart()).orElse(termin.getStart()), verspaetetAbMinuten,
				ersteErfassung.map(Erfassung::isOnlineAnwesend).orElse(Boolean.FALSE));

		// Mappe die Erfassungen
		AnwesenheitPersonViewModelExtern viewModel = new AnwesenheitPersonViewModelExtern();
		viewModel.setAnwesenheitStatus(anwesenheitenMapper.anwesenheitStatusPerson(status));
		viewModel.setErfassungen(erfassungen.stream().map(erfassungMapper::toViewModelExtern).toList());
		viewModel.setPerson(personMapper.toViewModelExtern(person));
		viewModel.setErsteErfassung(ersteErfassung.map(Erfassung::getZeitpunkt).orElse(null));
		return viewModel;

	}

}
