package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisErstellenViewModelExtern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für die Verarbeitung von ViewModels aus der externen API
 */
@Component
@RequiredArgsConstructor
public class AusweisRestControllerService {

    // Mapper
    private final AusweisMapper ausweisMapper;

    // Service
    private final AusweisService ausweisService;

    // Repository
    private final AusweisRepository ausweisRepository;

    /**
     * Fügt einen Ausweis anhand eines View Models hinzu
     *
     * @param viewModel View Model mit den Daten des Ausweises
     * @return Erstellter Ausweis
     * @throws IllegalArgumentException Exception mit Fehlern
     */
    public Ausweis fuegeAusweisHinzu(@NonNull AusweisErstellenViewModelExtern viewModel)
            throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Prüfe, ob ein Ausweis mit Kennung Seriennummmer schon existiert
        this.ausweisService.pruefeKennungEinmalig(ausweisRepository, viewModel.getKennung(), Optional.empty());

        // Prüfe, ob ein aktiver Ausweis zu der RFID Seriennummer vorliegt und
        // deaktiviere sie.
        ausweisRepository.findFirstByIdRfidSnrAndAktiv(viewModel.getRfidSnr(), true)
                .ifPresent(i -> ausweisRepository.save(i.toBuilder().aktiv(false).build()));

        // Anlage des Ausweises
        Ausweis ausweisZuErstellen = ausweisMapper.fromErstellenViewModelExtern(viewModel);

        // Zuweisung der Person
        this.ausweisService.personZuweisen(ausweisZuErstellen, Optional.ofNullable(viewModel.getPersonId()), Optional.ofNullable(viewModel.getPersonKennung()));

        // Ggf. Kennung erzeugen, wenn keine gesetzt
        BaseService.ergaenzeGenerierteKennung(ausweisZuErstellen);

        // Speichere den Ausweis
        return ausweisRepository.save(ausweisZuErstellen);

    }

    /**
     * Bearbeitet einen Ausweis anhand eines View Models
     *
     * @param viewModel View Model mit den Daten des Ausweises
     * @return Bearbeiteter Ausweis
     * @throws IllegalArgumentException Exception mit Fehlern
     */
    public Ausweis bearbeiteAusweis(@NonNull AusweisBearbeitenViewModelExtern viewModel)
            throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Anlage des Ausweises
        Ausweis ausweisZuBearbeiten = (StringUtils.isBlank(viewModel.getKennung())
                ? ausweisRepository.findFirstByIdRfidSnrAndAktiv(viewModel.getRfidSnr(), true)
                : ausweisRepository.findFirstByIdRfidSnrAndIdKennung(viewModel.getRfidSnr(), viewModel.getKennung()))
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Ausweis mit der RFID-Snr '%s' und der Kennung '%s' gefunden."
                                .formatted(viewModel.getRfidSnr(), viewModel.getKennung())));

        // Prüfe, ob bereits ein Ausweis zugeordnet wurde
        this.ausweisService.pruefePersonBeiBearbeitenUndWeiseZu(ausweisZuBearbeiten, Optional.ofNullable(viewModel.getPersonId()),
                Optional.ofNullable(viewModel.getPersonKennung()));

        ausweisZuBearbeiten = ausweisRepository.save(ausweisZuBearbeiten);

        // Führe ggf. eine Zuordnung der Person zu allen Fremdausweisen zu, die bisher
        // keine Person haben
        this.ausweisService.pruefeFremdausweisePersonZuordnung(ausweisZuBearbeiten);

        return ausweisZuBearbeiten;

    }

    /*
     * Validierungsmethoden für View Models (Externe API)
     */

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull AusweisErstellenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getRfidSnr()))
            throw new IllegalArgumentException("Die RFID-Snr darf nicht leer sein.");
        if (!StringUtils.isBlank(viewModel.getPersonId()) && !StringUtils.isBlank(viewModel.getPersonKennung()))
            throw new IllegalArgumentException("Es darf nur eine Kennung oder eine ID für eine Person gegeben werden.");
        if (viewModel.getKennung() != null && viewModel.getKennung().equals("-"))
            throw new IllegalArgumentException("Die Kennung '-' ist aus technischen Gründen unzulässig.");
    }

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull AusweisBearbeitenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getRfidSnr()))
            throw new IllegalArgumentException("Die RFID-Snr darf nicht leer sein.");
        if (!StringUtils.isBlank(viewModel.getPersonId()) && !StringUtils.isBlank(viewModel.getPersonKennung()))
            throw new IllegalArgumentException("Es darf nur eine Kennung oder eine ID für eine Person gegeben werden.");
        if (viewModel.getKennung() != null && viewModel.getKennung().equals("-"))
            throw new IllegalArgumentException("Die Kennung '-' ist aus technischen Gründen unzulässig.");
    }

}
