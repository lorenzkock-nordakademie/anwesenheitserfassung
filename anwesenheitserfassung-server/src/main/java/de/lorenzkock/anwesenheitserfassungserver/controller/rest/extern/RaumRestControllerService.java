package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.RaumMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumErstellenViewModelExtern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für die Verarbeitung von ViewModels zu Räumen aus der externen API
 */
@Component
@RequiredArgsConstructor
public class RaumRestControllerService {

    // Mapper
    private final RaumMapper raumMapper;

    // Service
    private final RaumService raumService;

    // Repository
    private final RaumRepository raumRepository;

    public Raum fuegeRaumHinzu(@NonNull RaumErstellenViewModelExtern viewModel) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einem Raum mit der Kennung gesucht, existiert bereits
        // eine, wird ein Fehler geworfen.
        this.raumService.pruefeKennungEinmalig(raumRepository, viewModel.getKennung(), Optional.empty());

        // Anlage des Raums
        Raum raumZuErstellen = raumMapper.fromErstellenViewModelExtern(viewModel);

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(raumZuErstellen);
        return raumRepository.save(raumZuErstellen);

    }

    public Raum bearbeiteRaum(@NonNull RaumBearbeitenViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine ID gesetzt ist, bearbeite den Raum anhand der ID
        if (!StringUtils.isBlank(viewModel.getId())) {

            Raum raumZuBearbeiten = raumRepository.findById(viewModel.getId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde kein Raum mit der ID '%s' gefunden.".formatted(viewModel.getId())));

            // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
            // existiert.
            this.raumService.pruefeKennungEinmalig(raumRepository, viewModel.getKennung(), Optional.of(raumZuBearbeiten.getId()));

            // Bearbeite die Daten
            raumZuBearbeiten = this.raumService.ergaenzeRaum(raumZuBearbeiten, viewModel.getBezeichnung(), Optional.ofNullable(viewModel.getKennung()));

            return raumRepository.save(raumZuBearbeiten);

        }

        // Wenn eine Kennung gesetzt ist, bearbeite den Raum anhand der Kennung
        if (!StringUtils.isBlank(viewModel.getKennung())) {

            // Suche den Raum mit der Kennung
            Raum raumZuBearbeiten = raumRepository.findByKennung(viewModel.getKennung())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde kein Raum mit der Kennung '%s' gefunden.".formatted(viewModel.getKennung())));

            // Bearbeite die Daten
            raumZuBearbeiten = this.raumService.ergaenzeRaum(raumZuBearbeiten, viewModel.getBezeichnung(), Optional.of(viewModel.getKennung()));

            return raumRepository.save(raumZuBearbeiten);

        }

        throw new IllegalArgumentException("Es wurde weder eine ID noch eine Kennung angegeben.");

    }

    /*
     * Validierungsmethoden für ViewModelExtern (Externe API)
     */


    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull  RaumErstellenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getBezeichnung()))
            throw new IllegalArgumentException("Die Bezeichnung des Raums darf nicht leer sein.");
    }

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final @NonNull RaumBearbeitenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getBezeichnung()))
            throw new IllegalArgumentException("Die Bezeichnung des Raums darf nicht leer sein.");
        if (StringUtils.isBlank(viewModel.getId()) && StringUtils.isBlank(viewModel.getKennung()))
            throw new IllegalArgumentException("Entweder die ID oder die Kennung muss angegeben werden.");
    }
}
