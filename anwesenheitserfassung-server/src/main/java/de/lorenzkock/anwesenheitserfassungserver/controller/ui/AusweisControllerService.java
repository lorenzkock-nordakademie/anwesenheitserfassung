package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisUmwandelnDto;
import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Service stellt Funktionalität für den AusweisController dar und stellt eine Zwischenebene
 * zu den Services dar.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class AusweisControllerService {

    // Service
    final AusweisService ausweisService;

    // Repository
    final AusweisRepository ausweisRepository;

    // Mapper
    final AusweisMapper ausweisMapper;

    /**
     * Anlegen eines Ausweises
     *
     * @param dto Daten zum Anlegen
     * @return Angelegter Ausweis
     * @throws IllegalArgumentException Fehler
     */
    public Ausweis fuegeAusweisHinzu(@NonNull AusweisErstellenDto dto) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        // Prüfe, ob genau dieser Ausweis schön existiert
        this.ausweisService.pruefeKennungEinmalig(ausweisRepository, dto.getKennung(), Optional.empty());

        // Prüfe, ob ein aktiver Ausweis zu der RFID Seriennummer vorliegt und
        // deaktiviere sie.
        ausweisRepository.findFirstByIdRfidSnrAndAktiv(dto.getRfidSnr(), true)
                .ifPresent(i -> ausweisRepository.save(i.toBuilder().aktiv(false).build()));

        // Anlage des Ausweises
        Ausweis ausweisZuErstellen = ausweisMapper.fromErstellenDto(dto);

        // Zuweisung der Person, keine Personennummer gegeben, da durch UI immer ID
        // gegeben ist, wenn eine Person ausgewählt ist
        this.ausweisService.personZuweisen(ausweisZuErstellen, Optional.of(dto.getPersonId()), Optional.empty());

        // Ggf. Kennung erzeugen, wenn keine gesetzt
        BaseService.ergaenzeGenerierteKennung(ausweisZuErstellen);

        return ausweisRepository.save(ausweisZuErstellen);

    }

    /**
     * Bearbeite einen Ausweis
     *
     * @param dto Daten zum Bearbeiten
     * @return Bearbeiteter Ausweis
     * @throws IllegalArgumentException Fehler
     */
    public Ausweis bearbeiteAusweis(@NonNull AusweisBearbeitenDto dto) throws IllegalArgumentException {

        // Anlage des Ausweises
        Ausweis ausweisZuBearbeiten = ausweisRepository
                .findFirstByIdRfidSnrAndIdKennung(dto.getRfidSnr(), dto.getKennung())
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Ausweis mit der RFID-Snr '%s' und der Kennung '%s' gefunden."
                                .formatted(dto.getRfidSnr(), dto.getKennung())));

        // Prüfe, ob bereits ein Ausweis zugeordnet wurde
        this.ausweisService.pruefePersonBeiBearbeitenUndWeiseZu(ausweisZuBearbeiten, Optional.of(dto.getPersonId()), Optional.empty());

        ausweisZuBearbeiten = ausweisRepository.save(ausweisZuBearbeiten);

        // Führe ggf. eine Zuordnung der Person zu allen Fremdausweisen zu, die bisher
        // keine Person haben
        this.ausweisService.pruefeFremdausweisePersonZuordnung(ausweisZuBearbeiten);

        return ausweisZuBearbeiten;

    }

    /**
     * Wandle einen Ausweis um
     *
     * @param dto Daten zum Bearbeiten
     * @return Bearbeiteter Ausweis
     * @throws IllegalArgumentException Fehler
     */
    public Ausweis wanldeAusweisUm(@NonNull AusweisUmwandelnDto dto) throws IllegalArgumentException {

        // Anlage des Ausweises
        Ausweis ausweisZuBearbeiten = ausweisRepository
                .findFirstByIdRfidSnrAndIdKennung(dto.getRfidSnr(), dto.getKennung())
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Ausweis mit der RFID-Snr '%s' und der Kennung '%s' gefunden."
                                .formatted(dto.getRfidSnr(), dto.getKennung())));

        if(dto.getEigenausweis()) {
        	ausweisZuBearbeiten.setFremdausweis(false);
        	ausweisZuBearbeiten.setEigenausweis(null);
        } else {
        	// TODO: Umwandeln zum Fremdausweis ermöglichen
        	throw new IllegalArgumentException("Ein Umwandeln zum Fremdausweis ist aktuell nicht möglich!");
        }
        
        ausweisZuBearbeiten = ausweisRepository.save(ausweisZuBearbeiten);

        // Führe ggf. eine Zuordnung der Person zu allen Fremdausweisen zu, die bisher
        // keine Person haben
        this.ausweisService.pruefeFremdausweisePersonZuordnung(ausweisZuBearbeiten);

        return ausweisZuBearbeiten;

    }
    /*
     * Validierungsmethoden für DTOs (UI)
     */

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull AusweisErstellenDto dto) {
        if (StringUtils.isBlank(dto.getRfidSnr()))
            throw new IllegalArgumentException("Die RFID-Snr darf nicht leer sein.");
    }

}
