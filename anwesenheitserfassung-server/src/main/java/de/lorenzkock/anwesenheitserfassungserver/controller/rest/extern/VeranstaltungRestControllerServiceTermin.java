package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.TerminMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.TerminBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.TerminErstellenViewModelExtern;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service für die Verarbeitung von ViewModels zu Terminen aus der externen API
 */
@Component
@RequiredArgsConstructor
public class VeranstaltungRestControllerServiceTermin {

    // Mapper
    private final TerminMapper terminMapper;

    // Service
    private final TerminService terminService;

    // Repository
    private final TerminRepository terminRepository;

    /**
     * Fügt einen Termin auf Grundlage des ViewModels hinzu
     *
     * @param viewModel ViewModel mit den Daten
     * @return Erstellter Termin
     * @throws IllegalArgumentException Fehler
     */
    public Termin fuegeTerminHinzu(TerminErstellenViewModelExtern viewModel) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einer Veranstaltung mit der Kennung gesucht, existiert
        // bereits eine,
        // wird ein Fehler geworfen.
        this.terminService.pruefeKennungEinmalig(terminRepository, viewModel.getKennung(), Optional.empty());

        // Anlage der Veranstaltung
        Termin terminZuErstellen = terminMapper.fromViewModelExtern(viewModel);

        // Zuweisung des Raums
        this.terminService.raeumeZuweisen(terminZuErstellen, viewModel.getRaumIds(), viewModel.getRaumKennungen());

        // Zuweisung der Veranstaltung
        this.terminService.veranstaltungZuweisen(terminZuErstellen, viewModel.getVeranstaltungId(),
                viewModel.getVeranstaltungKennung());

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(terminZuErstellen);

        return terminRepository.save(terminZuErstellen);

    }

    /**
     * Bearbeiten eines Termins anhand des ViewModels
     *
     * @param viewModel ViewModel mit den Daten
     * @return bearbeitete Veranstaltung
     * @throws NoSuchElementException   Fehler, wenn Termin nicht gefunden wird
     * @throws IllegalArgumentException Anderer Fehler
     */
    public Termin bearbeiteTermin(TerminBearbeitenViewModelExtern viewModel)
            throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereViewModelExternFachlich(viewModel);

        // Wenn eine ID gesetzt ist, bearbeite die Veranstaltung anhand der ID
        if (!StringUtils.isBlank(viewModel.getId())) {

            Termin terminZuBearbeiten = terminRepository.findById(viewModel.getId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde kein Termin mit der ID '%s' gefunden.".formatted(viewModel.getId())));

            // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
            // existiert.
            this.terminService.pruefeKennungEinmalig(terminRepository, viewModel.getKennung(), Optional.of(terminZuBearbeiten.getId()));

            // Ergänze Daten zu dem Termin
            this.terminService.ergaenzeTermin(terminZuBearbeiten, Optional.ofNullable(viewModel.getStart()),
                    Optional.ofNullable(viewModel.getEnde()), Optional.ofNullable(viewModel.getKennung()),
                    viewModel.getRaumIds(), viewModel.getRaumKennungen());

            return terminRepository.save(terminZuBearbeiten);

        }

        // Wenn eine Kennung gesetzt ist, bearbeite die Veranstaltung anhand der Kennung
        if (!StringUtils.isBlank(viewModel.getKennung())) {

            // Suche die Veranstaltung mit der Kennung
            Termin terminZuBearbeiten = terminRepository.findByKennung(viewModel.getKennung())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Es wurde kein Termin mit der Kennung '%s' gefunden.".formatted(viewModel.getKennung())));

            // Ergänze Daten zu des Termins
            this.terminService.ergaenzeTermin(terminZuBearbeiten, Optional.ofNullable(viewModel.getStart()),
                    Optional.ofNullable(viewModel.getEnde()), Optional.ofNullable(viewModel.getKennung()),
                    viewModel.getRaumIds(), viewModel.getRaumKennungen());

            return terminRepository.save(terminZuBearbeiten);

        }

        throw new IllegalArgumentException("Es wurde weder eine ID noch eine Kennung angegeben.");

    }

    /*
     * Validierungsmethoden für ViewModelExtern (Externe API)
     */

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final TerminErstellenViewModelExtern viewModel) {
        if (viewModel.getStart() == null)
            throw new IllegalArgumentException("Der Start-Zeitpunkt darf nicht leer sein.");
        if (viewModel.getEnde() == null)
            throw new IllegalArgumentException("Der Ende-Zeitpunkt darf nicht leer sein.");
        if (viewModel.getRaumIds() != null && !viewModel.getRaumIds().isEmpty() && viewModel.getRaumKennungen() != null && !viewModel.getRaumKennungen().isEmpty())
            throw new IllegalArgumentException("Es dürfen entweder nur Kennungen oder IDs für Räume gegeben werden.");
        if (!StringUtils.isBlank(viewModel.getVeranstaltungId())
                && !StringUtils.isBlank(viewModel.getVeranstaltungKennung()))
            throw new IllegalArgumentException(
                    "Es darf nur eine Kennung oder eine ID für eine Veranstaltung gegeben werden.");
        if (StringUtils.isBlank(viewModel.getVeranstaltungId())
                && StringUtils.isBlank(viewModel.getVeranstaltungKennung()))
            throw new IllegalArgumentException(
                    "Es muss eine Kennung oder eine ID für eine Veranstaltung angegeben werden.");
    }

    /**
     * Validiert das View Model
     *
     * @param viewModel View Model, das validiert werden soll
     */
    private void validiereViewModelExternFachlich(final TerminBearbeitenViewModelExtern viewModel) {
        if (StringUtils.isBlank(viewModel.getId()) && StringUtils.isBlank(viewModel.getKennung()))
            throw new IllegalArgumentException("Entweder die ID oder die Kennung des Termins muss angegeben werden.");
        if (viewModel.getRaumIds() != null && !viewModel.getRaumIds().isEmpty() && viewModel.getRaumKennungen() != null && !viewModel.getRaumKennungen().isEmpty())
            throw new IllegalArgumentException("Es dürfen entweder nur Kennungen oder IDs für Räume gegeben werden.");
    }
}
