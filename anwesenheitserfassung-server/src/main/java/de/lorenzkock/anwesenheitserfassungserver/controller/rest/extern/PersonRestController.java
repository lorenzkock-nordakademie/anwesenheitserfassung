package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.PersonApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.FehlerViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonViewModelExtern;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Verarbeitet Anfragen der externen API zu Personen
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class PersonRestController extends BaseRestController implements PersonApiExtern {

    // Service
    private final PersonenService personenService;

    // Mapper
    private final PersonMapper personMapper;

    // ControllerService
    private final PersonRestControllerService controllerService;

    // ControllerService
    private final PersonRepository personRepository;

    @Override
    public ResponseEntity<List<PersonViewModelExtern>> getPersonen(String vorname, String nachname, String kennung) {

        List<PersonViewModelExtern> personenVM = personenService.getPersonenMitFilter(Optional.ofNullable(vorname), Optional.ofNullable(nachname), Optional.ofNullable(kennung), Optional.empty())
                .stream()
                .map(personMapper::toViewModelExtern)
                .toList();

        return ResponseEntity.ok(personenVM);
    }

    @Override
    public ResponseEntity<PersonViewModelExtern> addPerson(PersonErstellenViewModelExtern personViewModelExtern) {

        Person person = controllerService.fuegePersonHinzu(personViewModelExtern);

        PersonViewModelExtern vm = personMapper.toViewModelExtern(person);
        return ResponseEntity.ok(vm);
    }

    @Override
    public ResponseEntity<PersonViewModelExtern> editPerson(PersonBearbeitenViewModelExtern personViewModelExtern) {

        Person person = controllerService.bearbeitePerson(personViewModelExtern);

        PersonViewModelExtern vm = personMapper.toViewModelExtern(person);
        return ResponseEntity.ok(vm);
    }

    @Override
    public ResponseEntity<PersonViewModelExtern> getPersonById(String id) {
        return ResponseEntity.ok(personenService.getMitID(personRepository, id)
                .map(personMapper::toViewModelExtern)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Person mit der ID '%s' gefunden werden.".formatted(id))));
    }

    @Override
    public ResponseEntity<PersonViewModelExtern> getPersonByKennung(String kennung) {
        return ResponseEntity.ok(personenService.getMitKennung(personRepository, kennung)
                .map(personMapper::toViewModelExtern)
                .orElseThrow(() -> new NoSuchElementException(
                        "Es konnte keine Person mit der Kennung '%s' gefunden werden.".formatted(kennung))));
    }

    /**
     * Fehlerhandling
     * @param ex Abzufangender Fehler
     * @return ViewModel mit dem Fehler
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(Exception ex) {
        return handleError(ex, PersonRestController.class);
    }

}
