package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.util.ArrayList;
import java.util.List;

import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class VeranstaltungAnwesenheitenDto {

	Veranstaltung veranstaltung;

	@Builder.Default
	List<TeilnehmerAnwesenheitPersonDto> teilnehmendePersonen = new ArrayList<>();

	@Builder.Default
	List<TeilnehmerAnwesenheitAusweisDto> teilnehmendeAusweise = new ArrayList<>();

	@Builder.Default
	List<TerminAnwesenheitenDto> termine = new ArrayList<>();

	public List<TeilnehmerAnwesenheitPersonDto> getTeilnehmendePersonenSorted() {
		return this.teilnehmendePersonen.stream().sorted(TeilnehmerAnwesenheitPersonDto::sortByPersonName).toList();
	}
}
