package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.AnwesenheitenApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuTerminMitVeranstaltungViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuTerminViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AnwesenheitenZuVeranstaltungViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.FehlerViewModelExtern;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Verarbeitet Anfragen der externen API zu Anwesenheiten
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AnwesenheitRestController extends BaseRestController implements AnwesenheitenApiExtern {


    // ControllerService
    private final AnwesenheitRestControllerService controllerService;

    /**
     * Anwesenheiten zu einem Termin mit einer ID
     */
    @Override
    public ResponseEntity<AnwesenheitenZuTerminMitVeranstaltungViewModelExtern> getAnwesenheitenZuTerminMitId(String terminId) {
        return ResponseEntity.ok(controllerService.anwesenheitenZuTerminMitId(terminId));
    }

    /**
     * Anwesenheiten zu einem Termin mit einer Kennung
     */
    @Override
    public ResponseEntity<AnwesenheitenZuTerminMitVeranstaltungViewModelExtern> getAnwesenheitenZuTerminMitKennung(String terminKennung) {
        return ResponseEntity.ok(controllerService.anwesenheitenZuTerminMitKennung(terminKennung));
    }

    /**
     * Anwesenheiten zu einer Veranstaltung mit einer ID
     */
    @Override
    public ResponseEntity<AnwesenheitenZuVeranstaltungViewModelExtern> getAnwesenheitenZuVeranstaltungMitId(String veranstaltungId) {
        return ResponseEntity.ok(controllerService.anwesenheitenZuVeranstaltungMitId(veranstaltungId));
    }

    /**
     * Anwesenheiten zu einer Veranstaltung mit einer Kennung
     */
    @Override
    public ResponseEntity<AnwesenheitenZuVeranstaltungViewModelExtern> getAnwesenheitenZuVeranstaltungMitKennung(String veranstaltungKennung) {
        return ResponseEntity.ok(controllerService.anwesenheitenZuVeranstaltungMitKennung(veranstaltungKennung));
    }

    /**
     * Fehlerhandling
     *
     * @param ex Abzufangender Fehler
     * @return ViewModel mit dem Fehler
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(Exception ex) {
        return handleError(ex, AnwesenheitRestController.class);
    }

}
