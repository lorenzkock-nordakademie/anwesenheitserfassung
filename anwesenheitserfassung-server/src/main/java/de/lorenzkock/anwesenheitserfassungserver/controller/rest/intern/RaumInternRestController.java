package de.lorenzkock.anwesenheitserfassungserver.controller.rest.intern;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.RaumMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.api.RaumApiIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.RaumViewModelIntern;
import lombok.RequiredArgsConstructor;

/**
 * Dieser Controller stellt für die interne API Endpunkte für das Abrufen von Räumen bereit
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/intern")
public class RaumInternRestController implements RaumApiIntern {

	// Service
	private final RaumService raumService;

	// Mapper
	private final RaumMapper raumMapper;

	/**
	 * Endpunkt zum Abfragen von aktiven Räumen
	 * @return Liste von aktiven Räumen
	 */
	@Override
	public ResponseEntity<List<RaumViewModelIntern>> getAktiveRaeume() {
		// Ermittle Räume und sortiere sie der Kennung nach
		List<Raum> raeume = raumService.getRaeumeMitFilter(Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE))
				.stream()
				.sorted(Comparator.comparing(Raum::getBezeichnung))
				.toList();

		// Mappe das Ergebnis und gib es zurück
		return ResponseEntity.ok(raeume.stream().map(raumMapper::toViewModelIntern).toList());
	}

}
