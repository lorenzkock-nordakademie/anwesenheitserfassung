package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BenutzerBearbeitenDto {

	String id;
	String benutzername;
	String passwort;
	String personId;

	List<String> berechtigungen;

}
