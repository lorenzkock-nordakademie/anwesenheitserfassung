package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AusweisZuPersonZuweisenDto {

	String rfidSnr;
	String personId;
	boolean fremdausweis;
	String terminId;

}
