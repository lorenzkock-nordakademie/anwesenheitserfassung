package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class ErfassungKomprimiertDto {

	ErfassungDto start;
	ErfassungDto ende;

	public static ErfassungKomprimiertDto of(ErfassungDto start) {
		return ErfassungKomprimiertDto.builder().start(start).build();
	}

	public static ErfassungKomprimiertDto of(ErfassungDto start, ErfassungDto ende) {
		return ErfassungKomprimiertDto.builder().start(start).ende(ende).build();
	}

}
