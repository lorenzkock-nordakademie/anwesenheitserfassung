package de.lorenzkock.anwesenheitserfassungserver.controller.rest.extern;

import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.AusweisMapper;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.api.AusweisApiExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisBearbeitenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.FehlerViewModelExtern;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Verarbeitet Anfragen der externen API zu Ausweisen
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AusweisRestController extends BaseRestController implements AusweisApiExtern {

    // Service
    private final AusweisService ausweisService;

    // Mapper
    private final AusweisMapper ausweisMapper;

    // Controller Service
    private final AusweisRestControllerService controllerService;

    /**
     * Erstellen eines Ausweises
     */
    @Override
    public ResponseEntity<AusweisViewModelExtern> addAusweis(
            AusweisErstellenViewModelExtern ausweisErstellenViewModel) {

        Ausweis i = controllerService.fuegeAusweisHinzu(ausweisErstellenViewModel);
        return ResponseEntity.ok(ausweisMapper.toViewModelExtern(i));

    }

    /**
     * Abfragen aller Ausweise zu Filter
     */
    @Override
    public ResponseEntity<List<AusweisViewModelExtern>> getAusweise(String rfidSnr, String kennung,
                                                                    String personId, String personKennung) {

        List<String> personIds = new ArrayList<>();
        if (personId != null) {
            personIds.add(personId);
        }
        List<String> personKennungen = new ArrayList<>();
        if (personKennung != null) {
            personKennungen.add(personKennung);
        }
        List<Ausweis> ausweise = ausweisService.findeAusweiseNachFilter(rfidSnr, kennung, personIds,
                personKennungen, Boolean.TRUE, null);

        return ResponseEntity.ok(ausweise.stream().map(ausweisMapper::toViewModelExtern).toList());

    }

    /**
     * Bearbeiten eines Ausweises
     */
    @Override
    public ResponseEntity<AusweisViewModelExtern> editAusweis(
            AusweisBearbeitenViewModelExtern ausweisBearbeitenViewModel) {
        Ausweis ausweis = controllerService.bearbeiteAusweis(ausweisBearbeitenViewModel);
        return ResponseEntity.ok(ausweisMapper.toViewModelExtern(ausweis));
    }

    /**
     * Fehlerhandling
     *
     * @param ex Abzufangender Fehler
     * @return ViewModel mit dem Fehler
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<FehlerViewModelExtern> handleError(Exception ex) {
        return handleError(ex, AusweisRestController.class);
    }

}
