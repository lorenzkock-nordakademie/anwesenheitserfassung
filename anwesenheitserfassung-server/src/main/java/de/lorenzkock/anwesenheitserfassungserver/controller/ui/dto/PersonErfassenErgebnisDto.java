package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.ErfassungPerson;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.enums.PersonErfassungValidierungErgebnis;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class PersonErfassenErgebnisDto {

	Person person;
	ErfassungPerson erfassung;
	PersonErfassungValidierungErgebnis ergebnis;

}
