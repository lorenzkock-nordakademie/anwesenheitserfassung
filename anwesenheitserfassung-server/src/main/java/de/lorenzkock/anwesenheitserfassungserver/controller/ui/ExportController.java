package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AnwesenheitenExportDto;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.exception.NotAuthorizedException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Controller zum Verarbeiten von Export-Aufträgen
 *
 * @author Lorenz Kock
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/export")
public class ExportController extends BaseController {

    // Personen Service
    final PersonenService personService;

    // Controller Service
    final AnwesenheitenControllerService erfassungControllerService;

    // Controller Service
    final ExportControllerService exportControllerService;

    @GetMapping(value = "/anwesenheiten/veranstaltung/{id}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody ResponseEntity<byte[]> exportAnwesenheitenZuVeranstaltungGet(@PathVariable(name = "id") String veranstaltungId) {

        // Ermittle Benutzer und Person
        Optional<Benutzer> benutzer = getBenutzer();
        Optional<Person> person = getPerson(this.personService);


        // Prüfe zunächst die Berechtigung, ob die Aktion für den Benutzer zugelassen ist
        try {
            AnwesenheitenController.pruefeBerechtigungZumLesen(benutzer, person, Optional.ofNullable(veranstaltungId), Optional.empty());
        } catch (NotAuthorizedException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        AnwesenheitenExportDto exportDto = exportControllerService.exportiereAnwesenheitenFuerVeranstaltung(veranstaltungId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        final String dateiname = "Anwesenheiten%s-%s.csv".formatted(exportDto.getVerantaltungName(), exportDto.getVeranstaltungKennung()).replaceAll("[^a-zA-Z0-9 \\_\\-\\.]", "");
        headers.setContentDispositionFormData(dateiname, dateiname);

        return new ResponseEntity<>(exportDto.getExportDaten(), headers, HttpStatus.OK);


    }

    @GetMapping(value = "/anwesenheiten/termin/{id}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody ResponseEntity<byte[]> exportAnwesenheitenZuTerminGet(@PathVariable(name = "id") String terminId) {

        // Ermittle Benutzer und Person
        Optional<Benutzer> benutzer = getBenutzer();
        Optional<Person> person = getPerson(this.personService);


        // Prüfe zunächst die Berechtigung, ob die Aktion für den Benutzer zugelassen ist
        try {
            AnwesenheitenController.pruefeBerechtigungZumLesen(benutzer, person, Optional.empty(), Optional.of(terminId));
        } catch (NotAuthorizedException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        AnwesenheitenExportDto exportDto = exportControllerService.exportiereAnwesenheitenFuerTermin(terminId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        final String dateiname = "Anwesenheiten%s-%s.csv".formatted(exportDto.getVerantaltungName(), exportDto.getVeranstaltungKennung());
        headers.setContentDispositionFormData(dateiname, dateiname);

        return new ResponseEntity<>(exportDto.getExportDaten(), headers, HttpStatus.OK);
    }


}
