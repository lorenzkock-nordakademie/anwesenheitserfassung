package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AnwesenheitManuellErfassenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisZuPersonZuweisenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungStartAendernDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungStartAendernErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErfassenErgebnisDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.AusweisService;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Berechtigung;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.TerminService;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.VeranstaltungService;
import de.lorenzkock.anwesenheitserfassungserver.enums.AusweisZuPersonZuweisenValidierungsErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.exception.NotAuthorizedException;
import de.lorenzkock.anwesenheitserfassungserver.mapper.TerminMapper;
import de.lorenzkock.anwesenheitserfassungserver.mapper.VeranstaltungMapper;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Dieser Controller verarbeitet die Anfragen aus der UI zu den Anwesenheiten
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/verwaltung/anwesenheiten")
public class AnwesenheitenController extends BaseController {

	// Verwendete Services
	final VeranstaltungService veranstaltungService;
	final TerminService terminService;
	final PersonenService personService;
	final AusweisService ausweisService;
	final RaumService raumService;

	// Verwendete Mapper
	final VeranstaltungMapper veranstaltungMapper;
	final TerminMapper terminMapper;

	// Controller Service
	final AnwesenheitenControllerService erfassungControllerService;

	// Repository
	final VeranstaltungRepository veranstaltungRepository;

	/**
	 * Endpunkt beim Aufrufen der Anwesenheitsseite. Gibt die Möglichkeit zum Wählen
	 * einer Veranstaltung aus allen Veranstaltungen, aus allen Terminen oder aus
	 * einer Liste an Terminen zu dem angemeldeten Benutzer
	 */
	@GetMapping
	public String anwesenheitenStartGet(@RequestParam(required = false) List<String> veranstaltungen,
			@RequestParam(required = false) List<String> raeume, @RequestParam(required = false) String zeitraum,
			@RequestParam(required = false, defaultValue = "0") Integer vergangene,
			@RequestParam(required = false) Integer eintraegeJeSeite, @RequestParam(required = false) Integer seite,
			Model model) {

		// Ermittle den angemeldeten Benutzer
		Benutzer benutzer = getBenutzer().orElseThrow(() -> new HttpClientErrorException(HttpStatus.UNAUTHORIZED));

		/*
		 * Bereich zur Auswahl über Veranstaltung und Termin
		 */

		// Liste von Veranstaltungen zur Auswahl, Termine werden nachgeladen
		model.addAttribute("auswahlVeranstaltungenListe", veranstaltungService.getVeranstaltungenMitFilter(
				Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(Boolean.FALSE)));

		/*
		 * Bereich für Meine Termine
		 */

		// Werte aus dem Filter
		List<String> veranstaltungenFilterIds = Optional.ofNullable(veranstaltungen).orElse(Collections.emptyList());
		List<String> raeumeFilterIds = Optional.ofNullable(raeume).orElse(Collections.emptyList());
		Optional<String> zeitraumFilter = StringUtils.isBlank(zeitraum) ? Optional.empty() : Optional.of(zeitraum);

		// Ermittle alle Termine zum Benutzer
		List<Termin> alleTermineZuBenutzer = terminService.getAlleTermineFuerBenutzer(benutzer);

		// Ermittle alle Termine für Benutzer mit dem gegebenen Filter
		List<Termin> alleTermineMitFilter = terminService.filterTermine(alleTermineZuBenutzer, veranstaltungenFilterIds,
				raeumeFilterIds, vergangene, zeitraumFilter);

		// Erstelle die Navigation und ermittle die Einträge für die Seite
		List<Termin> alleTermineFuerSeite = this.ermittleNavigation(model, alleTermineMitFilter, seite,
				eintraegeJeSeite);

		// Termine des Benutzers
		model.addAttribute("termineFuerBenutzer", alleTermineFuerSeite);

		// Informationen für den Filter der Termine zum Benutzer
		Set<Veranstaltung> veranstaltungSet = alleTermineZuBenutzer.stream()
				.map(Termin::getVeranstaltung)
				.collect(Collectors.toSet());
		Set<Raum> raeumeSet = new HashSet<>();
		alleTermineZuBenutzer.stream().map(Termin::getRaeume).forEach(raeumeSet::addAll);

		model.addAttribute("veranstaltungenFilterListe", veranstaltungSet);
		model.addAttribute("raeumeFilterListe", raeumeSet);
		model.addAttribute("filterVeranstaltungen", veranstaltungenFilterIds);
		model.addAttribute("filterRaeume", raeumeFilterIds);
		model.addAttribute("filterVergangene", vergangene);
		model.addAttribute("filterZeitraum", zeitraumFilter.orElse(""));

		return "verwaltung/anwesenheiten";
	}

	/**
	 * Endpunkt zum Anzeigen der Anwesenheiten zu einer Veranstaltung oder einem
	 * Termin
	 */
	@GetMapping("/nachVeranstaltung")
	public String anwesenheitenNachVeranstaltungGet(@RequestParam(required = false) String veranstaltung,
			@RequestParam(required = false) String termin, @RequestParam(required = false) boolean tabelle,
			Model model) {
		// Ermittle Benutzer und Person
		Optional<Benutzer> benutzer = getBenutzer();
		Optional<Person> person = getPerson(this.personService);

		// Prüfe zunächst die Berechtigung, ob die Aktion für den Benutzer zugelassen
		// ist
		try {
			pruefeBerechtigungZumLesen(benutzer, person, Optional.ofNullable(veranstaltung),
					Optional.ofNullable(termin));
		} catch (NotAuthorizedException e) {
			return "redirect:/verwaltung/anwesenheiten";
		}

		if (StringUtils.isBlank(veranstaltung) && StringUtils.isBlank(termin)) {
			return "redirect:/verwaltung/anwesenheiten?keineVeranstaltung";
		} else {
			if (StringUtils.isBlank(termin)) {
				assert veranstaltung != null;
				model.addAttribute("anwesenheitenZuVeranstaltung",
						erfassungControllerService.anwesenheitenZuVeranstaltung(veranstaltung));
			} else {
				model.addAttribute("anwesenheitenZuTermin",
						erfassungControllerService.anwesenheitenUndNichtZugewieseneAusweiseZuTermin(termin));
			}
		}

		// Setze die Berechtigungsinformationen zum Nachtragen und Zuweisen (Wird im
		// Frontend benötigt, um Panel ein- oder auszublenden)
		this.setzeBerechtigungenZumNachtragenUndZuweisen(model, benutzer, person);

		return tabelle ? "verwaltung/anwesenheitenAusschnitt" : "verwaltung/anwesenheiten";
	}

	/**
	 * Endpunkt, um Termine zu einer gewählten Veranstaltung zu finden.
	 */
	@GetMapping("/termineZuVeranstaltung/{veranstaltungId}")
	public String termineZuVeranstaltungenGet(@PathVariable String veranstaltungId, Model model) {

		Veranstaltung veranstaltung = veranstaltungService.getMitID(veranstaltungRepository, veranstaltungId)
				.orElse(null);
		if (veranstaltung == null) {
			model.addAttribute("termineZuVeranstaltung", Collections.emptyList());
		} else {
			model.addAttribute("termineZuVeranstaltung",
					veranstaltung.getTermine().stream().sorted(Comparator.comparing(Termin::getStart)).toList());
		}

		return "verwaltung/anwesenheitenAusschnitt";
	}

	/**
	 * Endpunkt zum Verarbeiten einer Änderung des Erfassungsstarts eines Termins
	 */
	@PostMapping("/erfassungStart")
	public String anwesenheitenErfassungStartAendernPost(ErfassungStartAendernDto dto, Model model) {

		// Ermittle Benutzer und Person
		Optional<Benutzer> benutzer = getBenutzer();
		Optional<Person> person = getPerson(this.personService);

		// Prüfe zunächst die Berechtigung, ob die Aktion für den Benutzer zugelassen
		// ist
		try {
			this.pruefeBerechtigungZumNachtragen(benutzer, person, Optional.ofNullable(dto.getTerminId()));

			// Verarbeite die Erfassung und gib das Ergebnis zurück
			ErfassungStartAendernErgebnisDto ergebnis = this.erfassungControllerService
					.erfassungStartZeitpunktAendern(dto);
			if (ergebnis.getFehler().isPresent()) {
				model.addAttribute("meldungenFehler", List.of(ergebnis.getFehler().get()));
				model.addAttribute("startZeitpunkt", dto.getZeitpunkt());

			} else {
				model.addAttribute("meldungenErfolg", List.of("Der Startzeitpunkt der Erfassung wurde geändert."));
				model.addAttribute("startZeitpunkt", ergebnis.getZeitpunkt().map(DatumHelper::toDateTime).orElse(""));
			}

		} catch (NotAuthorizedException e) {
			model.addAttribute("meldungenFehler", List.of(e.getMessage()));
		}

		return "verwaltung/anwesenheitenAusschnitt";
	}

	/**
	 * Endpunkt zum Verarbeiten einer manuellen Erfassung
	 */
	@PostMapping("/erfassen")
	public String anwesenheitenErfassenPost(AnwesenheitManuellErfassenDto dto, Model model) {

		// Ermittle Benutzer und Person
		Optional<Benutzer> benutzer = getBenutzer();
		Optional<Person> person = getPerson(this.personService);

		// Prüfe zunächst die Berechtigung, ob die Aktion für den Benutzer zugelassen
		// ist
		try {
			this.pruefeBerechtigungZumNachtragen(benutzer, person, Optional.ofNullable(dto.getTerminId()));

			// Verarbeite die Erfassung und gib das Ergebnis zurück
			PersonErfassenErgebnisDto ergebnis = this.erfassungControllerService.erfassungZuPersonVerarbeiten(
					dto.getPersonId(), dto.getTerminId(), OffsetDateTime.now(), Optional.ofNullable(dto.getRaumId()),
					Optional.ofNullable(dto.getOnline()).orElse(Boolean.FALSE));
			model.addAttribute(ergebnis.getErgebnis().isErfolgreich() ? "meldungenErfolg" : "meldungenFehler",
					List.of(ergebnis.getErgebnis().getStatusNachricht()));
		} catch (NotAuthorizedException e) {
			model.addAttribute("meldungenFehler", List.of(e.getMessage()));
		}

		// Ergänze die neuen Informationen zur Anwesenheit
		model.addAttribute("anwesenheitenZuTermin",
				erfassungControllerService.anwesenheitenUndNichtZugewieseneAusweiseZuTermin(dto.getTerminId()));

		// Setze die Berechtigungsinformationen zum Nachtragen und Zuweisen (Wird im
		// Frontend benötigt, um Panel ein- oder auszublenden)
		this.setzeBerechtigungenZumNachtragenUndZuweisen(model, benutzer, person);

		return "verwaltung/anwesenheitenAusschnitt";
	}

	/**
	 * Endpunkt zum Verarbeiten einer Ausweiszuweisung zu einer Person
	 */
	@PostMapping("/ausweisZuweisen")
	public String ausweisZuweisenPost(AusweisZuPersonZuweisenDto zuweisenDto, Model model) {

		// Ermittle Benutzer und Person
		Optional<Benutzer> benutzer = getBenutzer();
		Optional<Person> person = getPerson(this.personService);

		// Prüfe zunächst die Berechtigung, ob die Aktion für den Benutzer zugelassen
		// ist
		try {
			this.pruefeBerechtigungZumAusweisZuweisen(benutzer, person, Optional.of(zuweisenDto.getTerminId()));
			AusweisZuPersonZuweisenValidierungsErgebnis ergebnis = this.erfassungControllerService
					.ausweisZuweisen(zuweisenDto)
					.getErgebnis();
			model.addAttribute(ergebnis.isErfolgreich() ? "meldungenErfolg" : "meldungenFehler",
					List.of(ergebnis.getStatusNachricht()));

		} catch (NotAuthorizedException e) {
			model.addAttribute("meldungenFehler", List.of(e.getMessage()));
		}

		// Ergänze die neuen Informationen zur Anwesenheit
		model.addAttribute("anwesenheitenZuTermin",
				erfassungControllerService.anwesenheitenUndNichtZugewieseneAusweiseZuTermin(zuweisenDto.getTerminId()));

		// Setze die Berechtigungsinformationen zum Nachtragen und Zuweisen (Wird im
		// Frontend benötigt, um Panel ein- oder auszublenden)
		this.setzeBerechtigungenZumNachtragenUndZuweisen(model, benutzer, person);

		return "verwaltung/anwesenheitenAusschnitt";
	}

	/*
	 * Prüfe Berechtigungen für unterschiedliche Anwendungsfälle
	 */

	/**
	 * Prüft die Berechtigung eines Benutzers und einer Person, ob Anwesenheiten
	 * gelesen werden dürfen
	 *
	 * @param benutzer        Benutzer, zu dem die Berechtigung geprüft werden soll.
	 *                        Wirft {@link NotAuthorizedException}, wenn leer.
	 * @param person          Person, zu der die Berechtigung geprüft werden soll
	 * @param veranstaltungId ID der Veranstaltung, zu der die Berechtigung geprüft
	 *                        werden soll.
	 * @param terminId        ID des Termins, du dem die Berechtigung geprüft werden
	 *                        soll.
	 * @throws NotAuthorizedException Wenn keine Berechtigung vorliegt
	 */
	static void pruefeBerechtigungZumLesen(@NonNull Optional<Benutzer> benutzer, @NonNull Optional<Person> person,
			@NonNull Optional<String> veranstaltungId, @NonNull Optional<String> terminId)
			throws NotAuthorizedException {

		// Wenn kein Benutzer angegeben ist, gibt es auch keine Berechtigung
		if (benutzer.isEmpty())
			throw new NotAuthorizedException("Kein Benutzer angemeldet");

		// Administratoren dürfen alles
		if (benutzer.get().getBerechtigungen().contains(Berechtigung.ADMINISTRATION))
			return;

		// Erlaube, wenn die Berechtigung zum Lesen aller Anwesenheiten existiert
		if (benutzer.get().getBerechtigungen().contains(Berechtigung.ALLE_ANWESENHEITEN_EINSEHEN))
			return;

		// Prüfe, ob personenspezifische Berechtigungen zu der Veranstaltung oder dem
		// Termin vorliegen.
		pruefeBerechtigungZuVeranstaltungOderTermin(person, veranstaltungId, terminId);
	}

	/**
	 * Prüft die Berechtigung eines Benutzers und einer Person, ob Anwesenheiten
	 * manuell gesetzt werden dürfen
	 *
	 * @param benutzer Benutzer, zu dem die Berechtigung geprüft werden soll. Wirft
	 *                 {@link NotAuthorizedException}, wenn leer.
	 * @param person   Person, zu der die Berechtigung geprüft werden soll
	 * @param terminId ID des Termins, zu dem eine Anwesenheit nachgetragen werden
	 *                 soll.
	 * @throws NotAuthorizedException Wenn keine Berechtigung vorliegt
	 */
	private void pruefeBerechtigungZumNachtragen(@NonNull Optional<Benutzer> benutzer, @NonNull Optional<Person> person,
			@NonNull Optional<String> terminId) throws NotAuthorizedException {

		// Wenn kein Benutzer angegeben ist, gibt es auch keine Berechtigung
		if (benutzer.isEmpty())
			throw new NotAuthorizedException("Kein Benutzer angemeldet");

		// Administratoren dürfen alles
		if (benutzer.get().getBerechtigungen().contains(Berechtigung.ADMINISTRATION))
			return;

		// Erlaube, wenn die Berechtigung zum Nachtragen von Anwesenheiten existiert
		if (benutzer.get().getBerechtigungen().contains(Berechtigung.ANWESENHEIT_NACHTRAGEN))
			return;

		// Prüfe, ob eine veranstaltungs- oder terminbezogene Berechtigung vorliegt
		pruefeBerechtigungZuVeranstaltungOderTermin(person, Optional.empty(), terminId);

	}

	/**
	 * Prüft die Berechtigung eines Benutzers und einer Person, ob Ausweise manuell
	 * zugewiesen werden dürfen
	 *
	 * @param benutzer Benutzer, zu dem die Berechtigung geprüft werden soll. Wirft
	 *                 {@link NotAuthorizedException}, wenn leer.
	 * @param person   Person, zu der die Berechtigung geprüft werden soll
	 * @param terminId ID des Termins, in dem der nicht zugeordnete Ausweis
	 *                 vorkommt.
	 * @throws NotAuthorizedException Wenn keine Berechtigung vorliegt
	 */
	private void pruefeBerechtigungZumAusweisZuweisen(@NonNull Optional<Benutzer> benutzer,
			@NonNull Optional<Person> person, @NonNull Optional<String> terminId) throws NotAuthorizedException {

		// Wenn kein Benutzer angegeben ist, gibt es auch keine Berechtigung
		if (benutzer.isEmpty())
			throw new NotAuthorizedException("Kein Benutzer angemeldet");

		// Administratoren dürfen alles
		if (benutzer.get().getBerechtigungen().contains(Berechtigung.ADMINISTRATION))
			return;

		// Erlaube, wenn die Berechtigung zum Bestätigen des Ausweises hat
		if (benutzer.get().getBerechtigungen().contains(Berechtigung.AUSWEIS_BESTAETIGEN))
			return;

		// Prüfe, ob personenspezifische Berechtigungen zu der Veranstaltung oder dem
		// Termin vorliegen.
		pruefeBerechtigungZuVeranstaltungOderTermin(person, Optional.empty(), terminId);

	}

	/**
	 * Prüfe eine termin- oder veranstaltungsbezogene Berechtigung durch Zuweisen
	 * der Person oder eines Ausweises einer Person
	 *
	 * @param person          Zu prüfende Person
	 * @param veranstaltungId Veranstaltung, zu der die Berechtigung geprüft werden
	 *                        soll.
	 * @param terminId        Termin zu dem die Berechtigung geprüft werden soll
	 * @throws NotAuthorizedException Wenn keine Berechtigung vorliegt
	 */
	static void pruefeBerechtigungZuVeranstaltungOderTermin(@NonNull Optional<Person> person,
			@NonNull Optional<String> veranstaltungId, @NonNull Optional<String> terminId)
			throws NotAuthorizedException {

		// Lese die Person zum Benutzer, um veranstaltungs- und terminbezogene
		// Berechtigungen zu prüfen
		if (person.isEmpty())
			throw new NotAuthorizedException("Keine Veranstaltungs- oder Termin-bezogene Berechtigung.");

		// Wenn kein Termin angegeben wird, wird keine Berechtigung geprüft
		boolean autorisiertTermin = true;
		if (terminId.isPresent()) {
			// Ermittle alle Termine, zu denen die Person berechtigt ist und prüfe ob der zu
			// prüfende Termin enthalten ist
			autorisiertTermin = person.get()
					.getAlleBerechtigenTermine()
					.stream()
					.map(Termin::getId)
					.toList()
					.contains(terminId.get());
		}

		boolean autorisiertVeranstaltung = true;
		if (autorisiertTermin && veranstaltungId.isPresent()) {
			// Ermittle alle Veranstaltungen, zu denen die Person berechtigt ist und prüfe,
			// ob die zu prüfende Veranstaltung enthalten ist
			autorisiertVeranstaltung = person.get()
					.getBerechtigteVeranstaltungen()
					.stream()
					.map(Veranstaltung::getId)
					.toList()
					.contains(veranstaltungId.get());
		}

		// Prüfe, ob eine Veranstaltungs- oder Terminbezogene Berechtigung vorliegt
		if (!autorisiertTermin || !autorisiertVeranstaltung)
			throw new NotAuthorizedException("Keine veranstaltungs- oder terminbezogene Berechtigung.");

	}

	/**
	 * Im Frontend wird die Information benötigt, zu welchen Terminen der Benutzer
	 * berechtigt ist, Anwesenheiten nachzutragen. Dies ist er entweder über die
	 * Zuweisung der Person zur Veranstaltung / Termin oder über eine Berechtigung
	 * des Benutzers.
	 *
	 * @param model    Model, das um die Informationen ergänzt wird.
	 * @param benutzer Benutzer, dessen Berechtigung geprüft wird
	 * @param person   Dem Benutzer zugeordnete Person
	 */
	private void setzeBerechtigungenZumNachtragenUndZuweisen(@NonNull Model model, @NonNull Optional<Benutzer> benutzer,
			@NonNull Optional<Person> person) {

		try {
			// Prüft, ob die Berechtigung durch eine Berechtigung am Benutzer gegeben ist.
			this.pruefeBerechtigungZumAusweisZuweisen(benutzer, Optional.empty(), Optional.empty());

			// Wenn eine globale Benutzer-Berechtigung zum Zuweisen von Ausweisen vorliegt,
			// wird null als Liste berechtigter Termine übergeben
			// null bedeutet dabei, dass alle zugelassen sind
			model.addAttribute("berechtigteTermineZuweisen", null);
		} catch (NotAuthorizedException ex) {
			// Ansonsten werden alle berechtigten Termine übergeben
			model.addAttribute("berechtigteTermineZuweisen",
					this.erfassungControllerService.getBerechtigteTerminIdsZuPerson(person));
		}

		try {
			// Prüft, ob die Berechtigung durch eine Berechtigung am Benutzer gegeben ist.
			this.pruefeBerechtigungZumNachtragen(benutzer, Optional.empty(), Optional.empty());

			// Wenn eine globale Benutzer-Berechtigung zum Nachtragen vorliegt, wird null
			// als Liste berechtigter Termine übergeben
			// null bedeutet dabei, dass alle zugelassen sind
			model.addAttribute("berechtigteTermineNachtragen", null);
		} catch (NotAuthorizedException ex) {
			// Ansonsten werden alle berechtigten Termine übergeben
			model.addAttribute("berechtigteTermineNachtragen",
					this.erfassungControllerService.getBerechtigteTerminIdsZuPerson(person));
		}
	}

}
