package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.config.security.AppUserPrincipal;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import lombok.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Basis Controller mit Funktionalität für mehrere Controller
 * @author Lorenz Kock
 */
public class BaseController {

	/**
	 * Ermittle den Benutzer aus dem SecurityContext
	 * @return Aktuell angemeldeter Benutzer
	 */
	Optional<Benutzer> getBenutzer() {
		var be = SecurityContextHolder.getContext().getAuthentication();
		return Optional.ofNullable(be)
				.map(Authentication::getPrincipal)
				.filter(b -> b instanceof AppUserPrincipal)
				.map(b -> ((AppUserPrincipal) b).benutzer());
	}

	/**
	 * Ermittelt die Person zum angemeldeten Benutzer.
	 * @param personenService Service, über den die Person abgerufen wird.
	 * @return Person oder leeres Optional.
	 */
	Optional<Person> getPerson(@NonNull PersonenService personenService) {
		Optional<Benutzer> benutzer = getBenutzer();
		if(benutzer.isEmpty()) return Optional.empty();
		return personenService.getPersonZuBenutzer(benutzer.get());
	}

	final static Integer DEFAULT_SEITE = 1;
	final static Integer DEFAULT_EINTRAEGE_JE_SEITE = 10;

	/**
	 * Ermittelt zu einer gegebenen Liste, einer Seite sowie der Anzahl von Einträgen je Seite die anzuzeigenden
	 * Einträge sowie die Elemente für die Navigation
	 * @param model Model, dem die Informationen für die Navigation hinzugefügt werden
	 * @param alleElemente Liste von allen Elementen, die in der Tabelle angezeigt werden
	 * @param seite Anzuzeigende Seite. Wenn null oder kleiner 1 wird 1 gewählt
	 * @param eintraegeJeSeite Einträge, die je Seite angezeigt werden sollen
	 * @return Liste von für die angeforderte Seite gefilterten Einträgen aus der Liste aller
	 * @param <T> Typ der Elemente, die zu Filtern sind
	 */
	<T> List<T> ermittleNavigation(Model model, List<T> alleElemente, Integer seite, Integer eintraegeJeSeite) {

		// Bestimme Seitenzahl und einträge je Seite
		if (seite == null || seite < 0)
			seite = DEFAULT_SEITE;
		if (eintraegeJeSeite == null)
			eintraegeJeSeite = DEFAULT_EINTRAEGE_JE_SEITE;

		// Bestimme die Seitenzahl und passe die aktuelle Seite an, wenn diese größer ist
		int seitenanzahl = Math.max(1,
				(int) (Math.ceil(((double) alleElemente.size()) / eintraegeJeSeite)));
		if (seite > seitenanzahl) {
			seite = seitenanzahl;
		}

		// Ermittle die anzuzeigenden Termine
		List<T> alleTermineFuerSeite = alleElemente.stream()
				.skip((long) (seite - 1) * eintraegeJeSeite)
				.limit(eintraegeJeSeite)
				.toList();

		// Erstelle die Navigation durch Seiten, in jedem Fall die Seite 1, die letzte
		// Seite sowie die Seite selbst.
		Set<Integer> seitenFuerNavigation = new HashSet<>();
		seitenFuerNavigation.add(1);
		seitenFuerNavigation.add(seite);
		seitenFuerNavigation.add(seitenanzahl);

		// Ergänze um 2 Seiten vor der aktuellen sowie zwei Seiten nach der Aktuellen
		for (int i = seite - 2; i <= seite + 2; i++) {
			if (i > 0 && i < seitenanzahl) {
				seitenFuerNavigation.add(i);
			}
		}

		model.addAttribute("seitenGesamt", seitenanzahl);
		model.addAttribute("seitenFuerNavigation", seitenFuerNavigation);
		model.addAttribute("filterSeite", seite);
		model.addAttribute("filterEintraegeJeSeite", eintraegeJeSeite);

		return alleTermineFuerSeite;

	}

}
