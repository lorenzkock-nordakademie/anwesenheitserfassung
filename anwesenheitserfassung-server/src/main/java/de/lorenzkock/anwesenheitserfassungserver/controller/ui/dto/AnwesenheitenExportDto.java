package de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AnwesenheitenExportDto {

    private String verantaltungName;

    private String veranstaltungKennung;

    private byte[] exportDaten;

}
