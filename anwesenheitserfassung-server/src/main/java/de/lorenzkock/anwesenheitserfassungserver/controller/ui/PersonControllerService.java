package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.person.PersonenService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.PersonMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Service stellt Funktionalität für den PersonController dar und stellt eine Zwischenebene
 * zu den Services dar.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class PersonControllerService {

    // Service
    final PersonenService personenService;

    // Repository
    final PersonRepository personRepository;

    // Mapper
    final PersonMapper personMapper;


    /**
     * Verarbeitet das Hinzufügen einer Person
     *
     * @param dto DTO mit den Daten
     * @return hinzugefügte Person
     * @throws IllegalArgumentException Fehler, wenn andere Fehler auftreten
     */
    public Person fuegePersonHinzu(@NonNull PersonErstellenDto dto) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einer Person mit der Kennung gesucht, existiert bereits
        // eine, wird ein Fehler geworfen.
        this.personenService.pruefeKennungEinmalig(personRepository, dto.getKennung(), Optional.empty());

        // Anlage der Person
        Person personZuErstellen = personMapper.fromDto(dto);

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(personZuErstellen);

        return personRepository.save(personZuErstellen);

    }

    /**
     * Verarbeitet das Bearbeiten einer Person
     *
     * @param dto DTO mit den Daten
     * @return bearbeitete Person
     * @throws NoSuchElementException   Fehler, wenn Person nicht gefunden wurde
     * @throws IllegalArgumentException Fehler, wenn andere Fehler auftreten
     */
    public Person bearbeitePerson(@NonNull PersonBearbeitenDto dto) throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        Person personZuBearbeiten = personRepository.findById(dto.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde keine Person mit der ID '%s' gefunden.".formatted(dto.getId())));

        // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
        // existiert.
        this.personenService.pruefeKennungEinmalig(personRepository, dto.getKennung(), Optional.of(dto.getId()));

        // Bearbeite die Daten
        personZuBearbeiten = this.personenService.ergaenzePerson(personZuBearbeiten, dto.getVorname(), dto.getNachname(), Optional.ofNullable(dto.getKennung()));

        return personRepository.save(personZuBearbeiten);

    }
    /*
     * Validierungsmethoden für DTOs (UI)
     */

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull PersonErstellenDto dto) {
        if (StringUtils.isBlank(dto.getVorname()))
            throw new IllegalArgumentException("Der Vorname der Person darf nicht leer sein.");
        if (StringUtils.isBlank(dto.getNachname()))
            throw new IllegalArgumentException("Der Nachname der Person darf nicht leer sein.");
    }

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull PersonBearbeitenDto dto) {
        if (StringUtils.isBlank(dto.getVorname()))
            throw new IllegalArgumentException("Der Vorname der Person darf nicht leer sein.");
        if (StringUtils.isBlank(dto.getNachname()))
            throw new IllegalArgumentException("Der Nachname der Person darf nicht leer sein.");
    }

}
