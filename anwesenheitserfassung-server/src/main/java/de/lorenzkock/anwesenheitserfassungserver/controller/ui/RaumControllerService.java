package de.lorenzkock.anwesenheitserfassungserver.controller.ui;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.RaumBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.RaumErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.BaseService;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumRepository;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.RaumService;
import de.lorenzkock.anwesenheitserfassungserver.mapper.RaumMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Dieser Service stellt Funktionalität für den RaumController dar und stellt eine Zwischenebene
 * zu den Services dar.
 *
 * @author Lorenz Kock
 */
@Controller
@RequiredArgsConstructor

public class RaumControllerService {

    // Service
    final RaumService raumService;

    // Service
    final RaumRepository raumRepository;

    // Service
    final RaumMapper raumMapper;

    /**
     * Anlegen eines Raumes
     *
     * @param dto Daten zum Anlegen
     * @return Angelegter Raum
     * @throws IllegalArgumentException Fehler
     */
    public Raum fuegeRaumHinzu(@NonNull RaumErstellenDto dto) throws IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        // Wenn eine Kennung gesetzt ist, darf diese nur einmal vorkommen
        // es wird also nach einem Raum mit der Kennung gesucht, existiert bereits
        // eine, wird ein Fehler geworfen.
        this.raumService.pruefeKennungEinmalig(raumRepository, dto.getKennung(), Optional.empty());

        // Anlage des Raums
        Raum raumZuErstellen = raumMapper.fromDto(dto);

        // Generiere ggf. eine Kennung, falls keine existiert
        BaseService.ergaenzeGenerierteKennung(raumZuErstellen);

        return raumRepository.save(raumZuErstellen);

    }

    /**
     * Bearbeite einen Raum
     *
     * @param dto Daten zum Bearbeiten
     * @return Bearbeiteter Raum
     * @throws IllegalArgumentException Fehler
     */
    public Raum bearbeiteRaum(@NonNull RaumBearbeitenDto dto) throws NoSuchElementException, IllegalArgumentException {

        // Validiere zunächst die gesendeten Daten
        this.validiereDtoFachlich(dto);

        Raum raumZuBearbeiten = raumRepository.findById(dto.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "Es wurde kein Raum mit der ID '%s' gefunden.".formatted(dto.getId())));

        // Wenn eine Kennung gesetzt ist, prüfe, ob zu dieser Kennung ein anderer Eintrag
        // existiert.
        this.raumService.pruefeKennungEinmalig(raumRepository, dto.getKennung(), Optional.of(dto.getId()));

        // Bearbeite die Daten
        raumZuBearbeiten = this.raumService.ergaenzeRaum(raumZuBearbeiten, dto.getBezeichnung(), Optional.ofNullable(dto.getKennung()));

        return raumRepository.save(raumZuBearbeiten);

    }

    /*
     * Validierungsmethoden für DTOs (UI)
     */


    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull RaumErstellenDto dto) {
        if (StringUtils.isBlank(dto.getBezeichnung()))
            throw new IllegalArgumentException("Die Bezeichnung des Raums darf nicht leer sein.");
    }

    /**
     * Validiert das DTO
     *
     * @param dto DTO, das validiert werden soll
     */
    private void validiereDtoFachlich(final @NonNull RaumBearbeitenDto dto) {
        if (StringUtils.isBlank(dto.getBezeichnung()))
            throw new IllegalArgumentException("Die Bezeichnung des Raums darf nicht leer sein.");
    }
}
