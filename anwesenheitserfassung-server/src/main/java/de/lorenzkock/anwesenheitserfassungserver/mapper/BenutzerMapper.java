package de.lorenzkock.anwesenheitserfassungserver.mapper;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.BenutzerBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Berechtigung;
import lombok.NonNull;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Optional;

@Mapper(componentModel = "spring")
public interface BenutzerMapper {

    @Mapping(target = "passwort")
    @Mapping(source = "entity.person.id", target = "personId")
    BenutzerBearbeitenDto toBearbeitenDto(@NonNull Benutzer entity);


    /**
     * Ermittelt die Rollen auf Grundlage der Strings aus der Liste
     *
     * @param rollen Liste von Rollennamen
     * @return Liste von Rollen aus dem Enum
     */
    default List<Berechtigung> mapBerechtigungen(@NonNull List<String> rollen) {
        return rollen.stream().map(Berechtigung::valueOfOpt).filter(Optional::isPresent).map(Optional::get).toList();
    }

}
