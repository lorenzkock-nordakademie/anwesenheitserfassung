package de.lorenzkock.anwesenheitserfassungserver.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.PersonErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.PersonViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.PersonZuTerminViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.PersonZuTerminViewModelIntern.AktionEnum;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.PersonZuTerminViewModelIntern.StatusEnum;

@Mapper(componentModel = "spring")
public interface PersonMapper {

	PersonViewModelExtern toViewModelExtern(Person person);

	PersonViewModelIntern toViewModelIntern(Person person);

	@Mapping(target = "berechtigteVeranstaltungen", ignore = true)
	@Mapping(target = "teilnehmendeVeranstaltungen", ignore = true)
	Person fromViewModelExtern(PersonErstellenViewModelExtern personViewModelExtern);

	Person fromDto(PersonErstellenDto dto);

	PersonDto toDto(Person person);

	PersonBearbeitenDto toBearbeitenDto(Person entity);

	@Mapping(target = "status", expression = "java(statusZuPersonUndTermin(person, termin))")
	@Mapping(target = "aktion", expression = "java(aktionZuPersonUndTermin(person, termin))")
	@Mapping(source = "person", target = "person")
	PersonZuTerminViewModelIntern toPersonTerminViewModel(Person person, Termin termin);

	default StatusEnum statusZuPersonUndTermin(Person person, Termin termin) {
		long erfassungen = 0;

		// Alle Erfassungen zu einer Person zu dem Termin
		erfassungen += person.getErfassungenZuPerson().stream().filter(e -> e.getTermin().equals(termin)).count();

		// Alle Erfassungen zu den Ausweisen der Person zu dem Termin
		erfassungen += person.getAusweise()
				.stream()
				.map(i -> i.getErfassungen().stream().filter(e -> e.getTermin().equals(termin)).count())
				.reduce(0L, Long::sum);

		if (erfassungen == 0) {
			return StatusEnum.KEINE_ERFASSUNG;
		} else {
			if (termin.getVeranstaltung().isErlaubeEinUndAusgehendeBuchungen()) {
				if ((erfassungen % 2) == 0) {
					return StatusEnum.ERFASST_NICHT_ANWESEND;
				} else {
					return StatusEnum.ERFASST_ANWESEND;
				}
			} else {
				return StatusEnum.ERFASST;
			}
		}
	}

	default AktionEnum aktionZuPersonUndTermin(Person person, Termin termin) {

		long erfassungen = 0;

		// Alle Erfassungen zu einer Person zu dem Termin
		erfassungen += person.getErfassungenZuPerson().stream().filter(e -> e.getTermin().equals(termin)).count();

		// Alle Erfassungen zu den Ausweisen der Person zu dem Termin
		erfassungen += person.getAusweise()
				.stream()
				.map(i -> i.getErfassungen().stream().filter(e -> e.getTermin().equals(termin)).count())
				.reduce(0L, Long::sum);

		if (erfassungen == 0) {
			if (termin.getVeranstaltung().isErlaubeEinUndAusgehendeBuchungen()) {
				return AktionEnum.ERFASSEN_WIEDERKEHREND;
			} else {
				return AktionEnum.ERFASSEN;
			}
		} else {
			if (termin.getVeranstaltung().isErlaubeMehrfachBuchungen()) {
				if (termin.getVeranstaltung().isErlaubeEinUndAusgehendeBuchungen()) {
					if ((erfassungen % 2) == 0) {
						return AktionEnum.ERFASSEN_WIEDERKEHREND;
					} else {
						return AktionEnum.ERFASSEN_AUSGEHEND;
					}
				} else {
					return AktionEnum.ERFASSEN_MEHRFACH;
				}
			} else {
				if (termin.getVeranstaltung().isErlaubeEinUndAusgehendeBuchungen()) {
					if (erfassungen == 1) {
						return AktionEnum.ERFASSEN_AUSGEHEND;
					} else {
						return AktionEnum.KEINE_ERFASSUNG;
					}
				} else {
					return AktionEnum.KEINE_ERFASSUNG;
				}
			}
		}
	}

}
