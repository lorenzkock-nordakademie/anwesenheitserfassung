package de.lorenzkock.anwesenheitserfassungserver.mapper;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.ErfassungDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.erfassung.Erfassung;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.enums.AnwesenheitStatus;
import de.lorenzkock.anwesenheitserfassungserver.enums.ErfassungsModus;
import de.lorenzkock.anwesenheitserfassungserver.enums.ErgebnisMehrfachErfassung;
import de.lorenzkock.anwesenheitserfassungserver.enums.PersonErfassungValidierungErgebnis;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.*;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitAusweisErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitPersonErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.service.AnwesenheitService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AnwesenheitenMapper {

    @Mapping(target = "modus", source = "erfassung", qualifiedByName = "toModus")
    @Mapping(target = "raumBezeichnung", source = "erfassung.raum.bezeichnung")
    ErfassungDto toDto(Erfassung erfassung);

    @Mapping(target = "person", source = "erfassung", qualifiedByName = "extrahierePerson")
    ErfassungViewModelExtern toViewModelExtern(Erfassung erfassung);

    AnwesenheitenZuTerminViewModelExtern convertAnwesenheitenZuTermin(AnwesenheitenZuTerminMitVeranstaltungViewModelExtern anwesenheiten);

    AnwesenheitAusweisViewModelExtern.AnwesenheitStatusEnum anwesenheitStatusAusweis(AnwesenheitStatus status);
    AnwesenheitPersonViewModelExtern.AnwesenheitStatusEnum anwesenheitStatusPerson(AnwesenheitStatus status);

    @Named("toModus")
    default ErfassungsModus toModus(Erfassung erfassung) {
        return erfassung.isMitKarte() ? (erfassung.getAusweis().isFremdausweis() ? ErfassungsModus.AUSWEIS_FREMD : ErfassungsModus.AUSWEIS_EIGEN) : ErfassungsModus.MANUELL;
    }

    default PersonViewModelExtern personToViewModelExtern(Person person) {
        return Mappers.getMapper(PersonMapper.class).toViewModelExtern(person);
    }

    default AusweisViewModelExtern ausweisToViewModelExtern(Ausweis ausweis) {
        return Mappers.getMapper(AusweisMapper.class).toViewModelExtern(ausweis);
    }

    @Named("extrahierePerson")
    default PersonViewModelExtern extrahierePerson(Erfassung erfassung) {
        if (erfassung.getPerson() != null) {
            return this.personToViewModelExtern(erfassung.getPerson());
        } else if (erfassung.getAusweis() != null && erfassung.getAusweis().getPerson() != null) {
            return this.personToViewModelExtern(erfassung.getAusweis().getPerson());
        } else {
            return null;
        }
    }

    default ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum toAusweisErgebnisEnum(ErgebnisMehrfachErfassung ergebnisEnum) {
        return switch (ergebnisEnum) {
            case ERFOLGREICH -> ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH;
            case ERFOLGREICH_AUSGEHEND ->
                    ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_AUSGEHEND;
            case ERFOLGREICH_AUSGEHEND_WIEDERKEHREND ->
                    ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_AUSGEHEND_WIEDERKEHREND;
            case ERFOLGREICH_MEHRFACH ->
                    ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_MEHRFACH;
            case ERFOLGREICH_WIEDERKEHREND ->
                    ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_WIEDERKEHREND;
            case NICHT_ERFOLGREICH_MEHRFACH ->
                    ErfassungErstellenMitAusweisErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_MEHRFACH;
        };
    }

    default ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum toPersonErgebnisEnum(PersonErfassungValidierungErgebnis ergebnisEnum) {
        return switch (ergebnisEnum) {
            case ERFOLGREICH -> ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH;
            case ERFOLGREICH_AUSGEHEND ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_AUSGEHEND;
            case ERFOLGREICH_AUSGEHEND_WIEDERKEHREND ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_AUSGEHEND_WIEDERKEHREND;
            case ERFOLGREICH_MEHRFACH ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_MEHRFACH;
            case ERFOLGREICH_WIEDERKEHREND ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.ERFOLGREICH_WIEDERKEHREND;
            case NICHT_ERFOLGREICH_KEIN_TEILNEHMER ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_KEIN_TEILNEHMER;
            case NICHT_ERFOLGREICH_MEHRFACH ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_MEHRFACH;
            case NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN;
            case NICHT_ERFOLGREICH_TERMIN_UNBEKANNT ->
                    ErfassungErstellenMitPersonErgebnisViewModelIntern.ErgebnisEnum.NICHT_ERFOLGREICH_TERMIN_UNBEKANNT;
        };
    }
}
