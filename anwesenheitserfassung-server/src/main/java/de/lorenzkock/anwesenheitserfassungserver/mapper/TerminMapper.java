package de.lorenzkock.anwesenheitserfassungserver.mapper;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TerminBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.TerminErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.termin.Termin;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.TerminErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.TerminViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.AusweisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.TerminDetailViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.TerminListViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.VeranstaltungViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.util.DatumHelper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface TerminMapper {

    TerminViewModelExtern toViewModelExtern(Termin termin);

    @Mapping(source = "termin", target = "berechtigteAusweise", qualifiedByName = "ermittleTerminBerechtigteAusweise")
    @Mapping(source = "termin", target = "teilnehmendeAusweise", qualifiedByName = "ermittleTerminTeilnehmendeAusweise")
    TerminDetailViewModelIntern toDetailViewModelIntern(Termin termin);

    TerminListViewModelIntern toListViewModelIntern(Termin termin);

    Termin fromViewModelExtern(TerminErstellenViewModelExtern viewModel);

    Termin fromDto(TerminErstellenDto dto);

    @Mapping(target = "raumIds", source = "entity.raeume")
    TerminBearbeitenDto toBearbeitenDto(Termin entity);

    default OffsetDateTime toDateTime(String dateTimeString) {
        return DatumHelper.toDateTime(dateTimeString);
    }

    default String toDateTime(OffsetDateTime dateTime) {
        return DatumHelper.toDateTime(dateTime);
    }

    default RaumViewModelExtern raumToViewModelExtern(Raum raum) {
        return Mappers.getMapper(RaumMapper.class).toViewModelExtern(raum);
    }

    default String getRaumId(Raum raum) {
        if(raum == null) return null;
        return raum.getId();
    }

    default VeranstaltungViewModelIntern veranstaltungToViewModelIntern(Veranstaltung veranstaltung) {
        return Mappers.getMapper(VeranstaltungMapper.class).toViewModelIntern(veranstaltung);
    }

    default AusweisViewModelIntern ausweisToAusweisViewModelIntern(Ausweis ausweis) {
        return Mappers.getMapper(AusweisMapper.class).toViewModelIntern(ausweis);
    }

    @Named("ermittleTerminBerechtigteAusweise")
    default List<AusweisViewModelIntern> ermittleTerminBerechtigteAusweise(Termin termin) {
        Set<Ausweis> ausweise = new HashSet<>();
        ausweise.addAll(termin.getVeranstaltung().getBerechtigteAusweise());
        ausweise.addAll(termin.getBerechtigteAusweise());
        termin.getVeranstaltung()
                .getBerechtigtePersonen()
                .forEach(p -> ausweise.addAll(p.getAusweise()));
        termin.getBerechtigtePersonen().forEach(p -> ausweise.addAll(p.getAusweise()));

        return ausweise.stream().map(this::ausweisToAusweisViewModelIntern).toList();
    }

    @Named("ermittleTerminTeilnehmendeAusweise")
    default List<AusweisViewModelIntern> ermittleTerminTeilnehmendeAusweise(Termin termin) {
        return termin.getAlleTeilnehmendenAusweise(false)
                .stream()
                .map(this::ausweisToAusweisViewModelIntern)
                .toList();
    }

}
