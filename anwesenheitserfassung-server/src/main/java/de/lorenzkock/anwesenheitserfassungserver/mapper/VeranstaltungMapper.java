package de.lorenzkock.anwesenheitserfassungserver.mapper;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.VeranstaltungErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.*;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.VeranstaltungViewModelIntern;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface VeranstaltungMapper {

	VeranstaltungViewModelExtern toViewModelExtern(Veranstaltung veranstaltung);

	VeranstaltungViewModelIntern toViewModelIntern(Veranstaltung veranstaltung);

	Veranstaltung fromViewModelExtern(VeranstaltungErstellenViewModelExtern viewModel);

	Veranstaltung fromDto(VeranstaltungErstellenDto dto);

	VeranstaltungBearbeitenDto toBearbeitenDto(Veranstaltung entity);

	@Mapping(source = "veranstaltung.teilnehmendePersonen", target = "personen")
	@Mapping(source = "veranstaltung.teilnehmendeAusweise", target = "ausweise")
	PersonenUndAusweiseViewModelExtern teilnehmendePersonenUndAusweiseToViewModelExtern(
			Veranstaltung veranstaltung);

	@Mapping(source = "veranstaltung.berechtigtePersonen", target = "personen")
	@Mapping(source = "veranstaltung.berechtigteAusweise", target = "ausweise")
	PersonenUndAusweiseViewModelExtern berechtigtePersonenUndAusweiseToViewModelExtern(
			Veranstaltung veranstaltung);

	default PersonViewModelExtern personToViewModelExtern(Person person) {
		return Mappers.getMapper(PersonMapper.class).toViewModelExtern(person);
	}

	default AusweisViewModelExtern ausweisToViewModelExtern(Ausweis ausweis) {
		return Mappers.getMapper(AusweisMapper.class).toViewModelExtern(ausweis);
	}

	default RaumViewModelExtern raumToViewModelExtern(Raum raum) {
		return Mappers.getMapper(RaumMapper.class).toViewModelExtern(raum);
	}

	default List<String> map(List<Veranstaltung> value) {
		return value.stream().map(Veranstaltung::getId).toList();
	}

}
