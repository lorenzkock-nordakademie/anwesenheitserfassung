package de.lorenzkock.anwesenheitserfassungserver.mapper;

import org.mapstruct.Mapper;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.RaumBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.RaumErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.raum.Raum;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.RaumViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.RaumViewModelIntern;

@Mapper(componentModel = "spring")
public interface RaumMapper {

	RaumViewModelExtern toViewModelExtern(Raum raum);

	RaumViewModelIntern toViewModelIntern(Raum raum);

	Raum fromErstellenViewModelExtern(RaumErstellenViewModelExtern raumErstellenViewModelExtern);

	Raum fromDto(RaumErstellenDto dto);

	RaumBearbeitenDto toBearbeitenDto(Raum entity);
}
