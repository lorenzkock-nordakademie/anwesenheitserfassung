package de.lorenzkock.anwesenheitserfassungserver.mapper;

import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisBearbeitenDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisDto;
import de.lorenzkock.anwesenheitserfassungserver.controller.ui.dto.AusweisErstellenDto;
import de.lorenzkock.anwesenheitserfassungserver.data.ausweis.Ausweis;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Benutzer;
import de.lorenzkock.anwesenheitserfassungserver.data.benutzer.Berechtigung;
import de.lorenzkock.anwesenheitserfassungserver.data.person.Person;
import de.lorenzkock.anwesenheitserfassungserver.data.veranstaltung.Veranstaltung;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisErstellenViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.AusweisViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.extern.model.PersonViewModelExtern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.AusweisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.BerechtigungenViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.ErfassungErstellenMitAusweisErgebnisViewModelIntern;
import de.lorenzkock.anwesenheitserfassungserver.rest.intern.model.PersonViewModelIntern;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface AusweisMapper {

    @Mapping(source = "ausweis.id.rfidSnr", target = "rfidSnr")
    @Mapping(source = "ausweis.id.kennung", target = "kennung")
    AusweisViewModelExtern toViewModelExtern(Ausweis ausweis);

    @Mapping(source = "ausweis.id.rfidSnr", target = "rfidSnr")
    @Mapping(source = "ausweis.id.kennung", target = "kennung")
    AusweisViewModelIntern toViewModelIntern(Ausweis ausweis);

    @Mapping(source = "viewModel.rfidSnr", target = "id.rfidSnr")
    @Mapping(source = "viewModel.kennung", target = "id.kennung")
    @Mapping(target = "aktiv", constant = "true")
    Ausweis fromErstellenViewModelExtern(AusweisErstellenViewModelExtern viewModel);

    @Mapping(source = "dto.rfidSnr", target = "id.rfidSnr")
    @Mapping(source = "dto.kennung", target = "id.kennung")
    @Mapping(target = "aktiv", constant = "true")
    Ausweis fromErstellenDto(AusweisErstellenDto dto);

    @Mapping(source = "ausweis.id.rfidSnr", target = "rfidSnr")
    @Mapping(source = "ausweis.id.kennung", target = "kennung")
    @Mapping(source = "ausweis.person.id", target = "personId")
    AusweisBearbeitenDto toBearbeitenDto(Ausweis ausweis);

    @Mapping(source = "ausweis", target = "rollen", qualifiedByName = "ausweisPersonBenutzerRollenZuStringListe")
    @Mapping(source = "ausweis.id.rfidSnr", target = "rfidSnr")
    @Mapping(source = "ausweis.id.kennung", target = "kennung")
    @Mapping(source = "ausweis", target = "veranstaltungen", qualifiedByName = "alleVeranstaltungBerechtigungenZuAusweis")
    BerechtigungenViewModelIntern toBerechtigungViewModel(Ausweis ausweis);

    @Mapping(source = "ausweis.id.rfidSnr", target = "rfidSnr")
    @Mapping(source = "ausweis.id.kennung", target = "kennung")
    AusweisDto toDto(Ausweis ausweis);

    default PersonViewModelExtern personToViewModelExtern(Person person) {
        return Mappers.getMapper(PersonMapper.class).toViewModelExtern(person);
    }

    default List<String> veranstaltungenIdMapper(List<Veranstaltung> veranstaltungen) {
        return Mappers.getMapper(VeranstaltungMapper.class).map(veranstaltungen);
    }

    default PersonViewModelIntern personToPersonViewModelIntern(Person person) {
        return Mappers.getMapper(PersonMapper.class).toViewModelIntern(person);
    }

    @Named("alleVeranstaltungBerechtigungenZuAusweis")
    default List<String> alleVeranstaltungBerechtigungenZuAusweis(Ausweis ausweis) {
        Set<String> veranstaltungIds = new HashSet<>();
        veranstaltungIds.addAll(this.veranstaltungenIdMapper(ausweis.getBerechtigteVeranstaltungen()));
        veranstaltungIds.addAll(this.veranstaltungenIdMapper(Optional.ofNullable(ausweis.getPerson())
                .map(Person::getBerechtigteVeranstaltungen)
                .orElse(List.of())));
        return List.copyOf(veranstaltungIds);

    }

    @Named("ausweisPersonBenutzerRollenZuStringListe")
    default List<String> ausweisPersonBenutzerRollenZuStringListe(Ausweis ausweis) {
        if (ausweis == null) {
            return List.of();
        }
        Person person = ausweis.getPerson();
        if (person == null) {
            return List.of();
        }
        Benutzer benutzer = person.getBenutzer();
        if (benutzer == null) {
            return List.of();
        }
        List<Berechtigung> rollen = benutzer.getBerechtigungen();
        if (rollen == null) {
            return List.of();
        }
        return Optional.of(rollen).orElse(List.of()).stream().map(Berechtigung::name).toList();
    }

    default ErfassungErstellenMitAusweisErgebnisViewModelIntern.AusweisTypEnum bestimmeAusweisTyp(Ausweis ausweis) {
        if (ausweis == null)
            return null;
        if (ausweis.isFremdausweis()) {
            if (ausweis.getEigenausweis() == null) {
                return ErfassungErstellenMitAusweisErgebnisViewModelIntern.AusweisTypEnum.UNBESTAETIGTE_FREMD;
            } else {
                return ErfassungErstellenMitAusweisErgebnisViewModelIntern.AusweisTypEnum.BESTAETIGTE_FREMD;
            }
        } else {
            return ErfassungErstellenMitAusweisErgebnisViewModelIntern.AusweisTypEnum.EIGEN;
        }
    }
}
