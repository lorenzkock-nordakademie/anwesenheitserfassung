package de.lorenzkock.anwesenheitserfassungserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnwesenheitserfassungServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnwesenheitserfassungServerApplication.class, args);
	}

}
