package de.lorenzkock.anwesenheitserfassungserver.enums;

public enum AnwesenheitStatus {
	ANWESEND("Anwesend"), VERSPAETET("Verspätet"), KEINE_ERFASSUNG(""), ONLINE("Online"),
	ONLINE_VERSPAETET("Online-Verspätet");

	private String defaultBeschreibung;

	AnwesenheitStatus(String defaultBeschreibung) {
		this.defaultBeschreibung = defaultBeschreibung;
	}

	public String getDefaultBeschreibung() {
		return this.defaultBeschreibung;
	}

}
