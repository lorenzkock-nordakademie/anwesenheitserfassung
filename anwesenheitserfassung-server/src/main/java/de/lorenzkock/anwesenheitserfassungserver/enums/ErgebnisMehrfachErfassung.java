package de.lorenzkock.anwesenheitserfassungserver.enums;

import lombok.Getter;

@Getter
public enum ErgebnisMehrfachErfassung {
	ERFOLGREICH(true), ERFOLGREICH_AUSGEHEND(true), ERFOLGREICH_WIEDERKEHREND(true), ERFOLGREICH_MEHRFACH(true),
	ERFOLGREICH_AUSGEHEND_WIEDERKEHREND(true), NICHT_ERFOLGREICH_MEHRFACH(false);

	private final boolean erfolgreich;

	ErgebnisMehrfachErfassung(boolean erfolgreich) {
		this.erfolgreich = erfolgreich;
	}

}