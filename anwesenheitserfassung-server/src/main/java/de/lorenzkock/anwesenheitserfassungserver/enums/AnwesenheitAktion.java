package de.lorenzkock.anwesenheitserfassungserver.enums;

import lombok.Getter;

@Getter
public enum AnwesenheitAktion {

	ERFASSEN("btn-green", "done", "Erfassen"), ERFASSEN_MEHRFACH("btn-green", "replay", "Erneut erfassen"),
	WIEDERKEHREN("btn-green", "arrow_forward", "Wiederkehrend"), VERLASSEN("btn-orange", "arrow_back", "Verlassen");

	AnwesenheitAktion(String farbe, String icon, String beschreibung) {
		this.farbe = farbe;
		this.icon = icon;
		this.beschreibung = beschreibung;
	}

	private final String farbe;
	private final String icon;
	private final String beschreibung;

}