package de.lorenzkock.anwesenheitserfassungserver.enums;

import lombok.Getter;

@Getter
public enum ErfassungsModus {

	AUSWEIS_EIGEN("id_card", "Bekannter Ausweis"), AUSWEIS_FREMD("magnify_fullscreen", "Fremder Ausweis"), MANUELL("person", "Händisch");

	ErfassungsModus(String icon, String beschreibung) {
		this.icon = icon;
		this.beschreibung = beschreibung;
	}

	private final String icon;
	private final String beschreibung;

}