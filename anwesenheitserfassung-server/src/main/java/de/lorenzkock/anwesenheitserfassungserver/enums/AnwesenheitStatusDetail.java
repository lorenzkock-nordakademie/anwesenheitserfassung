package de.lorenzkock.anwesenheitserfassungserver.enums;

import lombok.Getter;

@Getter
public enum AnwesenheitStatusDetail {

	NICHT_ERFASST("red", "close", "Nicht erfasst"), ANWESEND("green", "done", "Anwesend"),
	ABWESEND("green", "arrow_back", "Abwesend"), ANWESEND_WIEDERKEHREND("green", "arrow_forward", "Erneut Anwesend"),
	ABWESEND_WIDERKEHREND("green", "arrow_back", "Erneut Abwesend"),
	ANWESEND_MEHRFACH("green", "replay", "Anwesend (Mehrfach)");

	AnwesenheitStatusDetail(String farbe, String icon, String beschreibung) {
		this.farbe = farbe;
		this.icon = icon;
		this.beschreibung = beschreibung;
	}

	private final String farbe;
	private final String icon;
	private final String beschreibung;

}