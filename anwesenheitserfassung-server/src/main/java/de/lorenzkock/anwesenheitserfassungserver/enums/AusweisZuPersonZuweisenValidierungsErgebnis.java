package de.lorenzkock.anwesenheitserfassungserver.enums;

import lombok.Getter;

@Getter
public enum AusweisZuPersonZuweisenValidierungsErgebnis {

	ERFOLGREICH(true, "Der Ausweis wurde zugewiesen."),
	NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN(false, "Person nicht gefunden."),
	NICHT_ERFOLGREICH_AUSWEIS_NICHT_GEFUNDEN(false, "Der Ausweis wurde nicht gefunden."),
	NICHT_ERFOLGREICH_AUSWEIS_BEREITS_ZUGEWIESEN(false, "Der Ausweis wurde bereits zugewiesen.");

	private final boolean erfolgreich;
	private final String statusNachricht;

	AusweisZuPersonZuweisenValidierungsErgebnis(boolean erfolgreich, String statusNachricht) {
		this.erfolgreich = erfolgreich;
		this.statusNachricht = statusNachricht;
	}

}
