package de.lorenzkock.anwesenheitserfassungserver.enums;

import lombok.Getter;

@Getter
public enum PersonErfassungValidierungErgebnis {

	ERFOLGREICH(true, "Anwesenheit erfolgreich erfasst."),
	ERFOLGREICH_AUSGEHEND(true, "Verlassen erfolgreich erfasst."),
	ERFOLGREICH_WIEDERKEHREND(true, "Wiederkehrende Anwesenheit erfolgreich erfasst."),
	ERFOLGREICH_MEHRFACH(true, "Erneute Anwesenheit erfolgreich erfasst."),
	ERFOLGREICH_AUSGEHEND_WIEDERKEHREND(true, "Erneutes Verlassen erfolgreich erfasst."),
	NICHT_ERFOLGREICH_MEHRFACH(false, "Anwesenheit wurde bereits festgestellt."),
	NICHT_ERFOLGREICH_KEIN_TEILNEHMER(false, "Kein Teilnehmer der Veranstaltung."),
	NICHT_ERFOLGREICH_TERMIN_UNBEKANNT(false, "Termin nicht gefunden"),
	NICHT_ERFOLGREICH_PERSON_NICHT_GEFUNDEN(false, "Person nicht gefunden.");

	private final boolean erfolgreich;
	private final String statusNachricht;

	PersonErfassungValidierungErgebnis(boolean erfolgreich, String statusNachricht) {
		this.erfolgreich = erfolgreich;
		this.statusNachricht = statusNachricht;
	}

}
